// This sketch if for an ESP32, it draws Jpeg images pulled from an SD Card
// onto the TFT.

// As well as the TFT_eSPI library you will need the JPEG Decoder library.
// A copy can be downloaded here, it is based on the library by Makoto Kurauchi.
// https://github.com/Bodmer/JPEGDecoder

// Images on SD Card must be put in the root folder (top level) to be found
// Use the SD library examples to verify your SD Card interface works!

// The example images used to test this sketch can be found in the library
// JPEGDecoder/extras folder
//----------------------------------------------------------------------------------------------------


#include <TinyGPS++.h>
#include <HardwareSerial.h>
    long lat,lon; 
    int counter = 0;     
    
    TinyGPSPlus gps;
 
 HardwareSerial SerialGPS(1); 

#include <SPI.h>

#include <Wire.h>
#include <LSM303.h>

LSM303 compass;

#include <SD.h>
#include "Free_Fonts.h"
#include <TFT_eSPI.h>
TFT_eSPI tft = TFT_eSPI();

// JPEG decoder library
#include <JPEGDecoder.h>




#define BLACK       0x0000      /*   0,   0,   0 */
#define NAVY        0x000F      /*   0,   0, 128 */
#define DARKGREEN   0x03E0      /*   0, 128,   0 */
#define DARKCYAN    0x03EF      /*   0, 128, 128 */
#define MAROON      0x7800      /* 128,   0,   0 */
#define PURPLE      0x780F      /* 128,   0, 128 */
#define OLIVE       0x7BE0      /* 128, 128,   0 */
#define LIGHTGREY   0xD69A      /* 211, 211, 211 */
#define DARKGREY    0x7BEF      /* 128, 128, 128 */
#define BLUE        0x001F      /*   0,   0, 255 */
#define GREEN       0x07E0      /*   0, 255,   0 */
#define CYAN        0x07FF      /*   0, 255, 255 */
#define RED         0xF800      /* 255,   0,   0 */
#define MAGENTA     0xF81F      /* 255,   0, 255 */
#define YELLOW      0xFFE0      /* 255, 255,   0 */
#define WHITE       0xFFFF      /* 255, 255, 255 */
#define ORANGE      0xFDA0      /* 255, 180,   0 */
#define GREENYELLOW 0xB7E0      /* 180, 255,   0 */
#define PINK        0xFE19      /* 255, 192, 203 */    
#define BROWN       0x9A60      /* 150,  75,   0 */
#define GOLD        0xFEA0      /* 255, 215,   0 */
#define SILVER      0xC618      /* 192, 192, 192 */
#define SKYBLUE     0x867D      /* 135, 206, 235 */
#define VIOLET      0x915C      /* 180,  46, 226 */

//####################################################################################################
// Setup
//####################################################################################################
void setup() {
delay(250);
   Wire.begin();
  compass.init();
  compass.enableDefault();
    compass.m_min = (LSM303::vector<int16_t>){-32767, -32767, -32767};
  compass.m_max = (LSM303::vector<int16_t>){+32767, +32767, +32767};
  Serial.begin(9600);
SerialGPS.begin(9600, SERIAL_8N1, 16, 34);
  // Set all chip selects high to avoid bus contention during initialisation of each peripheral
 // digitalWrite(22, HIGH); // Touch controller chip select (if used)
  //digitalWrite(15, HIGH); // TFT screen chip select
  digitalWrite( 5, HIGH); // SD card chips select, must use GPIO 5 (ESP32 SS)
pinMode( 26, OUTPUT);
  tft.begin();

  if (!SD.begin()) {
   // Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();

  if (cardType == CARD_NONE) {
//Serial.println("No SD card attached");
    return;
  }

 // Serial.print("SD Card Type: ");
  if (cardType == CARD_MMC) {
 //   Serial.println("MMC");
  } else if (cardType == CARD_SD) {
    Serial.println("SDSC");
  } else if (cardType == CARD_SDHC) {
    Serial.println("SDHC");
  } else {
    Serial.println("UNKNOWN");
  }

  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
 //// Serial.printf("SD Card Size: %lluMB\n", cardSize);

//  Serial.println("initialisation done.");


 tft.setRotation(1);  // portrait

   tft.setTextSize(1);

   drawSdJpeg("/spacexx.jpg", 120, 0);  // This draws a jpeg pulled off the SD Card
   tft.fillRect(0, 0, 60, 30, BLACK);
   tft.fillRect(0, 30, 60, 30, NAVY);
   tft.fillRect(0, 60, 60, 30, DARKGREEN);
   tft.fillRect(0, 90, 60, 30, DARKCYAN);
   tft.fillRect(0, 120, 60, 30,MAROON);
   tft.fillRect(0, 150, 60, 30, PURPLE);
   tft.fillRect(0, 180, 60, 30, OLIVE);
   tft.fillRect(0, 210, 60, 30, LIGHTGREY);
   
   tft.fillRect(60, 0, 60, 30, DARKGREY);
   tft.fillRect(60, 30, 60, 30, BLUE);
   tft.fillRect(60, 60, 60, 30, GREEN);
   tft.fillRect(60, 90, 60, 30, CYAN);
   tft.fillRect(60, 120, 60, 30, RED);
   tft.fillRect(60, 150, 60, 30, MAGENTA);
   tft.fillRect(60, 180, 60, 30, GOLD);
   tft.fillRect(60, 210, 60, 30, WHITE);

    tft.fillRect(120, 0, 60, 30, GREENYELLOW);
    tft.fillRect(180, 0, 60, 30, SKYBLUE);
    tft.fillRect(240, 0, 60, 30, VIOLET);
   
 
 tft.fillRoundRect(175, 55, 127, 149,7, CYAN);
 tft.fillRoundRect(177, 57, 123, 145,5, BLACK);

 tft.fillRoundRect(179, 60, 55, 25,5, WHITE); //İLK SATIR BAR
 tft.fillRoundRect(238, 60, 60, 25,5, WHITE);

 tft.fillRoundRect(179, 88, 55, 25,5, WHITE);//İKİNCİ SATIR BAR
 tft.fillRoundRect(238, 88, 60, 25,5, WHITE);

 tft.fillRoundRect(179, 116, 55, 25,5, WHITE);//İKİNCİ SATIR BAR
 tft.fillRoundRect(238, 116, 60, 25,5, WHITE);

 tft.fillRoundRect(179, 144, 30, 55,5, WHITE);//İKİNCİ SATIR BAR
 tft.fillRoundRect(213, 144, 84, 55,5, WHITE);
 
     tft.setFreeFont(FSI9); 
     tft.setTextColor( RED);
     tft.setCursor(182, 77);
     tft.print("TEMP.");
     tft.setTextColor( BLACK);
     tft.setCursor(240, 77);
     tft.print("60");
     tft.drawCircle(265,69 ,2, BLACK);
     tft.print("  F");

     tft.setFreeFont(FSB9);
     tft.setTextColor( RED);
     tft.setCursor(182, 105);
     tft.print("ALT.");
     tft.setTextColor( BLACK);
     tft.setCursor(240, 105);
     tft.print("2800M");

     tft.setFreeFont(FSI9);
     tft.setTextColor( RED);
     tft.setCursor(182, 133);
     tft.print("PUS.");
     tft.setTextColor( BLACK);
     tft.setCursor(240, 133);
     tft.print(heading);

     tft.setFreeFont(FSI9);
     tft.setTextColor( RED);
     tft.setCursor(178, 171);
     tft.print("GPS");
     tft.setTextColor( BLACK);






      
}

//####################################################################################################

void loop() {
 compass.read();

//  tft.fillScreen(random(0xFFFF));

  // The image is 300 x 300 pixels so we do some sums to position image in the middle of the screen!
  // Doing this by reading the image width and height from the jpeg info is left as an exercise!
  //int x = (tft.width()  - 300) / 2 - 1;
 // int y = (tft.height() - 300) / 2 - 1;

 /* drawSdJpeg("/appcent.jpg", 0, 0);     // This draws a jpeg pulled off the SD Card
  delay(2000);

  drawSdJpeg("/muho.jpg", 0, 0);     // This draws a jpeg pulled off the SD Card
  delay(2000);*/

   float heading = compass.heading();
  
  Serial.println(heading);
  delay(100);
//delay(2000);
      while(SerialGPS.available() > 0){ // check for gps data
       if(gps.encode(SerialGPS.read())){ // encode gps data
       if(counter > 2) {
        Serial.print("SATS: ");
        Serial.println(gps.satellites.value());
        Serial.print("LAT: ");
        Serial.println(gps.location.lat(), 6);
        Serial.print("LONG: ");
        Serial.println(gps.location.lng(), 6);
        Serial.print("ALT: ");
        Serial.println(gps.altitude.meters());
        
       /* Serial.print("SPEED: ");
        Serial.println(gps.speed.mps());*/
        
       /* Serial.print("Date: ");
        Serial.print(gps.date.day()); Serial.print("/");
        Serial.print(gps.date.month()); Serial.print("/");
        Serial.println(gps.date.year());
        
        Serial.print("Hour: ");
        Serial.print(gps.time.hour()); Serial.print(":");
        Serial.print(gps.time.minute()); Serial.print(":");
        Serial.println(gps.time.second());*/
     tft.fillRoundRect(213, 144, 84, 55,5, WHITE);
     tft.setCursor(215, 161);
     tft.print(gps.location.lat(), 6);
     tft.print(" ");
     tft.setCursor(215, 185);
     tft.print(gps.location.lng(), 6);
     tft.print(" ");
digitalWrite(26, HIGH);
delay(1100);
digitalWrite(26, LOW);
delay(500);
       Serial.println("---------------------------");
        counter = 0;
       }
       else counter++;

       }
      }

 // while(1); // Wait here
}

//####################################################################################################
// Draw a JPEG on the TFT pulled from SD Card
//####################################################################################################
// xpos, ypos is top left corner of plotted image
void drawSdJpeg(const char *filename, int xpos, int ypos) {

  // Open the named file (the Jpeg decoder library will close it)
  File jpegFile = SD.open( filename, FILE_READ);  // or, file handle reference for SD library
 
  if ( !jpegFile ) {
   // Serial.print("ERROR: File \""); Serial.print(filename); Serial.println ("\" not found!");
    return;
  }

///  Serial.println("===========================");
///  Serial.print("Drawing file: "); Serial.println(filename);
///  Serial.println("===========================");

  // Use one of the following methods to initialise the decoder:
  boolean decoded = JpegDec.decodeSdFile(jpegFile);  // Pass the SD file handle to the decoder,
  //boolean decoded = JpegDec.decodeSdFile(filename);  // or pass the filename (String or character array)

  if (decoded) {
    // print information about the image to the serial port
    jpegInfo();
    // render the image onto the screen at given coordinates
    jpegRender(xpos, ypos);
  }
  else {
   // Serial.println("Jpeg file format not supported!");
  }
}

//####################################################################################################
// Draw a JPEG on the TFT, images will be cropped on the right/bottom sides if they do not fit
//####################################################################################################
// This function assumes xpos,ypos is a valid screen coordinate. For convenience images that do not
// fit totally on the screen are cropped to the nearest MCU size and may leave right/bottom borders.
void jpegRender(int xpos, int ypos) {

  //jpegInfo(); // Print information from the JPEG file (could comment this line out)

  uint16_t *pImg;
  uint16_t mcu_w = JpegDec.MCUWidth;
  uint16_t mcu_h = JpegDec.MCUHeight;
  uint32_t max_x = JpegDec.width;
  uint32_t max_y = JpegDec.height;

  bool swapBytes = tft.getSwapBytes();
  tft.setSwapBytes(true);
  
  // Jpeg images are draw as a set of image block (tiles) called Minimum Coding Units (MCUs)
  // Typically these MCUs are 16x16 pixel blocks
  // Determine the width and height of the right and bottom edge image blocks
  uint32_t min_w = jpg_min(mcu_w, max_x % mcu_w);
  uint32_t min_h = jpg_min(mcu_h, max_y % mcu_h);

  // save the current image block size
  uint32_t win_w = mcu_w;
  uint32_t win_h = mcu_h;

  // record the current time so we can measure how long it takes to draw an image
  uint32_t drawTime = millis();

  // save the coordinate of the right and bottom edges to assist image cropping
  // to the screen size
  max_x += xpos;
  max_y += ypos;

  // Fetch data from the file, decode and display
  while (JpegDec.read()) {    // While there is more data in the file
    pImg = JpegDec.pImage ;   // Decode a MCU (Minimum Coding Unit, typically a 8x8 or 16x16 pixel block)

    // Calculate coordinates of top left corner of current MCU
    int mcu_x = JpegDec.MCUx * mcu_w + xpos;
    int mcu_y = JpegDec.MCUy * mcu_h + ypos;

    // check if the image block size needs to be changed for the right edge
    if (mcu_x + mcu_w <= max_x) win_w = mcu_w;
    else win_w = min_w;

    // check if the image block size needs to be changed for the bottom edge
    if (mcu_y + mcu_h <= max_y) win_h = mcu_h;
    else win_h = min_h;

    // copy pixels into a contiguous block
    if (win_w != mcu_w)
    {
      uint16_t *cImg;
      int p = 0;
      cImg = pImg + win_w;
      for (int h = 1; h < win_h; h++)
      {
        p += mcu_w;
        for (int w = 0; w < win_w; w++)
        {
          *cImg = *(pImg + w + p);
          cImg++;
        }
      }
    }

    // calculate how many pixels must be drawn
    uint32_t mcu_pixels = win_w * win_h;

    // draw image MCU block only if it will fit on the screen
    if (( mcu_x + win_w ) <= tft.width() && ( mcu_y + win_h ) <= tft.height())
      tft.pushImage(mcu_x, mcu_y, win_w, win_h, pImg);
    else if ( (mcu_y + win_h) >= tft.height())
      JpegDec.abort(); // Image has run off bottom of screen so abort decoding
  }

  tft.setSwapBytes(swapBytes);

  showTime(millis() - drawTime); // These lines are for sketch testing only
}

//####################################################################################################
// Print image information to the serial port (optional)
//####################################################################################################
// JpegDec.decodeFile(...) or JpegDec.decodeArray(...) must be called before this info is available!
void jpegInfo() {

  // Print information extracted from the JPEG file
  Serial.println("JPEG image info");
  Serial.println("===============");
  Serial.print("Width      :");
  Serial.println(JpegDec.width);
  Serial.print("Height     :");
  Serial.println(JpegDec.height);
  Serial.print("Components :");
  Serial.println(JpegDec.comps);
  Serial.print("MCU / row  :");
  Serial.println(JpegDec.MCUSPerRow);
  Serial.print("MCU / col  :");
  Serial.println(JpegDec.MCUSPerCol);
  Serial.print("Scan type  :");
  Serial.println(JpegDec.scanType);
  Serial.print("MCU width  :");
  Serial.println(JpegDec.MCUWidth);
  Serial.print("MCU height :");
  Serial.println(JpegDec.MCUHeight);
  Serial.println("===============");
  Serial.println("");
}

//####################################################################################################
// Show the execution time (optional)
//####################################################################################################
// WARNING: for UNO/AVR legacy reasons printing text to the screen with the Mega might not work for
// sketch sizes greater than ~70KBytes because 16 bit address pointers are used in some libraries.

// The Due will work fine with the HX8357_Due library.

void showTime(uint32_t msTime) {
  //tft.setCursor(0, 0);
  //tft.setTextFont(1);
  //tft.setTextSize(2);
  //tft.setTextColor(TFT_WHITE, TFT_BLACK);
  //tft.print(F(" JPEG drawn in "));
  //tft.print(msTime);
  //tft.println(F(" ms "));
  Serial.print(F(" JPEG drawn in "));
  Serial.print(msTime);
  Serial.println(F(" ms "));
}
