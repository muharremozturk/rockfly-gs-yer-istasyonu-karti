#include <TinyGPS++.h>


#include <HardwareSerial.h>
    long lat,lon; 
    int counter = 0;     
    
    TinyGPSPlus gps;
 HardwareSerial SerialGPS(1);  
    void setup(){
      Serial.begin(9600); // connect serial
    
 SerialGPS.begin(9600, SERIAL_8N1, 16, 17);

    }
     
    void loop(){
      while(SerialGPS.available() > 0){ // check for gps data
       if(gps.encode(SerialGPS.read())){ // encode gps data
       if(counter > 2) {
        Serial.print("SATS: ");
        Serial.println(gps.satellites.value());
        Serial.print("LAT: ");
        Serial.println(gps.location.lat(), 6);
        Serial.print("LONG: ");
        Serial.println(gps.location.lng(), 6);
        Serial.print("ALT: ");
        Serial.println(gps.altitude.meters());
        Serial.print("SPEED: ");
        Serial.println(gps.speed.mps());
        
        Serial.print("Date: ");
        Serial.print(gps.date.day()); Serial.print("/");
        Serial.print(gps.date.month()); Serial.print("/");
        Serial.println(gps.date.year());
        
        Serial.print("Hour: ");
        Serial.print(gps.time.hour()); Serial.print(":");
        Serial.print(gps.time.minute()); Serial.print(":");
        Serial.println(gps.time.second());
       Serial.println("---------------------------");
        counter = 0;
       }
       else counter++;

       }
      }

            
    }
