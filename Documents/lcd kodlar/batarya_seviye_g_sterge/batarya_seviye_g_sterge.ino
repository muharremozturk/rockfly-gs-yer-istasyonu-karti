

#include <Adafruit_GFX.h>
#include <MCUFRIEND_kbv.h>
MCUFRIEND_kbv tft;
#include <TouchScreen.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/FreeSans12pt7b.h>
#include <Fonts/FreeSerif12pt7b.h>

#include <FreeDefaultFonts.h>

#define MINPRESSURE 100
#define MAXPRESSURE 1000
float a=0;
int percent=0;
int plus;
int degree=67;

// ALL Touch panels and wiring is DIFFERENT
// copy-paste results from TouchScreen_Calibr_native.ino
const int XP = 7, XM = A1, YP = A2, YM = 6; //ID=0x9341
const int TS_LEFT = 136, TS_RT = 907, TS_TOP = 942, TS_BOT = 139;

TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

Adafruit_GFX_Button on_btn, zero_btn, off_btn;

int pixel_x, pixel_y;     //Touch_getXY() updates global vars
bool Touch_getXY(void)
{
    TSPoint p = ts.getPoint();
    pinMode(YP, OUTPUT);      //restore shared pins
    pinMode(XM, OUTPUT);
    digitalWrite(YP, HIGH);   //because TFT control pins
    digitalWrite(XM, HIGH);
    bool pressed = (p.z > MINPRESSURE && p.z < MAXPRESSURE);
    if (pressed) {
        pixel_x = map(p.x, TS_LEFT, TS_RT, 0, tft.width()); //.kbv makes sense to me
        pixel_y = map(p.y, TS_TOP, TS_BOT, 0, tft.height());
    }
    return pressed;
}

#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define GRAY    0xB2B2

void setup(void)
{
    Serial.begin(9600);
    uint16_t ID = tft.readID();
    Serial.print("TFT ID = 0x");
    Serial.println(ID, HEX);
    Serial.println("Calibrate for your Touch Panel");
    if (ID == 0xD3D3) ID = 0x9486; // write-only shield
    tft.begin(ID);
    tft.setRotation(1);            //PORTRAIT
    tft.fillScreen(WHITE);
    
    on_btn.initButton(&tft,  50, 250, 100, 40, WHITE, CYAN, BLACK, "+", 2);
    off_btn.initButton(&tft, 170, 250, 100, 40, WHITE, CYAN, BLACK, "-", 2);
    zero_btn.initButton(&tft, 360, 250, 150, 70, WHITE, RED, BLACK, "SIFIRLA", 2);
    
    on_btn.drawButton(false);
    off_btn.drawButton(false);
    zero_btn.drawButton(false);
    
    tft.fillRect(71, 10, 3, 12, BLACK);//batarya ucu

    
    tft.fillRoundRect(1, 1, 70, 30,2, BLACK);//batarya şase
 
    tft.fillRoundRect(2, 2, 68, 28,3, WHITE);//batarya içi
    //SICAKLIK GÖSTERGE
 /*   tft.fillCircle(450,100,20,BLACK);
    tft.fillRoundRect(441, 17, 19, 70,3, BLACK);
    tft.fillCircle(450,100,18,WHITE);
    tft.fillRoundRect(443, 19, 15, 69,3, WHITE);*/
    
}

/* two buttons are quite simple
 */
void loop(void)
{

  tft.fillRoundRect(2, 2, plus, 28,3, GREEN);
  
    bool down = Touch_getXY();
    on_btn.press(down && on_btn.contains(pixel_x, pixel_y));
    off_btn.press(down && off_btn.contains(pixel_x, pixel_y));
     zero_btn.press(down && zero_btn.contains(pixel_x, pixel_y));
     
    if (on_btn.justReleased())
        on_btn.drawButton();
        
           if (zero_btn.justReleased())
        zero_btn.drawButton();
        
    if (off_btn.justReleased())
        off_btn.drawButton();

   if (zero_btn.justPressed()) {
        zero_btn.drawButton(true);
   plus=0;
    degree=0;
    tft.fillRoundRect(2, 2, 68, 28,3, WHITE);
   // tft.fillRoundRect(444, 20, 13, 67,3, WHITE);//DEGREEE

    }
    
  if (on_btn.justPressed()) {
        on_btn.drawButton(true);
    plus =plus+1;
// degree=degree+1;
      //  if(degree>=67) degree=67;
        if(plus>=68) plus=68;
      //  tft.fillRoundRect(444, 20, 13, degree,3,RED);
         tft.fillRoundRect(2, 2, plus, 28,3, GREEN);
    }
    
    if (off_btn.justPressed()) {
        off_btn.drawButton(true);
       
        plus =plus-1;
          if(plus<=0) plus=0;
    //     degree=degree-1;
// if(degree<=0) degree=0;
          tft.fillRoundRect(2, 2, plus, 28,3, WHITE);
       // tft.fillRoundRect(444, 20, 13, degree,3, WHITE);
          
       
   
    }
 
    Serial.println(plus);
     Serial.print("degree=");
     Serial.println(degree);
  
     a = (plus*100);
     percent=(a/68);
       Serial.print(percent);
   tft.setTextColor( BLACK,WHITE);
    tft.setTextSize(1);
    tft.setCursor(80, 10);
    tft.print("%");
   tft.print(percent,1);
   tft.print(" ");
   
  //  tft.fillCircle(450,100,16,RED);
   // tft.fillRoundRect(444, 20, 13, degree,3, RED);

}


  /*  tft.fillCircle(450,100,16,RED);
    tft.fillRoundRect(444, 20, 13, 67,3, RED);
*/
