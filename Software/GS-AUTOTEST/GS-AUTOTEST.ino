
#include <TJpg_Decoder.h>
#include <Wire.h>
#include <LSM303.h>
#include <Adafruit_BMP280.h>
#include "LITTLEFS.h" 
#include <SPI.h>
#include "FS.h"
#include <TFT_eSPI.h>
#include <TinyGPS++.h>
#include <HardwareSerial.h>
#include <RadioLib.h>


#define BLACK       0x0000      /*   0,   0,   0 */
#define NAVY        0x000F      /*   0,   0, 128 */
#define DARKGREEN   0x03E0      /*   0, 128,   0 */
#define DARKCYAN    0x03EF      /*   0, 128, 128 */
#define MAROON      0x7800      /* 128,   0,   0 */
#define PURPLE      0x780F      /* 128,   0, 128 */
#define OLIVE       0x7BE0      /* 128, 128,   0 */
#define LIGHTGREY   0xD69A      /* 211, 211, 211 */
#define DARKGREY    0x7BEF      /* 128, 128, 128 */
#define BLUE        0x001F      /*   0,   0, 255 */
#define GREEN       0x07E0      /*   0, 255,   0 */
#define CYAN        0x07FF      /*   0, 255, 255 */
#define RED         0xF800      /* 255,   0,   0 */
#define MAGENTA     0xF81F      /* 255,   0, 255 */
#define YELLOW      0xFFE0      /* 255, 255,   0 */
#define WHITE       0xFFFF      /* 255, 255, 255 */
#define ORANGE      0xFDA0      /* 255, 180,   0 */
#define GREENYELLOW 0xB7E0      /* 180, 255,   0 */
#define PINK        0xFE19      /* 255, 192, 203 */    
#define BROWN       0x9A60      /* 150,  75,   0 */
#define GOLD        0xFEA0      /* 255, 215,   0 */
#define SILVER      0xC618      /* 192, 192, 192 */
#define SKYBLUE     0x867D      /* 135, 206, 235 */
#define VIOLET      0x915C      /* 180,  46, 226 */
#define GRAY        0xC618

#define CALIBRATION_FILE "/TouchCalData2"
#define REPEAT_CAL false

#define BattMin 3.2
#define BattMax 4.3
#define TempMin 23.0
#define TempMax 35.0
#define PressMin  88800
#define PressMax  98600


boolean SwitchOn = false;

// Comment out to stop drawing black spots
#define BLACK_SPOT

// Switch position and size
#define FRAME_X 100
#define FRAME_Y 64
#define FRAME_W 120
#define FRAME_H 50

// Red zone size
#define REDBUTTON_X FRAME_X
#define REDBUTTON_Y FRAME_Y
#define REDBUTTON_W (FRAME_W/2)
#define REDBUTTON_H FRAME_H

// Green zone size
#define GREENBUTTON_X (REDBUTTON_X + REDBUTTON_W)
#define GREENBUTTON_Y FRAME_Y
#define GREENBUTTON_W (FRAME_W/2)
#define GREENBUTTON_H FRAME_H



TFT_eSPI tft = TFT_eSPI();
Adafruit_BMP280 bmp;
LSM303 compass;

void setup() {

Serial.begin(9600); 
 
 while(!Serial);
  Serial.println("Serial    :[OK]");
 //Serial.flush();
  Serial.print("BUZZER OFF :");
  pinMode(26, OUTPUT);
  pinMode(35, INPUT);    
  digitalWrite(26, LOW);
  Serial.println("[OK]");
  //Serial.flush();
  
   fullTest();
}

void loop() {

   uint16_t x, y;

  // See if there's any touch data for us
  if (tft.getTouch(&x, &y))
  {
    // Draw a block spot to show where touch was calculated to be
    #ifdef BLACK_SPOT
      tft.fillCircle(x, y, 2, TFT_BLACK);
    #endif
    
    if (SwitchOn)
    {
      if ((x > REDBUTTON_X) && (x < (REDBUTTON_X + REDBUTTON_W))) {
        if ((y > REDBUTTON_Y) && (y <= (REDBUTTON_Y + REDBUTTON_H))) {
          Serial.println("Red btn hit");
          redBtn();
        }
      }
    }
    else //Record is off (SwitchOn == false)
    {
      if ((x > GREENBUTTON_X) && (x < (GREENBUTTON_X + GREENBUTTON_W))) {
        if ((y > GREENBUTTON_Y) && (y <= (GREENBUTTON_Y + GREENBUTTON_H))) {
          Serial.println("Green btn hit");
          greenBtn();
        }
      }
    }

    Serial.println(SwitchOn);

  }
  

}


void fullTest(){

  //--------- Analog ---------------------------------  
  Serial.print("AnalogRead   :");
  //Serial.flush();  
  double batV=ReadVoltage(35)*6.6;
  Serial.println("[OK]");
  Serial.print("Batarya Voltaji  (");
  Serial.print(batV);
  Serial.print(" V):");
  if(batV<BattMin or BattMax>4.3f){
    Serial.println("Batarya Voltaji Uygun Aralikta Degil!!!");
   // Serial.flush();
    while(1);
  }else Serial.println("[OK]");

  //--------- Wire ----------------------------------------  
  Serial.print("Wire Start   :");
  //Serial.flush();
  Wire.begin();
  Serial.println("[OK]");
  //Serial.flush();
  byte error, address;
  int nDevices=0;
  byte bmpS=0; byte lsmA=0; byte lsmM=0; byte ubloxG=0;
 for(address = 1; address < 127; address++ ) 
  {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error == 0)
    {
      if(address==119){//0x77
        Serial.println("BMP280 0x77    :[OK]");
       // Serial.flush();
        bmpS=1;
      }else if(address==25){//0x19
        Serial.println("LSM303DLHC 0x19 (Acceerometer)  :[OK]");
       // Serial.flush();
        lsmA=1;
      }else if(address==30){//0x1E
        Serial.println("LSM303DLHC 0x1E (Magnetometer) :[OK]");
        //Serial.flush();
        lsmM=1;
      }else{
        Serial.print("Bilinmeyen I2C aygit bulundu. Address 0x");
        if (address<16) 
        Serial.print("0");
        Serial.print(address,HEX);
        Serial.println("  !!!");
        //Serial.flush();
      }
      nDevices++;
    }
    else if (error==4) 
    {
      Serial.print("Bilinmeyen Hata. Address 0x");
      if (address<16) 
        Serial.print("0");
      Serial.println(address,HEX);
    }    
  }
  if (nDevices==3 && bmpS==1 && lsmA==1 && lsmM==1){
    Serial.println("Bulunan adres 3 :[OK]");
  }else{
    Serial.print("Bulunan adres :");
    Serial.println(nDevices);
    if(bmpS==0) Serial.println("BMP280 0x77    :[ERROR!]");
    if(lsmA==0) Serial.println("LSM303DLHC 0x19 (Acceerometer)  :[ERROR!]");
    if(lsmM==0) Serial.println("LSM303DLHC 0x1E (Magnetometer) :[ERROR!]");
    Serial.println("I2C aygitlarini ve PULLUP direnclerini kontrol edin!!!");
    //Serial.flush();
    while(1);
  }

  //-----------BMP280-------------------------------------------------  
Serial.print("BMP280 Start :");
//Serial.flush();
if (!bmp.begin()) {
  Serial.println("Hata!!! BMP280 Baslatilamadi");
  //Serial.flush();
  while (1) {}
  }
Serial.println("[OK]");
bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */ 
float tmpC=bmp.readTemperature();
Serial.print("Temperature = ");
Serial.print(tmpC);
Serial.print(" *C :");
if(tmpC<TempMin or tmpC>TempMax){
  Serial.println("Sicaklik Uygun Aralikta Degil!!!");
  //Serial.flush();
  while(1);
}else Serial.println("[OK]");

uint32_t tmpP=bmp.readPressure();
Serial.print("Pressure = ");
Serial.print(tmpP);
Serial.print(" Pa :");
if(tmpP<PressMin or tmpP>PressMax){
  Serial.println("Basinc Uygun Aralikta Degil!!!");
  //Serial.flush();
  while(1);
}else Serial.println("[OK]"); 
  
  //-------- LSM303 -------------------
Serial.print("LSM303 Start :");
  compass.init();
  compass.enableDefault();
  Serial.println("[OK]");

 compass.m_min = (LSM303::vector<int16_t>){ -300,   -790,   -566};
 
  compass.m_max = (LSM303::vector<int16_t>){+751,   +303,   +332 };
  Serial.print("LSM303 Read  :");
  compass.read();
  Serial.println("[OK]"); 
  Serial.print("Anteni Güneye Cevirin  :");
  while (compass.heading()<=150 || compass.heading()>=220){
   compass.read();
  
  }
  Serial.println("[OK]"); 

  //--------- SD kard------------//
  Serial.print("SD CARD STARTING :");
  if(!SD.begin()){
    Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();

  if(cardType == CARD_NONE){
    Serial.println("No SD card attached");
    return;
  }

  Serial.print("SD Card Type: ");
  if(cardType == CARD_MMC){
    Serial.println("MMC");
  } else if(cardType == CARD_SD){
    Serial.println("SDSC");
  } else if(cardType == CARD_SDHC){
    Serial.println("SDHC");
  } else {
    Serial.println("UNKNOWN");
  }

  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
  Serial.printf("SD Card Size: %lluMB\n", cardSize);

  writeFile(SD, "/hello.txt", "Hello ");
  appendFile(SD, "/hello.txt", "World!\n");
  readFile(SD, "/hello.txt");
  deleteFile(SD, "/hello.txt");
  Serial.println("[OK]");

  //----------- TFT_LCD---------//
  Serial.print("LCD STARTING :");
  tft.init();
  tft.setRotation(1);
  touch_calibrate();
  tft.fillScreen(TFT_BLUE);
  redBtn();

  digitalWrite(26,HIGH);
  delay(1000);
  digitalWrite(26,LOW);
  
Serial.println(F("  _______        _      ____  _  __"));
Serial.println(F(" |__   __|      | |    / __ \\| |/ /"));
Serial.println(F("    | | ___  ___| |_  | |  | | ' / "));
Serial.println(F("    | |/ _ \\/ __| __| | |  | |  <  "));
Serial.println(F("    | |  __/\\__ \\ |_  | |__| | . \\ "));
Serial.println(F("    |_|\\___||___/\\__|  \\____/|_|\\_\\"));
  }

double ReadVoltage(byte pin){
  double reading = analogRead(pin); // Reference voltage is 3v3 so maximum reading is 3v3 = 4095 in range 0 to 4095
  if(reading < 1 || reading > 4095) return 0;
  // return -0.000000000009824 * pow(reading,3) + 0.000000016557283 * pow(reading,2) + 0.000854596860691 * reading + 0.065440348345433;
  return -0.000000000000016 * pow(reading,4) + 0.000000000118171 * pow(reading,3)- 0.000000301211691 * pow(reading,2)+ 0.001109019271794 * reading + 0.034143524634089;
}

float readBatt() {

  float R1 = 560000.0; // 560K
  float R2 = 100000.0; // 100K
  float value = 0.0f;
  do {    
    value =analogRead(35);
    value +=analogRead(35);
    value +=analogRead(35);
    value = value / 3.0f;
    value = (value * 3.32) / 4096.0f;
    value = value / (R2/(R1+R2));
  } while (value > 10.0);
  return value ;

}


void readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\n", path);

  File file = fs.open(path);
  if(!file){
    Serial.println("Failed to open file for reading");
    return;
  }

  Serial.print("Read from file: ");
  while(file.available()){
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if(!file){
    Serial.println("Failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if(!file){
    Serial.println("Failed to open file for appending");
    return;
  }
  if(file.print(message)){
      Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}

void deleteFile(fs::FS &fs, const char * path){
  Serial.printf("Deleting file: %s\n", path);
  if(fs.remove(path)){
    Serial.println("File deleted");
  } else {
    Serial.println("Delete failed");
  }
}


void touch_calibrate()
{
  uint16_t calData[5];
  uint8_t calDataOK = 0;

  // check file system exists
  if (!SPIFFS.begin()) {
    Serial.println("Formating file system");
    SPIFFS.format();
    SPIFFS.begin();
  }

  // check if calibration file exists and size is correct
  if (SPIFFS.exists(CALIBRATION_FILE)) {
    if (REPEAT_CAL)
    {
      // Delete if we want to re-calibrate
      SPIFFS.remove(CALIBRATION_FILE);
    }
    else
    {
      File f = SPIFFS.open(CALIBRATION_FILE, "r");
      if (f) {
        if (f.readBytes((char *)calData, 14) == 14)
          calDataOK = 1;
        f.close();
      }
    }
  }

  if (calDataOK && !REPEAT_CAL) {
    // calibration data valid
    tft.setTouch(calData);
  } else {
    // data not valid so recalibrate
    tft.fillScreen(TFT_BLACK);
    tft.setCursor(20, 0);
    tft.setTextFont(2);
    tft.setTextSize(1);
    tft.setTextColor(TFT_WHITE, TFT_BLACK);

    tft.println("Touch corners as indicated");

    tft.setTextFont(1);
    tft.println();

    if (REPEAT_CAL) {
      tft.setTextColor(TFT_RED, TFT_BLACK);
      tft.println("Set REPEAT_CAL to false to stop this running again!");
    }

    tft.calibrateTouch(calData, TFT_MAGENTA, TFT_BLACK, 15);

    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.println("Calibration complete!");

    // store data
    File f = SPIFFS.open(CALIBRATION_FILE, "w");
    if (f) {
      f.write((const unsigned char *)calData, 14);
      f.close();
    }
  }
}

void drawFrame()
{
  tft.drawRect(FRAME_X, FRAME_Y, FRAME_W, FRAME_H, TFT_BLACK);
}

// Draw a red button
void redBtn()
{
  tft.fillRect(REDBUTTON_X, REDBUTTON_Y, REDBUTTON_W, REDBUTTON_H, TFT_RED);
  tft.fillRect(GREENBUTTON_X, GREENBUTTON_Y, GREENBUTTON_W, GREENBUTTON_H, TFT_DARKGREY);
  drawFrame();
  tft.setTextColor(TFT_WHITE);
  tft.setTextSize(2);
  tft.setTextDatum(MC_DATUM);
  tft.drawString("ON", GREENBUTTON_X + (GREENBUTTON_W / 2), GREENBUTTON_Y + (GREENBUTTON_H / 2));
  SwitchOn = false;
}

// Draw a green button
void greenBtn()
{
  tft.fillRect(GREENBUTTON_X, GREENBUTTON_Y, GREENBUTTON_W, GREENBUTTON_H, TFT_GREEN);
  tft.fillRect(REDBUTTON_X, REDBUTTON_Y, REDBUTTON_W, REDBUTTON_H, TFT_DARKGREY);
  drawFrame();
  tft.setTextColor(TFT_WHITE);
  tft.setTextSize(2);
  tft.setTextDatum(MC_DATUM);
  tft.drawString("OFF", REDBUTTON_X + (REDBUTTON_W / 2) + 1, REDBUTTON_Y + (REDBUTTON_H / 2));
  SwitchOn = true;
}
