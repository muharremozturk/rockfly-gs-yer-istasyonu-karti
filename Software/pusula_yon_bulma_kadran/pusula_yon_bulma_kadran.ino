// This example plots a rotated Sprite into another Sprite and then the resultant composited
// Sprite is pushed to the TFT screen. This example is for a 240 x 320 screen.

// The motivation for developing this capability is that animated dials can be drawn easily
// and the complex calculations involved are handled by the TFT_eSPI library. To create a dial
// with a moving needle a graphic of a meter needle is plotted at a specified angle into another
// Sprite that contains the dial face. When the needle Sprite is pushed to the dial Sprite the
// plotting ensures two pivot points for each Sprite coincide with pixel level accuracy.

// Two rotation pivot points must be set, one for the first Sprite and one for the second
// Sprite using setPivot(). These pivot points do not need to be within the Sprite boundaries.

// In this example a needle graphic is also be plotted direct to a defined TFT pivot point.

// The rotation angle is in degrees, an angle of 0 means no Sprite rotation.

// The pushRotated() function works with 1, 8 and 16 bit per pixel (bpp) Sprites.

// For 1 bpp Sprites the foreground and background colours are defined with the
// member function setBitmapColor(foregroundColor, backgroundColor).

// Created by Bodmer 6/1/19 as an example to the TFT_eSPI library:
// https://github.com/Bodmer/TFT_eSPI

#include <TFT_eSPI.h>
#include <Wire.h>
#include <LSM303.h>

LSM303 compass;

TFT_eSPI tft = TFT_eSPI();

TFT_eSprite dial   = TFT_eSprite(&tft); // Sprite object for dial
TFT_eSprite needle = TFT_eSprite(&tft); // Sprite object for needle

uint32_t startMillis;

int16_t angle = 0;

// =======================================================================================
// Setup
// =======================================================================================

void setup()   {
  Serial.begin(9600); // Debug only
  Wire.begin();
  compass.init();
  compass.enableDefault();
compass.m_min = (LSM303::vector<int16_t>){ -590,   -892,   -468};
compass.m_max = (LSM303::vector<int16_t>){  +553,   +186,   +553};
  
  tft.begin();
  tft.setRotation(1);

  // Clear TFT screen
  tft.fillScreen(TFT_NAVY);

  // Create the dial Sprite and dial (this temporarily hijacks the use of the needle Sprite)
  createDialScale(-180, 180, 30);   // create scale (start angle, end angle, increment angle)
  //drawEmptyDial("Label", 12345);    // draw the centre of dial in the Sprite

  //dial.pushSprite(110, 0);          // push a copy of the dial to the screen so we can see it

  // Create the needle Sprite
  createNeedle();                // draw the needle graphic
 // needle.pushSprite(95, 7);    // push a copy of the needle to the screen so we can see it
 
}

// =======================================================================================
// Loop
// =======================================================================================
int pusula;
float headingg;
void loop() {
  compass.read();

//40.0816916886053, 29.5203442139074
float flat1=40.081414;     // flat1 = our current latitude. flat is from the gps data. 
float flon1=29.520024;  // flon1 = our current longitude. flon is from the fps data.
float dist_calc=0;
float dist_calc2=0;
float diflat=0;
float diflon=0;
float x2lat=41.092661 ;  //enter a latitude point here   this is going to be your waypoint
float x2lon=29.117053 ;
//---------------------------------- distance formula below. Calculates distance from current location to waypoint
diflat=radians(x2lat-flat1);  //notice it must be done in radians
flat1=radians(flat1);    //convert current latitude to radians
x2lat=radians(x2lat);  //convert waypoint latitude to radians
diflon=radians((x2lon)-(flon1));   //subtract and convert longitudes to radians
dist_calc = (sin(diflat/2.0)*sin(diflat/2.0));
dist_calc2= cos(flat1);
dist_calc2*=cos(x2lat);
dist_calc2*=sin(diflon/2.0);                                       
dist_calc2*=sin(diflon/2.0);
dist_calc +=dist_calc2;
dist_calc=(2*atan2(sqrt(dist_calc),sqrt(1.0-dist_calc)));
dist_calc*=6371000.0; //Converting to meters
Serial.println("distance");
Serial.println(dist_calc);    //print the distance in meters

flon1 = radians(flon1);  //also must be done in radians

x2lon = radians(x2lon);  //radians duh.

headingg = atan2(sin(x2lon-flon1)*cos(x2lat),cos(flat1)*sin(x2lat)-sin(flat1)*cos(x2lat)*cos(x2lon-flon1)),2*3.1415926535;
headingg = headingg*180/3.1415926535;  // convert from radians to degrees

int head =headingg; //make it a integer now

if(head<0){

  headingg+=360;   //if the heading is negative then add 360 to make it positive

}

Serial.println("headingg:");

Serial.println(headingg);


  //int heading = compass.heading();
  for(int i=0;i<9;i++)
  {
    pusula+=compass.heading();;
    
    }
    pusula= pusula/10;
  int p= headingg-pusula;
  
  
  // Push the needle sprite to the dial Sprite at different angles and then push the dial to the screen
  // Use angle increments in range 1 to 6 for smoother or faster movement
  //  for (int16_t angle = -180; angle <= 180; angle += 3) {
  
    plotDial(100, 0, p, "ROCKET",  pusula);

  tft.fillRoundRect(5, 100, 95, 30,3, TFT_WHITE);
  tft.setFreeFont();
  tft.setTextSize(2);
  tft.setTextColor( TFT_BLACK,TFT_WHITE);
  tft.setCursor(10, 110);
  tft.print(dist_calc);
  tft.print(" M");
    
    //delay(50);
   // yield(); // Avoid a watchdog time-out
  //}
 //Serial.println(heading);
 // delay(100);
  //delay(1000);  // Pause

  // Update the dial Sprite with decreasing angle and plot to screen at 0,0, no delay
 /* for (int16_t angle = 180; angle >= -180; angle -= 6) {
    plotDial(100, 0, angle, "COMPASS", angle + 180);
    yield(); // Avoid a watchdog time-out
  }*/

  // Now show plotting of the rotated needle direct to the TFT
 // tft.setPivot(45, 150); // Set the TFT pivot point that the needle will rotate around

  // The needle graphic has a black border so if the angle increment is small
  // (6 degrees or less in this case) it wipes the last needle position when
  // it is rotated and hence it clears the swept area to black
 /* for (int16_t angle = 0; angle <= 360; angle += 5)
  {
    needle.pushRotated(angle); // Plot direct to TFT at specifed angle
    yield();                   // Avoid a watchdog time-out
  }*/
}

// =======================================================================================
// Create the dial sprite, the dial outer and place scale markers
// =======================================================================================

void createDialScale(int16_t start_angle, int16_t end_angle, int16_t increment)
{
  // Create the dial Sprite
  dial.setColorDepth(8);       // Size is odd (i.e. 91) so there is a centre pixel at 45,45
  dial.createSprite(320, 240);   // 8bpp requires 91 * 91 = 8281 bytes
  dial.setPivot(120, 120);       // set pivot in middle of dial Sprite

  // Draw dial outline
  dial.fillSprite(TFT_TRANSPARENT);           // Fill with transparent colour
  dial.fillCircle(120, 120, 95, TFT_RED);  // Draw dial outer

  // Hijack the use of the needle Sprite since that has not been used yet!
  needle.createSprite(4, 4);     // 3 pixels wide, 3 high
  needle.fillSprite(TFT_WHITE);  // Fill with white
  needle.setPivot(1, 95);        //  Set pivot point x to the Sprite centre and y to marker radius

  for (int16_t angle = start_angle; angle <= end_angle; angle += increment) {
    needle.pushRotated(&dial, angle); // Sprite is used to make scale markers
    yield(); // Avoid a watchdog time-out
  }

  needle.deleteSprite(); // Delete the hijacked Sprite
}


// =======================================================================================
// Add the empty dial face with label and value
// =======================================================================================

void drawEmptyDial(String label, int16_t val)
{
  // Draw black face
  dial.fillCircle(120, 120, 91, TFT_BLACK);
  dial.drawPixel(120, 120, TFT_WHITE);        // For demo only, mark pivot point with a while pixel

  dial.setTextDatum(TC_DATUM);              // Draw dial text
  dial.drawString(label, 120, 75, 2);
  dial.drawNumber(val, 120, 170, 2);
}

// =======================================================================================
// Update the dial and plot to screen with needle at defined angle
// =======================================================================================

void plotDial(int16_t x, int16_t y, int16_t angle, String label, uint16_t val)
{
  // Draw the blank dial in the Sprite, add label and number
  drawEmptyDial(label, val);

  // Push a rotated needle Sprite to the dial Sprite, with black as transparent colour
  needle.pushRotated(&dial, angle, TFT_BLACK); // dial is the destination Sprite

  // Push the resultant dial Sprite to the screen, with transparent colour
  dial.pushSprite(x, y, TFT_TRANSPARENT);
}

// =======================================================================================
// Create the needle Sprite and the image of the needle
// =======================================================================================

void createNeedle(void)
{
  needle.setColorDepth(8);
  needle.createSprite(11, 90); // create the needle Sprite 11 pixels wide by 49 high

  needle.fillSprite(TFT_BLACK);          // Fill with black

  // Define needle pivot point
  uint16_t piv_x = needle.width() / 2;   // x pivot of Sprite (middle)
  uint16_t piv_y = needle.height() - 10; // y pivot of Sprite (10 pixels from bottom)
  needle.setPivot(piv_x, piv_y);         // Set pivot point in this Sprite

  // Draw the red needle with a yellow tip
  // Keep needle tip 1 pixel inside dial circle to avoid leaving stray pixels
  needle.fillRect(piv_x - 1, 2, 3, piv_y +10, TFT_RED);
  needle.fillTriangle(5, 0, 0, 10,10,10, TFT_YELLOW);
 // needle.drawString(N, 120, 75, 2);
  // Draw needle centre boss
  needle.fillCircle(piv_x, piv_y, 5, TFT_MAROON);
  needle.drawPixel( piv_x, piv_y, TFT_WHITE);     // Mark needle pivot point with a white pixel
}

// =======================================================================================
