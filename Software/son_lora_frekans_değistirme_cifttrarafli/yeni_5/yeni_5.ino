//github linklerini ekle
#include "FS.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_BNO08x.h>
#include <Adafruit_H3LIS331.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "LITTLEFS.h"
#include <MicroNMEA.h>
#include "ms5611_spi.h"
#include "PCA9539.h"
#include <RadioLib.h>
#include "SparkFun_Ublox_Arduino_Library.h"
//#include "TimeLib.h"
#include <Vector.h>
#include <WiFi.h>  // Built-in
#include <WebServer.h>
#include <ESP32Time.h>
#include "EEPROM.h"
#include <SimpleKalmanFilter.h>

uint8_t timeZone = 3; //Changing the time zone
float mainPar = 5;  //main parachute replacement
bool measurementSystem = 0; //change the measurement system
uint8_t drogueDifference =1; //how many meters from the drogue will the parachute open

//flight and write modes must be turned on together
bool flightMode = 0; //turn on flight mode
bool writeMode = 0; //turn on write mode

const uint8_t tempData =10; //buffer size

#define SCREEN_WIDTH 128     // OLED display width, in pixels
#define SCREEN_HEIGHT 64     // OLED display height, in pixels
#define SCREEN_ADDRESS 0x3C  ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
bool screenMode = 1;
uint8_t switchScreen = 0;
uint8_t printDataCount = 0;

SimpleKalmanFilter speedKalmanFilter(1, 1, 0.1); // speed filter
SimpleKalmanFilter accelKalmanFilter(1, 1, 0.1); // acceleration filter

ESP32Time rtc; //real time clock

//wifi settings
#define ServerVersion "1.0"
String webpage = "";
#define SPIFFS LITTLEFS
bool SPIFFS_present = false;
const char *ssid = "RockFLY2022"; //wifi id
const char *password = "rockfly2022"; //wifi password
WebServer server(80);

//for pre-flight data
const int ELEMENT_COUNT_MAX = 50;
typedef Vector<String> Elements;
String storage_array[ELEMENT_COUNT_MAX];
Elements vector;

PCA9539 ioport(0x76); //io expert start

//for gps data
char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));
SFE_UBLOX_GPS myGPS;
int RXD2 = 26;
int TXD2 = 27;

#define buzzer 12 //buzzer pin  
#define pyro1 5 //pyro1 io expender pin
#define pyro2 6 //pyro2 io expender pin
#define pyro1S 36 //pyro1 measurement pin
#define pyro2S 39 //pyro2 measurement pin
#define led1 11 //led1 io expender pin
#define led2 12 //led2 io expender pin
#define batt 35 //battery measurement pin


// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33);

int transmissionState = ERR_NONE;
bool transmitFlag = false;
volatile bool enableInterrupt = true;
volatile bool operationDone = false;

//SX1262 interrupt
void setFlag(void) {
  if (!enableInterrupt) {
    return;
  }
  operationDone = true;
}

byte passw = 0x60; //password byte
byte errorData = 0x00; //error byte
byte setConfig = 0x10; //config set byte
byte sensData = 0x20; //send sensor data byte
byte flightSensData = 0x21;  // send flight sensor data byte
byte flightSensStatData = 0x22; // send flight stat data byte
byte sendTouchGrndData = 0x23; // send data after flight byte
byte sendTouchGrndDataStat = 0x24; // send stat data after flight byte
byte getConfigData = 0x30; // get congif information byte
byte getPrshtCon = 0x50; //get parachute information byte
byte sendFrqConfig = 0x31; //send frequency information byte
byte sendPrshtCon = 0x51; //send parachute information byte
byte drogueTest = 0x70; //send frequency information byte
byte mainTest = 0x71; //send parachute information byte
byte flashCleaning = 0x80; //send frequency information byte
byte check_frq = 0x40;

float loraFreq = 868.00; //set lora frequency
byte outputPower = 20; //set lora output power

uint8_t pyroStat; 
bool pyroStatArr[8]; //pyro state array

float accelMs5611; //ms5611 accel data
long accelMs5611TimerNew, accelMs5611TimerOld; //ms5611 accel timer
float speed; //ms5611 speed data
float speedOld; 
float altitude; //ms5611 altitude data
float fillRate;

//pyro variables
bool pyroFire1 = 0;
bool pyroFire2 = 0;
bool pyroProt1 = 0;
bool pyroProt2 = 0;

int cnt=0;
float last_frq,frquency_check;
byte last_dbm,dbm_check;

bool check_time=0;//comm timeout time

bool riseAccelStat=0;
bool fallAccelStat=0;
unsigned int riseAccelCount = 1;
unsigned int fallAccelCount = 1;
bool flashCleaningStat =0;
bool drogueTestStat =0;
bool mainTestStat =0;
bool apogeeCheck=0;
bool machLock = 0;
bool groundMode = 0;
bool apfallSpeedStart = 0;
bool apfallSpeedStop = 0;
unsigned int apfallSpeedCount = 0;
bool mainfallSpeedStart = 0;
bool mainfallSpeedStop = 0;
unsigned int mainfallSpeedCount = 0;
long pyroActivationTime=0;

unsigned long timeout=0;

String fileName[20];
String directoryName[20];
int countFileName = 0;
int countDirecName = 0;


struct flightData {
  float H3LIS331_data[3]; //400g accel x,y,z
  float Ms5611_data[4];   //temp, pressure, speed, accel
  float bnoAccel[3];      //accel x,y,z
  float bnoGyro[3];       //gyro x,y,z
  float bnoMag[3];        //magnitude x,y,z
  float bnoRaw[3];        //yaw, pitch, roll
  long GPS_data[4];       //latitude , longitude altitude speed
  float GpsSpeed;         //gps speed
  int GPS_time[6];        //time
  uint8_t SIV;            //number of satellites
  long unixTime;          //unix time 
  float battery;          //battery voltage 

  float maxAltitude;      //max altitude
  float minAltitude;      //min altitude
  float maxSpeed;         //max speed
  float maxAccel[3];      //max accel
  uint8_t maxSiv;         //max siv
  unsigned long engineBurnTime;//engine burn time 
  unsigned long apogeeTime;    //apogee time
  float apogeeFallSpeed;       //apogee fall speed
  float mainFallSpeed;         //main fall speed
  unsigned long descendTime;   //descend time
  unsigned long flightTime;    //flight time
  float riseAccel[3]={0,0,0};
  float fallAccel[3]={0,0,0};
};
flightData flightdata;

//lora byte array
struct datas {
  byte byteArray[200];
  int counter = 0;
};
datas data;

struct euler_t {
  float yaw;
  float pitch;
  float roll;
} ypr;

//bno080 definitions
#define BNO08X_CS 5
#define BNO08X_INT 25
#define BNO08X_RESET 14
Adafruit_BNO08x bno08x(BNO08X_RESET);
sh2_SensorValue_t sensorValue;

//ms5611 definitions
uint8_t MS_CS = 2;
baro_ms5611 MS5611(MS_CS);

//200g accel definitions
uint8_t accel200gCSPin = 17;
Adafruit_H3LIS331 lis = Adafruit_H3LIS331();

File file;

//temporary buffer data array
String All_data1[tempData];
String All_data2[tempData];
String All_data3[tempData];
String All_data4[tempData];

//temporary buffer data array for flash
String All_data_flash1[tempData];
String All_data_flash2[tempData];
String All_data_flash3[tempData];
String All_data_flash4[tempData];

//flash header 
String header = "200gAccX;200gAccY;200gAccZ;Temp;Press;Alt;Speed;Accelms5611;AccX;AccY;AccZ;GyrX;GyrY;GyrZ;MagX;MagY;MagZ;Yaw;Pitch;Roll;GpsLat;GpsLong;GpsAlt;GpsHız;SIV;Batt;PyroStat;GpsTime\n";
String headerStat = ";;;;;;;;;;;;;;;;;;;;;;;;;;;;maxAltitude;maxSpeed;maxAccelX;maxAccelY;maxAccelZ;maxSiv;engineBurnTime;apogeeTime;apogeeFallSpeed;mainFallSpeed;descendTime;pyroActivationTimeUs\n";
String statData;

int memory = 0; //buffer memory counter
int i = 0; //pre buffer memory counter

//flash check bits
bool flashCheck1 = 0;
bool flashCheck2 = 0;
bool flashCheck3 = 0;
bool flashCheck4 = 0;

//write check bits
bool writeCheck1 = 0;
bool writeCheck2 = 0;
bool writeCheck3 = 0;
bool writeCheck4 = 0;

uint8_t dataCheck = 0;
bool writeStart = 0;
bool error = 0;
bool sendSensorData = 0;
bool sendFrqConfigData = 0;
bool setConOk = 0;
bool sendPrchtConData = 0;

bool storageArrayWrite = 0;
bool setGpsTime = 0;
bool sendgroundData = 0;
bool sendgroundDataStat = 0;
bool sendStat = 0;
bool configFlashCheck = 0;
bool writeStatCheck = 0;

int pushButton = 0; //wifi mode button
unsigned long wifiModeTimer = 0; //

TaskHandle_t Task1; // second core variable

unsigned long start = 0; //for read data timer

void setup() {
  Serial.begin(115200);


  setPins();          //pin settings
  configFlash();      //flash settings

    frquency_check=EEPROM.readFloat(4);
  Serial.print(" içindeki frekans ");
  Serial.print(frquency_check);
  if(frquency_check>=868 && frquency_check<=915)loraFreq=frquency_check;
  else loraFreq=868.00;
  Serial.print(" frekans= ");
  Serial.print(loraFreq);

  dbm_check=EEPROM.readByte(8);
  Serial.print(" içindeki dbm ");
  Serial.print(dbm_check);
  if(dbm_check<=0 || dbm_check>=23)outputPower=20;
  else outputPower=dbm_check;
  Serial.print(" dbm= ");
  Serial.print(outputPower);
  configLora();       //lora settings
  configGps();        //Gps settings
  configH3lis();      //200g accel settings
  configMs5611();     //ms5611 settings
  configBno();        //bno080 settings
  configScreen();     //lcd screen settings
  buzzerToggle(5, 50);//star buzzer
  wifiMode();         //wifi mode settings

  //start core 2
  xTaskCreatePinnedToCore(
    Task1code, /* Task function. */
    "Task1",   /* name of task. */
    10000,     /* Stack size of task */
    NULL,      /* parameter of the task */
    1,         /* priority of the task */
    &Task1,    /* Task handle to keep track of created task */
    0);        /* pin task to core 0 */


delay(100);

  

}

// Flash recording in core 2
void Task1code(void *pvParameters) {

  for (;;) {
    vTaskDelay(1);
    if (writeMode) {
      if (!configFlashCheck) {
        appendFile(SPIFFS, "/Data.csv", header); //write header one times
        configFlashCheck = 1;
      }
      flashWrite(); //write data
    }
  }
}



void loop() {
  vTaskDelay(1);

  if (!flightMode == 1) {

    preFlightAlgorithm(); //Runs the pre-flight algorithm

  } else {

    readBno(); //read bno080
    if (millis() - start >= 90) {

      start = millis();
      readH3lis();        //read 400g accel
      readMs5611();       //read ms5611 
      readGPS();          //read GPS 
      readVoltage();      //read voltage
      flightAlgorithm();  //Runs the flight algorithm
      sendDataLora();     //send data lora 
      writeData();        //collects the data in an array and sends it to the 2nd core
    }
  }
}
void preFlightAlgorithm() {
  //if (!groundMode) readBno(); //read bno080 out of ground mode
  if (millis() - start >= 90) {
    start = millis();
    readGPS(); //read GPS
    if (!groundMode) {  //read data out of ground mode
      readH3lis();      //read 400g accel
      readMs5611();     //read ms5611
      readVoltage();    //read voltage
      printData();      //print lcd datas
      String data;
      addDataString(data);//add data to string

      //collect 50 data in vector
      if (vector.size() == vector.max_size()) {
        vector.remove(0);
      }
      vector.push_back(data);
    }
  }

  //flight detection
  if ((altitude > 5 /*&& ((-1.2 > flightdata.bnoAccel[0] || flightdata.bnoAccel[0] > 1.2) || (-1.2 > flightdata.bnoAccel[1] || flightdata.bnoAccel[1] > 1.2) || (-1.2 > flightdata.bnoAccel[2] || flightdata.bnoAccel[2] > 1.2)))
    ||((-2.5 > flightdata.bnoAccel[0] || flightdata.bnoAccel[0] > 2.5) || (-2.5 > flightdata.bnoAccel[1] || flightdata.bnoAccel[1] > 2.5) || (-2.5 > flightdata.bnoAccel[2] || flightdata.bnoAccel[2] > 2.5)*/))
  {
    radio.clearDio1Action();
    flightMode = 1;
    writeMode = 1;
    storageArrayWrite = 1;
    //buzzerToggle(5,100);
    flightdata.apogeeTime = millis();
    riseAccelStat=1;
  } 
  //lora in listening mode
  else if (operationDone) {
    Serial.println("GİRDİ");
    enableInterrupt = false;
    operationDone = false;
    if (transmitFlag) {
      if (transmissionState == ERR_NONE) {
      Serial.println(F("transmission finished!"));
      } else {
        Serial.print(F("failed, code "));
        Serial.println(transmissionState);
      }
      radio.startReceive();
      transmitFlag = false;
    } 
    //lora receives data
    else {
      int state = radio.readData(data.byteArray, 10);
      
      for(int p =0;p<10;p++){
        Serial.print(data.byteArray[p],HEX);
        Serial.print(",");
      } 
      Serial.println();
      if(state == ERR_CRC_MISMATCH){
      
      Serial.println(" CRC ERROR");
           
      }
      if (state == ERR_NONE) {
        //Serial.println(F("[SX1262] Received packet!"));
        //lora frequency and dbm settings
        if (data.byteArray[0] == passw && data.byteArray[1] == getConfigData) {
          error = 0;
          data.counter = 2;
          last_frq=loraFreq;  
          last_dbm=outputPower;
          loraFreq = byteToFloat(data.byteArray, &data.counter);
          outputPower = byteToByte(data.byteArray, &data.counter);
          radio.setFrequency(loraFreq);
          radio.setOutputPower(outputPower);
          Serial.println(" frekans değişti ");
          check_time=1;
        } 
        //lora frequency send back
        else if (data.byteArray[0] == passw && data.byteArray[1] == sendFrqConfig) {
     
          Serial.println(" frekans data istenmiş ");
          error = 0;
          sendFrqConfigData=1;
        } 
        //return lora parachute setting
        else if (data.byteArray[0] == passw && data.byteArray[1] == sendPrshtCon) {
          error = 0;
          sendPrchtConData = 1;
          
        } 
        //save lora setting
        else if (data.byteArray[0] == passw && data.byteArray[1] == check_frq) {
          error = 0;
          EEPROM.writeFloat(4, loraFreq);
          EEPROM.writeByte(8, outputPower);
          EEPROM.commit();
          Serial.println(" yeni frekansta kontrol istendi ");
          check_time=0;
          cnt=0;
          setConOk=1;
        } 
        //save parachute settings
        else if (data.byteArray[0] == passw && data.byteArray[1] == getPrshtCon) {
          error = 0;
          sendPrchtConData = 1;
          data.counter = 2;
          mainPar = byteToFloat(data.byteArray, &data.counter);
          EEPROM.writeFloat(0, mainPar);
          EEPROM.commit();
          Serial.println("getPrshtCon");
        } 
        //sensor data send command
        else if (data.byteArray[0] == passw && data.byteArray[1] == sensData) {
          sendSensorData = 1;
          Serial.println("sensData");
        } 
        //command to send ground data
        else if (data.byteArray[0] == passw && data.byteArray[1] == sendTouchGrndData) {
          sendgroundData = 1;
          Serial.println("sendTouchGrndData");
        } 
        //command to send ground stat data
        else if (data.byteArray[0] == passw && data.byteArray[1] == sendTouchGrndDataStat) {
          sendgroundDataStat = 1;
          Serial.println("sendTouchGrndDataStat");
        } 
        else if (data.byteArray[0] == passw && data.byteArray[1] == drogueTest) {
          pyroProt1=1;
          pyro1Fire();
          pyroProt1=0;
          drogueTestStat =1;
          Serial.println("drogueTest");
          delay(200);
        }
        else if (data.byteArray[0] == passw && data.byteArray[1] == mainTest) {
          pyroProt1=1;
          pyro2Fire();
          pyroProt1=0;
          mainTestStat =1;
          Serial.println("mainTest");
          delay(200);
        }
        else if (data.byteArray[0] == passw && data.byteArray[1] == flashCleaning) {
          flashCleaningStat=1;
          getDir(SPIFFS, "/", 1);
          deleteDir();
          calculateFillRate();
        }
        else {
          error = 1;
        }
      }
      //Serial.print(F("[SX1262] Sending another packet ... "));
      //lora transmits
      //lora sending settings
      if (error == 0 && sendFrqConfigData == 1) {
        sendFrqConfigData = 0;
        Serial.println("sendFrqConfigData");
        data.counter = 0;
        byteAddByte(passw, &data.counter, data.byteArray);
        byteAddByte(sendFrqConfig, &data.counter, data.byteArray);
        floatAddByte(loraFreq, &data.counter, data.byteArray);
        byteAddByte(outputPower, &data.counter, data.byteArray);
      // delay(100);
        transmissionState = radio.startTransmit(data.byteArray, 7);
      } 
      //lora sends parachute settings
      else if (error == 0 && sendPrchtConData == 1) {
        Serial.println("sendPrchtConData");
        sendPrchtConData = 0;
        data.counter = 0;
        byteAddByte(passw, &data.counter, data.byteArray);
        byteAddByte(sendPrshtCon, &data.counter, data.byteArray);
        floatAddByte(mainPar, &data.counter, data.byteArray);
       // delay(100);
        transmissionState = radio.startTransmit(data.byteArray, 6);
      } 
      //lora sends that the settings are ok
      else if (error == 0 && setConOk == 1) {
       
        setConOk = 0;
        data.counter = 0;
        byteAddByte(passw, &data.counter, data.byteArray);
        byteAddByte(check_frq, &data.counter, data.byteArray);
        transmissionState = radio.startTransmit(data.byteArray, 2);
        Serial.println("yeni frekans teyit edildi");
      } 
      //lora sending sensor data
      else if (error == 0 && sendSensorData == 1) {
        Serial.println("sendSensorData");
        sendSensorData = 0;
        data.counter = 0;
        byteAddByte(passw, &data.counter, data.byteArray);
        byteAddByte(sensData, &data.counter, data.byteArray);
        toByteData();
        unint16AddByte(fillRate, &data.counter, data.byteArray);
        transmissionState = radio.startTransmit(data.byteArray, 74);
      } 
      //lora sending ground data
      else if (error == 0 && sendgroundData == 1) {
        Serial.println("sendgroundData");
        sendgroundData = 0;
        data.counter = 0;
        byteAddByte(passw, &data.counter, data.byteArray);
        byteAddByte(sendTouchGrndData, &data.counter, data.byteArray);
        for (int k = 0; k < 3; k++) longAddByte(flightdata.GPS_data[k], &data.counter, data.byteArray);
        uint8AddByte(flightdata.SIV, &data.counter, data.byteArray);
        floatAddByte(flightdata.battery, &data.counter, data.byteArray);
        transmissionState = radio.startTransmit(data.byteArray, 24);
      } 
      //lora sending ground stat data
      else if (error == 0 && sendgroundDataStat == 1) {
        Serial.println("sendgroundDataStat");
        sendgroundDataStat = 0;
        data.counter = 0;
        byteAddByte(passw, &data.counter, data.byteArray);
        byteAddByte(sendTouchGrndDataStat, &data.counter, data.byteArray);
        for (int k = 0; k < 3; k++) longAddByte(flightdata.GPS_data[k], &data.counter, data.byteArray);
        uint8AddByte(flightdata.SIV, &data.counter, data.byteArray);
        floatAddByte(flightdata.battery, &data.counter, data.byteArray);
        toByteStat();
        unint16AddByte(fillRate, &data.counter, data.byteArray);
        transmissionState = radio.startTransmit(data.byteArray, 80);

      } 
      else if (error == 0 && drogueTestStat == 1) {
        data.counter = 0;
        byteAddByte(passw, &data.counter, data.byteArray);
        byteAddByte(drogueTest, &data.counter, data.byteArray);
        transmissionState = radio.startTransmit(data.byteArray, 3);
        Serial.println("drogue");
        pyro1Off();
        drogueTestStat=0;
      } 
      else if (error == 0 && mainTestStat == 1) {
        data.counter = 0;
        mainTestStat=0;
        byteAddByte(passw, &data.counter, data.byteArray);
        byteAddByte(drogueTest, &data.counter, data.byteArray);
        transmissionState = radio.startTransmit(data.byteArray, 3);
        Serial.println("main");
        pyro2Off();
      } 
      else if (error == 0 && flashCleaningStat == 1) {
        data.counter = 0;
        flashCleaningStat=0;
        byteAddByte(passw, &data.counter, data.byteArray);
        byteAddByte(flashCleaning, &data.counter, data.byteArray);
        unint16AddByte(fillRate, &data.counter, data.byteArray);
        transmissionState = radio.startTransmit(data.byteArray, 5);
        Serial.println("CCCCCC");
        //Serial.println("flashCleaningStat");
      } 
      //lora sends wrong data message
      else {
        error = 0;
        data.counter = 0;
        byteAddByte(passw, &data.counter, data.byteArray);
        //hatali mesaj
        byteAddByte(errorData, &data.counter, data.byteArray);
        transmissionState = radio.startTransmit(data.byteArray, 15);
        Serial.println("AAAAAAA");
      }

      transmitFlag = true;
    }

    
    enableInterrupt = true;
  }

  if(check_time==1){
 if(millis()-timeout > 1000){
     timeout = millis();
     cnt+=1;
     Serial.print(" timeout ");
     Serial.print(cnt);
     Serial.println(". saniye");
    }

    if(cnt>=7){
      loraFreq=last_frq;  
      outputPower=last_dbm; 
      radio.setFrequency(loraFreq);
      radio.setOutputPower(outputPower);
      EEPROM.writeFloat(4, loraFreq);
      EEPROM.writeByte(8, outputPower);
      EEPROM.commit();
      cnt=0;
      check_time=0;
      Serial.println("ESKİ FREKANSA GEÇİLDİ");
      Serial.println(loraFreq);
      //sendFrqConfigData=1;
      //radio.setDio1Action(setFlag);
     radio.startReceive(); 
   }
 }

}
//flight algorithm
void flightAlgorithm() {
  //If it exceeds 20 meters, the protection of pyro1 is removed.
  if (altitude > 20 && !pyroProt1) {
    pyroProt1 = 1;
  } 

  else if (pyroProt1 && !machLock && !pyroProt2) {
    //apogee detected
    if (flightdata.maxAltitude - altitude > 0 &&!apogeeCheck) {
      pyroActivationTime=micros();
      apogeeCheck=1;
      flightdata.apogeeTime = millis() - flightdata.apogeeTime;
      flightdata.descendTime = millis();
      riseAccelStat=0;
      for(int p=0;p<3;p++){
        flightdata.riseAccel[p]=flightdata.riseAccel[p]/riseAccelCount;
      }
      fallAccelStat=1;
    }
    //pyro1 is activated if it is dropped from the maximum height
    if (flightdata.maxAltitude - altitude > drogueDifference) {
      pyro1Fire();
      pyroActivationTime=micros()-pyroActivationTime;
      pyroProt2 = 1;
      apfallSpeedStart = 1;
      sendStat = 1;
      flightdata.minAltitude = flightdata.maxAltitude;
    }
  } 
  //pyro 2 is activated if pyro 2 protection is lifted and altitude is suitable for main parachute
  else if (!groundMode && pyroProt2 && (altitude - mainPar) < 5) {

    pyro2Fire();
    apfallSpeedStop = 1;
    mainfallSpeedStart = 1;
    groundMode = 1;
    flightdata.apogeeFallSpeed = flightdata.apogeeFallSpeed / (apfallSpeedCount + 1);
    apfallSpeedStart = 0;

  } 
  //landing is detected
  else if (groundMode) {
    //If there is a jump in altitude and the gyro sensors are inactive, a descent will occur.
    if ((flightdata.minAltitude - altitude < 0) && ((-0.2 < flightdata.bnoGyro[0] || flightdata.bnoGyro[0] < 0.2) || (-0.2 < flightdata.bnoGyro[1] || flightdata.bnoGyro[1] < 0.2) || (-0.2 < flightdata.bnoGyro[2] || flightdata.bnoGyro[2] < 0.2))) {
      //main fall speed calculation
      mainfallSpeedStop = 1;
      flightdata.mainFallSpeed = flightdata.mainFallSpeed / (mainfallSpeedCount + 1);
      mainfallSpeedStart = 0;
      flightdata.descendTime = millis() - flightdata.descendTime;
      flightdata.flightTime = flightdata.descendTime + flightdata.apogeeTime;
      fallAccelStat=0;
      for(int p=0;p<3;p++){
        flightdata.fallAccel[p]=flightdata.fallAccel[p]/fallAccelCount;
      }
      //pyros are shutting down
      pyroProt1 = 0;
      pyroProt2 = 0;
      pyro1Off();
      pyro2Off();
      //stats are written
      statData = ";;;;;;;;;;;;;;;;;;;;;;;;;;;;" + String(flightdata.maxAltitude) + ";" + String(flightdata.maxSpeed) 
      + ";" + String(flightdata.maxAccel[0]) + ";" + String(flightdata.maxAccel[1]) + ";" + String(flightdata.maxAccel[2])
      + ";" + String(flightdata.maxSiv) + ";" + String(flightdata.engineBurnTime) + ";" + String(flightdata.apogeeTime) 
      +";" + String(flightdata.apogeeFallSpeed) + ";" + String(flightdata.mainFallSpeed) + ";" + String(flightdata.descendTime) 
      +";" + String(pyroActivationTime) + ";" + flightdata.riseAccel[0]+ ";" + flightdata.riseAccel[1]+ ";" + flightdata.riseAccel[2]
      + ";" + flightdata.fallAccel[0]+ ";" + flightdata.fallAccel[1]+ ";" + flightdata.fallAccel[2]+"\n";
      statData.replace(".", ",");
      vTaskDelay(100);
      writeLastData();
      writeStatCheck = 1;
      while (writeStatCheck) vTaskDelay(1);

      //filename changing with date
      String recordTime;
      recordTime = "/Data_" + String(rtc.getHour()) + "_" + String(rtc.getMinute()) +"|";
      recordTime +=String(rtc.getDay()) + "_" + String(rtc.getMonth() + 1) + "_" + String(rtc.getYear())+ ".csv";
      char Buf[60];
      recordTime.toCharArray(Buf, recordTime.length()+1);
      renameFile(SPIFFS, "/Data.csv",Buf);

      flightMode = 0;
      writeMode = 0;
      vTaskDelay(100);
      calculateFillRate();
      radio.setDio1Action(setFlag);//Lora interrupt opens
      //Serial.print(F("[SX1262] Starting to listen ... "));
      radio.startReceive();//lora begins to listen
    }
  }
  if(riseAccelStat){
    for(int p=0;p<3;p++){
      flightdata.riseAccel[p]+=flightdata.bnoAccel[p];
    }
    riseAccelCount++;
  }
  if(fallAccelStat){
    for(int p=0;p<3;p++){
      flightdata.fallAccel[p]+=flightdata.bnoAccel[p];
    }
    fallAccelCount++;
  }
  //calculating the apogee fall speed
  if (apfallSpeedStart) {
    flightdata.apogeeFallSpeed += speed;
    apfallSpeedCount++;
  }
  //calculating the main fall speed
  if (mainfallSpeedStart) {
    flightdata.mainFallSpeed += speed;
    mainfallSpeedCount++;
  }

  float Gama = 1.4;
  float R = 287.05;
  float T = flightdata.Ms5611_data[0] + 273;  //convert to kelvin
  //mach lock protection
  if (sqrt(Gama * R * T) <= (abs(speed) / 0.93969)) {
    machLock = 1;
  } else
    machLock = 0;
}
//LCD display settings
void configScreen() {
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    screenMode = 0;
  }
  if (screenMode) display.clearDisplay();
}
//pin settings
void setPins() {
  configIOex();
  pinMode(BNO08X_CS, OUTPUT);
  pinMode(accel200gCSPin, OUTPUT);
  pinMode(MS_CS, OUTPUT);
  pinMode(buzzer, OUTPUT);
  digitalWrite(BNO08X_CS, 1);
  digitalWrite(accel200gCSPin, 1);
  digitalWrite(MS_CS, 1);
  digitalWrite(buzzer, 0);
  pinMode(pushButton, INPUT_PULLUP);
}
//lora settings
void configLora() {
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin(loraFreq, 500.0, 7, 5, 0x34, outputPower);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }
  radio.setCurrentLimit(110);
  if(!flightMode){
  radio.setDio1Action(setFlag);
  Serial.print(F("[SX1262] Starting to listen ... "));
  state = radio.startReceive();
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }
  }
}
//flash settings
void configFlash() {
  if (!SPIFFS.begin(1)) {
    Serial.println("LITTLEFS Mount Failed");
    while (1)
      ;
  }
  //flashRead();
  //flashClean();
  flashCreatPart();
  vector.setStorage(storage_array);
  EEPROM.begin(512);
  //If the data is stored in the eeprom, it reads from the eeprom.
  if (EEPROM.readFloat(0) > 0 && EEPROM.readFloat(4) > 0 && EEPROM.readByte(8) > 0) {
    mainPar = EEPROM.readFloat(0);
    loraFreq = EEPROM.readFloat(4);
    outputPower = EEPROM.readByte(8);
  }
  calculateFillRate(); //flash doluluk oranını hesplayan fonksiyon
}
//gps settings
void configGps() {
  do {
    Serial.println("GNSS: trying 38400 baud");
    Serial2.begin(38400, SERIAL_8N1, RXD2, TXD2);
    if (myGPS.begin(Serial2) == true) break;

    delay(100);
    Serial.println("GNSS: trying 9600 baud");
    Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
    if (myGPS.begin(Serial2) == true) {
      Serial.println("GNSS: connected at 9600 baud, switching to 38400");
      myGPS.setSerialRate(38400);
      delay(100);
    } else {
      //myGNSS.factoryReset();
      delay(2000);  //Wait a bit before trying again to limit the Serial output
    }
  } while (1);
  Serial.println("GNSS serial connected");

  myGPS.setNMEAOutputPort(Serial);
  myGPS.saveConfiguration();  //Save the current settings to flash and BBR
  myGPS.setNavigationFrequency(10);

  rtc.setTime(0, 0, 0, 17, 2, 2022); //rtc set
}
//bno080 settings
void configBno() {
  Serial.println("Adafruit BNO08x test!");
  if (!bno08x.begin_SPI(BNO08X_CS, BNO08X_INT)) {
    Serial.println("Failed to find BNO08x chip");
    //while (1) {
    //  delay(10);
    //}
  }
  Serial.println("BNO08x Found!");
  setReports();
  Serial.println("Reading events");
  delay(100);
}

// Here is where you define the bno080 outputs you want to receive
void setReports(void) {
  Serial.println("Setting desired reports");
  if (!bno08x.enableReport(SH2_ACCELEROMETER, 90)) {
    Serial.println("Could not enable accelerometer");
  }
  delay(2);
  if (!bno08x.enableReport(SH2_GYROSCOPE_CALIBRATED, 90)) {
    Serial.println("Could not enable gyroscope");
  }
  delay(2);
  if (!bno08x.enableReport(SH2_MAGNETIC_FIELD_CALIBRATED, 90)) {
    Serial.println("Could not enable magnetic field calibrated");
  }
  delay(2);
  if (!bno08x.enableReport(SH2_ROTATION_VECTOR, 90)) {
    Serial.println("Could not enable rotation vector");
  }
}
//quaternion to Euler transformation is done
void quaternionToEuler(float qr, float qi, float qj, float qk, euler_t *ypr, bool degrees = false) {

  float sqr = sq(qr);
  float sqi = sq(qi);
  float sqj = sq(qj);
  float sqk = sq(qk);

  ypr->yaw = atan2(2.0 * (qi * qj + qk * qr), (sqi - sqj - sqk + sqr));
  ypr->pitch = asin(-2.0 * (qi * qk - qj * qr) / (sqi + sqj + sqk + sqr));
  ypr->roll = atan2(2.0 * (qj * qk + qi * qr), (-sqi - sqj + sqk + sqr));

  if (degrees) {
    ypr->yaw *= RAD_TO_DEG;
    ypr->pitch *= RAD_TO_DEG;
    ypr->roll *= RAD_TO_DEG;
  }
}
//200g accel settings
void configH3lis() {
  if (!lis.begin_SPI(accel200gCSPin)) {
    Serial.println("Couldnt start");
    while (1) yield();
  }
  Serial.println("H3lis start");
  lis.setRange(H3LIS331_RANGE_100_G);  // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_50_HZ);
  //lis.enableHighPassFilter(1, LIS331_HPF_0_0025_ODR);
  lis.setLPFCutoff(LIS331_LPF_37_HZ);
}
//ms5611 settings
void configMs5611() {
  MS5611.initialize();
  MS5611.calibrateAltitude();// altitude sıfırlama
}
//io expender settings
void configIOex() {
  Wire.begin(21, 22);
  Wire.setClock(400000);
  ioport.pinMode(pyro1, OUTPUT);
  ioport.pinMode(pyro2, OUTPUT);
  ioport.pinMode(led1, OUTPUT);
  ioport.pinMode(led2, OUTPUT);
  delay(100);
}
//LCD display write function
void printData(void) {
  if (screenMode) {
    printDataCount++;
    if (printDataCount > 9) {
      printDataCount = 0;
      if (!digitalRead(pushButton)) {
        //while(!digitalRead(pushButton));
        switchScreen++;
        if (switchScreen > 4) switchScreen = 0;
      }
      if (switchScreen == 0) printGPS();
      else if (switchScreen == 1)
        printMs5611();
      else if (switchScreen == 2)
        printBno1();
      else if (switchScreen == 3)
        printBno2();
      else if (switchScreen == 4)
        print400g();
    }
  }
}
//LCD screen gps data printing
void printGPS() {
  display.clearDisplay();
  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(0, 0);              // Start at top-left corner
  String printGPS;

  printGPS = "GPS Time: " + String(rtc.getHour()) + ":" + String(rtc.getMinute()) + ":" + String(rtc.getSecond()) + "\n";
  printGPS += "GPS Date: " + String(rtc.getDay()) + "/" + String(rtc.getMonth() + 1) + "/" + String(rtc.getYear()) + "\n";
  printGPS += "GPS Vel : " + String(flightdata.GpsSpeed) + " m/s\n";
  printGPS += "GPS Lat : " + String(float(flightdata.GPS_data[0]) / 1000000, 6) + "\n";
  printGPS += "GPS Long: " + String(float(flightdata.GPS_data[1]) / 1000000, 6) + "\n";
  printGPS += "GPS Alt : " + String(float(flightdata.GPS_data[2]) / 1000) + " m\n";
  printGPS += "GPS Sat : " + String(flightdata.SIV);
  display.println(printGPS);
  display.display();
}
//LCD screen ms5611 data printing
void printMs5611() {
  display.clearDisplay();
  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(0, 0);              // Start at top-left corner

  String printGPS;
  printGPS = "Temp     : " + String(flightdata.Ms5611_data[0]) + "C\n";
  printGPS += "Altitude : " + String(flightdata.Ms5611_data[2]) + " m\n";
  printGPS += "Velocity : " + String(flightdata.Ms5611_data[3]) + " m/s\n";
  printGPS += "Battery  : " + String(flightdata.battery) + " V\n";
  printGPS += "Pyros    : " + String(pyroStatArr[0]) + "," + String(pyroStatArr[1]) + "\n";
  printGPS += "Main Meter: " + String(mainPar) + " m\n";
  display.println(printGPS);
  display.display();
}
//LCD screen bno080 data printing
void printBno2() {
  display.clearDisplay();
  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(0, 0);              // Start at top-left corner
  String printGPS;
  printGPS = "Gyro-X : " + String(flightdata.bnoGyro[0]) + " rad/s\n";
  printGPS += "Gyro-Y : " + String(flightdata.bnoGyro[1]) + " rad/s\n";
  printGPS += "Gyro-Z : " + String(flightdata.bnoGyro[2]) + " rad/s\n";
  printGPS += "Magno-X: " + String(flightdata.bnoMag[0]) + " uT\n";
  printGPS += "Magno-Y: " + String(flightdata.bnoMag[1]) + " uT\n";
  printGPS += "Magno-Z: " + String(flightdata.bnoMag[2]) + " uT\n";
  display.println(printGPS);
  display.display();
}
//LCD screen bno080 data printing
void printBno1() {
  display.clearDisplay();
  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(0, 0);              // Start at top-left corner
  String printGPS;
  printGPS = "Accel-X: " + String(flightdata.bnoAccel[0]) + " g\n";
  printGPS += "Accel-Y: " + String(flightdata.bnoAccel[1]) + " g\n";
  printGPS += "Accel-Z: " + String(flightdata.bnoAccel[2]) + " g\n";
  printGPS += "Yaw-X  : " + String(flightdata.bnoRaw[0]) + "\n";
  printGPS += "Pitch-Y: " + String(flightdata.bnoRaw[1]) + "\n";
  printGPS += "Roll-Z : " + String(flightdata.bnoRaw[2]) + "\n";
  display.println(printGPS);
  display.display();
}
//LCD screen 400g accel data printing
void print400g() {
  display.clearDisplay();
  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(0, 0);              // Start at top-left corner
  String printGPS;
  printGPS = "400g Acc-X: " + String(flightdata.H3LIS331_data[0]) + " g\n";
  printGPS += "400g Acc-Y: " + String(flightdata.H3LIS331_data[1]) + " g\n";
  printGPS += "400g Acc-Z: " + String(flightdata.H3LIS331_data[2]) + " g\n";
  display.println(printGPS);
  display.display();
}

//Reading bno080 data
void readBno() {

  if (bno08x.wasReset()) {
    Serial.print("sensor was reset ");
    setReports();
  }

  if (!bno08x.getSensorEvent(&sensorValue)) {
    return;
  }

  switch (sensorValue.sensorId) {

    case SH2_ACCELEROMETER:
      flightdata.bnoAccel[0] = (sensorValue.un.accelerometer.x) / SENSORS_GRAVITY_STANDARD;
      flightdata.bnoAccel[1] = (sensorValue.un.accelerometer.y) / SENSORS_GRAVITY_STANDARD;
      flightdata.bnoAccel[2] = (sensorValue.un.accelerometer.z) / SENSORS_GRAVITY_STANDARD;
      for (int k = 0; k < 3; k++) {
        if (abs(flightdata.bnoAccel[k]) > abs(flightdata.maxAccel[k])) flightdata.maxAccel[k] = flightdata.bnoAccel[k];
      }
      break;
    case SH2_GYROSCOPE_CALIBRATED:
      flightdata.bnoGyro[0] = (sensorValue.un.gyroscope.x);
      flightdata.bnoGyro[1] = (sensorValue.un.gyroscope.y);
      flightdata.bnoGyro[2] = (sensorValue.un.gyroscope.z);
      break;
    case SH2_MAGNETIC_FIELD_CALIBRATED:
      flightdata.bnoMag[0] = (sensorValue.un.magneticField.x);
      flightdata.bnoMag[1] = (sensorValue.un.magneticField.y);
      flightdata.bnoMag[2] = (sensorValue.un.magneticField.z);
      break;
    case SH2_ROTATION_VECTOR:
      quaternionToEuler(sensorValue.un.rotationVector.real, sensorValue.un.rotationVector.i, sensorValue.un.rotationVector.j, sensorValue.un.rotationVector.k, &ypr, true);
      flightdata.bnoRaw[0] = (ypr.yaw);
      flightdata.bnoRaw[1] = (ypr.pitch);
      flightdata.bnoRaw[2] = (ypr.roll);
      break;
  }
}
//Reading 400g accel data
void readH3lis() {

  sensors_event_t event;
  lis.getEvent(&event);
  flightdata.H3LIS331_data[0] = (-event.acceleration.y / SENSORS_GRAVITY_STANDARD);  //g
  flightdata.H3LIS331_data[1] = (event.acceleration.x / SENSORS_GRAVITY_STANDARD);   //g
  flightdata.H3LIS331_data[2] = (event.acceleration.z / SENSORS_GRAVITY_STANDARD);   //g
  digitalWrite(accel200gCSPin, 1);
  
  for (int k = 0; k < 3; k++) {
    if (abs(flightdata.H3LIS331_data[k]) > abs(flightdata.maxAccel[k])) flightdata.maxAccel[k] = flightdata.H3LIS331_data[k];
  }
}
//Reading ms5611 data
void readMs5611() {

  MS5611.updateData();
  MS5611.updateCalAltitudeKalman();
  MS5611.updateSpeedKalman();
  flightdata.Ms5611_data[0] = MS5611.getTemperature_degC();  //Celsius
  flightdata.Ms5611_data[1] = MS5611.getPressure_mbar();     //bar
  flightdata.Ms5611_data[2] = MS5611.getAltitudeKalman();    //meters
  altitude = flightdata.Ms5611_data[2];
  flightdata.Ms5611_data[3] = speedKalmanFilter.updateEstimate(MS5611.getVelMs());  //meter/second
  speed = flightdata.Ms5611_data[3];                                                //meter/second
  accelMs5611TimerNew = millis();
  accelMs5611 = (((speed - speedOld) * 1000) / (accelMs5611TimerNew - accelMs5611TimerOld)) / SENSORS_GRAVITY_STANDARD;
  accelMs5611TimerOld = millis();
  accelMs5611 = accelKalmanFilter.updateEstimate(accelMs5611);  //g
  speedOld = speed;

  if (measurementSystem == 0) {
    flightdata.Ms5611_data[3] = flightdata.Ms5611_data[3] * 3.6;  //km/hour
  } else {
    flightdata.Ms5611_data[0] = (flightdata.Ms5611_data[0] * 1.8f) + 32;  //Fahrenheit
    flightdata.Ms5611_data[2] = flightdata.Ms5611_data[2] * 3.2808399;    //feet
    flightdata.Ms5611_data[3] = flightdata.Ms5611_data[3] * 2.24;         //km/hour
  }
  if (abs(flightdata.Ms5611_data[3]) > abs(flightdata.maxSpeed)) flightdata.maxSpeed = flightdata.Ms5611_data[3];

}

//byte to functions
uint8_t byteToUint8t(byte *byterray, int *count) {
  uint8_t f;
  ((uint8_t *)&f)[0] = byterray[*count];
  (*count)++;
  return f;
}
float byteToFloat(byte *byterray, int *count) {
  float f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
long byteToLong(byte *byterray, int *count) {
  long f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
unsigned long byteToULong(byte *byterray, int *count) {
  unsigned long f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt(byte *byterray, int *count) {
  int f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt16(byte *byterray, int *count) {
  int16_t f;
  for (int m = 0; m < 2; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToUint16(byte *byterray, int *count) {
  uint16_t f;
  for (int m = 0; m < 2; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
byte byteToByte(byte *byterray, int *count) {
  byte f;
  ((uint8_t *)&f)[0] = byterray[*count];
  (*count)++;
  return f;
}
//byte conversion functions
void floatAddByte(float data, int *count, byte *array) {
  for (int m = 0; m < 4; m++) {
    array[*count] = ((uint8_t *)&data)[m];
    (*count)++;
  }
}
void longAddByte(long data, int *count, byte *array) {
  for (int m = 0; m < 4; m++) {
    array[*count] = ((uint8_t *)&data)[m];
    (*count)++;
  }
}
void unLongAddByte(unsigned long data, int *count, byte *array) {
  for (int m = 0; m < 4; m++) {
    array[*count] = ((uint8_t *)&data)[m];
    (*count)++;
  }
}
void int16AddByte(int16_t data, int *count, byte *array) {
  for (int m = 0; m < 2; m++) {
    array[*count] = ((uint8_t *)&data)[m];
    (*count)++;
  }
}
void unint16AddByte(uint16_t data, int *count, byte *array) {
  for (int m = 0; m < 2; m++) {
    array[*count] = ((uint8_t *)&data)[m];
    (*count)++;
  }
}
void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}
void uint8AddByte(uint8_t data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}
//The sensor data is converted to bytes and prepared to be sent and compared to generate statistical data.
void toByteData() {

  for (int k = 0; k < 3; k++) {
    int16AddByte(flightdata.H3LIS331_data[k] * 100, &data.counter, data.byteArray);
  }

  int16AddByte(flightdata.Ms5611_data[0] * 100, &data.counter, data.byteArray);
  floatAddByte(flightdata.Ms5611_data[2], &data.counter, data.byteArray);
  floatAddByte(flightdata.Ms5611_data[3], &data.counter, data.byteArray);

  if (flightdata.Ms5611_data[2] > flightdata.maxAltitude) flightdata.maxAltitude = flightdata.Ms5611_data[2];
  if (flightdata.Ms5611_data[2] < flightdata.minAltitude) flightdata.minAltitude = flightdata.Ms5611_data[2];
  
  for (int k = 0; k < 3; k++) {
    int16AddByte(flightdata.bnoAccel[k] * 100, &data.counter, data.byteArray);
  }

  for (int k = 0; k < 3; k++) int16AddByte(flightdata.bnoGyro[k] * 100, &data.counter, data.byteArray);

  for (int k = 0; k < 3; k++) int16AddByte(flightdata.bnoMag[k] * 100, &data.counter, data.byteArray);

  for (int k = 0; k < 3; k++) int16AddByte(flightdata.bnoRaw[k] * 100, &data.counter, data.byteArray);

  for (int k = 0; k < 3; k++) longAddByte(flightdata.GPS_data[k], &data.counter, data.byteArray);

  int16AddByte(flightdata.GpsSpeed * 100, &data.counter, data.byteArray);

  longAddByte(flightdata.unixTime, &data.counter, data.byteArray);

  int16AddByte(flightdata.battery * 100, &data.counter, data.byteArray);

  uint8AddByte(flightdata.SIV, &data.counter, data.byteArray);

  uint8AddByte(pyroStat, &data.counter, data.byteArray);
}
//Statistics data is prepared to be sent by converting to bytes.
void toByteStat() {

  floatAddByte(flightdata.maxAltitude, &data.counter, data.byteArray);
  int16AddByte(flightdata.maxSpeed * 50, &data.counter, data.byteArray);
  for (int k = 0; k < 3; k++) int16AddByte(flightdata.maxAccel[k] * 100, &data.counter, data.byteArray);
  uint8AddByte(flightdata.maxSiv, &data.counter, data.byteArray);
  uint8AddByte(flightdata.engineBurnTime / 1000, &data.counter, data.byteArray);
  unint16AddByte(flightdata.apogeeTime / 1000, &data.counter, data.byteArray);
  if (apfallSpeedStop) floatAddByte(flightdata.apogeeFallSpeed, &data.counter, data.byteArray);
  else
    floatAddByte(0, &data.counter, data.byteArray);
  if (mainfallSpeedStop) {
    floatAddByte(flightdata.mainFallSpeed, &data.counter, data.byteArray);
    unint16AddByte(flightdata.descendTime / 1000, &data.counter, data.byteArray);
  } else {
    floatAddByte(0, &data.counter, data.byteArray);
    unint16AddByte(0, &data.counter, data.byteArray);
  }
  longAddByte(pyroActivationTime, &data.counter, data.byteArray);
  floatAddByte(flightdata.riseAccel[0] , &data.counter, data.byteArray);
  floatAddByte(flightdata.riseAccel[1] , &data.counter, data.byteArray);
  floatAddByte(flightdata.riseAccel[2] , &data.counter, data.byteArray);
  
  if (mainfallSpeedStop) {
    floatAddByte(flightdata.fallAccel[0] , &data.counter, data.byteArray);
    floatAddByte(flightdata.fallAccel[1] , &data.counter, data.byteArray);
    floatAddByte(flightdata.fallAccel[2] , &data.counter, data.byteArray);
  }
  else {
    floatAddByte(0, &data.counter, data.byteArray);
    floatAddByte(0, &data.counter, data.byteArray);
    floatAddByte(0, &data.counter, data.byteArray);
  }
  // for (int k = 0; k < 3; k++) floatAddByte(flightdata.riseAccel[k] , &data.counter, data.byteArray);
  // if (mainfallSpeedStop) {
  //   for (int k = 0; k < 3; k++) floatAddByte(flightdata.fallAccel[k] , &data.counter, data.byteArray);
  // }
  // else {
  //   for (int k = 0; k < 3; k++) floatAddByte(0, &data.counter, data.byteArray);
  // }
}
//Sending data with lora
void sendDataLora() {
  int size = 70;
  data.counter = 0;
  byteAddByte(passw, &data.counter, data.byteArray);
  if (sendStat) {
    byteAddByte(flightSensStatData, &data.counter, data.byteArray);
  } else {
    byteAddByte(flightSensData, &data.counter, data.byteArray);
  }

  toByteData();
  if (sendStat) {
    toByteStat();
    size = 120;
  } else {
    size = 70;
  }
  for(int f = 0; f < size; f++)
  {
  Serial.print(data.byteArray[f],HEX);
  Serial.print(",");
  }
  Serial.println();
  int state = radio.transmit(data.byteArray, size);
  if (state == ERR_NONE) {

  } else if (state == ERR_PACKET_TOO_LONG) {
    // the supplied packet was longer than 256 bytes
    Serial.println(F("too long!"));

  } else if (state == ERR_TX_TIMEOUT) {
    // timeout occured while transmitting packetgps
    Serial.println(F("timeout!"));

  } else {
    // some other error occurred
    Serial.print(F("failed, code "));
    Serial.println(state);
  }
}
//Reading gps data
void readGPS() {

  myGPS.checkUblox();  //See if new data is available. Process bytes as they come in.
  if (nmea.isValid() == true) {
    flightdata.GPS_data[0] = nmea.getLatitude();
    flightdata.GPS_data[1] = nmea.getLongitude();
    nmea.getAltitude(flightdata.GPS_data[2]);
    flightdata.GPS_data[3] = nmea.getSpeed();
    flightdata.GpsSpeed = (float(flightdata.GPS_data[3]) / 1000) * 0.514444444;   // m/s
    if (measurementSystem == 0) flightdata.GpsSpeed = flightdata.GpsSpeed * 3.6;  // km/hour
    else
      flightdata.GpsSpeed = flightdata.GpsSpeed * 2.24;  //mile/hour
    flightdata.SIV = nmea.getNumSatellites();

    if (!setGpsTime) {
      rtc.setTime(nmea.getSecond(), nmea.getMinute(), nmea.getHour(), nmea.getDay(), nmea.getMonth(), nmea.getYear());
      flightdata.unixTime = rtc.getEpoch() + (timeZone * 60 * 60);
      rtc.setTime(flightdata.unixTime, 0);
      setGpsTime = 1;
    } else {
      flightdata.unixTime = rtc.getEpoch();
    }
    if (flightdata.SIV > flightdata.maxSiv) flightdata.maxSiv = flightdata.SIV;
  }
}
void SFE_UBLOX_GPS::processNMEA(char incoming) {
  //Take the incoming char from the Ublox I2C port and pass it on to the MicroNMEA lib
  //for sentence cracking
  nmea.process(incoming);
}
//Reading voltage data
void readVoltage() {
  flightdata.battery = readBatt();

  pyroStatArr[0] = pyro1Read();
  pyroStatArr[1] = pyro2Read();
  pyroStatArr[2] = pyroFire1;
  pyroStatArr[3] = pyroFire2;
  pyroStatArr[4] = machLock;

  pyroStat = 0;
  for (int i = 0; i < 5; i++) {
    pyroStat += pyroStatArr[i] * pow(2, i);
  }
  //Serial.println(pyroStat);
}
//fire pyro1
void pyro1Fire() {
  if (pyroProt1) {
    ioport.digitalWrite(pyro1, HIGH);
    pyroFire1 = 1;
  }
}
//Off pyro1
void pyro1Off() {
  ioport.digitalWrite(pyro1, LOW);
}
//read pyro1
bool pyro1Read() {
  if ((analogRead(pyro1S) * 3.3 / 4095) * 6.6 > 2)
    return 1;
  else
    return 0;
}
//fire pyro2
void pyro2Fire() {
  if (pyroProt2) {
    ioport.digitalWrite(pyro2, HIGH);
    pyroFire2 = 1;
  }
}
//Off pyro2
void pyro2Off() {
  ioport.digitalWrite(pyro2, LOW);
}
//read pyro2
bool pyro2Read() {
  if ((analogRead(pyro2S) * 3.3 / 4095) * 6.6 > 2)
    return 1;
  else
    return 0;
}
//led1 on
void led1On() {
  ioport.digitalWrite(led1, HIGH);
}
//led1 off
void led1Off() {
  ioport.digitalWrite(led1, LOW);
}
//led2 on
void led2On() {
  ioport.digitalWrite(led2, HIGH);
}
//led2 off
void led2Off() {
  ioport.digitalWrite(led2, LOW);
}
//Toggle buzzer
void buzzerToggle(int loop, int delayms) {
  for (int p = 0; p < loop; p++) {
    digitalWrite(buzzer, HIGH);
    delay(delayms);
    digitalWrite(buzzer, LOW);
    delay(delayms);
  }
}
//read battery
double readBatt() {
  double reading = analogRead(batt);  // Reference voltage is 3v3 so maximum reading is 3v3 = 4095 in range 0 to 4095
 
  if (reading < 1 || reading > 4095) return 0;   
    reading = (reading/4095.0)* 6.6*3.3;
  return 471.3768 + (0.9500909 - 471.3768)/(1 + pow((reading/423.8985),1.013072));
}
//creation of buffers for flash
void writeData() {

  if (dataCheck == 0) {
    while (flashCheck1) {
      vTaskDelay(1);
    }
    addDataString(All_data1[i]);
    //Serial.println(All_data1[i]);
    if (i == tempData-1) {
      i = 0;
      flashCheck1 = 1;
      dataCheck = 1;
    } else {
      i++;
    }
  } else if (dataCheck == 1) {
    while (flashCheck2) {
      vTaskDelay(1);
    }
    addDataString(All_data2[i]);
    if (i == tempData-1) {
      i = 0;
      flashCheck2 = 1;
      dataCheck = 2;
    } else {
      i++;
    }
  } else if (dataCheck == 2) {
    while (flashCheck3) {
      vTaskDelay(1);
    }
    addDataString(All_data3[i]);
    if (i == tempData-1) {
      i = 0;
      flashCheck3 = 1;
      dataCheck = 3;
    } else {
      i++;
    }
  } else if (dataCheck == 3) {
    while (flashCheck4) {
      vTaskDelay(1);
    }
    addDataString(All_data4[i]);
    if (i == tempData-1) {
      i = 0;
      flashCheck4 = 1;
      dataCheck = 0;
    } else {
      i++;
    }
  }
}
//transferring the remaining data to the buffer memory when the flight is completed
void writeLastData() {
  if (dataCheck == 0) {
    flashWriteData(All_data1, i);
    file.close();
  } else if (dataCheck == 1) {
    flashWriteData(All_data2, i);
    file.close();
  } else if (dataCheck == 2) {
    flashWriteData(All_data3, i);
    file.close();
  } else if (dataCheck == 3) {
    flashWriteData(All_data4, i);
    file.close();
  }
}
//Adds current data to data string
void addDataString(String &Data) {
  Data = String(flightdata.H3LIS331_data[0],3) + ";" + String(flightdata.H3LIS331_data[1],3) + ";" + String(flightdata.H3LIS331_data[2],3) + ";";
  Data += String(flightdata.Ms5611_data[0],3) + ";" + String(flightdata.Ms5611_data[1],3) + ";" + String(flightdata.Ms5611_data[2],3) + ";" + String(flightdata.Ms5611_data[3],3) + ";" + String(accelMs5611,3) + ";";
  Data += String(flightdata.bnoAccel[0],3) + ";" + String(flightdata.bnoAccel[1],3) + ";" + String(flightdata.bnoAccel[2],3) + ";";
  Data += String(flightdata.bnoGyro[0],3) + ";" + String(flightdata.bnoGyro[1],3) + ";" + String(flightdata.bnoGyro[2],3) + ";";
  Data += String(flightdata.bnoMag[0],3) + ";" + String(flightdata.bnoMag[1],3) + ";" + String(flightdata.bnoMag[2],3) + ";";
  Data += String(flightdata.bnoRaw[0],3) + ";" + String(flightdata.bnoRaw[1],3) + ";" + String(flightdata.bnoRaw[2],3) + ";";
  Data += String(flightdata.GPS_data[0]) + ";" + String(flightdata.GPS_data[1]) + ";" + String(flightdata.GPS_data[2]) + ";" + String(flightdata.GpsSpeed,3) + ";";
  Data += String(flightdata.SIV) + ";" + String(flightdata.battery,3) + ";" + String(pyroStat) + ";" + String(flightdata.unixTime) + "\n";
  Data.replace(".", ",");
}
//buffers are sequentially copied to flash and saved
void flashWrite() {

  if (storageArrayWrite) {
    storageArrayWrite = 0;
    flashWriteFirstData(storage_array);
  }
  if (flashCheck1 == 1) {
    for (int j = 0; j < tempData; j++) {
      All_data_flash1[j] = All_data1[j];
    }
    flashCheck1 = 0;
    writeCheck1 = 1;
  }

  if (flashCheck2 == 1) {
    for (int j = 0; j < tempData; j++) {
      All_data_flash2[j] = All_data2[j];
    }
    flashCheck2 = 0;
    writeCheck2 = 1;
  }

  if (flashCheck3 == 1) {
    for (int j = 0; j < tempData; j++) {
      All_data_flash3[j] = All_data3[j];
    }
    flashCheck3 = 0;
    writeCheck3 = 1;
  }

  if (flashCheck4 == 1) {
    for (int j = 0; j < tempData; j++) {
      All_data_flash4[j] = All_data4[j];
    }
    flashCheck4 = 0;
    writeCheck4 = 1;
  }

  if (writeCheck1 == 1) {
    flashWriteData(All_data_flash1, tempData);
    writeCheck1 = 0;
  }

  if (writeCheck2 == 1) {
    flashWriteData(All_data_flash2, tempData);
    writeCheck2 = 0;
  }

  if (writeCheck3 == 1) {
    flashWriteData(All_data_flash3, tempData);
    writeCheck3 = 0;
  }

  if (writeCheck4 == 1) {
    flashWriteData(All_data_flash4, tempData);
    writeCheck4 = 0;
  }
  if (writeStatCheck) {
    writeStat();
    writeStatCheck = 0;
  }
}
//statistics data is written to flash
void writeStat() {
  appendFile(SPIFFS, "/Data.csv", headerStat);
  appendFile(SPIFFS, "/Data.csv", statData);
}
//flight start data is written to flash
void flashWriteFirstData(String a[ELEMENT_COUNT_MAX]) {
  file = SPIFFS.open("/Data.csv", FILE_APPEND);
  for (int j = 0; j < ELEMENT_COUNT_MAX; j++) {
    file.print(a[j]);
  }
  file.close();
}
//buffer memory is written to flash
void flashWriteData(String a[tempData], uint8_t wCounter) {
  if (memory == 0) {
    file = SPIFFS.open("/Data.csv", FILE_APPEND);
    for (int j = 0; j < wCounter; j++) {
      file.print(a[j]);
    }
    memory++;
  }

  else if (memory < tempData-1) {
    for (int j = 0; j < wCounter; j++) {
      file.print(a[j]);
    }
    memory++;
  }

  else {
    for (int j = 0; j < wCounter; j++) {
      file.print(a[j]);
    }
    file.close();
    memory = 0;
  }
}

void flashRead() {
  readFile(SPIFFS, "/Data.csv");
}
//
void flashClean() {

  deleteFile(SPIFFS, "/Data.csv");
}
void flashCreatPart() {
  appendFile(SPIFFS, "/Data.csv", All_data_flash1[0]);
}

//list dir
void listDir(fs::FS &fs, const char *dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.print(file.name());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.print(file.size());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
    }
    file = root.openNextFile();
  }
}
// create dir
void createDir(fs::FS &fs, const char *path) {
  Serial.printf("Creating Dir: %s\n", path);
  if (fs.mkdir(path)) {
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}
//remove dir
void removeDir(fs::FS &fs, const char *path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}
// read file
void readFile(fs::FS &fs, const char *path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return;
  }

  Serial.println("- read from file:");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}
//write file
void writeFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_WRITE);
  file.print(message);
  file.close();
}
//append file
void appendFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_APPEND);
  file.print(message);
  file.close();
}
// delete file
void deleteFile(fs::FS &fs, const char *path) {
  fs.remove(path);
}
//rename file
void renameFile(fs::FS &fs, const char * path1, const char * path2){
    Serial.printf("Renaming file %s to %s\n", path1, path2);
    if (fs.rename(path1, path2)) {
        Serial.println("File renamed");
    } else {
        Serial.println("Rename failed");
    }
}
void getDir(fs::FS &fs, const char * dirname, uint8_t levels) {

  File root = fs.open(dirname);
  File file = root.openNextFile();

  while (file) {
    if (file.isDirectory()) {
      directoryName[countDirecName] = String(file.name());
      countDirecName++;
      if (levels) {
        getDir(fs, file.name(), levels - 1);
      }
    }
    else {
      fileName[countFileName] = String(file.name());
      ++countFileName;
    }
    file = root.openNextFile();
  }
}

void deleteDir() {
  for (int j = 0; j < countFileName; j++) {
    int str_len = fileName[j].length() + 1;
    char char_array[str_len];
    fileName[j].toCharArray(char_array, str_len);
    deleteFile(SPIFFS, char_array);
  }

  for (int i = 0; i < countDirecName; i++) {
    int str_len = directoryName[i].length() + 1;
    char char_array[str_len];
    directoryName[i].toCharArray(char_array, str_len);
    removeDir(SPIFFS, char_array);
  }
}

void calculateFillRate(){
  float totalBytes;
  float usedBytes;
  totalBytes=SPIFFS.totalBytes();
  usedBytes=SPIFFS.usedBytes();
  fillRate=(usedBytes/totalBytes)*100;
}
void wifiMode() {
  if (!digitalRead(pushButton)) {
    wifiModeTimer = millis();
    while (!digitalRead(pushButton)) {
      if (millis() - wifiModeTimer > 2000) {
        //buzzerToggle(2,500);
        led1On();
        writeMode = 0;
        //evdeki modem
        ssid = "VODAFONE_70CD";
        password = "ulaseren9519";
        WiFi.begin(ssid, password);

        while (WiFi.status() != WL_CONNECTED) {
          delay(500);
          Serial.print(".");
        }

        Serial.println("");
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());
        //esp32 modem oluyor
        /*
        WiFi.softAP(ssid, password);
        IPAddress IP = WiFi.softAPIP();
        Serial.print("AP IP address: ");
        Serial.println(IP);  
*/
        if (!SPIFFS.begin(true)) {
          Serial.println("SPIFFS initialisation failed...");
          SPIFFS_present = false;
        } else {
          Serial.println(F("SPIFFS initialised... file access enabled..."));
          SPIFFS_present = true;
        }
        //----------------------------------------------------------------------
        ///////////////////////////// Server Commands
        server.on("/", HomePage);
        server.on("/download", File_Download);
        server.on("/delete", File_Delete);
        server.on("/data", Data);
        server.on("/pyroTest", PyroTest);
        server.on("/settings", Settings);
        server.begin();
        Serial.println("HTTP server started");
        uint8_t ledTimer = 0;
        bool ledStat = 0;
        while (1) {
          delay(1);
          readBno();
          if (millis() - start >= 90) {
            start = millis();
            readH3lis();
            readMs5611();
            readGPS();
            readVoltage();
            if (ledTimer > 9 && ledStat) {
              led1On();
              ledStat = 0;
              ledTimer = 0;
            } else if (ledTimer > 9 && !ledStat) {
              led1Off();
              ledStat = 1;
              ledTimer = 0;
            }
            ledTimer++;
          }
          server.handleClient();
        }
      }
    }
  }
}
void append_page_header() {
  webpage = F("<!DOCTYPE html><html>");
  webpage += F("<script>function change(value){document.getElementById('download').value=value;}</script>");
  webpage += F("<head>");
  webpage += F("<title>File Server</title>");  // NOTE: 1em = 16px
  webpage += F("<meta name='viewport' content='user-scalable=yes,initial-scale=1.0,width=device-width'>");
  webpage += F("<style>");
  webpage += F("body{max-width:65%;margin:0 auto;font-family:arial;font-size:105%;text-align:center;color:blue;background-color:#F7F2Fd;}");
  webpage += F("ul{list-style-type:none;margin:0.1em;padding:0;border-radius:0.375em;overflow:hidden;background-color:#dcade6;font-size:1em;}");
  webpage += F("li{float:left;border-radius:0.375em;border-right:0.06em solid #bbb;}last-child {border-right:none;font-size:85%}");
  webpage += F("li a{display: block;border-radius:0.375em;padding:0.44em 0.44em;text-decoration:none;font-size:85%}");
  webpage += F("li a:hover{background-color:#EAE3EA;border-radius:0.375em;font-size:85%}");
  webpage += F("section {font-size:0.88em;}");
  webpage += F("h1{color:white;border-radius:0.5em;font-size:1em;padding:0.2em 0.2em;background:#558ED5;}");
  webpage += F("h2{color:orange;font-size:1.0em;}");
  webpage += F("h3{font-size:0.8em;}");
  webpage += F("table{font-family:arial,sans-serif;font-size:0.9em;border-collapse:collapse;width:85%;}");
  webpage += F("th,td {border:0.06em solid #dddddd;text-align:left;padding:0.3em;border-bottom:0.06em solid #dddddd;}");
  webpage += F("tr:nth-child(odd) {background-color:#eeeeee;}");
  webpage += F(".rcorners_n {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:20%;color:white;font-size:75%;}");
  webpage += F(".rcorners_m {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:50%;color:white;font-size:75%;}");
  webpage += F(".rcorners_w {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:70%;color:white;font-size:75%;}");
  webpage += F(".column{float:left;width:50%;height:45%;}");
  webpage += F(".row:after{content:'';display:table;clear:both;}");
  webpage += F("*{box-sizing:border-box;}");
  webpage += F("footer{background-color:#eedfff; text-align:center;padding:0.3em 0.3em;border-radius:0.375em;font-size:60%;}");
  webpage += F("button{border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:20%;color:white;font-size:130%;}");
  webpage += F(".buttons {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:15%;color:white;font-size:80%;}");
  webpage += F(".buttonsm{border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:9%; color:white;font-size:70%;}");
  webpage += F(".buttonm {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:15%;color:white;font-size:70%;}");
  webpage += F(".buttonw {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:40%;color:white;font-size:70%;}");
  webpage += F("a{font-size:75%;}");
  webpage += F("p{font-size:75%;}");
  webpage += F("</style></head><body><h1>File Server ");
  webpage += String(ServerVersion) + "</h1>";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void append_page_footer() {  // Saves repeating many lines of code for HTML page footers
  webpage += F("<ul>");
  webpage += F("<li><a href='/'>Home</a></li>");  // Lower Menu bar command entries
  webpage += F("<li><a href='/download'>Download</a></li>");
  //webpage += F("<li><a href='/upload'>Upload</a></li>");
  webpage += F("<li><a href='/delete'>Delete</a></li>");
  webpage += F("<li><a href='/data'>Data</a></li>");
  webpage += F("<li><a href='/pyroTest'>PyroTest</a></li>");
  webpage += F("<li><a href='/settings'>Settings</a></li>");
  webpage += F("</ul>");
  webpage += "<footer>&copy;appcent</footer>";
  webpage += F("</body></html>");
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// All supporting functions from here...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void HomePage() {
  SendHTML_Header();
  webpage += F("<a href='/download'><button>Download</button></a>");
  webpage += F("<a href='/delete'><button>Delete</button></a>");
  webpage += F("<a href='/data'><button>Data</button></a>");
  webpage += F("<a href='/pyroTest'><button>PyroTest</button></a>");
  webpage += F("<a href='/settings'><button>Settings</button></a>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();  // Stop is needed because no content length was sent
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void File_Download() {      // This gets called twice, the first pass selects the input, the second pass then processes the command line arguments
  if (server.args() > 0) {  // Arguments were received
    if (server.hasArg("download")) DownloadFile(server.arg(0));
  } else {  //SelectInput(String heading1, String command, String arg_calling_name) //SelectInput("Enter filename to download","download","download");
    SendHTML_Header();
    //webpage += F("<h3>Enter filename to download</h3>");
    webpage += F("<FORM action='/download' method='post'>");  // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
    webpage += F("<input type='hidden' name='download' id='download' value=''><br>");
    webpage += F("<type='submit' name='download' value=''><br><br>");
    webpage += F("<a href='/'>[Back]</a><br><br>");
    webpage += F("<table align='center'>");
    webpage += F("<tr><th>Name/Type</th><th>File Size</th><th></th></tr>");
    printDirectoryDownload("/", 0, "Download");
    webpage += F("</table><br><br>");
    append_page_footer();
    SendHTML_Content();
    SendHTML_Stop();
  }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void DownloadFile(String filename) {
  if (SPIFFS_present) {
    File download = SPIFFS.open("/" + filename, "r");
    if (download) {
      server.sendHeader("Content-Description","File Transfer");
      server.sendHeader("Content-Type", "application/octet-stream");
      server.sendHeader("Content-Transfer-Encoding","Binary");
      server.sendHeader("Content-Disposition", "attachment; filename=" + filename);
      server.sendHeader("Connection", "close");
      server.streamFile(download, "application/octet-stream");
      download.close();
    } else
      ReportFileNotPresent("download");
  } else
    ReportSPIFFSNotPresent();
}
void printDirectory(const char *dirname, uint8_t levels) {
  File root = SPIFFS.open(dirname);
  if (!root) {
    return;
  }
  if (!root.isDirectory()) {
    return;
  }
  File file = root.openNextFile();
  while (file) {
    if (webpage.length() > 1000) {
      SendHTML_Content();
    }
    if (file.isDirectory()) {
      webpage += "<tr><td>" + String(file.isDirectory() ? "Dir" : "File") + "</th><th>" + String(file.name()) + "</td><td></td></tr>";
      printDirectory(file.name(), levels - 1);
    } else {
      webpage += "<tr><td>" + String(file.name()) + "</td>";
      //webpage += "<td>"+String(file.isDirectory()?"Dir":"File")+"</td>";
      int bytes = file.size();
      String fsize = "";
      if (bytes < 1024) fsize = String(bytes) + " B";
      else if (bytes < (1024 * 1024))
        fsize = String(bytes / 1024.0, 3) + " KB";
      else if (bytes < (1024 * 1024 * 1024))
        fsize = String(bytes / 1024.0 / 1024.0, 3) + " MB";
      else
        fsize = String(bytes / 1024.0 / 1024.0 / 1024.0, 3) + " GB";
      webpage += "<td>" + fsize + "</td></tr>";
    }
    file = root.openNextFile();
  }
  file.close();
}
void printDirectoryDownload(const char *dirname, uint8_t levels, const char *buttonName) {
  File root = SPIFFS.open(dirname);
  if (!root) {
    return;
  }
  if (!root.isDirectory()) {
    return;
  }
  File file = root.openNextFile();
  while (file) {
    if (webpage.length() > 1000) {
      SendHTML_Content();
    }
    if (file.isDirectory()) {
      webpage += "<tr><td>" + String(file.isDirectory() ? "Dir" : "File") + "</th><th>" + String(file.name()) + "</td><td></td></tr>";
      printDirectory(file.name(), levels - 1);
    } else {
      webpage += "<tr><td>" + String(file.name()) + "</td>";  //
      //webpage += "<td>"+String(file.isDirectory()?"Dir":"File")+"</td>";
      int bytes = file.size();
      String fsize = "";
      if (bytes < 1024) fsize = String(bytes) + " B";
      else if (bytes < (1024 * 1024))
        fsize = String(bytes / 1024.0, 3) + " KB";
      else if (bytes < (1024 * 1024 * 1024))
        fsize = String(bytes / 1024.0 / 1024.0, 3) + " MB";
      else
        fsize = String(bytes / 1024.0 / 1024.0 / 1024.0, 3) + " GB";
      webpage += "<td>" + fsize + "</td><td><center><input type='submit' onclick=\"change('" + String(file.name()) + "');\" value='" + String(buttonName) + "'></center></td></tr>";
    }
    file = root.openNextFile();
  }
  file.close();
}

void Data() {
  SendHTML_Header();
  webpage += "<meta http-equiv='refresh' content='5'>";
  webpage += "<h3>System Information</h3>";
  webpage += "<h4>Bno080 Data</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Data</th><th>X</th><th>Y</th><th>Z</th><tr>";
  webpage += "<tr><td>Accel</td><td>" + String(flightdata.bnoAccel[0]) + "</td><td>" + String(flightdata.bnoAccel[1]) + "</td><td>" + String(flightdata.bnoAccel[2]) + "</td></tr> ";
  webpage += "<tr><td>Gyro</td><td>" + String(flightdata.bnoGyro[0]) + "</td><td>" + String(flightdata.bnoGyro[1]) + "</td><td>" + String(flightdata.bnoGyro[2]) + "</td></tr> ";
  webpage += "<tr><td>Magneto</td><td>" + String(flightdata.bnoMag[0]) + "</td><td>" + String(flightdata.bnoMag[1]) + "</td><td>" + String(flightdata.bnoMag[2]) + "</td></tr> ";
  webpage += "<tr><td>Euler</td><td>" + String(flightdata.bnoRaw[0]) + "</td><td>" + String(flightdata.bnoRaw[1]) + "</td><td>" + String(flightdata.bnoRaw[2]) + "</td></tr> ";
  webpage += "</table>";
  webpage += "<h4>H3lis331dl Data</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Data</th><th>X</th><th>Y</th><th>Z</th><tr>";
  webpage += "<tr><td>Accel</td><td>" + String(flightdata.H3LIS331_data[0]) + "</td><td>" + String(flightdata.H3LIS331_data[1]) + "</td><td>" + String(flightdata.H3LIS331_data[2]) + "</td></tr> ";
  webpage += "</table>";
  webpage += "<h4>MS5611</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Temp</th><th>Altitude</th><th>Velocity</th><tr>";
  webpage += "<tr><td>" + String(flightdata.Ms5611_data[0]) + "</td><td>" + String(flightdata.Ms5611_data[2]) + "</td><td>" + String(flightdata.Ms5611_data[3]) + "</td></tr>";
  webpage += "</table>";
  webpage += "<h4>GPS Data</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Latitude</th><th>Longitude</th><th>Altitude</th><th>Speed</th><th>SIV</th><tr>";
  webpage += "<tr><td>" + String(float(flightdata.GPS_data[0]) / 1000000, 6) + "</td><td>" + String(float(flightdata.GPS_data[1]) / 1000000, 6) + "</td><td>" + String(float(flightdata.GPS_data[2]) / 1000) + "</td><td>" + String(flightdata.GpsSpeed) + "</td><td>" + String(flightdata.SIV) + "</td></tr> ";
  webpage += "</table>";
  //webpage += "<h4></h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Time</th><th>Date</th><tr>";
  webpage += "<tr><td>" + String(rtc.getHour()) + ":" + String(rtc.getMinute()) + ":" + String(rtc.getSecond()) + "</td><td>" + String(rtc.getDay()) + "/" + String(rtc.getMonth() + 1) + "/" + String(rtc.getYear()) + "</td></tr> ";
  webpage += "</table>";
  webpage += "<h4>Pyro Information</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Batt</th><th>Pyro 1</th><th>Pyro 2</th></tr>";
  webpage += "<tr><td>" + String(flightdata.battery) + "</td><td>" + String(pyroStatArr[0]) + "</td><td>" + String(pyroStatArr[1]) + "</td></tr>";
  webpage += "</table> ";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
void PyroTest(){
  if (server.args() > 0) {  // Arguments were received
    if (server.hasArg("pyroTest")) {
      SendHTML_Header();
      if(server.arg(1)=="pyro1")webpage += "<h3> Pyro Drogue will fire after "+server.arg(0)+" seconds</h3>";
      if(server.arg(1)=="pyro2")webpage += "<h3> Pyro Main will fire after "+server.arg(0)+" seconds</h3>";
      webpage += "<h3> PLEASE WAIT!!!</h3>";
      webpage += F("<a href='/pyroTest'>[Back]</a><br><br>");
  
      append_page_footer();
      SendHTML_Content();
      SendHTML_Stop();
      if(server.arg(1)=="pyro1"){
        pyroProt1=1;
        delay(server.arg(0).toInt()*1000);
        pyro1Fire();
        pyroProt1=0;
      }
      if(server.arg(1)=="pyro2"){
        pyroProt2=1;
        delay(server.arg(0).toInt()*1000);
        pyro2Fire();
        pyroProt2=0;
      }
    }

  } else {
    SendHTML_Header();
    //webpage += "<meta http-equiv='refresh' content='5'>";
    webpage += F("<FORM action='/pyroTest' method='post' onsubmit=\"return confirm('Do you really want to fire the pyro?');\"> ");  // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
    //webpage += "<form action='/pyroTest' method='post'><br><br>";
    webpage += "<label for='pyroTest'>Pyro Delay (Second):</label>";
    webpage += "<input type='text' name='pyroTest' id='pyroTest' value='10'><br><br><br>";
    webpage += "<input type='radio' id='pyro1' name='pyro' value='pyro1' checked>";
    webpage += "<label for='pyro1'>Pyro Drogue</label><br>";
    webpage += "<input type='radio' id='pyro2' name='pyro' value='pyro2'>";
    webpage += "<label for='pyro2'>Pyro Main</label><br><br><br>";
    webpage += "<button type='submit'>Fire</button><br><br>";
    webpage += "</form>";
    append_page_footer();
    SendHTML_Content();
    SendHTML_Stop();

  }
}
void Settings() {
  if (server.args() > 0) {  // Arguments were received
    if (server.hasArg("settings")) {
      SendHTML_Header();
      webpage += "<h3>Data has been save</h3>";
      webpage += F("<a href='/settings'>[Back]</a><br><br>");
      Serial.print("server.arg(0)=");
      mainPar = server.arg(0).toFloat();
      EEPROM.writeFloat(0, mainPar);
      EEPROM.commit();
      Serial.println(server.arg(0));
      append_page_footer();
      SendHTML_Content();
      SendHTML_Stop();
    }

  } else {
    SendHTML_Header();
    //webpage += "<meta http-equiv='refresh' content='5'>";
    webpage += "<form action='/settings' method='post'><br><br>";
    webpage += "<label for='fmainPar'>Main Parachute:</label>";
    webpage += "<input type='text' name='settings' id='settings' value='" + String(mainPar) + "'><br><br><br>";
    webpage += "<button type='submit'>Submit</button><br><br>";
    webpage += "</form>";
    append_page_footer();
    SendHTML_Content();
    SendHTML_Stop();
  }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void File_Delete() {
  if (server.args() > 0) {  // Arguments were received
    if (server.hasArg("delete")) SPIFFS_file_delete(server.arg(0));
  } else {  //SelectInput(String heading1, String command, String arg_calling_name) //SelectInput("Enter filename to download","download","download");
    SendHTML_Header();
    //webpage += F("<h3>Enter filename to download</h3>");
    webpage += F("<FORM action='/delete' method='post' onsubmit=\"return confirm('Do you really want to delete the file?');\"> ");  // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
    webpage += F("<input type='hidden' name='delete' id='download' value=''><br>");
    webpage += F("<type='submit' name='delete' value=''><br><br>");
    webpage += F("<a href='/'>[Back]</a><br><br>");
    webpage += F("<table align='center'>");
    webpage += F("<tr><th>Name/Type</th><th>File Size</th><th></th></tr>");
    printDirectoryDownload("/", 0, "Delete");
    webpage += F("</table><br><br>");
    append_page_footer();
    SendHTML_Content();
    SendHTML_Stop();
  }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SPIFFS_file_delete(String filename) {  // Delete the file
  if (SPIFFS_present) {
    SendHTML_Header();
    //File dataFile = SPIFFS.open("/"+filename, "r"); // Now read data from SPIFFS Card
    //if (dataFile)
    //{
    if (SPIFFS.remove("/" + filename)) {
      Serial.println(F("File deleted successfully"));
      webpage += "<h3>File '" + filename + "' has been erased</h3>";
      webpage += F("<a href='/delete'>[Back]</a><br><br>");
    } else {
      webpage += F("<h3>File was not deleted - error</h3>");
      webpage += F("<a href='delete'>[Back]</a><br><br>");
    }
    //} else ReportFileNotPresent("delete");
    append_page_footer();
    SendHTML_Content();
    SendHTML_Stop();
  } else
    ReportSPIFFSNotPresent();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Header() {
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server.sendHeader("Pragma", "no-cache");
  server.sendHeader("Expires", "-1");
  server.setContentLength(CONTENT_LENGTH_UNKNOWN);
  server.send(200, "text/html", "");  // Empty content inhibits Content-length header so we have to close the socket ourselves.
  append_page_header();
  server.sendContent(webpage);
  webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Content() {
  server.sendContent(webpage);
  webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Stop() {
  server.sendContent("");
  server.client().stop();  // Stop is needed because no content length was sent
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SelectInput(String heading1, String command, String arg_calling_name) {
  SendHTML_Header();
  webpage += F("<h3>");
  webpage += heading1 + "</h3>";
  webpage += F("<FORM action='/");
  webpage += command + "' method='post'>";  // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
  webpage += F("<input type='text' name='");
  webpage += arg_calling_name;
  webpage += F("' value=''><br>");
  webpage += F("<type='submit' name='");
  webpage += arg_calling_name;
  webpage += F("' value=''><br><br>");
  webpage += F("<a href='/'>[Back]</a><br><br>");
  webpage += F("<table align='center'>");
  webpage += F("<tr><th>Name/Type</th><th>File Size</th></tr>");
  printDirectory("/", 0);
  webpage += F("</table><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportSPIFFSNotPresent() {
  SendHTML_Header();
  webpage += F("<h3>No SPIFFS Card present</h3>");
  webpage += F("<a href='/'>[Back]</a><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportFileNotPresent(String target) {
  SendHTML_Header();
  webpage += F("<h3>File does not exist</h3>");
  webpage += F("<a href='/");
  webpage += target + "'>[Back]</a><br><br>";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportCouldNotCreateFile(String target) {
  SendHTML_Header();
  webpage += F("<h3>Could Not Create Uploaded File (write-protected?)</h3>");
  webpage += F("<a href='/");
  webpage += target + "'>[Back]</a><br><br>";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
String file_size(int bytes) {
  String fsize = "";
  if (bytes < 1024) fsize = String(bytes) + " B";
  else if (bytes < (1024 * 1024))
    fsize = String(bytes / 1024.0, 3) + " KB";
  else if (bytes < (1024 * 1024 * 1024))
    fsize = String(bytes / 1024.0 / 1024.0, 3) + " MB";
  else
    fsize = String(bytes / 1024.0 / 1024.0 / 1024.0, 3) + " GB";
  return fsize;
}
