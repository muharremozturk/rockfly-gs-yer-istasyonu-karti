#include <Wire.h>
#include <LSM303.h>

LSM303 compass;
int pusula;
void setup() {
  Serial.begin(9600);
  Wire.begin();
  compass.init();
  compass.enableDefault();
  
  /* -633,   -636,   -536}    max: {  +412,   +404,   +389
  ********************************************************************
   çalışan calibration
    compass.m_min = (LSM303::vector<int16_t>){ -852,   -852,   -852};
 
  compass.m_max = (LSM303::vector<int16_t>){ +523,   +523,   +523};
  ********************************************************************
  */ compass.m_min = (LSM303::vector<int16_t>){ -300,   -790,   -566};
 
  compass.m_max = (LSM303::vector<int16_t>){+751,   +303,   +332 };
  
 // ******************************************************************** -598,   -810,   -506}    max: {  +376,   +255,   +493
 /* 
  //son montajda olacağı yer calibre ayarları en doğrusu//
  compass.m_min = (LSM303::vector<int16_t>){  -810,   -810,   -810};
  compass.m_max = (LSM303::vector<int16_t>){ +600,   +600,   +600};
  
/*  ********************************************************************
  Calibration values; the default values of +/-32767 for each axis
  lead to an assumed magnetometer bias of 0. Use the Calibrate example
  program to determine appropriate values for your particular unit.
  */
  /*compass.m_min = (LSM303::vector<int16_t>){ -636,   -636,   -636};
 
  compass.m_max = (LSM303::vector<int16_t>){ +481,   +481,   +481};*/

   
}

void loop() {
  compass.read();
  
  /*
  When given no arguments, the heading() function returns the angular
  difference in the horizontal plane between a default vector and
  north, in degrees.
  
  The default vector is chosen by the library to point along the
  surface of the PCB, in the direction of the top of the text on the
  silkscreen. This is the +X axis on the Pololu LSM303D carrier and
  the -Y axis on the Pololu LSM303DLHC, LSM303DLM, and LSM303DLH
  carriers.
  
  To use a different vector as a reference, use the version of heading()
  that takes a vector argument; for example, use
  
    compass.heading((LSM303::vector<int>){0, 0, 1});
  
  to use the +Z axis as a reference.
  */
  float heading = compass.heading();
/*for(int i=0;i<10;i++){
  
  
  pusula+=heading;
  
  
  }
  pusula=pusula/10;*/
  
  Serial.println(heading);
  delay(100);
}
