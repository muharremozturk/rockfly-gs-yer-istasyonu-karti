
float heading;
void setup() {
Serial.begin(115200);

}
// 40.081779934484, 29.518828765817048
void loop() {
float flat1=40.081414;     // flat1 = our current latitude. flat is from the gps data. 
float flon1=29.520024;  // flon1 = our current longitude. flon is from the fps data.
float dist_calc=0;
float dist_calc2=0;
float diflat=0;
float diflon=0;
float x2lat=40.081779 ;  //enter a latitude point here   this is going to be your waypoint
float x2lon=29.518828 ;
//---------------------------------- distance formula below. Calculates distance from current location to waypoint
diflat=radians(x2lat-flat1);  //notice it must be done in radians
flat1=radians(flat1);    //convert current latitude to radians
x2lat=radians(x2lat);  //convert waypoint latitude to radians
diflon=radians((x2lon)-(flon1));   //subtract and convert longitudes to radians
dist_calc = (sin(diflat/2.0)*sin(diflat/2.0));
dist_calc2= cos(flat1);
dist_calc2*=cos(x2lat);
dist_calc2*=sin(diflon/2.0);                                       
dist_calc2*=sin(diflon/2.0);
dist_calc +=dist_calc2;
dist_calc=(2*atan2(sqrt(dist_calc),sqrt(1.0-dist_calc)));
dist_calc*=6371000.0; //Converting to meters
Serial.println("distance");
Serial.println(dist_calc);    //print the distance in meters

flon1 = radians(flon1);  //also must be done in radians

x2lon = radians(x2lon);  //radians duh.

heading = atan2(sin(x2lon-flon1)*cos(x2lat),cos(flat1)*sin(x2lat)-sin(flat1)*cos(x2lat)*cos(x2lon-flon1)),2*3.1415926535;
heading = heading*180/3.1415926535;  // convert from radians to degrees

int head =heading; //make it a integer now

if(head<0){

  heading+=360;   //if the heading is negative then add 360 to make it positive

}

Serial.println("heading:");

Serial.println(heading);

 

}
