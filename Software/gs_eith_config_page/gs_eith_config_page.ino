#include "panda.h"
#include <TJpg_Decoder.h>
#include <Wire.h>
#include <LSM303.h>
#include <Adafruit_BMP280.h>
Adafruit_BMP280 bmp;
LSM303 compass;
#include "LITTLEFS.h" 
//#include <SPI.h>
#include "FS.h"
#include "Free_Fonts.h"
#include <TFT_eSPI.h>
TFT_eSPI tft = TFT_eSPI();





int screen_id,k,i,main,kts,t,v1,d,v2,u;
int apogee=500;
uint32_t a,b;
char *unit[] ={"m","i","M","m/s","km/h","C","ft","ft/s","mph","F"}; 
float my_temp,my_alt,rckt_temp,rckt_alt,rckt_v1,rckt_v2,rckt_dist;

TaskHandle_t Task1;
//TaskHandle_t Task2;
float heading;
float headingg;

#include <TinyGPS++.h>
#include <HardwareSerial.h>
    long lat,lon; 
    int counterr = 0;     
    
    TinyGPSPlus gps;
 HardwareSerial SerialGPS(1);  
 
#define lft_blck_x    0
#define lft_blck_y    22

#define rght_blck_x    164
#define rght_blck_y    22

#define box_y    15
#define box_x    65


#define BLACK       0x0000      /*   0,   0,   0 */
#define NAVY        0x000F      /*   0,   0, 128 */
#define DARKGREEN   0x03E0      /*   0, 128,   0 */
#define DARKCYAN    0x03EF      /*   0, 128, 128 */
#define MAROON      0x7800      /* 128,   0,   0 */
#define PURPLE      0x780F      /* 128,   0, 128 */
#define OLIVE       0x7BE0      /* 128, 128,   0 */
#define LIGHTGREY   0xD69A      /* 211, 211, 211 */
#define DARKGREY    0x7BEF      /* 128, 128, 128 */
#define BLUE        0x001F      /*   0,   0, 255 */
#define GREEN       0x07E0      /*   0, 255,   0 */
#define CYAN        0x07FF      /*   0, 255, 255 */
#define RED         0xF800      /* 255,   0,   0 */
#define MAGENTA     0xF81F      /* 255,   0, 255 */
#define YELLOW      0xFFE0      /* 255, 255,   0 */
#define WHITE       0xFFFF      /* 255, 255, 255 */
#define ORANGE      0xFDA0      /* 255, 180,   0 */
#define GREENYELLOW 0xB7E0      /* 180, 255,   0 */
#define PINK        0xFE19      /* 255, 192, 203 */    
#define BROWN       0x9A60      /* 150,  75,   0 */
#define GOLD        0xFEA0      /* 255, 215,   0 */
#define SILVER      0xC618      /* 192, 192, 192 */
#define SKYBLUE     0x867D      /* 135, 206, 235 */
#define VIOLET      0x915C      /* 180,  46, 226 */
#define GRAY        0xC618

#define CALIBRATION_FILE "/TouchCalData2"

// Set REPEAT_CAL to true instead of false to run calibration
// again, otherwise it will only be done once.
// Repeat calibration if you change the screen rotation.
#define REPEAT_CAL false
TFT_eSPI_Button on_btn,off_btn,minus_btn,plus_btn,snd_btn,save_btn,one_btn,ten_btn,hundred_btn,
                fivehundred_btn,imprl_btn,mtrc_btn,point_one_btn,point_t_btn,point_five_btn,
                one_thousand_btn,erase_btn,ok_btn;

void listDir(fs::FS &fs, const char * dirname, uint8_t levels);
void deleteFile(fs::FS &fs, const char * path);
void renameFile(fs::FS &fs, const char * path1, const char * path2);
void appendFile(fs::FS &fs, const char * path, const char * message);
void writeFile(fs::FS &fs, const char * path, const char * message);
void readFile(fs::FS &fs, const char * path);
void removeDir(fs::FS &fs, const char * path);
void createDir(fs::FS &fs, const char * path);



float ByteToFloat(byte *byterray,int ca)
{
  float f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

  long ByteTolong(byte *byterray,int ca)
{
  long f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

 int ByteToint(byte *byterray,int ca)
{
  int f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

    uint8_t ByteTouint8t(byte *byterray,int ca)
{
  uint8_t f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

  void floatAddByte(float data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void intAddByte(int data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}

void uint8tAddByte(uint8_t data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}

float H3LIS331_data[3];
float Ms5611_data[2];
float bnoAccel[3];
float bnoGyro[3];
float bnoMag[3];
float bnoRaw[3];
long GPS_data[3];


byte SIV;
long unix_time;
float battery;
uint32_t tt=0;

float aa;
long l;
int ii,c,m;
bool kk,ok;
char veri[] = "";

float frq=868.00;
int frq_ktss;
int dbm=20;
float frq_kts=0.1;

float carrierFreq;
uint8_t outputPower;
byte LoRaSet=0x00;

int rocketapoge;
int rocketmain;

byte passw = 0x60;
byte sensData = 0x20;
byte configData = 0x30;


#include <RadioLib.h>
#include "SPI.h"
SPIClass abc(HSPI);
SX1262 radio = new Module(15, 17, 27, 39,abc);


int transmissionState = ERR_NONE;
struct Data{
byte byteArray[100];
byte tr_array[16];
int counter=0;
};
Data data;



char strr[210];
String All_data1;
int All_dataa;
int stt=0;


#define INITIATING_NODE = true;
bool transmitFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// flag to indicate that a packet was sent or received
volatile bool operationDone = false;

// this function is called when a complete packet
// is transmitted or received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if(!enableInterrupt) {
    return;
  }

  // we sent aor received  packet, set the flag
  operationDone = true;
}



void setup() {
  
  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;  
  const int HSPI_CS = 15;

  abc.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS); 
  
  delay(500);
  
 Serial.begin(9600); 
 screen_id=0;
k=1;
i=0;
kts=1;
 SerialGPS.begin(9600, SERIAL_8N1, 16, 34);
 Wire.begin();
   tft.init();
   tft.setRotation(1); 
   TJpgDec.setJpgScale(1);
  // The byte order can be swapped (set true for TFT_eSPI)
  TJpgDec.setSwapBytes(true);
  // The decoder must be given the exact name of the rendering function above
  TJpgDec.setCallback(tft_output);
  compass.init();
  compass.enableDefault();
   compass.m_min = (LSM303::vector<int16_t>){   -620,   -854,   -583};
   compass.m_max = (LSM303::vector<int16_t>){ +466,   +266,   +519};
pinMode( 26, INPUT);
 if (!bmp.begin()) {
    //Serial.println(F("Could not find a valid BMP280 sensor, check wiring or "
//                      "try a different address!"));
    while (1) delay(10);
  }

  /* Default settings from datasheet. */
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
   
  xTaskCreatePinnedToCore(
                    Task1code,   /* Task function. */
                    "Task1",     /* name of task. */
                    40000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task1,      /* Task handle to keep track of created task */
                    0);          /* pin task to core 0 */                  
  delay(100); 

 if (!SD.begin()) {
   // Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();

  if (cardType == CARD_NONE) {
//Serial.println("No SD card attached");
    return;
  }

 // Serial.print("SD Card Type: ");
  if (cardType == CARD_MMC) {
 //   Serial.println("MMC");
  } else if (cardType == CARD_SD) {
  //  Serial.println("SDSC");
  } else if (cardType == CARD_SDHC) {
   // Serial.println("SDHC");
  } else {
   // Serial.println("UNKNOWN");
  }

  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
 //// Serial.printf("SD Card Size: %lluMB\n", cardSize);
 tft.fillScreen(BLACK);
 touch_calibrate();
 tft.setTextSize(1);

int state = radio.begin(frq, 500.0, 7, 5, 0x34,dbm);
  if (state == ERR_NONE) {
  //  Serial.println(F("success!"));
  } else {
   // Serial.print(F("failed, code "));
   // Serial.println(state);
    while (true);
  }

  // set the function that will be called
  // when new packet is received
 radio.setDio1Action(setFlag);
  #if defined(INITIATING_NODE)
    // send the first packet on this node
    Serial.print(F("[SX1262] Sending first packet ... "));
    transmissionState = radio.startTransmit(data.tr_array,16);
    transmitFlag = true;
  #else
    // start listening for LoRa packets on this node
    Serial.print(F("[SX1262] Starting to listen ... "));
    state = radio.startReceive();
    if (state == ERR_NONE) {
      Serial.println(F("success!"));
    } else {
      Serial.print(F("failed, code "));
      Serial.println(state);
      while (true);
    }
  #endif

  // if needed, 'listen' mode can be disabled by calling
  // any of the following methods:
  //
  // radio.standby()
  // radio.sleep()
  // radio.transmit();
  // radio.receive();
  // radio.readData();
  // radio.scanChannel();

}


// flag to indicate transmission or reception state


void Task1code( void * pvParameters ){
 // Serial.print("Task1 running on core ");
 // Serial.println(xPortGetCoreID());

  for(;;){
    
    vTaskDelay(3);
      
    if(operationDone) {
    // disable the interrupt service routine while
    // processing the data
    enableInterrupt = false;

    // reset flag
    operationDone = false;

      if(transmitFlag) {


      // the previous operation was transmission, listen for response
      // print the result
      if (transmissionState == ERR_NONE) {
        // packet was successfully sent
        Serial.println(F("transmission finished!"));
  
      } else {
        Serial.print(F("failed, code "));
        Serial.println(transmissionState);
  
      }

      // listen for response
      radio.startReceive();
      transmitFlag = false;
      Serial.println("bekliyor");
      
    }

    else{
    Serial.println("alım başlayacak");
    int state = radio.readData(data.byteArray,100);
    
    

    if (state == ERR_NONE) {

      if(data.byteArray[0] == passw && data.byteArray[1] == sensData){
      
      // packet was successfully received
    //  Serial.println(F("[SX1262] Received packet!"));
     
c=2;
for( ii=0;ii<3;ii++){ 
H3LIS331_data[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<2;ii++){ 
Ms5611_data[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}

for( ii=0;ii<3;ii++){ 
bnoAccel[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
bnoGyro[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
bnoMag[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
bnoRaw[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}

for( ii=0;ii<3;ii++){ 
GPS_data[ii]= ByteTolong(data.byteArray,c);
c+=4;
}

SIV=data.byteArray[c];
c+=1;

unix_time= ByteTolong(data.byteArray,c);


c=0;

All_data1=String(H3LIS331_data[0])+","+String(H3LIS331_data[1])+","+String(H3LIS331_data[2])+",";
    All_data1+=String(Ms5611_data[0])+","+String(Ms5611_data[1])+",";
    All_data1+=String(bnoAccel[0])+","+String(bnoAccel[1])+","+String(bnoAccel[2])+",";
    All_data1+=String(bnoGyro[0])+","+String(bnoGyro[1])+","+String(bnoGyro[2])+",";
    All_data1+=String(bnoMag[0])+","+String(bnoMag[1])+","+String(bnoMag[2])+",";
    All_data1+=String(bnoRaw[0])+","+String(bnoRaw[1])+","+String(bnoRaw[2])+",";
    All_data1+=String(GPS_data[0])+","+String(GPS_data[1])+","+String(GPS_data[2])+",";
    All_data1+=String(SIV)+","+String(unix_time)+"\n";

  //   uint32_t t = millis(); 
       
 All_dataa = All_data1.length() + 3; 
 char chrr[All_dataa]; 
 All_data1.toCharArray(chrr, All_dataa);
 appendFile(SD, "/yunus.csv", chrr);
 All_data1="";
 
    }

    if(data.byteArray[0] == passw && data.byteArray[1] == sensData){
      
      c=2;
      rocketapoge = ByteToint(data.byteArray,c);
      c+=4;
      rocketmain = ByteToint(data.byteArray,c);
      c+=4;
      carrierFreq = ByteToFloat(data.byteArray,c);
      c+=4;
      outputPower = ByteTouint8t(data.byteArray,c);
      c=0;
      kk=1;
      }
    
   }
    
}

    
    

    // we're ready to receive more packets,
    // enable interrupt service routine
    radio.startReceive();
    enableInterrupt = true;
  
  
    }

      // burada send butonuna basılırsa değişken aktif olup gönderme işlemi yapıyor.
    if(stt==1){
      kk=0;
      enableInterrupt = false;
      Serial.print(F("[SX1262] yunusyunusyunsu "));
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(configData, &data.counter, data.tr_array);
      floatAddByte(apogee,&data.counter,data.tr_array);
      floatAddByte(main,&data.counter,data.tr_array);
      floatAddByte(frq,&data.counter,data.tr_array);
      uint8tAddByte(dbm,&data.counter,data.tr_array);
      byteAddByte(LoRaSet, &data.counter, data.tr_array);
      transmissionState = radio.startTransmit(data.tr_array,16);
      transmitFlag = true;
      stt=0;
      enableInterrupt = true;
}

if(kk==1 && ok==1){
      LoRaSet=0x01;
      enableInterrupt = false;
      Serial.print(F("[SX1262] yunusyunusyunsu "));
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(configData, &data.counter, data.tr_array);
      intAddByte(apogee,&data.counter,data.tr_array);
      intAddByte(main,&data.counter,data.tr_array);
      floatAddByte(frq,&data.counter,data.tr_array);
      uint8tAddByte(dbm,&data.counter,data.tr_array);
      byteAddByte(LoRaSet, &data.counter, data.tr_array);
      transmissionState = radio.startTransmit(data.tr_array,16);
      frq=carrierFreq;
      dbm=outputPower;
      radio.setFrequency(frq);
      radio.setOutputPower(dbm);
      transmitFlag = true;
      kk=0;
      ok=0;
      LoRaSet=0x00;
      enableInterrupt = true;
  
  
  }
  

/*if(stt==1){
            enableInterrupt = false;
            floatAddByte(frq,&data.counter,data.tr_array);
            byteAddByte(dbm,&data.counter,data.tr_array);
            intAddByte(apogee,&data.counter,data.tr_array);
            intAddByte(main,&data.counter,data.tr_array);
       
            transmissionState = radio.startTransmit(data.tr_array,5);
            Serial.println(F("transmission finished!"));
            stt=0;
            enableInterrupt = true;
  }*/
 
  }
   
}




   void loop() {


      if(screen_id==4  ){
        tft.setFreeFont(FF33);
        tft.setTextSize(1);
        tft.fillRoundRect(lft_blck_x+134, lft_blck_y+134, box_x+117, box_y+3,3, WHITE); 
        tft.setCursor(lft_blck_x+140, lft_blck_y+147);
        tft.print(bmp.readTemperature());
      
        }
 
     compass.read();
     float heading = compass.heading();
     //   Serial.print(F("Approx altitude = "));
  //  Serial.print(bmp.readAltitude(1017)); /* Adjusted to local forecast! */

    
my_temp= 36;
my_alt = 940;

rckt_temp= 5;
rckt_alt = 3500;
rckt_v1= 140;
rckt_v2= 504;
rckt_dist=2000;

if(u==1){

my_temp= ((my_temp)*1.8)+32;
my_alt = ((my_alt)*3.2808);

rckt_temp= ((rckt_temp)*1.8)+32;
rckt_alt = ((rckt_alt)*3.2808);
rckt_v1= ((rckt_v1)*3.2808);
rckt_v2= ((rckt_v2)/1.609);
rckt_dist=((rckt_dist)*3.2808);

}




  if((screen_id==2  && k==0)){
  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.fillRoundRect(box_x+12, lft_blck_y+4, box_x+10, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+85, lft_blck_y+8); 
  tft.print(Ms5611_data[0]);
  tft.print(" ");
  tft.print(unit[t]);
  
  tft.fillRoundRect(box_x+11, lft_blck_y+25, box_x+10, box_y,3, WHITE);          //ikinci kutucuk
  tft.setCursor(lft_blck_x+82, lft_blck_y+29);
  tft.print(Ms5611_data[1]);
  tft.print(" ");
  tft.print(unit[d]);

  tft.fillRoundRect(box_x+11, lft_blck_y+50, box_x+10, box_y,3, WHITE);          //ikinci kutucuk
  tft.setCursor(lft_blck_x+82, lft_blck_y+54);
  tft.print(bnoGyro[0]);

  tft.fillRoundRect(box_x+11, lft_blck_y+69, box_x+10, box_y,3, WHITE);          //ikinci kutucuk
  tft.setCursor(lft_blck_x+82, lft_blck_y+73);
  tft.print(bnoGyro[1]);


  tft.fillRoundRect(box_x+11, lft_blck_y+88, box_x+10, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+92);
  tft.print(bnoGyro[2]);
  
  tft.fillRoundRect(box_x+11, lft_blck_y+118, box_x+10, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+122);
  tft.print(GPS_data[0]);

  tft.fillRoundRect(box_x+11, lft_blck_y+137, box_x+10, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+141);
  tft.print(GPS_data[1]);

  tft.fillRoundRect(box_x+26, lft_blck_y+156, box_x-5, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+94, lft_blck_y+160);
  tft.print(SIV);

  tft.fillRoundRect(rght_blck_x+76, rght_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk
  tft.setCursor(rght_blck_x+100, rght_blck_y+122);
  tft.print(bnoAccel[0]);

  tft.fillRoundRect(rght_blck_x+76, rght_blck_y+136, box_x+10, box_y,3, WHITE);          //ikinci kutucuk
  tft.setCursor(rght_blck_x+100, rght_blck_y+140);
  tft.print(bnoAccel[1]);

  tft.fillRoundRect(rght_blck_x+76, rght_blck_y+155, box_x+10, box_y,3, WHITE); 
  tft.setCursor(rght_blck_x+100, rght_blck_y+159);
  tft.print(bnoAccel[2]);

  tft.fillRoundRect(rght_blck_x+80, rght_blck_y+5, box_x+6, box_y,3, WHITE);
  tft.setCursor(rght_blck_x+83, rght_blck_y+8);
  tft.print(H3LIS331_data[0]);

  tft.fillRoundRect(rght_blck_x+80, rght_blck_y+24, box_x+6, box_y,3, WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+29);
  tft.print(H3LIS331_data[1]);

  tft.fillRoundRect(rght_blck_x+80, rght_blck_y+49, box_x+6, box_y,3, WHITE); 
  tft.setCursor(rght_blck_x+82, rght_blck_y+54);
  tft.print(H3LIS331_data[2]);

  tft.fillRoundRect(rght_blck_x+80, rght_blck_y+92, box_x+6, box_y,3, WHITE);          //ikinci kutucuk
  tft.setCursor(rght_blck_x+100, rght_blck_y+96);
  tft.print(unix_time);
  
 }


  if((screen_id==3  && k==0)){



  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);

tft.fillRoundRect(rght_blck_x+80, rght_blck_y+25, box_x+6, box_y,3, WHITE); 
tft.setCursor(rght_blck_x+86, rght_blck_y+29);
tft.print(bnoMag[0]);

tft.fillRoundRect(rght_blck_x+80, rght_blck_y+49, box_x+6, box_y,3, WHITE);
tft.setCursor(rght_blck_x+86, rght_blck_y+53);
tft.print(bnoMag[1]);

tft.fillRoundRect(rght_blck_x+80, rght_blck_y+67, box_x+6, box_y,3, WHITE);
tft.setCursor(rght_blck_x+86, rght_blck_y+71);
tft.print(bnoMag[2]);

tft.fillRoundRect(rght_blck_x+76, rght_blck_y+91, box_x+10, box_y,3, WHITE);
tft.setCursor(rght_blck_x+86, rght_blck_y+96);
tft.print(bnoRaw[0]);

tft.fillRoundRect(box_x+11, lft_blck_y+114, box_x+10, box_y,3, WHITE);
tft.setCursor(rght_blck_x+82, rght_blck_y+118);
tft.print(bnoRaw[1]);

tft.fillRoundRect(box_x+11, lft_blck_y+134, box_x+10, box_y,3, WHITE);  
tft.setCursor(rght_blck_x+82, rght_blck_y+138);
tft.print(bnoRaw[2]);


  }

  if((screen_id==4 && k==0) ){
 
       while(SerialGPS.available() > 0){ // check for gps data
       if(gps.encode(SerialGPS.read())){ // encode gps data
       if(counterr > 2) {
        Serial.print("SATS: ");
        Serial.println(gps.satellites.value());
        Serial.print("LAT: ");
        Serial.println(gps.location.lat(), 6);
        Serial.print("LONG: ");
        Serial.println(gps.location.lng(), 6);
        Serial.print("ALT: ");
        Serial.println(gps.altitude.meters()); 
        
        Serial.print("Date: ");
        Serial.print(gps.date.day()); Serial.print("/");
        Serial.print(gps.date.month()); Serial.print("/");
        Serial.println(gps.date.year());
    
        Serial.print("Hour: ");
        Serial.print(gps.time.hour()); Serial.print(":");
        Serial.print(gps.time.minute()); Serial.print(":");
        Serial.println(gps.time.second());
      tft.fillRoundRect(lft_blck_x+134, lft_blck_y+6, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk
     
     //tft.setFreeFont(FF33);
     tft.setTextColor(  BLACK,WHITE);
     tft.setFreeFont(FM9);
     tft.setCursor(lft_blck_x+140, lft_blck_y+19);
     tft.print(gps.location.lat(), 6);
     tft.fillRoundRect(lft_blck_x+134, lft_blck_y+27, box_x+117, box_y+3,3, WHITE);
     tft.setCursor(lft_blck_x+140, lft_blck_y+40);
     tft.print(gps.location.lng(), 6);
   
       Serial.println("---------------------------");
        counterr = 0;
       }
       else counterr++;
       }
      }
      
 }
 if(screen_id==4 && k==0){
   tft.setFreeFont(FM9);
   tft.setTextSize(1);
   tft.setTextColor( BLACK,WHITE);
   tft.fillRoundRect(lft_blck_x+134, lft_blck_y+114, box_x+117, box_y+3,3, WHITE);
   tft.setCursor(lft_blck_x+140, lft_blck_y+127);
   tft.print(heading,1);
  }
 
  /*  Serial.print(" ekr=");
    Serial.println(screen_id);
    Serial.print(" k=");
    Serial.print(k);
    Serial.print(" ");
    Serial.print(" i=");
    Serial.print(i);
    Serial.print(" frq kts");
    Serial.print(frq_kts);*/
    tft.setFreeFont(FF33);
    uint16_t t_x = 0, t_y = 0; // To store the touch coordinates
    // Pressed will be set true is there is a valid touch on the screen
    boolean pressed = tft.getTouch(&t_x, &t_y); 
  
    on_btn.press(pressed && on_btn.contains(t_x, t_y));
    off_btn.press(pressed && off_btn.contains(t_x, t_y));
    
    minus_btn.press(pressed && minus_btn.contains(t_x, t_y));
    plus_btn.press(pressed && plus_btn.contains(t_x, t_y));
    snd_btn.press(pressed && snd_btn.contains(t_x, t_y));
    erase_btn.press(pressed && erase_btn.contains(t_x, t_y));
    ok_btn.press(pressed && ok_btn.contains(t_x, t_y));
    
    one_btn.press(pressed && one_btn.contains(t_x, t_y));
    ten_btn.press(pressed && ten_btn.contains(t_x, t_y));
    hundred_btn.press(pressed && hundred_btn.contains(t_x, t_y));
    fivehundred_btn.press(pressed && fivehundred_btn.contains(t_x, t_y));

    point_one_btn.press(pressed && point_one_btn.contains(t_x, t_y));
    point_t_btn.press(pressed && point_t_btn.contains(t_x, t_y));
    point_five_btn.press(pressed && point_five_btn.contains(t_x, t_y));
    one_thousand_btn.press(pressed && one_thousand_btn.contains(t_x, t_y));
    
    imprl_btn.press(pressed && imprl_btn.contains(t_x, t_y));
    mtrc_btn.press(pressed && mtrc_btn.contains(t_x, t_y));





if(!(i==2 || i==3 || i==4 || i==5 || i==7)){
    if (off_btn.justPressed()) {
        off_btn.drawButton(true);     
        screen_id++;
        k=1;
    }
     if (on_btn.justPressed() ) {
        on_btn.drawButton(true);
           
         screen_id--;
         k=1; 
         }

        if (off_btn.justReleased())
            off_btn.drawButton();

        if (on_btn.justReleased())
            on_btn.drawButton();
}


if(i==5){ 
  
                 if (plus_btn.justReleased()) plus_btn.drawButton();
                 if (minus_btn.justReleased()) minus_btn.drawButton();  

                 if (plus_btn.justPressed() ){
                     plus_btn.drawButton(true);
                     dbm = dbm+1;
                     if(dbm >= 22)dbm=22;
                     tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                     tft.setTextColor( BLACK,WHITE);
                     tft.setCursor(122, 106);
                     tft.print(dbm);
                     tft.print(" dbm");

                 }        
                    

               if  (minus_btn.justPressed()){
                    minus_btn.drawButton(true);
                    dbm = dbm-1;
                    if(dbm <= 0)dbm=0;
                    tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                    tft.setTextColor( BLACK,WHITE);
                    tft.setCursor(122, 106);
                    tft.print(dbm);
                    tft.print(" dbm");
                } 

                 if (on_btn.justPressed() ) {
                     on_btn.drawButton(true);      
                     rocketconfig_page();
                    } 
                 }

    if(i==4){ 
                 if (plus_btn.justReleased()) plus_btn.drawButton();
                 if (minus_btn.justReleased()) minus_btn.drawButton();  
     
                 if(frq_ktss==1) point_one_btn.drawButton(true);
                     else point_one_btn.drawButton();
                 if(frq_ktss==2) point_t_btn.drawButton(true);
                     else point_t_btn.drawButton();
                 if(frq_ktss==3) point_five_btn.drawButton(true);
                     else  point_five_btn.drawButton();
                 if(frq_ktss==4) one_thousand_btn.drawButton(true);
                     else one_thousand_btn.drawButton();

                 if (point_one_btn.justPressed() ) {
                     point_one_btn.drawButton(true);     
                     frq_kts=0.10;
                     frq_ktss=1;
                    }    
                 if (point_t_btn.justPressed() ) {
                     point_t_btn.drawButton(true);      
                     frq_kts=0.20;
                     frq_ktss=2;
                    } 
                 if (point_five_btn.justPressed() ) {
                     point_five_btn.drawButton(true);      
                     frq_kts=0.50;
                     frq_ktss=3;
                    } 
                 if (one_thousand_btn.justPressed() ) {
                     one_thousand_btn.drawButton(true);      
                     frq_kts=1;
                     frq_ktss=4;
                    }                                  

                 if (plus_btn.justPressed() ){
                     plus_btn.drawButton(true);
                     frq = frq+frq_kts;
                     tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                     tft.setTextColor( BLACK,WHITE);
                     tft.setCursor(118, 106);
                     tft.print(frq);
                     tft.print(" MHz");

                 }

               if  (minus_btn.justPressed()){
                    minus_btn.drawButton(true);
                    frq = frq-frq_kts;
                    if(frq <= 860.00)frq=860.00;
                    tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                    tft.setTextColor( BLACK,WHITE);
                    tft.setCursor(118, 106);
                    tft.print(frq);
                    tft.print(" MHz");
                } 
           
                 if (on_btn.justPressed() ) {
                     on_btn.drawButton(true);      
                     rocketconfig_page();
                    } 
                 }

     if(i==3){

                 if (plus_btn.justReleased()) plus_btn.drawButton();
                 if (minus_btn.justReleased()) minus_btn.drawButton();  

                 if(kts==1) one_btn.drawButton(true);
                     else one_btn.drawButton();
                 if(kts==10) ten_btn.drawButton(true);
                     else ten_btn.drawButton();
                 if(kts==100) hundred_btn.drawButton(true);
                     else  hundred_btn.drawButton();
                 if(kts==500) fivehundred_btn.drawButton(true);
                     else fivehundred_btn.drawButton();

                 if (on_btn.justPressed() ) {
                     on_btn.drawButton(true);      
                     rocketconfig_page();
                    }

                 if (one_btn.justPressed() ) {
                     one_btn.drawButton(true);     
                     kts=1;
                    }    
                 if (ten_btn.justPressed() ) {
                     ten_btn.drawButton(true);      
                     kts=10;
                    } 
                 if (hundred_btn.justPressed() ) {
                     hundred_btn.drawButton(true);      
                     kts=100;
                    } 
                 if (fivehundred_btn.justPressed() ) {
                     fivehundred_btn.drawButton(true);      
                     kts=500;
                    }                                  
 
                 if (plus_btn.justPressed() ){
                     plus_btn.drawButton(true);
                     main = main+kts;
                     tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                     tft.setTextColor( BLACK,WHITE);
                     tft.setCursor(122, 106);
                     tft.print(main);
                     tft.print(" ");
                     tft.print(unit[d]);

                 }

               if (minus_btn.justPressed() ){
                    minus_btn.drawButton(true);
                    main = main-kts;
                    if(main <= 0)main=0;
                    tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                    tft.setTextColor( BLACK,WHITE);
                    tft.setCursor(122, 106);
                    tft.print(main);
                    tft.print(" ");
                    tft.print(unit[d]);
                    
                } 

              }

if(i==2){

                      if (plus_btn.justReleased()) plus_btn.drawButton();
                      if (minus_btn.justReleased()) minus_btn.drawButton();  

                      if(kts==1) one_btn.drawButton(true);
                        else one_btn.drawButton();
                     if(kts==10) ten_btn.drawButton(true);
                        else ten_btn.drawButton();
                     if(kts==100) hundred_btn.drawButton(true);
                        else  hundred_btn.drawButton();
                     if(kts==500) fivehundred_btn.drawButton(true);
                        else fivehundred_btn.drawButton();

                       if (on_btn.justPressed() ) {
                           on_btn.drawButton(true);      
                           rocketconfig_page();
                        }

                          if (one_btn.justPressed() ) {
                          one_btn.drawButton(true);     
                          kts=1;
                        }    
                      if (ten_btn.justPressed() ) {
                          ten_btn.drawButton(true);      
                          kts=10;
                        } 
                          if (hundred_btn.justPressed() ) {
                          hundred_btn.drawButton(true);      
                          kts=100;
                        } 
                      if (fivehundred_btn.justPressed() ) {
                          fivehundred_btn.drawButton(true);      
                          kts=500;
                        }                                  

                      if (plus_btn.justPressed() ){
                      plus_btn.drawButton(true);
                      apogee = apogee+kts;
                      tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                      tft.setTextColor( BLACK,WHITE);
                      tft.setCursor(122, 106);
                      tft.print(apogee);
                      tft.print(" ");
                      tft.print(unit[d]);

                    }

                 if (minus_btn.justPressed() ){
                 minus_btn.drawButton(true);
                 apogee = apogee-kts;
                 if(apogee <= 0)apogee=0;
                      tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                      tft.setTextColor( BLACK,WHITE);
                      tft.setCursor(122, 106);
                      tft.print(apogee);
                      tft.print(" ");
                      tft.print(unit[d]);
  
                   }
            }

    if(i==1){


      
              if((t_x > 120 && t_x < 215) && (t_y > 119 && t_y < 144 )){
        
                 i=4;
                 tft.fillRoundRect(120, 119, 95, 25,3, DARKGREY);
                 a= DARKGREY;
                 tft.setTextColor( BLACK,a);
                 tft.setCursor(124, 135);
                 tft.print(frq);
                 tft.print(" MHz");
                 delay(250);
                 frqconfig_page(frq);

                }

                if((t_x > 120 && t_x < 215) && (t_y > 151 && t_y < 176 )){
        
                 i=5;
                 tft.fillRoundRect(120, 151, 95, 25,3, DARKGREY);
                 a= DARKGREY;
                 tft.setTextColor( BLACK,a);
                 tft.setCursor(128, 167);
                 tft.print(dbm);
                 tft.print(" dbm");
                 delay(250);
                dbmconfig_page(dbm);

                }


              if((t_x > 120 && t_x < 215) && (t_y > 87 && t_y < 112 ) ){

                   tft.setTextColor( BLACK,a);
                   tft.fillRoundRect(120, 87, 95, 25,3, DARKGREY);
                   tft.setCursor(140, 104);
                   tft.print("ACTIVE");
                   delay(250);
                   tft.fillRoundRect(120, 87, 95, 25,3, WHITE);
                   tft.setCursor(140, 104);
                   tft.print("ACTIVE");
              }
              
               if(snd_btn.justReleased()) snd_btn.drawButton();
               if(snd_btn.justPressed()){
                  snd_btn.drawButton(true);
                  tft.fillRoundRect(70, 50, 150, 100,3, YELLOW);
                  tft.setTextColor( RED,BLACK);
                  tft.setCursor(105, 80);
                  tft.print("WAITING");
                  stt=1;
                  i=7;
                  delay(10);

               }



                 if(erase_btn.justReleased()) erase_btn.drawButton();
                   if(erase_btn.justPressed()){
                    erase_btn.drawButton(true);
                    tft.setTextColor( WHITE,BLACK);
                    tft.setCursor(235, 120);
                    tft.print("ERASED");

               }

              if((t_x > 120 && t_x < 215) && (t_y > 23 && t_y < 48 ) )
             { 
                 i=2;
                 tft.fillRoundRect(120, 23, 95, 25,3, DARKGREY);
                 a= DARKGREY;
                 
                 tft.setTextColor( BLACK,a);
                 tft.setCursor(125, 40);
                 tft.print(apogee);
                 tft.print(" ");
                 tft.print(unit[d]);
                 delay(250);
                 apogee_config(apogee);

              }
           if((t_x > 120 && t_x < 215) && (t_y > 55 && t_y < 80 ) )
             { 
                 i=3;
                 tft.fillRoundRect(120, 55, 95, 25,3, DARKGREY);
                 a= DARKGREY;
                 tft.setTextColor( BLACK,a);
                 tft.setCursor(125, 72);
                 tft.print(main);
                 tft.print(" ");
                 tft.print(unit[d]);
                 delay(250);
                 apogee_config(main);

              }

     }  
       if(i==7){

        if(kk==1&& ok==0){
               ok_btn.initButton(&tft,  95, 180, 60, 25, WHITE, GREEN, BLACK, "OK",1);
               if(ok_btn.justReleased()) ok_btn.drawButton();
               if(ok_btn.justPressed()){
                  snd_btn.drawButton(true);
                  tft.fillRoundRect(70, 50, 150, 100,3, RED);
                  tft.setTextColor( WHITE,BLACK);
                  tft.setCursor(75, 100);
                  tft.print("UPGRATED");
                  ok=1;
                  delay(1000);
                  rocketconfig_page();

               }
       }
       }

         if(screen_id==0){
         
            if(imprl_btn.justReleased()) imprl_btn.drawButton();
            if(mtrc_btn.justReleased()) mtrc_btn.drawButton();

             if (imprl_btn.justPressed() ){
                imprl_btn.drawButton(true);
                 u=1;
                 d=6;
                 v1=7;
                 v2=8;
                 t=9;
               }
              if (mtrc_btn.justPressed() ){
                  mtrc_btn.drawButton(true);

                u=0;
                d=2;
                v1=3;
                v2=4;
                t=5;
                }
          
                tft.setFreeFont();
                tft.setCursor(126, 1);
                tft.setTextColor(WHITE, BLACK);
                tft.print(unit[u]);



            }
    
    if(screen_id==0 && k==1)myconfig_page();
    if(screen_id==1 && k==1)rocketconfig_page();
    if(screen_id==2 && k==1)rocketpage1();
    if(screen_id==3 && k==1)rocketpage2();
    if(screen_id==4 && k==1)grndstn();
    if(screen_id==5 && k==1)flightsttcs_page();
    if(screen_id==6 && k==1)baboon();
    if(screen_id==7 && k==1)mainpage();
    
    
    
    
    
    
    
    
    

    if(screen_id==-1)screen_id=7;
    if(screen_id==8)screen_id=0;
    
}

void flightsttcs_page()
{
k=0;
i=6;

 tft.fillScreen(BLACK);
 tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
   tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  tft.setCursor(150, 1);
  tft.print("12:30");
  tft.setCursor(90, 1);
  tft.print("(");
  tft.print("unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");
  
  tft.fillRoundRect(75, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(95, 227);
  tft.print("FLIGT STATICS");
  
 tft.fillRoundRect(lft_blck_x, lft_blck_y, 156, 176,3, CYAN);               //sol blok


tft.fillRoundRect(lft_blck_x+2, lft_blck_y+2, 152, 19,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+4, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+4, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+25, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+25, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+46, 152, 62,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+50, box_x+4, box_y*3+8,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+50, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
//tft.fillRoundRect(lft_blck_x+5, lft_blck_y+69, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+69, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+89, 152, 19,2, BLACK);           //5. satır kutu altlığı
//tft.fillRoundRect(lft_blck_x+5, lft_blck_y+88, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+88, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+114, 152, 60,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+118, box_x*2+16, box_y,3, WHITE);      //ilk kutucuk
//tft.fillRoundRect(box_x+11, lft_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+137, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+137, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+155, 152, 19,2, BLACK);           //8. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+156, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+156, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+8);
  tft.print("FLIGHT TIME");

  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+8);
  tft.print("3:25");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+29);
  tft.print("MAX ALT.");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+29);
  tft.print("3500");
   tft.print(" ");
  tft.print(unit[d]);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+54);
  tft.print(rckt_v2);
   tft.print(" ");
  tft.print(unit[v2]);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);

  tft.setCursor(lft_blck_x+25, lft_blck_y+60);
  tft.print("MAX");
  tft.setCursor(lft_blck_x+14, lft_blck_y+73);
  tft.print("VELOCITY");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+73);
  tft.print("1.51 mach");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+92);
  tft.print("VELOCITY");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+92);
  tft.print(rckt_v1);
   tft.print(" ");
  tft.print(unit[v1]);  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+24, lft_blck_y+122);
  tft.print("LAST GPS LOCATION"); 
/*
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+122);
  tft.print("-179.123456");  */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+141);
  tft.print("GPS LAT");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+141);
  tft.print("-179.123456"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+160);
  tft.print("GPS Lng"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+160);
  tft.print("59.560833");   
//****************************************************************************************

 tft.fillRoundRect(rght_blck_x, rght_blck_y, 156, 176,3, CYAN);               //sağ blok


tft.fillRoundRect(rght_blck_x+2, rght_blck_y+2, 152, 40,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+5, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+5, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+24, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+24, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+45, 152, 41,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+49, box_x+20, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+93, rght_blck_y+49, box_x-7, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+67, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+67, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+92, 152, 19,2, BLACK);           //5. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+94, box_x+81, box_y,3, WHITE);      //ilk kutucuk
//tft.fillRoundRect(rght_blck_x+80, rght_blck_y+92, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+114, 152, 60,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+118, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+136, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+136, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+155, 152, 19,2, BLACK);           //8. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+155, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+155, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+9);
  tft.print("APOGEE TIME");
  tft.setCursor(rght_blck_x+85, rght_blck_y+9);
  tft.print("50 sn");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+83, rght_blck_y+8);
  tft.print("SLEEPY MODE");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+28);
  tft.print("DESCENT TIME");
  tft.setCursor(rght_blck_x+85, rght_blck_y+28);
  tft.print("2:45");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+29);
  tft.print("11.1 V");*/
 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7,rght_blck_y+53);
  tft.print("ENGINE BURN T.");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+97, rght_blck_y+54);
  tft.print("5 sec.");


  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+71);
  tft.print("PYRO BURN T.");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+85, rght_blck_y+72);
  tft.print("0.01 sec.");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+30, rght_blck_y+98);
  tft.print("MAX ACCELERATION");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+96);
  tft.print("11.1 V");  */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+122);
  tft.print("ACCEL-X");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+122);
  tft.print("-200 g");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+140);
  tft.print("ACCEL-Y");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+140);
  tft.print("400 g");   

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+159);
  tft.print("ACCEL-Z");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+159);
  tft.print("20 g");   
  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+14, rght_blck_y+138);
  tft.print("GPS Lng");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+138);
  tft.print("59.560833"); */

     tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);

  }

void apogee_config(int apg){


tft.fillScreen(BLACK);

  tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(122, 106);
  tft.print(apg);
  tft.print(" ");
  tft.print(unit[d]);

one_btn.initButton(&tft,  45, 55, 40, 25, WHITE, RED, BLACK, "1", 1);  
ten_btn.initButton(&tft,  125, 55, 40, 25, WHITE, RED, BLACK, "10", 1);
hundred_btn.initButton(&tft,  190, 55, 40, 25, WHITE, RED, BLACK, "100", 1);
fivehundred_btn.initButton(&tft,  270, 55, 40, 25, WHITE, RED, BLACK, "500", 1);
one_btn.drawButton(false); 
ten_btn.drawButton(false); 
hundred_btn.drawButton(false); 
fivehundred_btn.drawButton(false); 

plus_btn.initButton(&tft,  70, 100, 60, 25, WHITE, GREEN, BLACK, "+", 1);
minus_btn.initButton(&tft,  250, 100, 60, 25, WHITE, BLUE, BLACK, "-", 1);
plus_btn.drawButton(false); 
minus_btn.drawButton(false); 



on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 

}

/*void main_config(){

i=3;
tft.fillScreen(BLACK);

tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(122, 106);
  tft.print(main);
  tft.print(" ");
  tft.print(unit[d]);

one_btn.initButton(&tft,  45, 55, 40, 25, WHITE, RED, BLACK, "1", 1);  
ten_btn.initButton(&tft,  125, 55, 40, 25, WHITE, RED, BLACK, "10", 1);
hundred_btn.initButton(&tft,  190, 55, 40, 25, WHITE, RED, BLACK, "100", 1);
fivehundred_btn.initButton(&tft,  270, 55, 40, 25, WHITE, RED, BLACK, "500", 1);
one_btn.drawButton(false); 
ten_btn.drawButton(false); 
hundred_btn.drawButton(false); 
fivehundred_btn.drawButton(false); 

plus_btn.initButton(&tft,  70, 100, 60, 25, WHITE, GREEN, BLACK, "+", 1);
minus_btn.initButton(&tft,  250, 100, 60, 25, WHITE, BLUE, BLACK, "-", 1);
plus_btn.drawButton(false); 
minus_btn.drawButton(false); 



on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 

}*/
void dbmconfig_page(int dbm){
  
  
tft.fillScreen(BLACK);

  tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(118, 106);
  tft.print(dbm);
  tft.print(" dbm");


plus_btn.initButton(&tft,  70, 100, 60, 25, WHITE, GREEN, BLACK, "+", 1);
minus_btn.initButton(&tft,  250, 100, 60, 25, WHITE, BLUE, BLACK, "-", 1);
plus_btn.drawButton(false); 
minus_btn.drawButton(false); 



on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 
  
  
  }


void frqconfig_page(float frq){
  
  
tft.fillScreen(BLACK);

  tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(115, 106);
  tft.print(frq);
  tft.print(" MHz");


point_one_btn.initButton(&tft,  45, 55, 40, 25, WHITE, RED, BLACK, "0.1", 1);  
point_t_btn.initButton(&tft,  125, 55, 40, 25, WHITE, RED, BLACK, "0.2", 1);
point_five_btn.initButton(&tft,  190, 55, 40, 25, WHITE, RED, BLACK, "0.5", 1);
one_thousand_btn.initButton(&tft,  270, 55, 40, 25, WHITE, RED, BLACK, "1", 1);
point_one_btn.drawButton(false); 
point_t_btn.drawButton(false); 
point_five_btn.drawButton(false); 
one_thousand_btn.drawButton(false); 

plus_btn.initButton(&tft,  70, 100, 60, 25, WHITE, GREEN, BLACK, "+", 1);
minus_btn.initButton(&tft,  250, 100, 60, 25, WHITE, BLUE, BLACK, "-", 1);
plus_btn.drawButton(false); 
minus_btn.drawButton(false); 



on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 
  
  
  }

void myconfig_page(){
  k=0;
  i=6;
  tft.fillScreen(BLACK);
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(WHITE, BLACK);
  tft.setCursor(150, 1);
  tft.print("12:30");
  tft.setCursor(90, 1);
  tft.print("(");
  tft.print("unit=");
  tft.setTextColor(RED, BLACK);
  tft.print(unit[u]);
  tft.setTextColor(WHITE, BLACK);
  tft.print(")");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");

  tft.fillRoundRect(78, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("MY CONFIG PAGE");
  
     tft.setFreeFont(FF33);
     tft.fillRoundRect(16, 23, 290, 25,3, CYAN);  // MEASURİNG UNIT paraşüt kutusu

     imprl_btn.initButton(&tft,  90, 70, 130, 25, WHITE, GREEN, BLACK, "IMPERIAL", 1);
     mtrc_btn.initButton(&tft,  230, 70, 130, 25, WHITE, GREEN, BLACK, "MERTIC", 1);

      tft.fillRoundRect(28, 97, 125, 25,3, CYAN);  // priave key kutusu

     tft.fillRoundRect(174, 97, 115, 25,3, WHITE);  //priave key değer kutucuğu

 
  tft.setCursor(29, 114);
  tft.print("PRIVATE KEY");

  tft.setTextColor( BLACK,a);
  tft.setCursor(193, 114);
  tft.print("978258");
 
     
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  
     imprl_btn.drawButton(false);  
     mtrc_btn.drawButton(false);


  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,CYAN);
  tft.setCursor(75, 40);
  tft.print("MEASURMENT UNIT");

  
  }


void rocketconfig_page(){
    k=0;
    i=1;
  tft.fillScreen(BLACK);
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(WHITE, BLACK);
  tft.setCursor(150, 1);
  tft.print("12:30");
   tft.setCursor(90, 1);
  tft.print("(");
  tft.print("unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");

  tft.fillRoundRect(75, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("ROCKET CONFIG");
  
     tft.setFreeFont(FF33);
     snd_btn.initButton(&tft,  268, 190, 100, 25, WHITE, GREEN, BLACK, "SEND", 1);
     erase_btn.initButton(&tft,  268, 60, 100, 75, WHITE, RED, BLACK, "ERASE", 1);
     
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  
     snd_btn.drawButton(false);
     erase_btn.drawButton(false);

     tft.fillRoundRect(0, 23, 115, 25,3, CYAN);  // drag paraşüt kutusu
     tft.fillRoundRect(0, 55, 115, 25,3, CYAN);  // main parşüt kutusu
     tft.fillRoundRect(0, 87, 115, 25,3, CYAN);  // roket durum kutusu
     tft.fillRoundRect(0, 119, 115, 25,3, CYAN);  // FREKANS KUTUSU
     tft.fillRoundRect(0, 151, 115, 25,3, CYAN);  // DBM KUTUSU

     tft.fillRoundRect(120, 23, 95, 25,3, WHITE);  // drag paraşüt değer kutucuğu
     tft.fillRoundRect(120, 55, 95, 25,3, WHITE);  // main paraşüt değer kutucuğu
     tft.fillRoundRect(120, 87, 95, 25,3, WHITE);  //roket durum değer kutucuğu
     tft.fillRoundRect(120, 119, 95, 25,3, WHITE);  // FREKANS DEĞER KUTUSU
     tft.fillRoundRect(120, 151, 95, 25,3, WHITE);  // DBM DEĞER KUTUSU

  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,CYAN);
  tft.setCursor(1, 40);
  tft.print("DRAG PRSHT");

  tft.setCursor(1, 72);
  tft.print("MAIN PRSHT");

  tft.setCursor(25, 104);
  tft.print("STATUS");

  tft.setTextColor( BLACK,a);
  tft.setCursor(128, 104);
  tft.print("IN SLEEP");

  tft.setCursor(1, 135);
  tft.print("FREQUENCY");

  tft.setTextColor( BLACK,a);
  tft.setCursor(124, 135);
  tft.print(frq);
  tft.print(" MHz");

  tft.setCursor(30, 167);
  tft.print("DBM");

  tft.setTextColor( BLACK,a);
  tft.setCursor(128, 167);
  tft.print(dbm);
  tft.print(" dbm");

  tft.setTextColor( BLACK,a);
  tft.setCursor(125, 40);
  tft.print(apogee);
  tft.print(" ");
  tft.print(unit[d]);

  tft.setTextColor( BLACK,b);
  tft.setCursor(125, 72);
  tft.print(main);
  tft.print(" ");
  tft.print(unit[d]);

  }


void baboon(){
  
  k=0;
  i=6;
   TJpgDec.drawJpg(0, 0, kiz, sizeof(kiz));
   tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  

  }

void mainpage()
{   
   k=0;
    i=6;
     tft.fillScreen(BLACK);
      TJpgDec.drawJpg(0, 0, uzay, sizeof(uzay));
     tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  
     
  }

void grndstn()
{
  k=0; 
  i=6;
  tft.fillScreen(BLACK);
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(WHITE, BLACK);
  tft.setCursor(150, 1);
  tft.print("12:30");
   tft.setCursor(90, 1);
  tft.print("(");
  tft.print("unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");
  tft.fillRoundRect(lft_blck_x, lft_blck_y, 320, 159,3, CYAN);
 
 tft.fillRoundRect(lft_blck_x+2, lft_blck_y+3, 316, 66,2, BLACK);           //1. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+6, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+6, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+27, box_x+60, box_y+3,3, WHITE);      //2. SATIR ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+27, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+48, box_x+60, box_y+3,3, WHITE);      //3. SATIR ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+48, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk 


 tft.fillRoundRect(lft_blck_x+2, lft_blck_y+72,316, 84,2, BLACK);           //4. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+74, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+74, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

 //tft.fillRoundRect(lft_blck_x+2, lft_blck_y+68, 196, 62,2, BLACK);           //5. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+94, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+94, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

  //tft.fillRoundRect(lft_blck_x+2, lft_blck_y+68, 196, 62,2, BLACK);           //6. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+114, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+114, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

  //tft.fillRoundRect(lft_blck_x+2, lft_blck_y+68, 196, 62,2, BLACK);           //7. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+134, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+134, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

 /*tft.fillRoundRect(rght_blck_x+4, rght_blck_y+47, 142, 79,2, BLACK);           //2. satır kutu altlığı
 tft.fillRoundRect(rght_blck_x+7, rght_blck_y+50, box_x-20, box_y*2+9,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(rght_blck_x+58, rght_blck_y+50, box_x+20, box_y*2+9,3, WHITE);          //ikinci kutucuk*/

  tft.fillRoundRect(75, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(80, 227);
  tft.print("GROUND STATION");

  
  tft.setFreeFont(FM9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+6, lft_blck_y+19);
  tft.print("GPS LAT.");

 
 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+19);
  tft.print("-179.123456");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+6, lft_blck_y+40);
  tft.print("GPS LONG.");


  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+40);
  tft.print("59.123456");*/


 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+6, lft_blck_y+60);
  tft.print("GPS SAT");


  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+60);
  tft.print("8 SAT");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+87);
  tft.print("TEMPERATURE");

  tft.setFreeFont(FM9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+87);
  tft.print(my_temp);
  tft.print(" ");
  tft.print(unit[t]);


  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+106);
  tft.print("ALTITUDE");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+106);
  tft.print(my_alt);
  tft.print(" ");
  tft.print(unit[d]);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+127);
  tft.print("COMPASS");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+147);
  tft.print("CC1200:"); 

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+147);
   tft.print(rckt_dist);
  tft.print(unit[d]);   */

  tft.setFreeFont(FF33);
      on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);
  
  }

  void rocketpage2()
{

   k=0;
   i=6;

  tft.fillScreen(BLACK);
  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  tft.setCursor(150, 1);
  tft.print("12:30");
   tft.setCursor(90, 1);
  tft.print("(");
  tft.print("unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");
  
  tft.fillRoundRect(80, 211, 150, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("ROCKET PAGE 2");
  
  tft.fillRoundRect(lft_blck_x, lft_blck_y, 156, 154,3, CYAN);               //sol blok

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+2, 152, 19,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+4, box_x*2+16, box_y,3, WHITE);      //ilk kutucuk
//tft.fillRoundRect(box_x+11, lft_blck_y+4, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+23, 152, 63,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+27, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+27, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+45, 152, 63,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+46, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+46, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+65, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+65, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+89, 152, 19,2, BLACK);           //5. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+91, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+91, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+111, 152, 41,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+114, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+114, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+134, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+134, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+57, lft_blck_y+8);
  tft.print("BNO080");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+31);
  tft.print("GYRO-X");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+31);
  tft.print("-00.00 rad/s");
 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+51);
  tft.print("GYRO-Y");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+51);
  tft.print("-34.56 rad/s");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+70);
  tft.print("GYRO-Z");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+70);
  tft.print("-25.17 rad/s");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+94);
  tft.print("ACCEL-X");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+94);
  tft.print("-8.0 g");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+118);
  tft.print("ACCEL-Y"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+118);
  tft.print("5.2 g");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+138);
  tft.print("ACCEL-Z");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+138);
  tft.print("0.0 g"); 
//****************************************************************************************

 tft.fillRoundRect(rght_blck_x, rght_blck_y, 156, 154,3, CYAN);               //sağ blok


tft.fillRoundRect(rght_blck_x+2, rght_blck_y+2, 152, 19,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+4, box_x*2+16, box_y,3, WHITE);     //ilk kutucuk
//tft.fillRoundRect(rght_blck_x+80, rght_blck_y+4, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+25, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+25, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+45, 152, 41,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+49, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+49, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+67, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+67, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+89, 152, 19,2, BLACK);           //5. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+91, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+91, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+111, 152, 41,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+114, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+114, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+134, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+134, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+57, rght_blck_y+8);
  tft.print("BNO080");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+29);
  tft.print("MAGNO-X");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+29);
  tft.print("53 uT");*/
 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11,rght_blck_y+53);
  tft.print("MAGNO-Y");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+54);
  tft.print("930 mph");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+71);
  tft.print("MAGNO-Z");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+71);
  //tft.print("MAGNO-Z");*/

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+73);
  tft.print("0.44 mach");*/


  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+96);
  tft.print("YAW");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+96);
  tft.print("-180 dgree"); */ 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+118);
  tft.print("PITCH"); 

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+118);
  tft.print("90 dgree"); */ 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+138);
  tft.print("ROLL");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+138);
  tft.print("90 dgree");   */
  
  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+14, rght_blck_y+138);
  tft.print("GPS Lng");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+138);
  tft.print("59.560833"); */

  tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);
  
  }

  
void rocketpage1()
{
k=0;
i=6;

 tft.fillScreen(BLACK);
 tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
   tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  tft.setCursor(150, 1);
  tft.print("12:30");
   tft.setCursor(90, 1);
  tft.print("(");
  tft.print("unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");
  
  tft.fillRoundRect(80, 211, 150, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("ROCKET PAGE 1");
  
 tft.fillRoundRect(lft_blck_x, lft_blck_y, 156, 176,3, CYAN);               //sol blok


tft.fillRoundRect(lft_blck_x+2, lft_blck_y+2, 152, 19,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+4, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+4, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+25, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+25, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+46, 152, 62,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+50, box_x+4, box_y*3+8,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+50, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
//tft.fillRoundRect(lft_blck_x+5, lft_blck_y+69, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+69, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+89, 152, 19,2, BLACK);           //5. satır kutu altlığı
//tft.fillRoundRect(lft_blck_x+5, lft_blck_y+88, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+88, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+114, 152, 60,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+118, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+137, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+137, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+155, 152, 19,2, BLACK);           //8. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+156, box_x+19, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+26, lft_blck_y+156, box_x-5, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+8);
  tft.print("TEMPERATURE");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+29);
  tft.print("ALTITUDE");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+29);
  tft.print(rckt_alt);
   tft.print(" ");
  tft.print(unit[d]);*/
 
 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+54);
  tft.print("VELOCITY");*/

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+54);
  tft.print(rckt_v2);
   tft.print(" ");
  tft.print(unit[v2]);*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+73);
  tft.print("VELOCITY");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+73);
  tft.print("0.44 mach");*/

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+92);
  tft.print("VELOCITY");*/

/*  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+92);
  tft.print(rckt_v1);
   tft.print(" ");
  tft.print(unit[v1]);  */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+122);
  tft.print("GPS Lat"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
 // tft.setCursor(lft_blck_x+82, lft_blck_y+122);
 //tft.print("-179.123456");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+141);
  tft.print("GPS Lng");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  //tft.setCursor(lft_blck_x+82, lft_blck_y+141);
 //tft.print("59.560833"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+160);
  tft.print("GPS SATALITE"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+94, lft_blck_y+160);
  tft.print("8 SAT.");   
//****************************************************************************************

 tft.fillRoundRect(rght_blck_x, rght_blck_y, 156, 176,3, CYAN);               //sağ blok


tft.fillRoundRect(rght_blck_x+2, rght_blck_y+2, 152, 40,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+5, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+5, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+24, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+24, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+45, 152, 41,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+49, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+49, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+67, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+67, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+90, 152, 19,2, BLACK);           //5. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+92, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+92, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+114, 152, 60,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+118, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+136, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+136, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+155, 152, 19,2, BLACK);           //8. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+155, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+155, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+9);
  tft.print("400 G-X");

  //tft.fillCircle(rght_blck_x+93, rght_blck_y+12, 5, GREEN);
  //tft.fillCircle(rght_blck_x+113, rght_blck_y+12,5, RED);

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+83, rght_blck_y+8);
  tft.print("SLEEPY MODE");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+28);
  tft.print("400 G-Y");

  //tft.fillCircle(rght_blck_x+93, rght_blck_y+31, 5, GREEN);
  //tft.fillCircle(rght_blck_x+113, rght_blck_y+31,5, RED);

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+29);
  tft.print("11.1 V");*/
 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7,rght_blck_y+53);
  tft.print("400 G-Z");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+54);
  tft.print("930 mph");*/
  //tft.fillCircle(rght_blck_x+93, rght_blck_y+56, 5, GREEN);
  //tft.fillCircle(rght_blck_x+113, rght_blck_y+56,5, RED);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+71);
  tft.print("PYRO BCKP 2");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+73);
  tft.print("0.44 mach");*/

    tft.fillCircle(rght_blck_x+93, rght_blck_y+74, 5, GREEN);
   tft.fillCircle(rght_blck_x+113, rght_blck_y+74, 5, RED);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+96);
  tft.print("BATTERY");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+96);
  tft.print("11.1 V");  */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+122);
  tft.print("ACCEL-X");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+122);
  tft.print("-200 g");  */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+140);
  tft.print("ACCEL-Y");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+140);
  tft.print("400 g");   */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+159);
  tft.print("ACCEL-Z");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+159);
  tft.print("20 g");   */
  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+14, rght_blck_y+138);
  tft.print("GPS Lng");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+138);
  tft.print("59.560833"); */

     tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);

  }

void showTime(uint32_t msTime) {
  //tft.setCursor(0, 0);
  //tft.setTextFont(1);
  //tft.setTextSize(2);
  //tft.setTextColor(TFT_WHITE, TFT_BLACK);
  //tft.print(F(" JPEG drawn in "));
  //tft.print(msTime);
  //tft.println(F(" ms "));
  Serial.print(F(" JPEG drawn in "));
  Serial.print(msTime);
  Serial.println(F(" ms "));
}

bool tft_output(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t* bitmap)
{
   // Stop further decoding as image is running off bottom of screen
  if ( y >= tft.height() ) return 0;

  // This function will clip the image block rendering automatically at the TFT boundaries
  tft.pushImage(x, y, w, h, bitmap);

  // This might work instead if you adapt the sketch to use the Adafruit_GFX library
  // tft.drawRGBBitmap(x, y, bitmap, w, h);

  // Return 1 to decode next block
  return 1;
}



void touch_calibrate()
{
  uint16_t calData[5];
  uint8_t calDataOK = 0;

  // check file system exists
  if (!LITTLEFS.begin()) {
    Serial.println("Formating file system");
    LITTLEFS.format();
    LITTLEFS.begin();
  }

  // check if calibration file exists and size is correct
  if (LITTLEFS.exists(CALIBRATION_FILE)) {
    if (REPEAT_CAL)
    {
      // Delete if we want to re-calibrate
      LITTLEFS.remove(CALIBRATION_FILE);
    }
    else
    {
      File f = LITTLEFS.open(CALIBRATION_FILE, "r");
      if (f) {
        if (f.readBytes((char *)calData, 14) == 14)
          calDataOK = 1;
        f.close();
      }
    }
  }

  if (calDataOK && !REPEAT_CAL) {
    // calibration data valid
    tft.setTouch(calData);
  } else {
    // data not valid so recalibrate
    tft.fillScreen(TFT_BLACK);
    tft.setCursor(20, 0);
    tft.setTextFont(2);
    tft.setTextSize(1);
    tft.setTextColor(TFT_WHITE, TFT_BLACK);

    tft.println("Touch corners as indicated");

    tft.setTextFont(1);
    tft.println();

    if (REPEAT_CAL) {
      tft.setTextColor(TFT_RED, TFT_BLACK);
      tft.println("Set REPEAT_CAL to false to stop this running again!");
    }

    tft.calibrateTouch(calData, TFT_MAGENTA, TFT_BLACK, 15);

    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.println("Calibration complete!");

    // store data
    File f = LITTLEFS.open(CALIBRATION_FILE, "w");
    if (f) {
      f.write((const unsigned char *)calData, 14);
      f.close();
    }
  }
}

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if(!root){
    Serial.println("Failed to open directory");
    return;
  }
  if(!root.isDirectory()){
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while(file){
    if(file.isDirectory()){
      Serial.print("  DIR : ");
      Serial.println(file.name());
      if(levels){
        listDir(fs, file.name(), levels -1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.println(file.size());
    }
    file = root.openNextFile();
  }
}

void createDir(fs::FS &fs, const char * path){
  Serial.printf("Creating Dir: %s\n", path);
  if(fs.mkdir(path)){
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

void removeDir(fs::FS &fs, const char * path){
  Serial.printf("Removing Dir: %s\n", path);
  if(fs.rmdir(path)){
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\n", path);

  File file = fs.open(path);
  if(!file){
    Serial.println("Failed to open file for reading");
    return;
  }

  Serial.print("Read from file: ");
  while(file.available()){
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if(!file){
    Serial.println("Failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message){
 // Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if(!file){
    Serial.println("Failed to open file for appending");
    return;
  }
  if(file.print(message)){
      Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}

void renameFile(fs::FS &fs, const char * path1, const char * path2){
  Serial.printf("Renaming file %s to %s\n", path1, path2);
  if (fs.rename(path1, path2)) {
    Serial.println("File renamed");
  } else {
    Serial.println("Rename failed");
  }
}

void deleteFile(fs::FS &fs, const char * path){
  Serial.printf("Deleting file: %s\n", path);
  if(fs.remove(path)){
    Serial.println("File deleted");
  } else {
    Serial.println("Delete failed");
  }
}
