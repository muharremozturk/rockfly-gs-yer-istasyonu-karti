

#include <EEPROM.h>

#include "LITTLEFS.h" 
//#include <SPI.h>
#include "FS.h"

TaskHandle_t Task1;
#include <RadioLib.h>
#include "SPI.h"
SPIClass abc(HSPI);
SX1262 radio = new Module(15, 17, 27, 39,abc);


float ByteToFloat(byte *byterray,int ca)
{
  float f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

  int16_t ByteToint16(byte *byterray,int ca)
{
  int16_t f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
 
return f;
  }

    uint16_t ByteTouint16(byte *byterray,int ca)
{
  uint16_t f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
 
return f;
  }

  long ByteTolong(byte *byterray,int ca)
{
  long f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

   unsigned long ByteTounsignedlong(byte *byterray,int ca)
{
 unsigned long f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

 int ByteToint(byte *byterray,int ca)
{
  int f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

   int ByteTobyte(byte *byterray,int ca)
{
  byte f;
  ((uint8_t*)&f)[0]=byterray[ca];
return f;
  }

/*    uint8_t ByteTouint8t(byte *byterray,int ca)
{
  uint8_t f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }*/

  void floatAddByte(float data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void intAddByte(int data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}

void uint8tAddByte(uint8_t data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}



uint8_t byteToUint8t(byte *byterray, int *count) {
  uint8_t f;
  ((uint8_t *)&f)[0] = byterray[*count];
  (*count)++;
  return f;
}
float byteToFloat(byte *byterray, int *count) {
  float f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t* )&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
long byteToLong(byte *byterray, int *count) {
  long f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t* )&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
unsigned long byteToULong(byte *byterray, int *count) {
  unsigned long f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt(byte *byterray, int *count) {
  int f;
  for (int m = 0; m < 4; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt16(byte *byterray, int *count) {
  int16_t f;
  for (int m = 0; m < 2; m++) {
    ((uint8_t *)&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToUint16(byte *byterray, int *count) {
  uint16_t f;
  for (int m = 0; m < 2; m++) {
    ((uint8_t* )&f)[m] = byterray[*count];
    (*count)++;
  }
  return f;
}
byte byteToByte(byte *byterray, int *count) {
  byte f;
  ((uint8_t *)&f)[0] = byterray[*count];
  (*count)++;
  return f;
}



struct flightdata{
  
float H3LIS331_data[3];
float Ms5611_data[4];
float bnoAccel[3];
float bnoGyro[3];
float bnoMag[3];
float bnoRaw[3];
float MAX_ALT;
float GPS_speed;

uint16_t APOGEE_TIME;
uint16_t DESCENT_TIME;
uint8_t ENGINEBURN_TIME;
float PYROBURN_TIME;
float MAX_ACCEL[3];
float MAX_SPEED;
float APOGEE_FALL_SPEED;
float MAIN_FALL_SPEED;
long GPS_data[3];
byte SIV;
byte MAX_SIV;
long unix_time;
float batteryy;
float RISE_ACC[3];
float FALL_ACC[3];
long deneme;
long pyro_burn_time;
float rocket_flash;
};
flightdata flightData;
flightdata flightstatMax;
flightdata flightstatMin;

unsigned long FLIGHT_TIME;
int compmin_x,compmin_y,compmin_z,compmax_x,compmax_y,compmax_z;
int screen_id,k,i,kts,t,v1,d,v2,u;
float apogee=500;
float main=500.0;
uint32_t a,b;
char *unit[] ={"m","i","M","m/s","km/h","C","ft","ft/s","mph","F"}; 
char buf[40];
float my_temp,my_alt,rckt_temp,rckt_alt,rckt_v1,rckt_v2,rckt_dist;



uint32_t tt=0;

float aa,cardSize,cardused;
long l;
int ii,c,m,last_st;
bool kk,ok,flg,comm,alt_calc=0;
char veri[] = "";


float last_frq,frquency_check;
int last_dbm,dbm_check,sd_cap,grnd_alt;
float frq=868.00;
int frq_ktss;
int dbm=20;
float frq_kts=0.1;

float carrierFreq;
uint8_t outputPower;
uint8_t pyro_stat=0;
bool pyro_stat_arr[8];
bool tch_grnd=0,header_count=1,gps_cnt=1,tst=1,flash_warning,flash_succes,wrng,flash_error,new_frq,get_frq_data,let_send,cls=0,check_time;

int rocketapoge;
int rocketmain;

byte passw = 0x60;
byte getsensData = 0x20;
byte getflightData= 0x21;
byte getsensData_flgtstcs= 0x22;
byte getsensData_tch_grnd= 0x23;
byte getsensData_tch_grnd_all= 0x24;

byte send_prsht_config = 0x50;
byte get_prsht_config  = 0x51;
byte send_frq_config = 0x30;
byte get_frq_config = 0x31; 


byte pyro_drg_config = 0x70;
byte pyro_main_config = 0x71;

byte flash_erase = 0x80;

byte check_frq = 0x40;
byte setConfig = 0x10;

byte drg_prst_time=0;
byte main_prst_time=0;

bool drg_prst_count=0;
bool main_prst_count=0;


unsigned long time_gpsfix=0;

unsigned long timeout_hz=0;
unsigned long timeout=0;
unsigned long timeout_sens=0;
unsigned long timeout_fall=0;
unsigned long timeout_comm=0;
unsigned long timeout_flash=0;

unsigned long drg_prst_timer=0;
unsigned long main_prst_timer=0;

int transmissionState = ERR_NONE;

struct Data{
byte byteArray[200];
byte tr_array[10];
int counter=0;
};
Data data;



char strr[210];
String All_data2;
String header = "GpsTime,200gAccX,200gAccY,200gAccZ,Temp,Pressure,Alt,Speed,BNOAccX,BNOAccY,BNOAccZ,BNOGyrX,BNOGyrY,BNOGyrZ,BNOMagX,BNOMagY,BNOMagZ,BNOYaw,BNOPitch,BNORoll,GpsLat,GpsLong,GpsAlt,GpsSpeed,SIV,Batt,PyroDraque,PyroMain,MAX_ALTITUDE,MAX_SPEED,MAX_ACCEL_X,MAX_ACCEL_Y,MAX_ACCEL_Z,RISE_ACC_X,RISE_ACC_Y,RISE_ACC_Z,FALL_ACC_X,FALL_ACC_Y,FALL_ACC_Z,MAX_SIV,ENGINE_BURN_TIME,PYRO_BURN_TIME,APOGEE_TIME,APOGEE_FALL_SPEED,MAIN_FALL_SPEED,DESCENT_TIME,FLIGHT_TIME,UnixTIME\n";
String All_data1;
int All_dataa,cnt;
int frst_cnt,stt=0,ct=0;
int crc_err,compare=2;
byte v=0,selffrq_set=0;

bool try_com,check_data,time_com,rqst_err,snd_err,prsht_rqst_err,prsht_snd_err,config_page,pyro_succes=0;

#define INITIATING_NODE = true;
bool transmitFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// flag to indicate that a packet was sent or received
volatile bool operationDone = false;

// this function is called when a complete packet
// is transmitted or received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if(!enableInterrupt) {
    return;
  }

  // we sent aor received  packet, set the flag
  operationDone = true;
}



void setup() {
  
  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;  
  const int HSPI_CS = 15;

  abc.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS); 
  
  delay(500);
  
 Serial.begin(9600); 
k=1;
i=0;
kts=1;

 
  EEPROM.begin(512);
  
  
  
  xTaskCreatePinnedToCore(
                    Task1code,   /* Task function. */
                    "Task1",     /* name of task. */
                    40000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task1,      /* Task handle to keep track of created task */
                    0);          /* pin task to core 0 */                  
  delay(100); 

 
 /*preferences.begin("my-config", false);
 
  frquency_check=preferences.getFloat("frq",0);
  Serial.print(frquency_check);
  if(frquency_check==0)frq=868.00;
  else frq=frquency_check;
  Serial.print(" frekans= ");
  Serial.print(frq);

  dbm_check=preferences.getInt("dbm",0);
  Serial.print(dbm_check);
  if(dbm_check==0)dbm=20;
  else dbm=dbm_check;
  Serial.print(" dbm= ");
  Serial.print(dbm);*/

  
 // EEPROM.writeFloat(0,frq);
 // EEPROM.writeInt(6,dbm);
 // EEPROM.commit();
 //EEPROM.writeFloat(0,frq);
  frquency_check=EEPROM.readFloat(4);
  Serial.print(" içindeki frekans ");
  Serial.print(frquency_check);
  if(frquency_check>=868 && frquency_check<=915)frq=frquency_check;
  else frq=868.00;
  Serial.print(" frekans= ");
  Serial.print(frq);

  dbm_check=EEPROM.readByte(8);
  Serial.print(" içindeki dbm ");
  Serial.print(dbm_check);
  if(dbm_check<=0 || dbm_check>=23)dbm=20;
  else dbm=dbm_check;
  Serial.print(" dbm= ");
  Serial.print(dbm);


int state = radio.begin(frq, 500.0, 7, 5, 0x34,dbm);
  if (state == ERR_NONE) {
  //  Serial.println(F("success!"));
  } else {
   // Serial.print(F("failed, code "));
   // Serial.println(state);
    while (true);
  }
radio.setCurrentLimit(110);
  // set the function that will be called
  // when new packet is received
 radio.setDio1Action(setFlag);
  #if defined(INITIATING_NODE)
    // send the first packet on this node
    Serial.print(F("[SX1262] Sending first packet ... "));
    transmissionState = radio.startTransmit(data.tr_array,10);
    transmitFlag = true;
  #else
    // start listening for LoRa packets on this node
    Serial.print(F("[SX1262] Starting to listen ... "));
    state = radio.startReceive();
    if (state == ERR_NONE) {
      Serial.println(F("success!"));
    } else {
      Serial.print(F("failed, code "));
      Serial.println(state);
      while (true);
    }
  #endif



}



// flag to indicate transmission or reception state


void Task1code( void * pvParameters ){
 // Serial.print("Task1 running on core ");
 // Serial.println(xPortGetCoreID());

  for(;;){

    vTaskDelay(1);
    

  if((screen_id==3 || screen_id==4) && flg ==0){
    
  if(millis()-timeout_sens > 5000){
    
      kk=0;
      enableInterrupt = false;
      Serial.print(" roketten sensör data istendi ");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(getsensData, &data.counter, data.tr_array);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      stt=0;
      //check_data=0;  
      enableInterrupt = true;
     timeout_sens = millis();
     

}
  }

      // burada rqst butonuna basılırsa değişken aktif olup config isteme gönderme işlemi yapıyor.
    if(stt==1){
      kk=0;
      enableInterrupt = false;
      Serial.print(" roketten frq config istendi ");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(get_frq_config, &data.counter, data.tr_array);
     // delay(100);
      transmissionState = radio.startTransmit(data.tr_array,2);
       for(int p =0;p<2;p++){
        Serial.print(data.tr_array[p],HEX);
        Serial.print(",");
        
      }
      Serial.println("");
      transmitFlag = true;
      stt=0;
      //check_data=0;  
      enableInterrupt = true;

}
    
     // burada send butonuna basılırsa değişken aktif olup config gönderme işlemi yapıyor.
    if(stt==2){
      kk=0;
      enableInterrupt = false;
      Serial.print(" içimdeki frq Datalar gönderildi");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(get_frq_config, &data.counter, data.tr_array);
      floatAddByte(frq,&data.counter,data.tr_array);
      uint8tAddByte(dbm,&data.counter,data.tr_array);
       for(int p =0;p<7;p++){
        Serial.print(data.tr_array[p],HEX);
        Serial.print(",");
        
      }
      Serial.println("");
   //   delay(100);
      transmissionState = radio.startTransmit(data.tr_array,7);
      transmitFlag = true;
      stt=0;
      //ok=1;
      enableInterrupt = true;

}

    if(stt==3){
      kk=0;
      enableInterrupt = false;
      Serial.print(" prsht Data teyit gönderildi");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(send_prsht_config, &data.counter, data.tr_array);
     // floatAddByte(apogee,&data.counter,data.tr_array);
     // floatAddByte(main,&data.counter,data.tr_array);
     // delay(100);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      stt=0;
      enableInterrupt = true;

}

    if(stt==4){
      kk=0;
      enableInterrupt = false;
     Serial.println(" prhst Datalar gönderildi");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(get_prsht_config, &data.counter, data.tr_array);
      floatAddByte(main,&data.counter,data.tr_array);
     // delay(100);
      transmissionState = radio.startTransmit(data.tr_array,6);
      transmitFlag = true;
      stt=0;
      enableInterrupt = true;

}

 

   if(stt==8){
      kk=0;
      enableInterrupt = false;
      Serial.println("kontrol teyiti gönderildi");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(check_frq, &data.counter, data.tr_array);
     // delay(100);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      stt=0;
      enableInterrupt = true;

}




/*  if( check_frq==0 && time_com==1 && last_st==1 ){
    

   if(millis()-timeout > 1000){
     stt=8;
     timeout = millis();
     cnt+=1;
     Serial.print(" yeni frq kontrol ");
     Serial.print(cnt);
     Serial.print(". kez gönderildi");
    }
   if(cnt>=5){cnt=0;
      delay(2000);
      frq=last_frq;
      dbm=last_dbm;
      radio.setFrequency(frq);
      radio.setOutputPower(dbm);
      Serial.println("ESKİ FREKANSA GEÇİLDİ");
      last_st=0;
      time_com=1;
        }
    }*/
 
    if(operationDone) {
    // disable the interrupt service routine while
    // processing the data
    enableInterrupt = false;

    // reset flag
    operationDone = false;

      if(transmitFlag) {


      // the previous operation was transmission, listen for response
      // print the result
      if (transmissionState == ERR_NONE) {
        // packet was successfully sent
       // Serial.println(F("transmission finished!"));
  
      } else {
        Serial.print(F("failed, code "));
        Serial.println(transmissionState);
  
      }

      // listen for response
      radio.startReceive();
      transmitFlag = false;
     // Serial.println("bekliyor");
      
    }

    else{
   // Serial.println("alım başlayacak");
    int state = radio.readData(data.byteArray,150);
    
    if(state == ERR_CRC_MISMATCH){
      
      crc_err+=1;
           
      }
    



    if (state == ERR_NONE) {
      for(int p=0;p<7;p++) {
            Serial.print(data.byteArray[p], HEX);
            Serial.print(",");
          }

    if(data.byteArray[0] == passw && data.byteArray[1] == get_frq_config){
      
     /* data.counter=2;
      carrierFreq = byteToFloat(data.byteArray, &data.counter);
      outputPower = byteToUint8t(data.byteArray, &data.counter);*/
      Serial.println(" frekans data istenmiş ");
      stt=2;
    
            }

    else if(data.byteArray[0] == passw && data.byteArray[1] == send_frq_config){
      data.counter=2;
      last_frq=frq;  
      last_dbm=dbm;
      carrierFreq = byteToFloat(data.byteArray, &data.counter);
      outputPower = byteToUint8t(data.byteArray, &data.counter);
      frq=carrierFreq;
      dbm=outputPower;
      radio.setFrequency(frq);
      radio.setOutputPower(dbm);
      Serial.println(" frekans değişti ");
      check_time=1;
     
     }
          

     else if(data.byteArray[0] == passw && data.byteArray[1] == get_prsht_config){

      stt=4;
     }

      else if(data.byteArray[0] == passw && data.byteArray[1] == send_prsht_config){
      data.counter=2;
      main = byteToFloat(data.byteArray, &data.counter);
      stt=3;
     }
          

         

      else if(data.byteArray[0] == passw && data.byteArray[1] == check_frq) {
        Serial.print(" yeni frekasta kontrol istendi ");
          EEPROM.writeFloat(4, frq);
          EEPROM.writeByte(8, dbm);
          EEPROM.commit();
          stt=8;
          check_time=0;
        }
        
     
   }
 }
    

    radio.startReceive();
    enableInterrupt = true;

 }

if(check_time==1){
 if(millis()-timeout > 1000){
     timeout = millis();
     cnt+=1;
     Serial.print(" timeout ");
     Serial.print(cnt);
     Serial.print(". saniye");
    }

    if(cnt>=7){
      frq=last_frq;  
      dbm=last_dbm;
      radio.setFrequency(frq);
      radio.setOutputPower(dbm);
      delay(100);
      cnt=0;
      check_time=0;
               
   }
}


/*
Serial.print(" V= ");
Serial.println(v);

Serial.print(" try_com= ");
Serial.println(try_com);

Serial.print(" compare= ");
Serial.println(compare);

Serial.print(" time_com= ");
Serial.println(time_com);*/
/*timeout_hz=millis()-timeout_hz;

if(timeout_hz >= 100){
Serial.print(" 0. çekirdek süre ");
Serial.println(timeout_hz);}*/

}
 
}

void loop(){
  delay(1000);
  Serial.println(main);
  }
