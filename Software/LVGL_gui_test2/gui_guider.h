/*
 * Copyright 2022 NXP
 * SPDX-License-Identifier: MIT
 */

#ifndef GUİ_GUİDER_H
#define GUİ_GUİDER_H
#ifdef __cplusplus
extern "C" {
#endif

#include <lvgl.h>
#include "guider_fonts.h"
typedef struct
{
	lv_obj_t *screen;
	lv_obj_t *screen_cont_top_bar;
	lv_obj_t *screen_cont_page;
	lv_obj_t *screen_gs_bat_img;
	lv_obj_t *screen_img_1;
	lv_obj_t *screen_img_2;
	lv_obj_t *screen_label_gps;
	lv_obj_t *screen_label_1;
	lv_obj_t *screen_img_3;
	lv_obj_t *screen_img_4;
	lv_obj_t *screen_label_2;
	lv_obj_t *screen_imgbtn_1;
	lv_obj_t *screen_imgbtn_1_label;
	lv_obj_t *screen_imgbtn_2;
	lv_obj_t *screen_imgbtn_2_label;
	lv_obj_t *screen_imgbtn_3;
	lv_obj_t *screen_imgbtn_3_label;
	lv_obj_t *screen_imgbtn_4;
	lv_obj_t *screen_imgbtn_4_label;
	lv_obj_t *screen_imgbtn_5;
	lv_obj_t *screen_imgbtn_5_label;
	lv_obj_t *screen_imgbtn_6;
	lv_obj_t *screen_imgbtn_6_label;
	lv_obj_t *screen_imgbtn_7;
	lv_obj_t *screen_imgbtn_7_label;
	lv_obj_t *screen_imgbtn_8;
	lv_obj_t *screen_imgbtn_8_label;
	lv_obj_t *cont_page2;
	lv_obj_t *cont_page2_cont_top_bar;
	lv_obj_t *cont_page2_cont_page;
	lv_obj_t *cont_page2_gs_bat_img;
	lv_obj_t *cont_page2_img_1;
	lv_obj_t *cont_page2_img_2;
	lv_obj_t *cont_page2_label_gps;
	lv_obj_t *cont_page2_label_1;
	lv_obj_t *cont_page2_img_3;
	lv_obj_t *cont_page2_win_1;
	lv_obj_t *cont_page2_win_1_item0;
	lv_obj_t *cont_page2_img_4;
	lv_obj_t *cont_page2_label_2;
	lv_obj_t *cont_page2_btn_1;
	lv_obj_t *cont_page2_btn_1_label;
	lv_obj_t *cont_page2_btn_2;
	lv_obj_t *cont_page2_btn_2_label;
	lv_obj_t *cont_page2_label_3;
}lv_ui;

void setup_ui(lv_ui *ui);
extern lv_ui guider_ui;
void setup_scr_screen(lv_ui *ui);
void setup_scr_cont_page2(lv_ui *ui);
LV_IMG_DECLARE(tasks);
LV_IMG_DECLARE(settings64);
LV_IMG_DECLARE(micro_sd_card);
LV_IMG_DECLARE(status);
LV_IMG_DECLARE(alarmm);
LV_IMG_DECLARE(satellite_gps2);
LV_IMG_DECLARE(low_battery_level1);
LV_IMG_DECLARE(wifi);
LV_IMG_DECLARE(compass);
LV_IMG_DECLARE(configuration);
LV_IMG_DECLARE(search);
LV_IMG_DECLARE(navigation);
LV_IMG_DECLARE(send);

#ifdef __cplusplus
}
#endif
#endif
