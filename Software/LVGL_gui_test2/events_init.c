/*
 * Copyright 2022 NXP
 * SPDX-License-Identifier: MIT
 */


#include <lvgl.h>

#include "events_init.h"
#include <stdio.h>
static unsigned int cnt = 0;
static char buf[4]; 
lv_ui dataa;
void events_init(lv_ui *ui)
{
}

static void screen_imgbtn_3_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_PRESSED:
	{
		if (!lv_obj_is_valid(dataa.cont_page2))
			setup_scr_cont_page2(&dataa);
		lv_scr_load_anim(dataa.cont_page2, LV_SCR_LOAD_ANIM_MOVE_BOTTOM, 0, 1000, true);
	}
		break;
	default:
		break;
	}
}

void events_init_screen(lv_ui *ui)
{
	lv_obj_add_event_cb(ui->screen_imgbtn_3, screen_imgbtn_3_event_handler, LV_EVENT_ALL, NULL);
}

static void cont_page2_btn_2_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_PRESSED:
	{
		cnt++;
		sprintf(buf,  "%d" , cnt);
		lv_label_set_text(dataa.cont_page2_label_3, buf);
	}
		break;
	default:
		break;
	}
}

void events_init_cont_page2(lv_ui *ui)
{
	lv_obj_add_event_cb(ui->cont_page2_btn_2, cont_page2_btn_2_event_handler, LV_EVENT_ALL, NULL);
}
