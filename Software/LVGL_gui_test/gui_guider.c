/*
 * Copyright 2022 NXP
 * SPDX-License-Identifier: MIT
 */

#include <lvgl.h>
#include <stdio.h>
#include <lvgl.h>

#include "gui_guider.h"


void setup_ui(lv_ui *ui){
	setup_scr_m1m(ui);
	lv_scr_load(ui->m1m);
}
