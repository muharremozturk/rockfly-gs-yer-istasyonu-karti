/*
 * Copyright 2022 NXP
 * SPDX-License-Identifier: MIT
 */

#include <lvgl.h>
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "custom.h"


void setup_scr_m1m(lv_ui *ui){

	//Write codes m1m
	ui->m1m = lv_obj_create(NULL);

	//Write codes m1m_tileview_1
	ui->m1m_tileview_1 = lv_tileview_create(ui->m1m);
	lv_obj_set_pos(ui->m1m_tileview_1, 0, 0);
	lv_obj_set_size(ui->m1m_tileview_1, 320, 240);

	//Write style state: LV_STATE_DEFAULT for style_m1m_tileview_1_main_maın_default
	static lv_style_t style_m1m_tileview_1_main_main_default;
	lv_style_reset(&style_m1m_tileview_1_main_main_default);
	lv_style_set_radius(&style_m1m_tileview_1_main_main_default, 0);
	lv_style_set_bg_color(&style_m1m_tileview_1_main_main_default, lv_color_make(0xf6, 0xf6, 0xf6));
	lv_style_set_bg_grad_color(&style_m1m_tileview_1_main_main_default, lv_color_make(0xf6, 0xf6, 0xf6));
	lv_style_set_bg_grad_dir(&style_m1m_tileview_1_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_m1m_tileview_1_main_main_default, 255);
	lv_obj_add_style(ui->m1m_tileview_1, &style_m1m_tileview_1_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write style state: LV_STATE_DEFAULT for style_m1m_tileview_1_main_scrollbar_default
	static lv_style_t style_m1m_tileview_1_main_scrollbar_default;
	lv_style_reset(&style_m1m_tileview_1_main_scrollbar_default);
	lv_style_set_radius(&style_m1m_tileview_1_main_scrollbar_default, 0);
	lv_style_set_bg_color(&style_m1m_tileview_1_main_scrollbar_default, lv_color_make(0xea, 0xef, 0xf3));
	lv_style_set_bg_opa(&style_m1m_tileview_1_main_scrollbar_default, 255);
	lv_obj_add_style(ui->m1m_tileview_1, &style_m1m_tileview_1_main_scrollbar_default, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);

	//add new tile m1m_tileview_1_tileview
	ui->m1m_tileview_1_tileview = lv_tileview_add_tile(ui->m1m_tileview_1, 0, 0, LV_DIR_RIGHT);

	//Write codes m1m_btn_1
	ui->m1m_btn_1 = lv_btn_create(ui->m1m_tileview_1_tileview);
	lv_obj_set_pos(ui->m1m_btn_1, 117, 180);
	lv_obj_set_size(ui->m1m_btn_1, 100, 50);
	lv_obj_add_flag(ui->m1m_btn_1, LV_OBJ_FLAG_CHECKABLE);
	ui->m1m_btn_1_label = lv_label_create(ui->m1m_btn_1);
	lv_label_set_text(ui->m1m_btn_1_label, "basma");
	lv_obj_set_style_text_color(ui->m1m_btn_1_label, lv_color_make(0x00, 0x00, 0x00), LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->m1m_btn_1_label, &lv_font_simsun_12, LV_STATE_DEFAULT);
	lv_obj_set_style_pad_all(ui->m1m_btn_1, 0, LV_STATE_DEFAULT);
	lv_obj_align(ui->m1m_btn_1_label, LV_ALIGN_CENTER, 0, 0);

	//Write codes m1m_label_1
	ui->m1m_label_1 = lv_label_create(ui->m1m);
	lv_obj_set_pos(ui->m1m_label_1, 115, 123);
	lv_obj_set_size(ui->m1m_label_1, 100, 32);
	lv_label_set_text(ui->m1m_label_1, "");
	lv_label_set_long_mode(ui->m1m_label_1, LV_LABEL_LONG_WRAP);
	lv_obj_set_style_text_align(ui->m1m_label_1, LV_TEXT_ALIGN_CENTER, 0);

	//Write style state: LV_STATE_DEFAULT for style_m1m_label_1_main_maın_default
	static lv_style_t style_m1m_label_1_main_main_default;
	lv_style_reset(&style_m1m_label_1_main_main_default);
	lv_style_set_radius(&style_m1m_label_1_main_main_default, 0);
	lv_style_set_bg_color(&style_m1m_label_1_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_color(&style_m1m_label_1_main_main_default, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_bg_grad_dir(&style_m1m_label_1_main_main_default, LV_GRAD_DIR_VER);
	lv_style_set_bg_opa(&style_m1m_label_1_main_main_default, 0);
	lv_style_set_text_color(&style_m1m_label_1_main_main_default, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_text_letter_space(&style_m1m_label_1_main_main_default, 2);
	lv_style_set_pad_left(&style_m1m_label_1_main_main_default, 0);
	lv_style_set_pad_right(&style_m1m_label_1_main_main_default, 0);
	lv_style_set_pad_top(&style_m1m_label_1_main_main_default, 0);
	lv_style_set_pad_bottom(&style_m1m_label_1_main_main_default, 0);
	lv_obj_add_style(ui->m1m_label_1, &style_m1m_label_1_main_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Init events for screen
	events_init_m1m(ui);
}
