
#include <TinyGPS++.h>
#include <HardwareSerial.h>
   long lat,lon; 
   int counter = 0;     
    
    TinyGPSPlus gps;
  long A,B;
 HardwareSerial SerialGPS(1); 

#include <SPI.h>

#include <Wire.h>
#include <LSM303.h>
LSM303 compass;

#include <SD.h>
#include "Free_Fonts.h"
#include <TFT_eSPI.h>
TFT_eSPI tft = TFT_eSPI();
#include <JPEGDecoder.h>

int screen_id,k;


#define lft_blck_x    2
#define lft_blck_y    75

#define rght_blck_x    168
#define rght_blck_y    75

#define box_y    32
#define box_x    65


#define BLACK       0x0000      /*   0,   0,   0 */
#define NAVY        0x000F      /*   0,   0, 128 */
#define DARKGREEN   0x03E0      /*   0, 128,   0 */
#define DARKCYAN    0x03EF      /*   0, 128, 128 */
#define MAROON      0x7800      /* 128,   0,   0 */
#define PURPLE      0x780F      /* 128,   0, 128 */
#define OLIVE       0x7BE0      /* 128, 128,   0 */
#define LIGHTGREY   0xD69A      /* 211, 211, 211 */
#define DARKGREY    0x7BEF      /* 128, 128, 128 */
#define BLUE        0x001F      /*   0,   0, 255 */
#define GREEN       0x07E0      /*   0, 255,   0 */
#define CYAN        0x07FF      /*   0, 255, 255 */
#define RED         0xF800      /* 255,   0,   0 */
#define MAGENTA     0xF81F      /* 255,   0, 255 */
#define YELLOW      0xFFE0      /* 255, 255,   0 */
#define WHITE       0xFFFF      /* 255, 255, 255 */
#define ORANGE      0xFDA0      /* 255, 180,   0 */
#define GREENYELLOW 0xB7E0      /* 180, 255,   0 */
#define PINK        0xFE19      /* 255, 192, 203 */    
#define BROWN       0x9A60      /* 150,  75,   0 */
#define GOLD        0xFEA0      /* 255, 215,   0 */
#define SILVER      0xC618      /* 192, 192, 192 */
#define SKYBLUE     0x867D      /* 135, 206, 235 */
#define VIOLET      0x915C      /* 180,  46, 226 */

#define CALIBRATION_FILE "/TouchCalData2"

// Set REPEAT_CAL to true instead of false to run calibration
// again, otherwise it will only be done once.
// Repeat calibration if you change the screen rotation.
#define REPEAT_CAL true
TFT_eSPI_Button on_btn,off_btn,main_btn;

// Setup
//####################################################################################################
void setup() {
delay(250);
screen_id=0;
k=1;
A= rght_blck_y+69;
B= rght_blck_y+100;
   Wire.begin();
    compass.init();
    compass.enableDefault();
    compass.m_min = (LSM303::vector<int16_t>){-32767, -32767, -32767};
    compass.m_max = (LSM303::vector<int16_t>){+32767, +32767, +32767};
  
  Serial.begin(9600);
SerialGPS.begin(9600, SERIAL_8N1, 16, 34);

  digitalWrite( 5, HIGH); // SD card chips select, must use GPIO 5 (ESP32 SS)
  if (!SD.begin()) {
   // Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();

  if (cardType == CARD_NONE) {
//Serial.println("No SD card attached");
    return;
  }

 // Serial.print("SD Card Type: ");
  if (cardType == CARD_MMC) {
 //   Serial.println("MMC");
  } else if (cardType == CARD_SD) {
    Serial.println("SDSC");
  } else if (cardType == CARD_SDHC) {
    Serial.println("SDHC");
  } else {
    Serial.println("UNKNOWN");
  }

  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
 //// Serial.printf("SD Card Size: %lluMB\n", cardSize);
    tft.init();
 tft.fillScreen(BLACK);

 tft.setRotation(1);  // portrait

 tft.setTextSize(1);
 touch_calibrate();

 

}

//####################################################################################################

void loop() {
     compass.read();
     float heading = compass.heading();

 
  if((screen_id==1  && k==0)|| (screen_id==4 && k==0)){
  
  tft.setFreeFont(FF17);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.fillRoundRect(box_x+15, lft_blck_y+7, box_x+10, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+80, lft_blck_y+27); 
  tft.print(heading,1);
 }

  if((screen_id==2 && k==0) || (screen_id==4 && k==0)){
 
       while(SerialGPS.available() > 0){ // check for gps data
       if(gps.encode(SerialGPS.read())){ // encode gps data
       if(counter > 2) {
        Serial.print("SATS: ");
        Serial.println(gps.satellites.value());
        Serial.print("LAT: ");
        Serial.println(gps.location.lat(), 6);
        Serial.print("LONG: ");
        Serial.println(gps.location.lng(), 6);
        Serial.print("ALT: ");
        Serial.println(gps.altitude.meters()); 
        
        Serial.print("Date: ");
        Serial.print(gps.date.day()); Serial.print("/");
        Serial.print(gps.date.month()); Serial.print("/");
        Serial.println(gps.date.year());
    
        Serial.print("Hour: ");
        Serial.print(gps.time.hour()); Serial.print(":");
        Serial.print(gps.time.minute()); Serial.print(":");
        Serial.println(gps.time.second());
     tft.fillRoundRect(rght_blck_x+58, rght_blck_y+50, box_x+20, box_y*2+9,3, BLACK);  
     tft.setFreeFont(FF33);
       tft.setTextColor(  WHITE,BLACK);
     tft.setCursor(rght_blck_x+64,A);
     tft.print(gps.location.lat(), 6);
   
     tft.setCursor(rght_blck_x+64, B);
     tft.print(gps.location.lng(), 6);
   
       Serial.println("---------------------------");
        counter = 0;
       }
       else counter++;
       }
      }
      
 }  Serial.print("ekr=");
   Serial.println(screen_id);
      Serial.print("k=");
      Serial.print(k);
   delay(100);
    tft.setFreeFont(FF33);
    uint16_t t_x = 0, t_y = 0; // To store the touch coordinates
    // Pressed will be set true is there is a valid touch on the screen
    boolean pressed = tft.getTouch(&t_x, &t_y); 
  
    on_btn.press(pressed && on_btn.contains(t_x, t_y));
    off_btn.press(pressed && off_btn.contains(t_x, t_y));

        if (off_btn.justReleased())
            off_btn.drawButton();

        if (on_btn.justReleased())
            on_btn.drawButton();
        
    if (on_btn.justPressed() ) {
        on_btn.drawButton(true);
           
         screen_id--;
         k=1; 
         }


    if (off_btn.justPressed()) {
        off_btn.drawButton(true);     
        
        screen_id++;
        k=1;
    }

    if(screen_id==0 && k==1) mainpage();
    if(screen_id==1 && k==1)rocketpage();
    if(screen_id==2 && k==1)grndstn();
    if(screen_id==3 && k==1)baboon();
    if(screen_id==4 && k==1)baboon_box();
    if(screen_id==5 && k==1)muho();
    if(screen_id==6 && k==1)appcent();
    if(screen_id==7 && k==1)appcentt();
    if(screen_id==8 && k==1)eagle();

    if(screen_id==-1)screen_id=8;
    if(screen_id==9)screen_id=0;

}
void eagle(){  
   k=0;    
   drawSdJpeg("/EagleEye.jpg", 0, 0);  // This draws a jpeg pulled off the SD Card
    on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);

  }

void appcentt(){  
   k=0;    
   drawSdJpeg("/appcentt.jpg", 0, 0);  // This draws a jpeg pulled off the SD Card
    on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);

  }
  
void appcent(){  
   k=0;    
   drawSdJpeg("/appcent.jpg", 0, 0);  // This draws a jpeg pulled off the SD Card
    on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);

  }

void muho(){  
   k=0;    
   drawSdJpeg("/muho.jpg", 0, 0);  // This draws a jpeg pulled off the SD Card
    on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);

  }

void baboon_box(){
    k=0;
    
   drawSdJpeg("/Baboon40.jpg", 0, 0);  // This draws a jpeg pulled off the SD Card
   tft.fillRoundRect(rght_blck_x+4, rght_blck_y+4, 142, 38,2, WHITE);           //1. satır kutu altlığı
 tft.fillRoundRect(rght_blck_x+7, rght_blck_y+7, box_x+15, box_y,3, BLACK);      //ilk kutucuk
 tft.fillRoundRect(rght_blck_x+93, rght_blck_y+7, box_x-15, box_y,3, BLACK);          //ikinci kutucuk

 tft.fillRoundRect(rght_blck_x+4, rght_blck_y+47, 142, 79,2, WHITE);           //2. satır kutu altlığı
 tft.fillRoundRect(rght_blck_x+7, rght_blck_y+50, box_x-20, box_y*2+9,3, BLACK);      //ilk kutucuk
 tft.fillRoundRect(rght_blck_x+58, rght_blck_y+50, box_x+20, box_y*2+9,3, BLACK);          //ikinci kutucuk
 tft.fillRoundRect(2, 0, 316, 36,7, WHITE); 
 tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(25, 23);
  tft.print("MUHARREMIN ISTEDIGI SAYFA");


  tft.setFreeFont(FSS9);
  tft.setTextSize(1);
  tft.setTextColor(  WHITE,BLACK);
  tft.setCursor(rght_blck_x+95, rght_blck_y+27);
  tft.print("TEMP.");

  
  tft.setFreeFont(FF1);
  tft.setTextSize(1);
  tft.setTextColor(  WHITE,BLACK);
  tft.setCursor(rght_blck_x+8, rght_blck_y+27);
  tft.print("TEMP");

   tft.setFreeFont(FF9);
  tft.setTextSize(1);
  tft.setTextColor( WHITE,BLACK);
  tft.setCursor(rght_blck_x+11, rght_blck_y+89);
  tft.print("GPS");

  tft.fillRoundRect(lft_blck_x+4, lft_blck_y+4, 152, 38,2, BLACK );           //2. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+7, lft_blck_y+7, box_x, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+15, lft_blck_y+7, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+4, lft_blck_y+47, 152, 38,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+7, lft_blck_y+50, box_x, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+15, lft_blck_y+50, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+4, lft_blck_y+89, 152, 38,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+7, lft_blck_y+92, box_x, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+15, lft_blck_y+92, box_x+10, box_y,3,WHITE);          //ikinci kutucuk
tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+27);
  tft.print("COMP.");


  tft.setFreeFont(FMB9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+69);
  tft.print("ALT.");

  tft.setFreeFont(FM9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+80, lft_blck_y+69);
  tft.print("ALT.");

  tft.setFreeFont(FSSO9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+80, lft_blck_y+111);
  tft.print("PRESS..");

  tft.setFreeFont(FSB9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+111);
  tft.print("PRESS..");

  tft.setFreeFont(FF33);
      on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);
  
  }

void baboon(){
  
  k=0;
   drawSdJpeg("/Baboon40.jpg", 0, 0);  // This draws a jpeg pulled off the SD Card
  
  on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  
  }

void mainpage()
{   
   k=0;
    
     tft.fillScreen(BLACK);
     drawSdJpeg("/spacex.jpg", 0, 0);  // This draws a jpeg pulled off the SD Card
     tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  
     

  }

void grndstn()
{

     k=0; 
   tft.fillScreen(BLACK);
   tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(WHITE, BLACK);
  tft.setCursor(150, 1);
  tft.print("12:30");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");
    tft.fillRoundRect(2, 35, 316, 36,7, WHITE); 
  tft.fillRoundRect(rght_blck_x, rght_blck_y, 150, 133,3, CYAN);
 
 tft.fillRoundRect(rght_blck_x+4, rght_blck_y+4, 142, 38,2, BLACK);           //1. satır kutu altlığı
 tft.fillRoundRect(rght_blck_x+7, rght_blck_y+7, box_x+15, box_y,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(rght_blck_x+93, rght_blck_y+7, box_x-15, box_y,3, WHITE);          //ikinci kutucuk

 tft.fillRoundRect(rght_blck_x+4, rght_blck_y+47, 142, 79,2, BLACK);           //2. satır kutu altlığı
 tft.fillRoundRect(rght_blck_x+7, rght_blck_y+50, box_x-20, box_y*2+9,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(rght_blck_x+58, rght_blck_y+50, box_x+20, box_y*2+9,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(80, 58);
  tft.print("GROUND STATION");


  tft.setFreeFont(FSS9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+95, rght_blck_y+27);
  tft.print("TEMP.");

  
  tft.setFreeFont(FF1);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+8, rght_blck_y+27);
  tft.print("TEMP");

   tft.setFreeFont(FF9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+89);
  tft.print("GPS");
  tft.setFreeFont(FF33);
      on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);


  
  }

  
void rocketpage()
{k=0;


 tft.fillScreen(BLACK);
 tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
   tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  tft.setCursor(150, 1);
  tft.print("12:30");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");
 tft.fillRoundRect(2, 23, 316, 26,7, WHITE);               //başlık
 tft.fillRoundRect(lft_blck_x, lft_blck_y, 160, 133,3, CYAN);               //sol blok

   tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(130, 40);
  tft.print("ROCKET");

tft.fillRoundRect(lft_blck_x+4, lft_blck_y+4, 152, 38,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+7, lft_blck_y+7, box_x, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+15, lft_blck_y+7, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+4, lft_blck_y+47, 152, 38,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+7, lft_blck_y+50, box_x, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+15, lft_blck_y+50, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+4, lft_blck_y+89, 152, 38,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+7, lft_blck_y+92, box_x, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+15, lft_blck_y+92, box_x+10, box_y,3, WHITE);          //ikinci kutucuk
tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+27);
  tft.print("COMPASS");


  tft.setFreeFont(FMB9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+69);
  tft.print("ALT.");

  tft.setFreeFont(FM9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+80, lft_blck_y+69);
  tft.print("ALT.");

  tft.setFreeFont(FSSO9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+80, lft_blck_y+111);
  tft.print("PRESS..");

  tft.setFreeFont(FSB9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+111);
  tft.print("PRESS..");
// tft.setFreeFont(FF9);
tft.setFreeFont(FF33);
        on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);

  }

void showTime(uint32_t msTime) {
  //tft.setCursor(0, 0);
  //tft.setTextFont(1);
  //tft.setTextSize(2);
  //tft.setTextColor(TFT_WHITE, TFT_BLACK);
  //tft.print(F(" JPEG drawn in "));
  //tft.print(msTime);
  //tft.println(F(" ms "));
  Serial.print(F(" JPEG drawn in "));
  Serial.print(msTime);
  Serial.println(F(" ms "));
}

void drawSdJpeg(const char *filename, int xpos, int ypos) {

  // Open the named file (the Jpeg decoder library will close it)
  File jpegFile = SD.open( filename, FILE_READ);  // or, file handle reference for SD library
 
  if ( !jpegFile ) {
   // Serial.print("ERROR: File \""); Serial.print(filename); Serial.println ("\" not found!");
    return;
  }

///  Serial.println("===========================");
///  Serial.print("Drawing file: "); Serial.println(filename);
///  Serial.println("===========================");

  // Use one of the following methods to initialise the decoder:
  boolean decoded = JpegDec.decodeSdFile(jpegFile);  // Pass the SD file handle to the decoder,
  //boolean decoded = JpegDec.decodeSdFile(filename);  // or pass the filename (String or character array)

  if (decoded) {
    // print information about the image to the serial port
    jpegInfo();
    // render the image onto the screen at given coordinates
    jpegRender(xpos, ypos);
  }
  else {
   // Serial.println("Jpeg file format not supported!");
  }
}

//####################################################################################################
// Draw a JPEG on the TFT, images will be cropped on the right/bottom sides if they do not fit
//####################################################################################################
// This function assumes xpos,ypos is a valid screen coordinate. For convenience images that do not
// fit totally on the screen are cropped to the nearest MCU size and may leave right/bottom borders.
void jpegRender(int xpos, int ypos) {

  //jpegInfo(); // Print information from the JPEG file (could comment this line out)

  uint16_t *pImg;
  uint16_t mcu_w = JpegDec.MCUWidth;
  uint16_t mcu_h = JpegDec.MCUHeight;
  uint32_t max_x = JpegDec.width;
  uint32_t max_y = JpegDec.height;

  bool swapBytes = tft.getSwapBytes();
  tft.setSwapBytes(true);
  
  // Jpeg images are draw as a set of image block (tiles) called Minimum Coding Units (MCUs)
  // Typically these MCUs are 16x16 pixel blocks
  // Determine the width and height of the right and bottom edge image blocks
  uint32_t min_w = jpg_min(mcu_w, max_x % mcu_w);
  uint32_t min_h = jpg_min(mcu_h, max_y % mcu_h);

  // save the current image block size
  uint32_t win_w = mcu_w;
  uint32_t win_h = mcu_h;

  // record the current time so we can measure how long it takes to draw an image
  uint32_t drawTime = millis();

  // save the coordinate of the right and bottom edges to assist image cropping
  // to the screen size
  max_x += xpos;
  max_y += ypos;

  // Fetch data from the file, decode and display
  while (JpegDec.read()) {    // While there is more data in the file
    pImg = JpegDec.pImage ;   // Decode a MCU (Minimum Coding Unit, typically a 8x8 or 16x16 pixel block)

    // Calculate coordinates of top left corner of current MCU
    int mcu_x = JpegDec.MCUx * mcu_w + xpos;
    int mcu_y = JpegDec.MCUy * mcu_h + ypos;

    // check if the image block size needs to be changed for the right edge
    if (mcu_x + mcu_w <= max_x) win_w = mcu_w;
    else win_w = min_w;

    // check if the image block size needs to be changed for the bottom edge
    if (mcu_y + mcu_h <= max_y) win_h = mcu_h;
    else win_h = min_h;

    // copy pixels into a contiguous block
    if (win_w != mcu_w)
    {
      uint16_t *cImg;
      int p = 0;
      cImg = pImg + win_w;
      for (int h = 1; h < win_h; h++)
      {
        p += mcu_w;
        for (int w = 0; w < win_w; w++)
        {
          *cImg = *(pImg + w + p);
          cImg++;
        }
      }
    }

    // calculate how many pixels must be drawn
    uint32_t mcu_pixels = win_w * win_h;

    // draw image MCU block only if it will fit on the screen
    if (( mcu_x + win_w ) <= tft.width() && ( mcu_y + win_h ) <= tft.height())
      tft.pushImage(mcu_x, mcu_y, win_w, win_h, pImg);
    else if ( (mcu_y + win_h) >= tft.height())
      JpegDec.abort(); // Image has run off bottom of screen so abort decoding
  }

  tft.setSwapBytes(swapBytes);

  showTime(millis() - drawTime); // These lines are for sketch testing only
}

//####################################################################################################
// Print image information to the serial port (optional)
//####################################################################################################
// JpegDec.decodeFile(...) or JpegDec.decodeArray(...) must be called before this info is available!
void jpegInfo() {

  // Print information extracted from the JPEG file
  Serial.println("JPEG image info");
  Serial.println("===============");
  Serial.print("Width      :");
  Serial.println(JpegDec.width);
  Serial.print("Height     :");
  Serial.println(JpegDec.height);
  Serial.print("Components :");
  Serial.println(JpegDec.comps);
  Serial.print("MCU / row  :");
  Serial.println(JpegDec.MCUSPerRow);
  Serial.print("MCU / col  :");
  Serial.println(JpegDec.MCUSPerCol);
  Serial.print("Scan type  :");
  Serial.println(JpegDec.scanType);
  Serial.print("MCU width  :");
  Serial.println(JpegDec.MCUWidth);
  Serial.print("MCU height :");
  Serial.println(JpegDec.MCUHeight);
  Serial.println("===============");
  Serial.println("");
}

void touch_calibrate()
{
  uint16_t calData[5];
  uint8_t calDataOK = 0;

  // check file system exists
  if (!SPIFFS.begin()) {
    Serial.println("Formating file system");
    SPIFFS.format();
    SPIFFS.begin();
  }

  // check if calibration file exists and size is correct
  if (SPIFFS.exists(CALIBRATION_FILE)) {
    if (REPEAT_CAL)
    {
      // Delete if we want to re-calibrate
      SPIFFS.remove(CALIBRATION_FILE);
    }
    else
    {
      File f = SPIFFS.open(CALIBRATION_FILE, "r");
      if (f) {
        if (f.readBytes((char *)calData, 14) == 14)
          calDataOK = 1;
        f.close();
      }
    }
  }

  if (calDataOK && !REPEAT_CAL) {
    // calibration data valid
    tft.setTouch(calData);
  } else {
    // data not valid so recalibrate
    tft.fillScreen(TFT_BLACK);
    tft.setCursor(20, 0);
    tft.setTextFont(2);
    tft.setTextSize(1);
    tft.setTextColor(TFT_WHITE, TFT_BLACK);

    tft.println("Touch corners as indicated");

    tft.setTextFont(1);
    tft.println();

    if (REPEAT_CAL) {
      tft.setTextColor(TFT_RED, TFT_BLACK);
      tft.println("Set REPEAT_CAL to false to stop this running again!");
    }

    tft.calibrateTouch(calData, TFT_MAGENTA, TFT_BLACK, 15);

    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.println("Calibration complete!");

    // store data
    File f = SPIFFS.open(CALIBRATION_FILE, "w");
    if (f) {
      f.write((const unsigned char *)calData, 14);
      f.close();
    }
  }
}
