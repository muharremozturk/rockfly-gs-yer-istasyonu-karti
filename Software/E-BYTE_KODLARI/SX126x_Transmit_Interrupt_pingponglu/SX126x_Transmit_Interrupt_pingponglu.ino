/*
   RadioLib SX126x Transmit with Interrupts Example

   This example transmits LoRa packets with one second delays
   between them. Each packet contains up to 256 bytes
   of data, in the form of:
    - Arduino String
    - null-terminated char array (C-string)
    - arbitrary binary data (byte array)

   Other modules from SX126x family can also be used.

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>
#include "SPI.h"
SPIClass SPI2(HSPI);

// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  17
// NRST pin:  27
// BUSY pin:  39
SX1262 radio = new Module(15, 17, 27, 39,SPI2);

TaskHandle_t Task1;
#include <Wire.h>
#include <LSM303.h>

LSM303 compass;

// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;

// save transmission state between loops
int transmissionState = ERR_NONE;


struct Data{
byte byteArray[76];
byte tr_array[16];
int counter=0;
};
  Data data;


  
// flag to indicate that a packet was sent
bool transmitFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// flag to indicate that a packet was sent or received
volatile bool operationDone = false;

// this function is called when a complete packet
// is transmitted or received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if(!enableInterrupt) {
    return;
  }

  // we sent aor received  packet, set the flag
  operationDone = true;
}

int dataa[2];
float H3LIS331_data[4];
float Ms5611_data[2];
float bnoAccel[3];
float bnoGyro[3];
float bnoMag[3];
float bnoRaw[3];
long GPS_data[3];
byte SIV;
long unix_time;

void setup() {
  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;  
  const int HSPI_CS = 15;

  SPI2.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS); 
  
  delay(1000);
  Serial.begin(115200);
pinMode(26,OUTPUT);

  
  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  
  int state = radio.begin(868.0, 500.0, 7, 5, 0x34, 20);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }

  // set the function that will be called
  // when packet transmission is finished
  radio.setDio1Action(setFlag);

 xTaskCreatePinnedToCore(
                    Task1code,   /* Task function. */
                    "Task1",     /* name of task. */
                    10000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task1,      /* Task handle to keep track of created task */
                    0);          /* pin task to core 0 */                  
  delay(100); 
  
    floatAddByte(0.49,&data.counter,data.byteArray);
    floatAddByte(-0.49,&data.counter,data.byteArray);
    floatAddByte(1.22,&data.counter,data.byteArray);

    floatAddByte(36.52,&data.counter,data.byteArray);
    floatAddByte(996.33,&data.counter,data.byteArray);

    floatAddByte(-0.46,&data.counter,data.byteArray);
    floatAddByte(-0.14,&data.counter,data.byteArray);
    floatAddByte(-9.66,&data.counter,data.byteArray);
    floatAddByte(-0.00,&data.counter,data.byteArray);
    floatAddByte(0.00,&data.counter,data.byteArray);
    floatAddByte(0.00,&data.counter,data.byteArray);
    floatAddByte(-25.69,&data.counter,data.byteArray);
    floatAddByte(-35.19,&data.counter,data.byteArray);
    floatAddByte(12.44,&data.counter,data.byteArray);
    floatAddByte(-179.00,&data.counter,data.byteArray);
    floatAddByte(2.93,&data.counter,data.byteArray);
    floatAddByte(-33.18,&data.counter,data.byteArray);

    longAddByte(-118334965,&data.counter,data.byteArray);
    longAddByte(95731051,&data.counter,data.byteArray);
    //longAddByte(892087,&data.counter,data.byteArray);
    //byteAddByte(0,&data.counter,data.byteArray);
    //longAddByte(2021824,&data.counter,data.byteArray);
   // longAddByte(14564,&data.counter,data.byteArray);


#if defined(INITIATING_NODE)
    // send the first packet on this node
    Serial.print(F("[SX1262] Sending first packet ... "));
    transmissionState = radio.startTransmit(data.byteArray,76);
    transmitFlag = true;
  #else
    // start listening for LoRa packets on this node
    Serial.print(F("[SX1262] Starting to listen ... "));
    state = radio.startReceive();
    if (state == ERR_NONE) {
      Serial.println(F("success!"));
    } else {
      Serial.print(F("failed, code "));
      Serial.println(state);
      while (true);
    }
  #endif

   
}

int c;

void Task1code( void * pvParameters ){
 // Serial.print("Task1 running on core ");
 // Serial.println(xPortGetCoreID());
  
  for(;;){
    vTaskDelay(3);

  // check if the previous transmission finished
    if(operationDone) {
    // disable the interrupt service routine while
    // processing the data
    enableInterrupt = false;

    // reset flag
    operationDone = false;

      if(transmitFlag) {


      // the previous operation was transmission, listen for response
      // print the result
      if (transmissionState == ERR_NONE) {
        // packet was successfully sent
        Serial.println(F("transmission finished!"));
  
      } else {
        Serial.print(F("failed, code "));
        Serial.println(transmissionState);
  
      }

      // listen for response
      radio.startReceive();
      transmitFlag = false;
      Serial.println("bekliyor");
      
    }

    else{
    Serial.println("alım başlayacak");
    int state = radio.readData(data.tr_array,16);
    
    

    if (state == ERR_NONE) {
      // packet was successfully received
    //  Serial.println(F("[SX1262] Received packet!"));
     
c=0;
H3LIS331_data[0]= ByteToByte(data.tr_array,c);

c+=1;
H3LIS331_data[1]= ByteToByte(data.tr_array,c);

c+=1;
H3LIS331_data[2]= ByteToFloat(data.tr_array,c);

c+=4;
H3LIS331_data[3]= ByteToFloat(data.tr_array,c);

c+=4;
H3LIS331_data[4]= ByteToFloat(data.tr_array,c);

c+=4;
H3LIS331_data[5]= ByteTouint8t(data.tr_array,c);


Serial.print("passw ");
Serial.println(H3LIS331_data[0]);

Serial.print("config ");
Serial.println(H3LIS331_data[1]);

Serial.print("APOGEE ");
Serial.println(H3LIS331_data[2]);

Serial.print("MAİN ");
Serial.println(H3LIS331_data[3]);

Serial.print("freakans ");
Serial.println(H3LIS331_data[4]);

Serial.print("DBM ");
Serial.println(H3LIS331_data[5]);
 
    }
      Serial.print(F("[SX1262] Sending another packet ... "));
      transmissionState = radio.startTransmit(data.byteArray,76);
      transmitFlag = true;
    
}
    // put module back to listen mode
    
    

    // we're ready to receive more packets,
    // enable interrupt service routine
    
    enableInterrupt = true;
  
  
    }
     
   
 
  } 
}


void loop() {

  
  
}

uint8_t ByteTouint8t(byte *byterray,int ca)
{
  uint8_t f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

float ByteToFloat(byte *byterray,int ca)
{
  float f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

  long ByteTolong(byte *byterray,int ca)
{
  long f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }
    int ByteToint(byte *byterray,int ca)
{
  int f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

    byte ByteToByte(byte *byterray,int ca)
{
  byte f;
  ((uint8_t*)&f)[0]=byterray[ca];
  
return f;
  }



void floatAddByte(float data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void intAddByte(int data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}
