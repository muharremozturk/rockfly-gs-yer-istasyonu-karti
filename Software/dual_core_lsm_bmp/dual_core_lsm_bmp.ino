#include <JPEGDecoder.h>
#include <Wire.h>
#include <LSM303.h>
#include <Adafruit_BMP280.h>
Adafruit_BMP280 bmp;
LSM303 compass;
#include "SPIFFS.h" 
#include <SPI.h>
#include "FS.h"
#include "Free_Fonts.h"
#include <TFT_eSPI.h>
TFT_eSPI tft = TFT_eSPI();

int screen_id,k,i,apogee,main,kts,t,v1,d,v2,u;
uint32_t a,b;
char *unit[] ={"m","i","M","m/s","km/h","C","ft","ft/s","mph","F"}; 
float my_temp,my_alt,rckt_temp,rckt_alt,rckt_v1,rckt_v2,rckt_dist;

TaskHandle_t Task1;
TaskHandle_t Task2;
float heading;
float headingg;

#include <TinyGPS++.h>
#include <HardwareSerial.h>
    long lat,lon; 
    int counterr = 0;     
    
    TinyGPSPlus gps;
 HardwareSerial SerialGPS(1);  
 
#define lft_blck_x    0
#define lft_blck_y    22

#define rght_blck_x    164
#define rght_blck_y    22

#define box_y    15
#define box_x    65


#define BLACK       0x0000      /*   0,   0,   0 */
#define NAVY        0x000F      /*   0,   0, 128 */
#define DARKGREEN   0x03E0      /*   0, 128,   0 */
#define DARKCYAN    0x03EF      /*   0, 128, 128 */
#define MAROON      0x7800      /* 128,   0,   0 */
#define PURPLE      0x780F      /* 128,   0, 128 */
#define OLIVE       0x7BE0      /* 128, 128,   0 */
#define LIGHTGREY   0xD69A      /* 211, 211, 211 */
#define DARKGREY    0x7BEF      /* 128, 128, 128 */
#define BLUE        0x001F      /*   0,   0, 255 */
#define GREEN       0x07E0      /*   0, 255,   0 */
#define CYAN        0x07FF      /*   0, 255, 255 */
#define RED         0xF800      /* 255,   0,   0 */
#define MAGENTA     0xF81F      /* 255,   0, 255 */
#define YELLOW      0xFFE0      /* 255, 255,   0 */
#define WHITE       0xFFFF      /* 255, 255, 255 */
#define ORANGE      0xFDA0      /* 255, 180,   0 */
#define GREENYELLOW 0xB7E0      /* 180, 255,   0 */
#define PINK        0xFE19      /* 255, 192, 203 */    
#define BROWN       0x9A60      /* 150,  75,   0 */
#define GOLD        0xFEA0      /* 255, 215,   0 */
#define SILVER      0xC618      /* 192, 192, 192 */
#define SKYBLUE     0x867D      /* 135, 206, 235 */
#define VIOLET      0x915C      /* 180,  46, 226 */
#define GRAY        0xC618

#define CALIBRATION_FILE "/TouchCalData2"

// Set REPEAT_CAL to true instead of false to run calibration
// again, otherwise it will only be done once.
// Repeat calibration if you change the screen rotation.
#define REPEAT_CAL false
TFT_eSPI_Button on_btn,off_btn,minus_btn,plus_btn,snd_btn,save_btn,one_btn,ten_btn,hundred_btn,fivehundred_btn,imprl_btn,mtrc_btn;



#include"CC1200.h"        // TI CC1200 RF Radio

//#define MODE // Define this for TX, otherwise code is RX

// Node Addresses
#ifdef MODE
#define THIS_NODE 0x01
#define TARG_NODE 0x02
#else
#define THIS_NODE 0x02
#define TARG_NODE 0x02
#endif

// CC1200 Radio Interrupt Pin
#define RadioTXRXpin  27  // CC1200 Packet Semaphore 


// BUFFER
#define FIFO_SIZE   64    // TX and RX Buffer Size
byte readBytes; // Number of bytes read from RX FIFO
byte rxBuffer[FIFO_SIZE]; // Fill with RX FIFO
byte writeBytes; // Number of bytes written to TX FIFO
byte txBuffer[FIFO_SIZE]; // Fill with TX FIFO

#define idxLength   0   // Length index
#define idxTargetNode 1   // TargetNode index
#define idxFrameCount 3   // FrameCount index
#define TIMEOUT         50  // TIMEOUT


union DoubleBytes
{
  double DoubleVal;
  byte AsBytes[sizeof(double)]; // 4 Bytes
} DoubleBytes;

String str = "Hello World!";
char arr[23];


// GLOBAL VARIABLES
byte counter = 0x00;
volatile bool packetSemaphore; // RX/TX success flag. Volatile prevents undesired optimizations by the compiler

// Set Packet Semaphore 
void setSemaphore() {
  packetSemaphore = true;
}

// Clear Packet Semaphore 
void clearSemaphore() {
  packetSemaphore = false;
}

// Try receiving incoming, if any, within the TOUT duration.
bool TryReceive(unsigned long qTimeout) {
  bool receiveStatus = false;
  byte marcstate = 0x00;
  Serial.println("Wait Reception...");

  do
  {
    marcstate = cc1200.GetStat(StatType::MARC_STATE, 0x1F); // MARC_STATE[4:0]
    if (marcstate == MARC_STATE_RX_FIFO_ERR)
    {
      cc1200.FlushRxFifo(); delay(1);
      cc1200.Receive(); delay(3);
      //Serial.println("\tTry RX.");
      Serial.println("\tRXFIFO Flushed!");
    }
    else if (marcstate == MARC_STATE_TX_FIFO_ERR)
    {
      cc1200.FlushTxFifo(); delay(1);
      Serial.println("\tTXFIFO Flushed!");
    }
    else if (marcstate >= MARC_STATE_XOFF && marcstate <= MARC_STATE_ENDCAL)
    {
      Serial.print("\tSettling: "); Serial.println(marcstate);
    }
    else if (marcstate != MARC_STATE_RX)
    {
      //Serial.println("\tTry RX.");
      cc1200.Receive(); delay(7);
    }

    // Timeout Implementation
    if (millis() >= qTimeout) //isTimeOut(qTimeout)
    {
      Serial.println("\tTOUT!");
      cc1200.Idle();
      break;
    }

    delay(10);
  } while (!packetSemaphore);

  // Packet received i.e. no timeout occurred
  if (packetSemaphore)
  {
    readBytes = cc1200.ReadRxFifo(rxBuffer);

    if (readBytes >= 0) // Fail-Safe. 2 bytes header (Len-Adrs) + 2 appended bytes (CRC-RSSI) footer
    {
      receiveStatus = true;
    }

    cc1200.Idle(); delay(1);
    cc1200.FlushRxFifo();
    clearSemaphore();
  }

  return receiveStatus;
}

String yazi = "";

void setup() {
  
 Serial.begin(9600); 
 screen_id=0;
k=1;
i=0;
kts=1;
 SerialGPS.begin(9600, SERIAL_8N1, 16, 34);
 Wire.begin();
   tft.init();
  compass.init();
  compass.enableDefault();
   compass.m_min = (LSM303::vector<int16_t>){  -598,   -810,   -506};
   compass.m_max = (LSM303::vector<int16_t>){  +376,   +255,   +493};
pinMode( 26, OUTPUT);
 if (!bmp.begin()) {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring or "
                      "try a different address!"));
    while (1) delay(10);
  }

  /* Default settings from datasheet. */
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
   
   xTaskCreatePinnedToCore(
                    Task1code,   /* Task function. */
                    "Task1",     /* name of task. */
                    10000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task1,      /* Task handle to keep track of created task */
                    0);          /* pin task to core 0 */                  
  delay(100); 

  //create a task that will be executed in the Task2code() function, with priority 1 and executed on core 1
  xTaskCreatePinnedToCore(
                    Task2code,   /* Task function. */
                    "Task2",     /* name of task. */
                    10000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task2,      /* Task handle to keep track of created task */
                    1);          /* pin task to core 1 */
    delay(100); 
 if (!SD.begin()) {
   // Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();

  if (cardType == CARD_NONE) {
//Serial.println("No SD card attached");
    return;
  }

 // Serial.print("SD Card Type: ");
  if (cardType == CARD_MMC) {
 //   Serial.println("MMC");
  } else if (cardType == CARD_SD) {
    Serial.println("SDSC");
  } else if (cardType == CARD_SDHC) {
    Serial.println("SDHC");
  } else {
    Serial.println("UNKNOWN");
  }

  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
 //// Serial.printf("SD Card Size: %lluMB\n", cardSize);
    tft.init();
 tft.fillScreen(BLACK);
 touch_calibrate();
 tft.setTextSize(1);

  uint8_t MISO_PIN = 12;
  uint8_t MOSI_PIN = 13;
  uint8_t SCK_PIN = 14;  
  uint8_t SS_PIN = 15;
  // CC1200 RADIO
  cc1200.Init( SS_PIN,  MOSI_PIN,  MISO_PIN, SCK_PIN, PIN_UNUSED); // SS, MOSI, MISO, SCK, RadioResetpin
  cc1200.Configure(rxSniffSettings, rxSniffSettLen); // 2sec internal delay
  cc1200.SetAddress(THIS_NODE); delay(10);
  cc1200.Strobe(CC120X_SCAL); delay(1000);
  byte readNode = cc1200.GetAddress(false);
  Serial.print("\tNode: "); Serial.println(readNode);
  if (THIS_NODE == readNode)
  {
        Serial.print("\tNode "); Serial.print(THIS_NODE); Serial.println(" Ok!");
  }
    else{
        Serial.print("\tERROR \n");
        while (true);
    }
  cc1200.FlushRxFifo(); delay(100);
  cc1200.FlushTxFifo(); delay(100);
  Serial.println("\tRadio Config");

  // RADIO INTERRUPT
  packetSemaphore = false;
  pinMode(RadioTXRXpin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(RadioTXRXpin), setSemaphore, FALLING); // LOW, CHANGE, RISING, FALLING
  Serial.println("\tPacket Interrupt Config");

    Serial.flush();

    writeBytes = 4 + 19; // idxFrameCount+1
    txBuffer[idxLength] = writeBytes;
    txBuffer[idxTargetNode] = TARG_NODE;
    txBuffer[idxFrameCount] = counter;
    str.toCharArray(arr, 23);
 
}



void Task1code( void * pvParameters ){
  Serial.print("Task1 running on core ");
  Serial.println(xPortGetCoreID());
  
  for(;;){
    vTaskDelay(3);
   /*  digitalWrite( 26, HIGH);
     vTaskDelay(10000);
       digitalWrite( 26, LOW);
     vTaskDelay(1000);*/
if (TryReceive(TIMEOUT + millis()))
{
    Serial.print(F("RX: "));
    for (int i = 0; i < readBytes; i++)
    {
        Serial.print(rxBuffer[i]); Serial.print(F(", "));
    }
    Serial.println(F(""));
    // DoubleBytes.AsBytes[0] = rxBuffer[4];
    // DoubleBytes.AsBytes[1] = rxBuffer[5];
    // DoubleBytes.AsBytes[2] = rxBuffer[6];
    // DoubleBytes.AsBytes[3] = rxBuffer[7];
    // Serial.print(F("Double: ")); Serial.println(DoubleBytes.DoubleVal);
    Serial.print(F("\tText: "));
      yazi = "";
    for (int i = 0; i < 19; i++)
    {
    
         Serial.print(char(rxBuffer[4+i]));
         yazi+=char(rxBuffer[4+i]);
    }

    Serial.print(F("\n"));
}
     
   

    
  } 
}

void Task2code( void * pvParameters ){
  Serial.print("Task2 running on core ");
  Serial.println(xPortGetCoreID());

  for(;;){
       vTaskDelay(1);
    }
 
  }

void loop() {
  //vTaskDelay(2);  
 tft.setRotation(1);  
 compass.read();
 heading = compass.heading();
  //Serial.println(heading);
 /*  Serial.print(F("Approx altitude = "));
    Serial.print(bmp.readAltitude(1017)); /* Adjusted to local forecast! */
 //   Serial.println(" m");

    my_temp= 36;
my_alt = 940;

rckt_temp= 5;
rckt_alt = 3500;
rckt_v1= 140;
rckt_v2= 504;
rckt_dist=2000;

if(u==1){

my_temp= ((my_temp)*1.8)+32;
my_alt = ((my_alt)*3.2808);

rckt_temp= ((rckt_temp)*1.8)+32;
rckt_alt = ((rckt_alt)*3.2808);
rckt_v1= ((rckt_v1)*3.2808);
rckt_v2= ((rckt_v2)/1.609);
rckt_dist=((rckt_dist)*3.2808);

}

  if((screen_id==2  && k==0)){
  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.fillRoundRect(box_x+12, lft_blck_y+4, box_x+10, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+85, lft_blck_y+8); 
  tft.print(bmp.readAltitude(1017));
  tft.print(" ");
    tft.print(unit[t]);
 }

  if((screen_id==4 && k==0) ){
 
       while(SerialGPS.available() > 0){ // check for gps data
       if(gps.encode(SerialGPS.read())){ // encode gps data
       if(counter > 2) {
        Serial.print("SATS: ");
        Serial.println(gps.satellites.value());
        Serial.print("LAT: ");
        Serial.println(gps.location.lat(), 6);
        Serial.print("LONG: ");
        Serial.println(gps.location.lng(), 6);
        Serial.print("ALT: ");
        Serial.println(gps.altitude.meters()); 
        
        Serial.print("Date: ");
        Serial.print(gps.date.day()); Serial.print("/");
        Serial.print(gps.date.month()); Serial.print("/");
        Serial.println(gps.date.year());
    
        Serial.print("Hour: ");
        Serial.print(gps.time.hour()); Serial.print(":");
        Serial.print(gps.time.minute()); Serial.print(":");
        Serial.println(gps.time.second());
      tft.fillRoundRect(lft_blck_x+134, lft_blck_y+6, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk
     
     //tft.setFreeFont(FF33);
     tft.setTextColor(  BLACK,WHITE);
     tft.setFreeFont(FM9);
     tft.setCursor(lft_blck_x+140, lft_blck_y+19);
     tft.print(gps.location.lat(), 6);
     tft.fillRoundRect(lft_blck_x+134, lft_blck_y+27, box_x+117, box_y+3,3, WHITE);
     tft.setCursor(lft_blck_x+140, lft_blck_y+40);
     tft.print(gps.location.lng(), 6);
   
       Serial.println("---------------------------");
        counter = 0;
       }
       else counter++;
       }
      }
      
 }

 if(screen_id==4 && k==0){
   tft.setFreeFont(FM9);
   tft.setTextSize(1);
   tft.setTextColor( BLACK,WHITE);
   tft.fillRoundRect(lft_blck_x+134, lft_blck_y+114, box_x+117, box_y+3,3, WHITE);
   tft.setCursor(lft_blck_x+140, lft_blck_y+127);
   tft.print(yazi);
  }
 
    Serial.print("ekr=");
    Serial.println(screen_id);
    Serial.print("k=");
    Serial.print(k);
    Serial.println(" ");
    Serial.print("i=");
    
    Serial.print(i);
    delay(10);
    tft.setFreeFont(FF33);
    uint16_t t_x = 0, t_y = 0; // To store the touch coordinates
    // Pressed will be set true is there is a valid touch on the screen
    boolean pressed = tft.getTouch(&t_x, &t_y); 
  
    on_btn.press(pressed && on_btn.contains(t_x, t_y));
    off_btn.press(pressed && off_btn.contains(t_x, t_y));
    minus_btn.press(pressed && minus_btn.contains(t_x, t_y));
    plus_btn.press(pressed && plus_btn.contains(t_x, t_y));
    snd_btn.press(pressed && snd_btn.contains(t_x, t_y));
    one_btn.press(pressed && one_btn.contains(t_x, t_y));
    ten_btn.press(pressed && ten_btn.contains(t_x, t_y));
    hundred_btn.press(pressed && hundred_btn.contains(t_x, t_y));
    fivehundred_btn.press(pressed && fivehundred_btn.contains(t_x, t_y));
    imprl_btn.press(pressed && imprl_btn.contains(t_x, t_y));
    mtrc_btn.press(pressed && mtrc_btn.contains(t_x, t_y));




if(!(i==2 || i==3)){  
    if (on_btn.justPressed() ) {
        on_btn.drawButton(true);
           
         screen_id--;
         k=1; 
         }

        if (off_btn.justReleased())
            off_btn.drawButton();

        if (on_btn.justReleased())
            on_btn.drawButton();
}
if(!(i==2 || i==3)){
    if (off_btn.justPressed()) {
        off_btn.drawButton(true);     
        screen_id++;
        k=1;
    }
}

if(i==3){

                 if (plus_btn.justReleased()) plus_btn.drawButton();
                 if (minus_btn.justReleased()) minus_btn.drawButton();  

                 if(kts==1) one_btn.drawButton(true);
                     else one_btn.drawButton();
                 if(kts==10) ten_btn.drawButton(true);
                     else ten_btn.drawButton();
                 if(kts==100) hundred_btn.drawButton(true);
                     else  hundred_btn.drawButton();
                 if(kts==500) fivehundred_btn.drawButton(true);
                     else fivehundred_btn.drawButton();



                 if (on_btn.justPressed() ) {
                     on_btn.drawButton(true);      
                     rocketconfig_page();
                    }

                 if (one_btn.justPressed() ) {
                     one_btn.drawButton(true);     
                     kts=1;
                    }    
                 if (ten_btn.justPressed() ) {
                     ten_btn.drawButton(true);      
                     kts=10;
                    } 
                 if (hundred_btn.justPressed() ) {
                     hundred_btn.drawButton(true);      
                     kts=100;
                    } 
                 if (fivehundred_btn.justPressed() ) {
                     fivehundred_btn.drawButton(true);      
                     kts=500;
                    }                                  
 
                 if (plus_btn.justPressed() ){
                     plus_btn.drawButton(true);
                     main = main+kts;
                     tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                     tft.setTextColor( BLACK,WHITE);
                     tft.setCursor(122, 106);
                     tft.print(main);
                     tft.print(" ");
                     tft.print(unit[d]);

                 }

               if (minus_btn.justPressed() ){
                    minus_btn.drawButton(true);
                    main = main-kts;
                    if(main <= 0)main=0;
                    tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                    tft.setTextColor( BLACK,WHITE);
                    tft.setCursor(122, 106);
                    tft.print(main);
                    tft.print(" ");
                    tft.print(unit[d]);
                    
  

                } 

       }

if(i==2){

                      if (plus_btn.justReleased()) plus_btn.drawButton();
                      if (minus_btn.justReleased()) minus_btn.drawButton();  

                      if(kts==1) one_btn.drawButton(true);
                        else one_btn.drawButton();
                     if(kts==10) ten_btn.drawButton(true);
                        else ten_btn.drawButton();
                     if(kts==100) hundred_btn.drawButton(true);
                        else  hundred_btn.drawButton();
                     if(kts==500) fivehundred_btn.drawButton(true);
                        else fivehundred_btn.drawButton();



                       if (on_btn.justPressed() ) {
                          on_btn.drawButton(true);      
                          rocketconfig_page();
                        }

                          if (one_btn.justPressed() ) {
                          one_btn.drawButton(true);     
                          kts=1;
                        }    
                      if (ten_btn.justPressed() ) {
                          ten_btn.drawButton(true);      
                          kts=10;
                        } 
                          if (hundred_btn.justPressed() ) {
                          hundred_btn.drawButton(true);      
                          kts=100;
                        } 
                      if (fivehundred_btn.justPressed() ) {
                          fivehundred_btn.drawButton(true);      
                          kts=500;
                        }                                  

                      if (plus_btn.justPressed() ){
                      plus_btn.drawButton(true);
                      apogee = apogee+kts;
                      tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                      tft.setTextColor( BLACK,WHITE);
                      tft.setCursor(122, 106);
                      tft.print(apogee);
                      tft.print(" ");
                      tft.print(unit[d]);

                    }

                 if (minus_btn.justPressed() ){
                 minus_btn.drawButton(true);
                 apogee = apogee-kts;
                 if(apogee <= 0)apogee=0;
                      tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                      tft.setTextColor( BLACK,WHITE);
                      tft.setCursor(122, 106);
                      tft.print(apogee);
                      tft.print(" ");
                      tft.print(unit[d]);
  

                   }

           }

    if(i==1){


              if((t_x > 120 && t_x < 235) && (t_y > 87 && t_y < 112 ) ){


                   tft.setTextColor( BLACK,a);
                   tft.fillRoundRect(120, 87, 115, 25,3, DARKGREY);
                   tft.setCursor(140, 104);
                   tft.print("ACTIVE");
                   delay(250);
                   tft.fillRoundRect(120, 87, 115, 25,3, WHITE);
                   tft.setCursor(140, 104);
                   tft.print("ACTIVE");
              }
                   if(snd_btn.justReleased()) snd_btn.drawButton();
                   if(snd_btn.justPressed()){
                    snd_btn.drawButton(true);
                    tft.setTextColor( WHITE,BLACK);
                    tft.setCursor(60, 165);
                    tft.print("I will send for you Babe ;)");

               }

              if((t_x > 120 && t_x < 235) && (t_y > 23 && t_y < 48 ) )
             { 
                 tft.fillRoundRect(120, 23, 115, 25,3, DARKGREY);
                 a= DARKGREY;
                 
                 tft.setTextColor( BLACK,a);
                 tft.setCursor(125, 40);
                 tft.print(apogee);
                 tft.print(" ");
                 tft.print(unit[d]);
                 delay(250);
                 apogee_config();

              }
           if((t_x > 120 && t_x < 235) && (t_y > 55 && t_y < 80 ) )
             { 
                 tft.fillRoundRect(120, 55, 115, 25,3, DARKGREY);
                 a= DARKGREY;
                 tft.setTextColor( BLACK,a);
                 tft.setCursor(125, 72);
                 tft.print(main);
                 tft.print(" ");
                 tft.print(unit[d]);
                 delay(250);
                 main_config();

              }

     }  

         if(screen_id==0){
         
            if(imprl_btn.justReleased()) imprl_btn.drawButton();
            if(mtrc_btn.justReleased()) mtrc_btn.drawButton();

             if (imprl_btn.justPressed() ){
                imprl_btn.drawButton(true);
                 u=1;
                 d=6;
                 v1=7;
                 v2=8;
                 t=9;
               }
              if (mtrc_btn.justPressed() ){
                  mtrc_btn.drawButton(true);

                u=0;
                d=2;
                v1=3;
                v2=4;
                t=5;
                }
          
                tft.setFreeFont();
                tft.setCursor(126, 1);
                tft.setTextColor(RED, BLACK);
                tft.print(unit[u]);


            }
    
    if(screen_id==0 && k==1)myconfig_page();
    if(screen_id==1 && k==1)rocketconfig_page();
    if(screen_id==2 && k==1)rocketpage1();
    if(screen_id==3 && k==1)rocketpage2();
    if(screen_id==4 && k==1)grndstn();
    if(screen_id==5 && k==1)flightsttcs_page();
    if(screen_id==6 && k==1)baboon();
    if(screen_id==7 && k==1)mainpage();
    
   

    if(screen_id==-1)screen_id=7;
    if(screen_id==8)screen_id=0;

}


void flightsttcs_page()
{
k=0;
i=6;

 tft.fillScreen(BLACK);
 tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
   tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  tft.setCursor(150, 1);
  tft.print("12:30");
  tft.setCursor(90, 1);
  tft.print("(");
  tft.print("unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");
  
  tft.fillRoundRect(75, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(95, 227);
  tft.print("FLIGT STATICS");
  
 tft.fillRoundRect(lft_blck_x, lft_blck_y, 156, 176,3, CYAN);               //sol blok


tft.fillRoundRect(lft_blck_x+2, lft_blck_y+2, 152, 19,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+4, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+4, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+25, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+25, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+46, 152, 62,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+50, box_x+4, box_y*3+8,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+50, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
//tft.fillRoundRect(lft_blck_x+5, lft_blck_y+69, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+69, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+89, 152, 19,2, BLACK);           //5. satır kutu altlığı
//tft.fillRoundRect(lft_blck_x+5, lft_blck_y+88, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+88, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+114, 152, 60,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+118, box_x*2+16, box_y,3, WHITE);      //ilk kutucuk
//tft.fillRoundRect(box_x+11, lft_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+137, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+137, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+155, 152, 19,2, BLACK);           //8. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+156, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+156, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+8);
  tft.print("FLIGHT TIME");

  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+8);
  tft.print("3:25");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+29);
  tft.print("MAX ALT.");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+29);
  tft.print("3500");
   tft.print(" ");
  tft.print(unit[d]);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+54);
  tft.print(rckt_v2);
   tft.print(" ");
  tft.print(unit[v2]);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);

  tft.setCursor(lft_blck_x+25, lft_blck_y+60);
  tft.print("MAX");
  tft.setCursor(lft_blck_x+14, lft_blck_y+73);
  tft.print("VELOCITY");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+73);
  tft.print("1.51 mach");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+92);
  tft.print("VELOCITY");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+92);
  tft.print(rckt_v1);
   tft.print(" ");
  tft.print(unit[v1]);  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+24, lft_blck_y+122);
  tft.print("LAST GPS LOCATION"); 
/*
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+122);
  tft.print("-179.123456");  */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+141);
  tft.print("GPS LAT");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+141);
  tft.print("-179.123456"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+160);
  tft.print("GPS Lng"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+160);
  tft.print("59.560833");   
//****************************************************************************************

 tft.fillRoundRect(rght_blck_x, rght_blck_y, 156, 176,3, CYAN);               //sağ blok


tft.fillRoundRect(rght_blck_x+2, rght_blck_y+2, 152, 40,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+5, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+5, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+24, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+24, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+45, 152, 41,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+49, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+49, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+67, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+67, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+92, 152, 19,2, BLACK);           //5. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+94, box_x+81, box_y,3, WHITE);      //ilk kutucuk
//tft.fillRoundRect(rght_blck_x+80, rght_blck_y+92, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+114, 152, 60,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+118, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+136, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+136, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+155, 152, 19,2, BLACK);           //8. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+155, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+155, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+9);
  tft.print("APOGEE TIME");
  tft.setCursor(rght_blck_x+85, rght_blck_y+9);
  tft.print("50 sn");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+83, rght_blck_y+8);
  tft.print("SLEEPY MODE");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+28);
  tft.print("DESCENT TIME");
  tft.setCursor(rght_blck_x+85, rght_blck_y+28);
  tft.print("2:45");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+29);
  tft.print("11.1 V");*/
 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7,rght_blck_y+53);
  tft.print("");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+54);
  tft.print("930 mph");*/


  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+71);
  tft.print("");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+73);
  tft.print("0.44 mach");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+30, rght_blck_y+98);
  tft.print("MAX ACCELERATION");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+96);
  tft.print("11.1 V");  */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+122);
  tft.print("ACCEL-X");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+122);
  tft.print("-200 g");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+140);
  tft.print("ACCEL-Y");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+140);
  tft.print("400 g");   

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+159);
  tft.print("ACCEL-Z");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+159);
  tft.print("20 g");   
  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+14, rght_blck_y+138);
  tft.print("GPS Lng");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+138);
  tft.print("59.560833"); */

     tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);

  }

void apogee_config(){

i=2;
tft.fillScreen(BLACK);

  tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(122, 106);
  tft.print(apogee);
  tft.print(" ");
  tft.print(unit[d]);

one_btn.initButton(&tft,  45, 55, 40, 25, WHITE, RED, BLACK, "1", 1);  
ten_btn.initButton(&tft,  125, 55, 40, 25, WHITE, RED, BLACK, "10", 1);
hundred_btn.initButton(&tft,  190, 55, 40, 25, WHITE, RED, BLACK, "100", 1);
fivehundred_btn.initButton(&tft,  270, 55, 40, 25, WHITE, RED, BLACK, "500", 1);
one_btn.drawButton(false); 
ten_btn.drawButton(false); 
hundred_btn.drawButton(false); 
fivehundred_btn.drawButton(false); 

plus_btn.initButton(&tft,  70, 100, 60, 25, WHITE, GREEN, BLACK, "+", 1);
minus_btn.initButton(&tft,  250, 100, 60, 25, WHITE, BLUE, BLACK, "-", 1);
plus_btn.drawButton(false); 
minus_btn.drawButton(false); 



on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 

}

void main_config(){

i=3;
tft.fillScreen(BLACK);

tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(122, 106);
  tft.print(main);
  tft.print(" ");
  tft.print(unit[d]);

one_btn.initButton(&tft,  45, 55, 40, 25, WHITE, RED, BLACK, "1", 1);  
ten_btn.initButton(&tft,  125, 55, 40, 25, WHITE, RED, BLACK, "10", 1);
hundred_btn.initButton(&tft,  190, 55, 40, 25, WHITE, RED, BLACK, "100", 1);
fivehundred_btn.initButton(&tft,  270, 55, 40, 25, WHITE, RED, BLACK, "500", 1);
one_btn.drawButton(false); 
ten_btn.drawButton(false); 
hundred_btn.drawButton(false); 
fivehundred_btn.drawButton(false); 

plus_btn.initButton(&tft,  70, 100, 60, 25, WHITE, GREEN, BLACK, "+", 1);
minus_btn.initButton(&tft,  250, 100, 60, 25, WHITE, BLUE, BLACK, "-", 1);
plus_btn.drawButton(false); 
minus_btn.drawButton(false); 



on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 

}

void myconfig_page(){
  k=0;
  i=6;
  tft.fillScreen(BLACK);
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(WHITE, BLACK);
  tft.setCursor(150, 1);
  tft.print("12:30");
  tft.setCursor(90, 1);
  tft.print("(");
  tft.print("unit=");
  tft.setTextColor(RED, BLACK);
  tft.print(unit[u]);
  tft.setTextColor(WHITE, BLACK);
  tft.print(")");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");

  tft.fillRoundRect(78, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("MY CONFIG PAGE");
  
     tft.setFreeFont(FF33);
     tft.fillRoundRect(16, 23, 290, 25,3, CYAN);  // MEASURİNG UNIT paraşüt kutusu

     imprl_btn.initButton(&tft,  90, 70, 130, 25, WHITE, GREEN, BLACK, "IMPERIAL", 1);
     mtrc_btn.initButton(&tft,  230, 70, 130, 25, WHITE, GREEN, BLACK, "MERTIC", 1);

      tft.fillRoundRect(28, 97, 125, 25,3, CYAN);  // priave key kutusu

     tft.fillRoundRect(174, 97, 115, 25,3, WHITE);  //priave key değer kutucuğu

 
  tft.setCursor(29, 114);
  tft.print("PRIVATE KEY");

  tft.setTextColor( BLACK,a);
  tft.setCursor(193, 114);
  tft.print("978258");
 
     
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  
     imprl_btn.drawButton(false);  
     mtrc_btn.drawButton(false);


  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,CYAN);
  tft.setCursor(75, 40);
  tft.print("MEASURMENT UNIT");

  
  }


void rocketconfig_page(){
    k=0;
    i=1;
  tft.fillScreen(BLACK);
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(WHITE, BLACK);
  tft.setCursor(150, 1);
  tft.print("12:30");
   tft.setCursor(90, 1);
  tft.print("(");
  tft.print("unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");

  tft.fillRoundRect(75, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("ROCKET CONFIG");
  
     tft.setFreeFont(FF33);
     snd_btn.initButton(&tft,  160, 190, 100, 25, WHITE, GREEN, BLACK, "SEND", 1);
     
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  
     snd_btn.drawButton(false);

     tft.fillRoundRect(0, 23, 115, 25,3, CYAN);  // drag paraşüt kutusu
     tft.fillRoundRect(0, 55, 115, 25,3, CYAN);  // main parşüt kutusu
     tft.fillRoundRect(0, 87, 115, 25,3, CYAN);  // roket durum kutusu

     tft.fillRoundRect(120, 23, 115, 25,3, WHITE);  // drag paraşüt değer kutucuğu
     tft.fillRoundRect(120, 55, 115, 25,3, WHITE);  // main paraşüt değer kutucuğu
     tft.fillRoundRect(120, 87, 115, 25,3, WHITE);  //roket durum değer kutucuğu

  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,CYAN);
  tft.setCursor(1, 40);
  tft.print("DRAG PRSHT");

  tft.setCursor(1, 72);
  tft.print("MAIN PRSHT");

  tft.setCursor(25, 104);
  tft.print("STATUS");

  tft.setTextColor( BLACK,a);
  tft.setCursor(135, 104);
  tft.print("IN SLEEP");

  tft.setTextColor( BLACK,a);
  tft.setCursor(125, 40);
  tft.print(apogee);
  tft.print(" ");
  tft.print(unit[d]);

  tft.setTextColor( BLACK,b);
  tft.setCursor(125, 72);
  tft.print(main);
  tft.print(" ");
  tft.print(unit[d]);

  }

void baboon(){
  
  k=0;
  i=6;
   drawSdJpeg("/Baboon40.jpg", 0, 0);  // This draws a jpeg pulled off the SD Card
  
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  

  }

void mainpage()
{   
   k=0;
    i=6;
     tft.fillScreen(BLACK);
     drawSdJpeg("/spacex.jpg", 0, 0);  // This draws a jpeg pulled off the SD Card
     tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  
     
  }

void grndstn()
{
  k=0; 
  i=6;
  tft.fillScreen(BLACK);
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(WHITE, BLACK);
  tft.setCursor(150, 1);
  tft.print("12:30");
   tft.setCursor(90, 1);
  tft.print("(");
  tft.print("unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");
  tft.fillRoundRect(lft_blck_x, lft_blck_y, 320, 159,3, CYAN);
 
 tft.fillRoundRect(lft_blck_x+2, lft_blck_y+3, 316, 66,2, BLACK);           //1. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+6, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+6, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+27, box_x+60, box_y+3,3, WHITE);      //2. SATIR ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+27, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+48, box_x+60, box_y+3,3, WHITE);      //3. SATIR ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+48, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk 


 tft.fillRoundRect(lft_blck_x+2, lft_blck_y+72,316, 84,2, BLACK);           //4. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+74, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+74, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

 //tft.fillRoundRect(lft_blck_x+2, lft_blck_y+68, 196, 62,2, BLACK);           //5. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+94, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+94, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

  //tft.fillRoundRect(lft_blck_x+2, lft_blck_y+68, 196, 62,2, BLACK);           //6. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+114, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+114, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

  //tft.fillRoundRect(lft_blck_x+2, lft_blck_y+68, 196, 62,2, BLACK);           //7. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+134, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+134, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

 /*tft.fillRoundRect(rght_blck_x+4, rght_blck_y+47, 142, 79,2, BLACK);           //2. satır kutu altlığı
 tft.fillRoundRect(rght_blck_x+7, rght_blck_y+50, box_x-20, box_y*2+9,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(rght_blck_x+58, rght_blck_y+50, box_x+20, box_y*2+9,3, WHITE);          //ikinci kutucuk*/

  tft.fillRoundRect(75, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(80, 227);
  tft.print("GROUND STATION");

  
  tft.setFreeFont(FM9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+6, lft_blck_y+19);
  tft.print("GPS LAT.");

 
 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+19);
  tft.print("-179.123456");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+6, lft_blck_y+40);
  tft.print("GPS LONG.");


  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+40);
  tft.print("59.123456");*/


 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+6, lft_blck_y+60);
  tft.print("GPS SAT");


  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+60);
  tft.print("8 SAT");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+87);
  tft.print("TEMPERATURE");

  tft.setFreeFont(FM9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+87);
  tft.print(my_temp);
  tft.print(" ");
  tft.print(unit[t]);


  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+106);
  tft.print("ALTITUDE");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+106);
  tft.print(my_alt);
  tft.print(" ");
  tft.print(unit[d]);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+127);
  tft.print("COMPASS");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+147);
  tft.print("DISTANCE"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+147);
   tft.print(rckt_dist);
  tft.print(unit[d]);   

  tft.setFreeFont(FF33);
      on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);
  
  }

  void rocketpage2()
{

   k=0;
   i=6;

  tft.fillScreen(BLACK);
  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  tft.setCursor(150, 1);
  tft.print("12:30");
   tft.setCursor(90, 1);
  tft.print("(");
  tft.print("unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");
  
  tft.fillRoundRect(80, 211, 150, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("ROCKET PAGE 2");
  
  tft.fillRoundRect(lft_blck_x, lft_blck_y, 156, 154,3, CYAN);               //sol blok

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+2, 152, 19,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+4, box_x*2+16, box_y,3, WHITE);      //ilk kutucuk
//tft.fillRoundRect(box_x+11, lft_blck_y+4, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+23, 152, 63,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+27, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+27, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+45, 152, 63,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+46, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+46, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+65, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+65, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+89, 152, 19,2, BLACK);           //5. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+91, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+91, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+111, 152, 41,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+114, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+114, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+134, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+134, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+57, lft_blck_y+8);
  tft.print("BNO080");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+31);
  tft.print("GYRO-X");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+31);
  tft.print("-00.00 rad/s");
 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+51);
  tft.print("GYRO-Y");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+51);
  tft.print("-34.56 rad/s");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+70);
  tft.print("GYRO-Z");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+70);
  tft.print("-25.17 rad/s");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+94);
  tft.print("ACCEL-X");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+94);
  tft.print("-8.0 g");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+118);
  tft.print("ACCEL-Y"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+118);
  tft.print("5.2 g");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+138);
  tft.print("ACCEL-Z");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+138);
  tft.print("0.0 g"); 
//****************************************************************************************

 tft.fillRoundRect(rght_blck_x, rght_blck_y, 156, 154,3, CYAN);               //sağ blok


tft.fillRoundRect(rght_blck_x+2, rght_blck_y+2, 152, 19,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+4, box_x*2+16, box_y,3, WHITE);     //ilk kutucuk
//tft.fillRoundRect(rght_blck_x+80, rght_blck_y+4, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+25, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+25, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+45, 152, 41,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+49, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+49, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+67, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+67, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+89, 152, 19,2, BLACK);           //5. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+91, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+91, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+111, 152, 41,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+114, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+114, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+134, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+134, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+57, rght_blck_y+8);
  tft.print("400 g sensor");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+29);
  tft.print("MAGNO-X");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+29);
  tft.print("53 uT");
 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11,rght_blck_y+53);
  tft.print("MAGNO-Y");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+54);
  tft.print("930 mph");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+71);
  tft.print("MAGNO-Z");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+71);
  tft.print("MAGNO-Z");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+73);
  tft.print("0.44 mach");*/


  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+96);
  tft.print("YAW");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+96);
  tft.print("-180 dgree");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+118);
  tft.print("PITCH"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+118);
  tft.print("90 dgree");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+138);
  tft.print("ROLL");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+138);
  tft.print("90 dgree");   
  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+14, rght_blck_y+138);
  tft.print("GPS Lng");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+138);
  tft.print("59.560833"); */

  tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);
  
  }

  
void rocketpage1()
{
k=0;
i=6;

 tft.fillScreen(BLACK);
 tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
   tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  tft.setCursor(150, 1);
  tft.print("12:30");
   tft.setCursor(90, 1);
  tft.print("(");
  tft.print("unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(260, 1);
  tft.print("24.12.1997");
  
  tft.fillRoundRect(80, 211, 150, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("ROCKET PAGE 1");
  
 tft.fillRoundRect(lft_blck_x, lft_blck_y, 156, 176,3, CYAN);               //sol blok


tft.fillRoundRect(lft_blck_x+2, lft_blck_y+2, 152, 19,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+4, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+4, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+25, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+25, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+46, 152, 62,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+50, box_x+4, box_y*3+8,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+50, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
//tft.fillRoundRect(lft_blck_x+5, lft_blck_y+69, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+69, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+89, 152, 19,2, BLACK);           //5. satır kutu altlığı
//tft.fillRoundRect(lft_blck_x+5, lft_blck_y+88, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+88, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+114, 152, 60,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+118, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+137, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+137, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+155, 152, 19,2, BLACK);           //8. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+156, box_x+19, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+26, lft_blck_y+156, box_x-5, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+8);
  tft.print("TEMPERATURE");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+29);
  tft.print("ALTITUDE");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+29);
  tft.print(rckt_alt);
   tft.print(" ");
  tft.print(unit[d]);
 
 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+54);
  tft.print("VELOCITY");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+54);
  tft.print(rckt_v2);
   tft.print(" ");
  tft.print(unit[v2]);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+73);
  tft.print("VELOCITY");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+73);
  tft.print("0.44 mach");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+92);
  tft.print("VELOCITY");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+92);
  tft.print(rckt_v1);
   tft.print(" ");
  tft.print(unit[v1]);  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+122);
  tft.print("GPS Lat"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+122);
  tft.print("-179.123456");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+141);
  tft.print("GPS Lng");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+141);
  tft.print("59.560833"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+160);
  tft.print("GPS SATALITE"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+94, lft_blck_y+160);
  tft.print("8 SAT.");   
//****************************************************************************************

 tft.fillRoundRect(rght_blck_x, rght_blck_y, 156, 176,3, CYAN);               //sağ blok


tft.fillRoundRect(rght_blck_x+2, rght_blck_y+2, 152, 40,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+5, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+5, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+24, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+24, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+45, 152, 41,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+49, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+49, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+67, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+67, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+90, 152, 19,2, BLACK);           //5. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+92, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+92, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+114, 152, 60,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+118, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+136, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+136, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+155, 152, 19,2, BLACK);           //8. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+155, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+155, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+9);
  tft.print("PYRO MAIN 1");

  tft.fillCircle(rght_blck_x+93, rght_blck_y+12, 5, GREEN);
  tft.fillCircle(rght_blck_x+113, rght_blck_y+12,5, RED);

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+83, rght_blck_y+8);
  tft.print("SLEEPY MODE");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+28);
  tft.print("PYRO MAIN 2");

  tft.fillCircle(rght_blck_x+93, rght_blck_y+31, 5, GREEN);
  tft.fillCircle(rght_blck_x+113, rght_blck_y+31,5, RED);

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+29);
  tft.print("11.1 V");*/
 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7,rght_blck_y+53);
  tft.print("PYRO BCKP 1");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+54);
  tft.print("930 mph");*/
  tft.fillCircle(rght_blck_x+93, rght_blck_y+56, 5, GREEN);
   tft.fillCircle(rght_blck_x+113, rght_blck_y+56,5, RED);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+71);
  tft.print("PYRO BCKP 2");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+73);
  tft.print("0.44 mach");*/

    tft.fillCircle(rght_blck_x+93, rght_blck_y+74, 5, GREEN);
   tft.fillCircle(rght_blck_x+113, rght_blck_y+74, 5, RED);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+96);
  tft.print("BATTERY");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+96);
  tft.print("11.1 V");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+122);
  tft.print("ACCEL-X");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+122);
  tft.print("-200 g");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+140);
  tft.print("ACCEL-Y");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+140);
  tft.print("400 g");   

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+159);
  tft.print("ACCEL-Z");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+159);
  tft.print("20 g");   
  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+14, rght_blck_y+138);
  tft.print("GPS Lng");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+138);
  tft.print("59.560833"); */

  tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);

  }

void showTime(uint32_t msTime) {
  //tft.setCursor(0, 0);
  //tft.setTextFont(1);
  //tft.setTextSize(2);
  //tft.setTextColor(TFT_WHITE, TFT_BLACK);
  //tft.print(F(" JPEG drawn in "));
  //tft.print(msTime);
  //tft.println(F(" ms "));
  Serial.print(F(" JPEG drawn in "));
  Serial.print(msTime);
  Serial.println(F(" ms "));
}

void drawSdJpeg(const char *filename, int xpos, int ypos) {

  // Open the named file (the Jpeg decoder library will close it)
  File jpegFile = SD.open( filename, FILE_READ);  // or, file handle reference for SD library
 
  if ( !jpegFile ) {
   // Serial.print("ERROR: File \""); Serial.print(filename); Serial.println ("\" not found!");
    return;
  }

///  Serial.println("===========================");
///  Serial.print("Drawing file: "); Serial.println(filename);
///  Serial.println("===========================");

  // Use one of the following methods to initialise the decoder:
  boolean decoded = JpegDec.decodeSdFile(jpegFile);  // Pass the SD file handle to the decoder,
  //boolean decoded = JpegDec.decodeSdFile(filename);  // or pass the filename (String or character array)

  if (decoded) {
    // print information about the image to the serial port
    jpegInfo();
    // render the image onto the screen at given coordinates
    jpegRender(xpos, ypos);
  }
  else {
   // Serial.println("Jpeg file format not supported!");
  }
}

//####################################################################################################
// Draw a JPEG on the TFT, images will be cropped on the right/bottom sides if they do not fit
//####################################################################################################
// This function assumes xpos,ypos is a valid screen coordinate. For convenience images that do not
// fit totally on the screen are cropped to the nearest MCU size and may leave right/bottom borders.
void jpegRender(int xpos, int ypos) {

  //jpegInfo(); // Print information from the JPEG file (could comment this line out)

  uint16_t *pImg;
  uint16_t mcu_w = JpegDec.MCUWidth;
  uint16_t mcu_h = JpegDec.MCUHeight;
  uint32_t max_x = JpegDec.width;
  uint32_t max_y = JpegDec.height;

  bool swapBytes = tft.getSwapBytes();
  tft.setSwapBytes(true);
  
  // Jpeg images are draw as a set of image block (tiles) called Minimum Coding Units (MCUs)
  // Typically these MCUs are 16x16 pixel blocks
  // Determine the width and height of the right and bottom edge image blocks
  uint32_t min_w = jpg_min(mcu_w, max_x % mcu_w);
  uint32_t min_h = jpg_min(mcu_h, max_y % mcu_h);

  // save the current image block size
  uint32_t win_w = mcu_w;
  uint32_t win_h = mcu_h;

  // record the current time so we can measure how long it takes to draw an image
  uint32_t drawTime = millis();

  // save the coordinate of the right and bottom edges to assist image cropping
  // to the screen size
  max_x += xpos;
  max_y += ypos;

  // Fetch data from the file, decode and display
  while (JpegDec.read()) {    // While there is more data in the file
    pImg = JpegDec.pImage ;   // Decode a MCU (Minimum Coding Unit, typically a 8x8 or 16x16 pixel block)

    // Calculate coordinates of top left corner of current MCU
    int mcu_x = JpegDec.MCUx * mcu_w + xpos;
    int mcu_y = JpegDec.MCUy * mcu_h + ypos;

    // check if the image block size needs to be changed for the right edge
    if (mcu_x + mcu_w <= max_x) win_w = mcu_w;
    else win_w = min_w;

    // check if the image block size needs to be changed for the bottom edge
    if (mcu_y + mcu_h <= max_y) win_h = mcu_h;
    else win_h = min_h;

    // copy pixels into a contiguous block
    if (win_w != mcu_w)
    {
      uint16_t *cImg;
      int p = 0;
      cImg = pImg + win_w;
      for (int h = 1; h < win_h; h++)
      {
        p += mcu_w;
        for (int w = 0; w < win_w; w++)
        {
          *cImg = *(pImg + w + p);
          cImg++;
        }
      }
    }

    // calculate how many pixels must be drawn
    uint32_t mcu_pixels = win_w * win_h;

    // draw image MCU block only if it will fit on the screen
    if (( mcu_x + win_w ) <= tft.width() && ( mcu_y + win_h ) <= tft.height())
      tft.pushImage(mcu_x, mcu_y, win_w, win_h, pImg);
    else if ( (mcu_y + win_h) >= tft.height())
      JpegDec.abort(); // Image has run off bottom of screen so abort decoding
  }

  tft.setSwapBytes(swapBytes);

  showTime(millis() - drawTime); // These lines are for sketch testing only
}

//####################################################################################################
// Print image information to the serial port (optional)
//####################################################################################################
// JpegDec.decodeFile(...) or JpegDec.decodeArray(...) must be called before this info is available!
void jpegInfo() {

  // Print information extracted from the JPEG file
  Serial.println("JPEG image info");
  Serial.println("===============");
  Serial.print("Width      :");
  Serial.println(JpegDec.width);
  Serial.print("Height     :");
  Serial.println(JpegDec.height);
  Serial.print("Components :");
  Serial.println(JpegDec.comps);
  Serial.print("MCU / row  :");
  Serial.println(JpegDec.MCUSPerRow);
  Serial.print("MCU / col  :");
  Serial.println(JpegDec.MCUSPerCol);
  Serial.print("Scan type  :");
  Serial.println(JpegDec.scanType);
  Serial.print("MCU width  :");
  Serial.println(JpegDec.MCUWidth);
  Serial.print("MCU height :");
  Serial.println(JpegDec.MCUHeight);
  Serial.println("===============");
  Serial.println("");
}

void touch_calibrate()
{
  uint16_t calData[5];
  uint8_t calDataOK = 0;

  // check file system exists
  if (!SPIFFS.begin()) {
    Serial.println("Formating file system");
    SPIFFS.format();
    SPIFFS.begin();
  }

  // check if calibration file exists and size is correct
  if (SPIFFS.exists(CALIBRATION_FILE)) {
    if (REPEAT_CAL)
    {
      // Delete if we want to re-calibrate
      SPIFFS.remove(CALIBRATION_FILE);
    }
    else
    {
      File f = SPIFFS.open(CALIBRATION_FILE, "r");
      if (f) {
        if (f.readBytes((char *)calData, 14) == 14)
          calDataOK = 1;
        f.close();
      }
    }
  }

  if (calDataOK && !REPEAT_CAL) {
    // calibration data valid
    tft.setTouch(calData);
  } else {
    // data not valid so recalibrate
    tft.fillScreen(TFT_BLACK);
    tft.setCursor(20, 0);
    tft.setTextFont(2);
    tft.setTextSize(1);
    tft.setTextColor(TFT_WHITE, TFT_BLACK);

    tft.println("Touch corners as indicated");

    tft.setTextFont(1);
    tft.println();

    if (REPEAT_CAL) {
      tft.setTextColor(TFT_RED, TFT_BLACK);
      tft.println("Set REPEAT_CAL to false to stop this running again!");
    }

    tft.calibrateTouch(calData, TFT_MAGENTA, TFT_BLACK, 15);

    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.println("Calibration complete!");

    // store data
    File f = SPIFFS.open(CALIBRATION_FILE, "w");
    if (f) {
      f.write((const unsigned char *)calData, 14);
      f.close();
    }
  }
}
