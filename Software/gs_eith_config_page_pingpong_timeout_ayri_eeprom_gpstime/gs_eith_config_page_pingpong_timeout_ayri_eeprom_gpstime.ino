#include "panda.h"
#include <TJpg_Decoder.h>
#include <Wire.h>
#include <LSM303.h>
#include <Adafruit_BMP280.h>
#include <EEPROM.h>
#include <Timezone.h>
#include "TimeLib.h"  
#include <SimpleKalmanFilter.h>

SimpleKalmanFilter simpleKalmanFilter(2, 2, 0.1);

Adafruit_BMP280 bmp;
LSM303 compass;
LSM303::vector<int16_t> running_min = {32767, 32767, 32767}, running_max = {-32768, -32768, -32768};


#include "LITTLEFS.h" 
//#include <SPI.h>
#include "FS.h"
#include "Free_Fonts.h"

#include <TFT_eSPI.h>
TFT_eSPI tft = TFT_eSPI();
TFT_eSprite dial   = TFT_eSprite(&tft); // Sprite object for dial
TFT_eSprite needle = TFT_eSprite(&tft); // Sprite object for needle


#include <RadioLib.h>
#include "SPI.h"
SPIClass abc(HSPI);
SX1262 radio = new Module(15, 17, 27, 39,abc);


int utc_value=180,UTC;

TimeChangeRule CEST = {"CEST", Last, Sun, Mar, 2, utc_value};
//Central European Time (Frankfurt, Paris)  60  = +1 hour in normal time (winter)
TimeChangeRule CET = {"CET ", Last, Sun, Oct, 3, utc_value};
Timezone CE(CEST, CET);

// time variables
tmElements_t te;  //Time elements structure
time_t local, utc, prev_set,unixTime;
int timesetinterval = 0; //set microcontroller time every 60 seconds

TaskHandle_t Task1;

float heading;
float headingg;
int16_t angle = 0;
int pusula;

#include <TinyGPS++.h>
#include <HardwareSerial.h>
    long lat,lon; 
    int counterr = 0;     
    TinyGPSPlus gps;
 HardwareSerial SerialGPS(1);  
 
#define lft_blck_x    0
#define lft_blck_y    22

#define rght_blck_x    164
#define rght_blck_y    22

#define box_y    15
#define box_x    65


#define BLACK       0x0000      /*   0,   0,   0 */
#define NAVY        0x000F      /*   0,   0, 128 */
#define DARKGREEN   0x03E0      /*   0, 128,   0 */
#define DARKCYAN    0x03EF      /*   0, 128, 128 */
#define MAROON      0x7800      /* 128,   0,   0 */
#define PURPLE      0x780F      /* 128,   0, 128 */
#define OLIVE       0x7BE0      /* 128, 128,   0 */
#define LIGHTGREY   0xD69A      /* 211, 211, 211 */
#define DARKGREY    0x7BEF      /* 128, 128, 128 */
#define BLUE        0x001F      /*   0,   0, 255 */
#define GREEN       0x07E0      /*   0, 255,   0 */
#define CYAN        0x07FF      /*   0, 255, 255 */
#define RED         0xF800      /* 255,   0,   0 */
#define MAGENTA     0xF81F      /* 255,   0, 255 */
#define YELLOW      0xFFE0      /* 255, 255,   0 */
#define WHITE       0xFFFF      /* 255, 255, 255 */
#define ORANGE      0xFDA0      /* 255, 180,   0 */
#define GREENYELLOW 0xB7E0      /* 180, 255,   0 */
#define PINK        0xFE19      /* 255, 192, 203 */    
#define BROWN       0x9A60      /* 150,  75,   0 */
#define GOLD        0xFEA0      /* 255, 215,   0 */
#define SILVER      0xC618      /* 192, 192, 192 */
#define SKYBLUE     0x867D      /* 135, 206, 235 */
#define VIOLET      0x915C      /* 180,  46, 226 */
#define GRAY        0xC618

#define CALIBRATION_FILE "/TouchCalData2"

int color_press_rqst=GREEN;
int color_press_snd=DARKGREY;

int PYRO1_COLOR=GREEN;
int PYRO2_COLOR=GREEN;
int MACH_LOCK_COLOR=GREEN;

// Set REPEAT_CAL to true instead of false to run calibration
// again, otherwise it will only be done once.
// Repeat calibration if you change the screen rotation.
#define REPEAT_CAL false
TFT_eSPI_Button on_btn,off_btn,minus_btn,plus_btn,snd_btn,save_btn,one_btn,ten_btn,hundred_btn,
                fivehundred_btn,imprl_btn,mtrc_btn,point_one_btn,point_t_btn,point_five_btn,
                one_thousand_btn,erase_btn,ok_btn,rqst_btn,try_btn,prsht_btn,frq_btn,lsm_btn,
                cnf_btn,roc_one_btn,roc_two_btn,gs_btn,flight_sttcs_btn,finder_btn,main_btn,utc_btn;
                

void listDir(fs::FS &fs, const char * dirname, uint8_t levels);
void deleteFile(fs::FS &fs, const char * path);
void renameFile(fs::FS &fs, const char * path1, const char * path2);
void appendFile(fs::FS &fs, const char * path, String message);
void writeFile(fs::FS &fs, const char * path, const char * message);
void readFile(fs::FS &fs, const char * path);
void removeDir(fs::FS &fs, const char * path);
void createDir(fs::FS &fs, const char * path);



float ByteToFloat(byte *byterray,int ca)
{
  float f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

  long ByteTolong(byte *byterray,int ca)
{
  long f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

   unsigned long ByteTounsignedlong(byte *byterray,int ca)
{
 unsigned long f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

 int ByteToint(byte *byterray,int ca)
{
  int f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

   int ByteTobyte(byte *byterray,int ca)
{
  byte f;
  ((uint8_t*)&f)[0]=byterray[ca];
return f;
  }

    uint8_t ByteTouint8t(byte *byterray,int ca)
{
  uint8_t f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

  void floatAddByte(float data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void intAddByte(int data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}

void uint8tAddByte(uint8_t data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}


   static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do
  {
    // If data has come in from the GPS module
    while (SerialGPS.available())
      gps.encode(SerialGPS.read()); // Send it to the encode function
    // tinyGPS.encode(char) continues to "load" the tinGPS object with new
    // data coming in from the GPS module. As full NMEA strings begin to come in
    // the tinyGPS library will be able to start parsing them for pertinent info
  } while (millis() - start < ms);
}

int Year;
byte Month;
byte Day;
byte Hour;
byte Minute;
byte Seconde;

void setthetime(void)
{
   Year = gps.date.year();
   Month = gps.date.month();
   Day = gps.date.day();
   Hour = gps.time.hour();
   Minute = gps.time.minute();
   Seconde = gps.time.second();
   // Set Time from GPS data string
   setTime(Hour, Minute, Seconde, Day, Month, Year);  // set the time of the microcontroller to the UTC time from the GPS
}


float flat1;  
float flon1;
float dist_calc=0;
float dist_calc2=0;
float diflat=0;
float diflon=0;
float x2lat;
float x2lon;


struct flightdata{
  
float H3LIS331_data[3];
float Ms5611_data[4];
float bnoAccel[3];
float bnoGyro[3];
float bnoMag[3];
float bnoRaw[3];
float MAX_ALT;

unsigned long APOGEE_TIME;
unsigned long DESCENT_TIME;
unsigned long ENGINEBURN_TIME;
float PYROBURN_TIME;
float MAX_ACCEL[3];
float MAX_SPEED;
float APOGEE_FALL_SPEED;
float MAIN_FALL_SPEED;
long GPS_data[4];
byte SIV;
byte MAX_SIV;
long unix_time;
float batteryy;
};
flightdata flightData;
flightdata flightstatMax;
flightdata flightstatMin;

unsigned long FLIGHT_TIME;
int compmin_x,compmin_y,compmin_z,compmax_x,compmax_y,compmax_z;
int screen_id,k,i,kts,t,v1,d,v2,u;
float apogee=500;
float main;
uint32_t a,b;
char *unit[] ={"m","i","M","m/s","km/h","C","ft","ft/s","mph","F"}; 
char buf[40];
float my_temp,my_alt,rckt_temp,rckt_alt,rckt_v1,rckt_v2,rckt_dist;



uint32_t tt=0;

float aa,cardSize,cardused;
long l;
int ii,c,m,last_st;
bool kk,ok,flg,comm,alt_calc=0;
char veri[] = "";


float last_frq,frquency_check;
int last_dbm,dbm_check,sd_cap,grnd_alt;
float frq=868.00;
int frq_ktss;
int dbm=22;
float frq_kts=0.1;

float carrierFreq;
uint8_t outputPower;
uint8_t pyro_stat=0;
bool pyro_stat_arr[8];
bool tch_grnd=0,header_count=1,gps_cnt=1,tst=1;

int rocketapoge;
int rocketmain;

byte passw = 0x60;
byte getsensData = 0x20;
byte getflightData= 0x21;
byte getsensData_flgtstcs= 0x22;
byte getsensData_tch_grnd= 0x23;

byte send_prsht_config = 0x50;
byte get_prsht_config  = 0x51;
byte send_frq_config = 0x30;
byte get_frq_config = 0x31; 

byte getconfig = 0x40;
byte setConfig = 0x10;

unsigned long time_gpsfix=0;

unsigned long timeout_hz=0;
unsigned long timeout=0;
unsigned long timeout_sens=0;
unsigned long timeout_fall=0;
unsigned long timeout_comm=0;

int transmissionState = ERR_NONE;

struct Data{
byte byteArray[165];
byte tr_array[16];
int counter=0;
};
Data data;



char strr[210];
String All_data2;
String header = "GpsTime,200gAccX,200gAccY,200gAccZ,Temp,Pressure,Alt,Speed,BNOAccX,BNOAccY,BNOAccZ,BNOGyrX,BNOGyrY,BNOGyrZ,BNOMagX,BNOMagY,BNOMagZ,BNOYaw,BNOPitch,BNORoll,GpsLat,GpsLong,GpsAlt,GpsSpeed,SIV,Batt,PyroDraque,PyroMain,MAX_ALTITUDE,MAX_SPEED,MAX_ACCEL_X,MAX_ACCEL_Y,MAX_ACCEL_Z,MAX_SIV,APOGEE_TIME,DESCENT_TIME,ENGINE_BURN_TIME,APOGEE_FALL_SPEED,MAIN_FALL_SPEED,FLIGHT_TIME,UnixTIME\n";
String All_data1;
int All_dataa,cnt;
int frst_cnt,stt=0;
int crc_err,compare=2;
byte v=0,selffrq_set=0;

bool try_com,check_data,time_com,rqst_err,snd_err,prsht_rqst_err,prsht_snd_err,config_page;

#define INITIATING_NODE = true;
bool transmitFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// flag to indicate that a packet was sent or received
volatile bool operationDone = false;

// this function is called when a complete packet
// is transmitted or received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if(!enableInterrupt) {
    return;
  }

  // we sent aor received  packet, set the flag
  operationDone = true;
}



void setup() {
  
  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;  
  const int HSPI_CS = 15;

  abc.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS); 
  
  delay(500);
  
 Serial.begin(9600); 
 screen_id=0;
k=1;
i=0;
kts=1;
 SerialGPS.begin(9600, SERIAL_8N1, 16, 34);

 while (!gps.time.isValid()) // wait a valid GPS UTC time (valid is defined in the TinyGPS++ library)
  {
    smartDelay(10);
  }
  setthetime();
  prev_set = now();

  Wire.begin();
  tft.init();
  tft.setRotation(1); 
  TJpgDec.setJpgScale(1);
  // The byte order can be swapped (set true for TFT_eSPI)
  TJpgDec.setSwapBytes(true);
  // The decoder must be given the exact name of the rendering function above
  TJpgDec.setCallback(tft_output);
 
  EEPROM.begin(512);
  compass.init();
  compass.enableDefault();
  compmin_x=EEPROM.readInt(11);
  compmin_y=EEPROM.readInt(16);
  compmin_z=EEPROM.readInt(21);
  compmax_x=EEPROM.readInt(26);
  compmax_y=EEPROM.readInt(31);
  compmax_z=EEPROM.readInt(36);
  
   compass.m_min = (LSM303::vector<int16_t>){ compmin_x,   compmin_y,   compmin_z};
   compass.m_max = (LSM303::vector<int16_t>){ compmax_x,   compmax_y,   compmax_z};
   pinMode( 26, OUTPUT);
 if (!bmp.begin()) {
    //Serial.println(F("Could not find a valid BMP280 sensor, check wiring or "
//                      "try a different address!"));
    while (1) delay(10);
  }

  /* Default settings from datasheet. */
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
   
  xTaskCreatePinnedToCore(
                    Task1code,   /* Task function. */
                    "Task1",     /* name of task. */
                    40000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task1,      /* Task handle to keep track of created task */
                    0);          /* pin task to core 0 */                  
  delay(100); 

 if (!SD.begin()) {
   // Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();

  if (cardType == CARD_NONE) {
//Serial.println("No SD card attached");
    return;
  }

 // Serial.print("SD Card Type: ");
  if (cardType == CARD_MMC) {
 //   Serial.println("MMC");
  } else if (cardType == CARD_SD) {
  //  Serial.println("SDSC");
  } else if (cardType == CARD_SDHC) {
   // Serial.println("SDHC");
  } else {
   // Serial.println("UNKNOWN");
  }

  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
 //// Serial.printf("SD Card Size: %lluMB\n", cardSize);
 tft.fillScreen(BLACK);
 touch_calibrate();
 tft.setTextSize(1);
 
 /*preferences.begin("my-config", false);
 
  frquency_check=preferences.getFloat("frq",0);
  Serial.print(frquency_check);
  if(frquency_check==0)frq=868.00;
  else frq=frquency_check;
  Serial.print(" frekans= ");
  Serial.print(frq);

  dbm_check=preferences.getInt("dbm",0);
  Serial.print(dbm_check);
  if(dbm_check==0)dbm=20;
  else dbm=dbm_check;
  Serial.print(" dbm= ");
  Serial.print(dbm);*/
  utc_value=EEPROM.readInt(38);
  CEST.offset=utc_value;
  CET.offset=utc_value;
  CE.setRules(CEST,CET);
  
  EEPROM.writeFloat(0,frq);
  EEPROM.writeInt(6,dbm);
  EEPROM.commit();
    //EEPROM.writeFloat(0,frq);
  frquency_check=EEPROM.readFloat(0);
  Serial.print(" içindeki frekans ");
  Serial.print(frquency_check);
  if(frquency_check>=868 && frquency_check<=915)frq=frquency_check;
  else frq=868.00;
  Serial.print(" frekans= ");
  Serial.print(frq);

  dbm_check=EEPROM.readInt(6);
  Serial.print(" içindeki dbm ");
  Serial.print(dbm_check);
  if(dbm_check<=0 || dbm_check>=23)dbm=20;
  else dbm=dbm_check;
  Serial.print(" dbm= ");
  Serial.print(dbm);


int state = radio.begin(frq, 500.0, 7, 5, 0x34,dbm);
  if (state == ERR_NONE) {
  //  Serial.println(F("success!"));
  } else {
   // Serial.print(F("failed, code "));
   // Serial.println(state);
    while (true);
  }
radio.setCurrentLimit(110);
  // set the function that will be called
  // when new packet is received
 radio.setDio1Action(setFlag);
  #if defined(INITIATING_NODE)
    // send the first packet on this node
    Serial.print(F("[SX1262] Sending first packet ... "));
    transmissionState = radio.startTransmit(data.tr_array,16);
    transmitFlag = true;
  #else
    // start listening for LoRa packets on this node
    Serial.print(F("[SX1262] Starting to listen ... "));
    state = radio.startReceive();
    if (state == ERR_NONE) {
      Serial.println(F("success!"));
    } else {
      Serial.print(F("failed, code "));
      Serial.println(state);
      while (true);
    }
  #endif

  // if needed, 'listen' mode can be disabled by calling
  // any of the following methods:
  //
  // radio.standby()
  // radio.sleep()
  // radio.transmit();
  // radio.receive();
  // radio.readData();
  // radio.scanChannel();
                u=0;
                d=2;
                v1=3;
                v2=4;
                t=5;

}


// flag to indicate transmission or reception state


void Task1code( void * pvParameters ){
 // Serial.print("Task1 running on core ");
 // Serial.println(xPortGetCoreID());

  for(;;){

    timeout_hz=millis();
    vTaskDelay(1);
    
cardSize = SD.cardSize() / (1024 * 1024);
cardused = SD.usedBytes() / (1024 * 1024);
cardSize=cardSize/1000;
cardused=cardused/1000;



sd_cap=(100*cardused)/cardSize;




  if((screen_id==3 || screen_id==4) && flg ==0){
    
  if(millis()-timeout_sens > 2000){
    
     // kk=0;
      enableInterrupt = false;
      Serial.print(" roketten sensör data istendi ");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(getsensData, &data.counter, data.tr_array);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      //stt=0;
      //check_data=0;  
      enableInterrupt = true;
     timeout_sens = millis();
     

}
  }

      // burada rqst butonuna basılırsa değişken aktif olup config isteme gönderme işlemi yapıyor.
    if(stt==1){
      kk=0;
      enableInterrupt = false;
      Serial.print(" roketten frq config istendi ");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(get_frq_config, &data.counter, data.tr_array);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      stt=0;
      //check_data=0;  
      enableInterrupt = true;

}
    
     // burada send butonuna basılırsa değişken aktif olup config gönderme işlemi yapıyor.
    if(stt==2){
      kk=0;
      enableInterrupt = false;
      Serial.print(" frq Datalar gönderildi");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(send_frq_config, &data.counter, data.tr_array);
      floatAddByte(frq,&data.counter,data.tr_array);
      uint8tAddByte(dbm,&data.counter,data.tr_array);
      Serial.print(dbm);
      Serial.print(frq);
      transmissionState = radio.startTransmit(data.tr_array,7);
      transmitFlag = true;
      stt=0;
      ok=1;
      enableInterrupt = true;

}

if(stt==3){
      kk=0;
      enableInterrupt = false;
      Serial.print(" prsht Datalar gönderildi");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(send_prsht_config, &data.counter, data.tr_array);
      floatAddByte(main,&data.counter,data.tr_array);
      Serial.print(apogee);
      Serial.print(main);
      transmissionState = radio.startTransmit(data.tr_array,6);
      transmitFlag = true;
      stt=0;
      enableInterrupt = true;

}

    if(stt==4){
      kk=0;
      enableInterrupt = false;
      Serial.print(" prhst Datalar istendi");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(get_prsht_config, &data.counter, data.tr_array);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      stt=0;
      enableInterrupt = true;

}




if((kk==0 && i==7 && time_com==0 && rqst_err==1) || (kk==0 && i==7 && time_com==0 && prsht_rqst_err==1)){


  
  if(millis()-timeout > 1000){
     if(rqst_err==1)stt=1;
      else stt=4;
     timeout = millis();
     cnt+=1;
     Serial.print(" veriler ");
     Serial.print(cnt);
     Serial.print(". kez istendi");
    }

    if(cnt>=5) {
      
      time_com=1; v=2;     //time_com haberleşmede 5 defa denemeyi kesmek için kullanılan değişken.--- V ise ekrana sadece bir defa uyarı basmak için kullanılıyor.
      if(last_st==1 && kk==0){
      delay(3000);
      cnt=0;
      frq=last_frq;
      dbm=last_dbm;
      radio.setFrequency(frq);
      radio.setOutputPower(dbm);
      rqst_err=1;
      time_com=0;
      Serial.println("ESKİ FREKANSA GEÇİLDİ");
      last_st=0;
        }
      
      
      }
    
  }
  else if((kk==0 && i==7 && time_com==0 && snd_err==1)|| (kk==0 && i==7 && time_com==0 && prsht_snd_err==1)){


    if(millis()-timeout > 1000){
     if(snd_err==1) stt=2;
      else stt=3;
     timeout = millis();
     cnt+=1;
     Serial.print(" veriler ");
     Serial.print(cnt);
     Serial.print(". kez gönderildi");
    }

    if(cnt>=5) {time_com=1; v=2;}     // haberleşmede 5 defa denemeyi kesmek için kullanılan değişken.
  }
  
    
    
    if(operationDone) {
    // disable the interrupt service routine while
    // processing the data
    enableInterrupt = false;

    // reset flag
    operationDone = false;

      if(transmitFlag) {


      // the previous operation was transmission, listen for response
      // print the result
      if (transmissionState == ERR_NONE) {
        // packet was successfully sent
       // Serial.println(F("transmission finished!"));
  
      } else {
        Serial.print(F("failed, code "));
        Serial.println(transmissionState);
  
      }

      // listen for response
      radio.startReceive();
      transmitFlag = false;
     // Serial.println("bekliyor");
      
    }

    else{
   // Serial.println("alım başlayacak");
    int state = radio.readData(data.byteArray,165);
    
    if(state == ERR_CRC_MISMATCH){
      
      crc_err+=1;
           
      }
    



    if (state == ERR_NONE) {
      
      timeout_comm= millis();

      if(data.byteArray[0] == passw && data.byteArray[1] == getsensData ){
   
     
c=2;
for( ii=0;ii<3;ii++){ 
flightData.H3LIS331_data[ii]= ByteToFloat(data.byteArray,c);
     if(flightData.H3LIS331_data[ii]>flightstatMax.H3LIS331_data[ii])flightstatMax.H3LIS331_data[ii]=flightData.H3LIS331_data[ii];
else if(flightData.H3LIS331_data[ii]>flightstatMin.H3LIS331_data[ii])flightstatMin.H3LIS331_data[ii]=flightData.H3LIS331_data[ii];
c+=4;
}
for( ii=0;ii<4;ii++){ 
flightData.Ms5611_data[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
     if(flightData.Ms5611_data[2]>flightData.MAX_ALT)flightData.MAX_ALT=flightData.Ms5611_data[2];
else if(flightData.Ms5611_data[3]>flightData.MAX_SPEED)flightData.MAX_SPEED=flightData.Ms5611_data[3];

for( ii=0;ii<3;ii++){ 
flightData.bnoAccel[ii]= ByteToFloat(data.byteArray,c);
     if(abs(flightData.bnoAccel[ii])>abs(flightData.MAX_ACCEL[ii]))flightData.MAX_ACCEL[ii]=flightData.bnoAccel[ii];
c+=4;
}

for( ii=0;ii<3;ii++){ 
flightData.bnoGyro[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
flightData.bnoMag[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
flightData.bnoRaw[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}

for( ii=0;ii<4;ii++){ 
flightData.GPS_data[ii]= ByteTolong(data.byteArray,c);
c+=4;
}

flightData.unix_time= ByteTolong(data.byteArray,c);
c+=4;


flightData.batteryy= ByteToFloat(data.byteArray,c);
c+=4;

flightData.SIV=data.byteArray[c];
c+=1;

pyro_stat = data.byteArray[c];
Serial.print(pyro_stat);
for (int i = 0;i<8; i++) {
        pyro_stat_arr[i] = 0;   
    }
for (int i = 0; pyro_stat > 0; i++) {
        pyro_stat_arr[i] = pyro_stat % 2;
        pyro_stat = pyro_stat / 2;   
    }
    

c=0;
FLIGHT_TIME = (flightData.APOGEE_TIME/1000) + (flightData.DESCENT_TIME/1000);
print_date_time();

    All_data1=String(buf)+","+String(flightData.H3LIS331_data[0])+","+String(flightData.H3LIS331_data[1])+","+String(flightData.H3LIS331_data[2])+",";
    All_data1+=String(flightData.Ms5611_data[0])+","+String(flightData.Ms5611_data[1])+","+String(flightData.Ms5611_data[2])+","+String(flightData.Ms5611_data[3])+",";
    All_data1+=String(flightData.bnoAccel[0])+","+String(flightData.bnoAccel[1])+","+String(flightData.bnoAccel[2])+",";
    All_data1+=String(flightData.bnoGyro[0])+","+String(flightData.bnoGyro[1])+","+String(flightData.bnoGyro[2])+",";
    All_data1+=String(flightData.bnoMag[0])+","+String(flightData.bnoMag[1])+","+String(flightData.bnoMag[2])+",";
    All_data1+=String(flightData.bnoRaw[0])+","+String(flightData.bnoRaw[1])+","+String(flightData.bnoRaw[2])+",";
    All_data1+=String(flightData.GPS_data[0])+","+String(flightData.GPS_data[1])+","+String(flightData.GPS_data[2])+","+String(float(flightData.GPS_data[3])/1000)+",";
    All_data1+=String(flightData.SIV)+","+String(flightData.batteryy)+","+String(pyro_stat_arr[0])+","+String(pyro_stat_arr[2])+",";
    All_data1+=String(flightData.MAX_ALT)+","+String(flightData.MAX_SPEED)+","+String(flightData.MAX_ACCEL[0])+","+String(flightData.MAX_ACCEL[1])+","+String(flightData.MAX_ACCEL[2])+",";
    All_data1+=String(flightData.MAX_SIV)+","+String(flightData.ENGINEBURN_TIME)+","+String(flightData.APOGEE_TIME)+","+String(flightData.APOGEE_FALL_SPEED)+",";
    All_data1+=String(flightData.MAIN_FALL_SPEED)+","+String(flightData.DESCENT_TIME)+","+String(FLIGHT_TIME)+","+String(flightData.unix_time)+"\n";

  //   uint32_t t = millis(); 
       
/* All_dataa = All_data1.length() + 3; 
 char chrr[All_dataa]; 
 char chrr_header[100]; 
 All_data1.toCharArray(chrr, All_dataa);
 header.toCharArray(chrr_header, All_dataa);*/
 if(header_count==1){
  appendFile(SD, "/GROUNDSTATION.csv", header);
  header_count=0;
  }
 appendFile(SD, "/GROUNDSTATION.csv",  All_data1);
 All_data1="";
        
        }
     
      if( data.byteArray[0] == passw && data.byteArray[1] == getflightData ){
      flg=1;  
     
c=2;
for( ii=0;ii<3;ii++){ 
flightData.H3LIS331_data[ii]= ByteToFloat(data.byteArray,c);
     if(flightData.H3LIS331_data[ii]>flightstatMax.H3LIS331_data[ii])flightstatMax.H3LIS331_data[ii]=flightData.H3LIS331_data[ii];
else if(flightData.H3LIS331_data[ii]>flightstatMin.H3LIS331_data[ii])flightstatMin.H3LIS331_data[ii]=flightData.H3LIS331_data[ii];
c+=4;
}
for( ii=0;ii<4;ii++){ 
flightData.Ms5611_data[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
     if(flightData.Ms5611_data[2]>flightData.MAX_ALT)flightData.MAX_ALT=flightData.Ms5611_data[2];
else if(flightData.Ms5611_data[3]>flightData.MAX_SPEED)flightData.MAX_SPEED=flightData.Ms5611_data[3];

for( ii=0;ii<3;ii++){ 
flightData.bnoAccel[ii]= ByteToFloat(data.byteArray,c);
     if(abs(flightData.bnoAccel[ii])>abs(flightData.MAX_ACCEL[ii]))flightData.MAX_ACCEL[ii]=flightData.bnoAccel[ii];
c+=4;
}

for( ii=0;ii<3;ii++){ 
flightData.bnoGyro[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
flightData.bnoMag[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
flightData.bnoRaw[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}

for( ii=0;ii<4;ii++){ 
flightData.GPS_data[ii]= ByteTolong(data.byteArray,c);
c+=4;
}

flightData.unix_time= ByteTolong(data.byteArray,c);
c+=4;


flightData.batteryy= ByteToFloat(data.byteArray,c);
c+=4;

flightData.SIV=data.byteArray[c];
c+=1;

pyro_stat = data.byteArray[c];
Serial.print(pyro_stat);
for (int i = 0;i<8; i++) {
        pyro_stat_arr[i] = 0;   
    }
for (int i = 0; pyro_stat > 0; i++) {
        pyro_stat_arr[i] = pyro_stat % 2;
        pyro_stat = pyro_stat / 2;   
    }
    

c=0;
FLIGHT_TIME = (flightData.APOGEE_TIME/1000) + (flightData.DESCENT_TIME/1000);

print_date_time();

     All_data1=String(buf)+","+String(flightData.H3LIS331_data[0])+","+String(flightData.H3LIS331_data[1])+","+String(flightData.H3LIS331_data[2])+",";
    All_data1+=String(flightData.Ms5611_data[0])+","+String(flightData.Ms5611_data[1])+","+String(flightData.Ms5611_data[2])+","+String(flightData.Ms5611_data[3])+",";
    All_data1+=String(flightData.bnoAccel[0])+","+String(flightData.bnoAccel[1])+","+String(flightData.bnoAccel[2])+",";
    All_data1+=String(flightData.bnoGyro[0])+","+String(flightData.bnoGyro[1])+","+String(flightData.bnoGyro[2])+",";
    All_data1+=String(flightData.bnoMag[0])+","+String(flightData.bnoMag[1])+","+String(flightData.bnoMag[2])+",";
    All_data1+=String(flightData.bnoRaw[0])+","+String(flightData.bnoRaw[1])+","+String(flightData.bnoRaw[2])+",";
    All_data1+=String(flightData.GPS_data[0])+","+String(flightData.GPS_data[1])+","+String(flightData.GPS_data[2])+","+String(float(flightData.GPS_data[3])/1000)+",";
    All_data1+=String(flightData.SIV)+","+String(flightData.batteryy)+","+String(pyro_stat_arr[0])+","+String(pyro_stat_arr[2])+",";
    All_data1+=String(flightData.MAX_ALT)+","+String(flightData.MAX_SPEED)+","+String(flightData.MAX_ACCEL[0])+","+String(flightData.MAX_ACCEL[1])+","+String(flightData.MAX_ACCEL[2])+",";
    All_data1+=String(flightData.MAX_SIV)+","+String(flightData.ENGINEBURN_TIME)+","+String(flightData.APOGEE_TIME)+","+String(flightData.APOGEE_FALL_SPEED)+",";
    All_data1+=String(flightData.MAIN_FALL_SPEED)+","+String(flightData.DESCENT_TIME)+","+String(FLIGHT_TIME)+","+String(flightData.unix_time)+"\n";


  //   uint32_t t = millis(); 
       
/* All_dataa = All_data1.length() + 3; 
 char chrr[All_dataa]; 
 char chrr_header[All_dataa]; 
 All_data1.toCharArray(chrr, All_dataa);
 header.toCharArray(chrr_header, All_dataa);*/
 if(header_count==1){
  appendFile(SD, "/GROUNDSTATION.csv", header);
  header_count=0;
  }
 appendFile(SD, "/GROUNDSTATION.csv",  All_data1);
 All_data1="";
 
    }

 if(data.byteArray[0] == passw && data.byteArray[1] == getsensData_flgtstcs  ){
  
c=2;
for( ii=0;ii<3;ii++){ 
flightData.H3LIS331_data[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<4;ii++){ 
flightData.Ms5611_data[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
flightData.bnoAccel[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
flightData.bnoGyro[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
flightData.bnoMag[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
flightData.bnoRaw[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}

for( ii=0;ii<4;ii++){ 
flightData.GPS_data[ii]= ByteTolong(data.byteArray,c);
c+=4;
}


flightData.unix_time= ByteTolong(data.byteArray,c);
c+=4;


flightData.batteryy= ByteToFloat(data.byteArray,c);
c+=4;

flightData.SIV=data.byteArray[c];
c+=1;

pyro_stat = data.byteArray[c];
Serial.print(pyro_stat);
for (int i = 0;i<8; i++) {
        pyro_stat_arr[i] = 0;   
    }
for (int i = 0; pyro_stat > 0; i++) {
        pyro_stat_arr[i] = pyro_stat % 2;
        pyro_stat = pyro_stat / 2;   
    }
    

c+=1;

flightData.MAX_ALT= ByteToFloat(data.byteArray,c);
c+=4;


flightData.MAX_SPEED= ByteToFloat(data.byteArray,c);
c+=4;

for( ii=0;ii<3;ii++){ 
flightstatMax.MAX_ACCEL[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}

flightData.MAX_SIV= data.byteArray[c];
c+=1;

flightData.ENGINEBURN_TIME = ByteTounsignedlong(data.byteArray,c);
c+=4;

flightData.APOGEE_TIME= ByteTounsignedlong(data.byteArray,c);
c+=4;


flightData.APOGEE_FALL_SPEED = ByteToFloat(data.byteArray,c);
c+=4;

flightData.MAIN_FALL_SPEED = ByteToFloat(data.byteArray,c);
c+=4;

flightData.DESCENT_TIME= ByteTounsignedlong(data.byteArray,c);

FLIGHT_TIME = (flightData.APOGEE_TIME/1000) + (flightData.DESCENT_TIME/1000);
c=0;




    print_date_time();

    All_data1=String(buf)+","+String(flightData.H3LIS331_data[0])+","+String(flightData.H3LIS331_data[1])+","+String(flightData.H3LIS331_data[2])+",";
    All_data1+=String(flightData.Ms5611_data[0])+","+String(flightData.Ms5611_data[1])+","+String(flightData.Ms5611_data[2])+","+String(flightData.Ms5611_data[3])+",";
    All_data1+=String(flightData.bnoAccel[0])+","+String(flightData.bnoAccel[1])+","+String(flightData.bnoAccel[2])+",";
    All_data1+=String(flightData.bnoGyro[0])+","+String(flightData.bnoGyro[1])+","+String(flightData.bnoGyro[2])+",";
    All_data1+=String(flightData.bnoMag[0])+","+String(flightData.bnoMag[1])+","+String(flightData.bnoMag[2])+",";
    All_data1+=String(flightData.bnoRaw[0])+","+String(flightData.bnoRaw[1])+","+String(flightData.bnoRaw[2])+",";
    All_data1+=String(flightData.GPS_data[0])+","+String(flightData.GPS_data[1])+","+String(flightData.GPS_data[2])+","+String(float(flightData.GPS_data[3])/1000)+",";
    All_data1+=String(flightData.SIV)+","+String(flightData.batteryy)+","+String(pyro_stat_arr[0])+","+String(pyro_stat_arr[2])+",";
    All_data1+=String(flightData.MAX_ALT)+","+String(flightData.MAX_SPEED)+","+String(flightData.MAX_ACCEL[0])+","+String(flightData.MAX_ACCEL[1])+","+String(flightData.MAX_ACCEL[2])+",";
    All_data1+=String(flightData.MAX_SIV)+","+String(flightData.ENGINEBURN_TIME)+","+String(flightData.APOGEE_TIME)+","+String(flightData.APOGEE_FALL_SPEED)+",";
    All_data1+=String(flightData.MAIN_FALL_SPEED)+","+String(flightData.DESCENT_TIME)+","+String(FLIGHT_TIME)+","+String(flightData.unix_time)+"\n";


  //   uint32_t t = millis(); 
       
 /*All_dataa = All_data1.length() + 3; 
 char chrr[All_dataa]; 
 char chrr_header[All_dataa]; 
 All_data1.toCharArray(chrr, All_dataa);
 header.toCharArray(chrr_header, All_dataa);*/
 if(header_count==1){
  appendFile(SD, "/GROUNDSTATION.csv", header);
  header_count=0;
  }
 appendFile(SD, "/GROUNDSTATION.csv",  All_data1);
 All_data1="";
 
    }



if(data.byteArray[0] == passw && data.byteArray[1] == getsensData_tch_grnd){

 c=2;
 for( ii=0;ii<3;ii++){ 
flightData.GPS_data[ii]= ByteTolong(data.byteArray,c);
c+=4;
}
c+=1; 
flightData.batteryy= ByteToFloat(data.byteArray,c);
c=0;
}

EEPROM.writeFloat(42,flightData.GPS_data[0]);
EEPROM.writeFloat(46,flightData.GPS_data[1]);

    if(data.byteArray[0] == passw && data.byteArray[1] == get_frq_config){
      
      c=2;
      carrierFreq = ByteToFloat(data.byteArray,c);
      c+=4;
      outputPower = ByteTouint8t(data.byteArray,c);
      c=0;
      kk=1;                                                // config alındığına dair bilgi için değişken.
      cnt=0;
      frst_cnt+=1;
      
      if(frq==carrierFreq && dbm==outputPower && frst_cnt > 1){ compare=1; Serial.print(" karşılaştırma başarılı ");   }   // data karşılaştırma
        else if(frst_cnt==1){ compare=4; Serial.print(" ilk config alındı ");}
        else if(!(frq==carrierFreq && dbm==outputPower )){compare=0; v=4; Serial.print(" karşılaştırma başşarısız ");}
      

     //  ilk data istemede gelen datalar istasyondaki değişkenlere atanıyor ekranda görebilmek için
     
      if(check_data==0 && !(compare==0)){
        
        frq=carrierFreq;
        dbm=outputPower;
        last_frq=frq;  
        last_dbm=dbm;
        check_data=1;                // sadece bir defa eşitleme yapması için kullanılıyor. başarısız haberleşme sonucunda tekrar istemede 0 yapılacak
   
            }   
          }

     else if(data.byteArray[0] == passw && data.byteArray[1] == get_prsht_config){
      
      c=2;
      rocketmain = ByteToFloat(data.byteArray,c);
      c=0;
      kk=1;                         // config alındığına dair bilgi için değişken.
      cnt=0;                        // tekrar göndermedeki sayacı sıfırlamak için değişken
      frst_cnt+=1;                  // ilk alımda karşılaştırma yapmaması için
      
      if( main==rocketmain && frst_cnt > 1){ compare=1; Serial.print(" paraşüt karşılaştırma başarılı ");   }   // data karşılaştırma
        else if(frst_cnt==1){ compare=4; Serial.print(" paraşüt ilk config alındı ");}
        else if(!(main==rocketmain)){compare=0; v=4; Serial.print(" paraşüt karşılaştırma başşarısız ");}
      

     //  ilk data istemede gelen datalar istasyondaki değişkenlere atanıyor ekranda görebilmek için
     
      if(check_data==0 && !(compare==0)){
        
        main=rocketmain;
        check_data=1;                // sadece bir defa eşitleme yapması için kullanılıyor. başarısız haberleşme sonucunda tekrar istemede 0 yapılacak
   
            }   
          }
         
       }
    }

    radio.startReceive();
    enableInterrupt = true;

 }

if(gps_cnt==0 && tst==1){
  
 All_data2=String(buf)+","+String(time_gpsfix/1000)+"/n";
appendFile(SD, "/gpstime.csv",  All_data2);
 All_data2="";
 tst=0;
}

 if(millis()-timeout_comm > 10000 && pyro_stat_arr[4]==1){
tch_grnd=1;
timeout_comm = millis();
}

if(screen_id==9 && tch_grnd==1){
  if(millis()-timeout_fall > 1000){
    
      kk=0;
      enableInterrupt = false;
      Serial.print("  GPS DATA İSTENDİ ");
      data.counter=0;
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(getsensData_tch_grnd, &data.counter, data.tr_array);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      enableInterrupt = true;
      timeout_fall = millis();

}
}


if(kk==1 && ok==1){

      enableInterrupt = false;
      data.counter=0;
      Serial.print(" güncelleme bilgisi gönderildi ");
      byteAddByte(passw, &data.counter, data.tr_array);
      byteAddByte(setConfig, &data.counter, data.tr_array);
      transmissionState = radio.startTransmit(data.tr_array,2);
      transmitFlag = true;
      radio.setFrequency(frq);
      radio.setOutputPower(dbm);

      EEPROM.writeFloat(0,frq);
      EEPROM.writeInt(6,dbm);
      EEPROM.commit();
      Serial.print(" HAFIZAYA ALINDI ");
      
      kk=0;
      ok=0;
      rqst_err=1;
      last_st=1;
      enableInterrupt = true;
      delay(1000);


  }

  if(selffrq_set==1){
  
  radio.setFrequency(frq);
  radio.setOutputPower(dbm);
  EEPROM.writeFloat(0,frq);
  EEPROM.writeInt(6,dbm);
  EEPROM.commit();
  selffrq_set=0;
  }
/*
Serial.print(" V= ");
Serial.println(v);

Serial.print(" try_com= ");
Serial.println(try_com);

Serial.print(" compare= ");
Serial.println(compare);

Serial.print(" time_com= ");
Serial.println(time_com);*/
timeout_hz=millis()-timeout_hz;

if(timeout_hz >= 100){
Serial.print(" 0. çekirdek süre ");
Serial.println(timeout_hz);}

}
 
}

void print_date_time() { //easy way to print date and time
  sprintf(buf, "%02d/%02d/%4d %02d:%02d:%02d", Day, Month, Year, Hour, Minute, Seconde);
}

  void loop() {

    tft.setFreeFont(FF33);
    uint16_t t_x = 0, t_y = 0; // To store the touch coordinates
    // Pressed will be set true is there is a valid touch on the screen
    boolean pressed = tft.getTouch(&t_x, &t_y); 
  
    on_btn.press(pressed && on_btn.contains(t_x, t_y));
    off_btn.press(pressed && off_btn.contains(t_x, t_y));

    frq_btn.press(pressed && frq_btn.contains(t_x, t_y));
    prsht_btn.press(pressed && prsht_btn.contains(t_x, t_y));
    
    minus_btn.press(pressed && minus_btn.contains(t_x, t_y));
    plus_btn.press(pressed && plus_btn.contains(t_x, t_y));
    snd_btn.press(pressed && snd_btn.contains(t_x, t_y));
    erase_btn.press(pressed && erase_btn.contains(t_x, t_y));
    ok_btn.press(pressed && ok_btn.contains(t_x, t_y));
    rqst_btn.press(pressed && rqst_btn.contains(t_x, t_y));
    try_btn.press(pressed && try_btn.contains(t_x, t_y));
   
    
    one_btn.press(pressed && one_btn.contains(t_x, t_y));
    ten_btn.press(pressed && ten_btn.contains(t_x, t_y));
    hundred_btn.press(pressed && hundred_btn.contains(t_x, t_y));
    fivehundred_btn.press(pressed && fivehundred_btn.contains(t_x, t_y));

    point_one_btn.press(pressed && point_one_btn.contains(t_x, t_y));
    point_t_btn.press(pressed && point_t_btn.contains(t_x, t_y));
    point_five_btn.press(pressed && point_five_btn.contains(t_x, t_y));
    one_thousand_btn.press(pressed && one_thousand_btn.contains(t_x, t_y));

    cnf_btn.press(pressed && cnf_btn.contains(t_x, t_y));
    roc_one_btn.press(pressed && roc_one_btn.contains(t_x, t_y));
    roc_two_btn.press(pressed && roc_two_btn.contains(t_x, t_y));
    gs_btn.press(pressed && gs_btn.contains(t_x, t_y));
    finder_btn.press(pressed && finder_btn.contains(t_x, t_y));
    main_btn.press(pressed && main_btn.contains(t_x, t_y));
    flight_sttcs_btn.press(pressed && flight_sttcs_btn.contains(t_x, t_y));
    
    imprl_btn.press(pressed && imprl_btn.contains(t_x, t_y));
    mtrc_btn.press(pressed && mtrc_btn.contains(t_x, t_y));
    lsm_btn.press(pressed && lsm_btn.contains(t_x, t_y));
    utc_btn.press(pressed && utc_btn.contains(t_x, t_y));

    
 if (now() - prev_set > timesetinterval && gps.time.isValid())  // set the microcontroller time every interval, only if there is a valid GPS time
  {
    setthetime();
    prev_set = now();
    smartDelay(10);
  }
   utc = now();  // read the time in the correct format to change via the TimeChangeRules
  local = CE.toLocal(utc);

  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));

      if(screen_id==5  ){
        tft.setFreeFont(FF33);
        tft.setTextSize(1);
        tft.fillRoundRect(lft_blck_x+134, lft_blck_y+134, box_x+117, box_y+3,3, WHITE); 
        tft.setCursor(lft_blck_x+140, lft_blck_y+147);
        tft.print(bmp.readTemperature());
      
        }
 

   //   Serial.print(F("Approx altitude = "));
  //  Serial.print(bmp.readAltitude(1017)); /* Adjusted to local forecast! */

    
my_temp= 36;
my_alt = 940;

rckt_temp= 5;
rckt_alt = 3500;
rckt_v1= 140;
rckt_v2= 504;
rckt_dist=2000;

if(u==1){

my_temp= ((my_temp)*1.8)+32;
my_alt = ((my_alt)*3.2808);

rckt_temp= ((rckt_temp)*1.8)+32;
rckt_alt = ((rckt_alt)*3.2808);
rckt_v1= ((rckt_v1)*3.2808);
rckt_v2= ((rckt_v2)/1.609);
rckt_dist=((rckt_dist)*3.2808);

}




  if((screen_id==3  && k==0)){
  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.fillRoundRect(box_x+12, lft_blck_y+4, box_x+10, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+85, lft_blck_y+8); 
  tft.print(time_gpsfix/1000);
  tft.print(" sn");
  //tft.print(unit[t]);
  
  tft.fillRoundRect(box_x+11, lft_blck_y+25, box_x+10, box_y,3, WHITE);          //ikinci kutucuk
  tft.setCursor(lft_blck_x+82, lft_blck_y+29);
  tft.print(flightData.Ms5611_data[2]);
  tft.print(" ");
  tft.print(unit[d]);

  tft.fillRoundRect(box_x+11, lft_blck_y+50, box_x+10, box_y,3, WHITE);          //ikinci kutucuk
  tft.setCursor(lft_blck_x+82, lft_blck_y+54);
  tft.print(flightData.Ms5611_data[3]);
  tft.print(unit[v1]);
 /* tft.print(flightData.bnoGyro[0]);*/

  tft.fillRoundRect(box_x+11, lft_blck_y+69, box_x+10, box_y,3, WHITE);          //ikinci kutucuk
  tft.setCursor(lft_blck_x+82, lft_blck_y+73);
  tft.print(float(flightData.GPS_data[3])/1000);
  tft.print(unit[v1]);

/*
  tft.fillRoundRect(box_x+11, lft_blck_y+88, box_x+10, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+92);
  tft.print(flightData.bnoGyro[2]);*/
  if(gps.location.lat()>0)tft.fillCircle(lft_blck_x+86, lft_blck_y+98, 5, GREEN);
  else tft.fillCircle(lft_blck_x+86, lft_blck_y+96, 5, DARKGREY);
  
  
  tft.fillRoundRect(box_x+11, lft_blck_y+118, box_x+10, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+122);
  tft.print(double(flightData.GPS_data[0])/1000000,6);

  tft.fillRoundRect(box_x+11, lft_blck_y+137, box_x+10, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+141);
  tft.print(double(flightData.GPS_data[1])/1000000,6);

  tft.fillRoundRect(box_x+26, lft_blck_y+156, box_x-5, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+94, lft_blck_y+160);
  tft.print(flightData.SIV);

  tft.fillRoundRect(rght_blck_x+76, rght_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk
  tft.setCursor(rght_blck_x+100, rght_blck_y+122);
  tft.print(flightData.bnoAccel[0]);

  tft.fillRoundRect(rght_blck_x+76, rght_blck_y+136, box_x+10, box_y,3, WHITE);          //ikinci kutucuk
  tft.setCursor(rght_blck_x+100, rght_blck_y+140);
  tft.print(flightData.bnoAccel[1]);

  tft.fillRoundRect(rght_blck_x+76, rght_blck_y+155, box_x+10, box_y,3, WHITE); 
  tft.setCursor(rght_blck_x+100, rght_blck_y+159);
  tft.print(flightData.bnoAccel[2]);

  tft.fillRoundRect(rght_blck_x+80, rght_blck_y+5, box_x+6, box_y,3, WHITE);
  tft.setCursor(rght_blck_x+83, rght_blck_y+8);
  tft.print(flightData.H3LIS331_data[0]);

  tft.fillRoundRect(rght_blck_x+80, rght_blck_y+24, box_x+6, box_y,3, WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+29);
  tft.print(flightData.H3LIS331_data[1]);

  tft.fillRoundRect(rght_blck_x+80, rght_blck_y+49, box_x+6, box_y,3, WHITE); 
  tft.setCursor(rght_blck_x+82, rght_blck_y+54);
  tft.print(flightData.H3LIS331_data[2]);

  if(pyro_stat_arr[0]==0)PYRO1_COLOR=DARKGREY;
  else PYRO1_COLOR=GREEN;
  
  if(pyro_stat_arr[1]==0)PYRO2_COLOR=DARKGREY;
  else PYRO2_COLOR=GREEN;
  
  if(pyro_stat_arr[2]==1 )PYRO1_COLOR=RED;
  if(pyro_stat_arr[3]==1 )PYRO2_COLOR=RED;

  
 /* Serial.print(pyro_stat_arr[0]);
  Serial.print(pyro_stat_arr[1]);
  Serial.print(pyro_stat_arr[2]);
  Serial.println(pyro_stat_arr[3]);*/

  tft.fillCircle(rght_blck_x+97, rght_blck_y+74, 5, PYRO1_COLOR);
  tft.fillCircle(rght_blck_x+127, rght_blck_y+74, 5, PYRO2_COLOR);

  tft.fillRoundRect(rght_blck_x+80, rght_blck_y+92, box_x+6, box_y,3, WHITE);          //ikinci kutucuk
  tft.setCursor(rght_blck_x+93, rght_blck_y+96);
  dist_calculation();
  tft.print(dist_calc);
  
 }


  if((screen_id==4  && k==0)){



  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,WHITE);
  
  tft.fillRoundRect(box_x+11, lft_blck_y+27, box_x+10, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+31);
  tft.print(flightData.bnoGyro[0]);

  tft.fillRoundRect(box_x+11, lft_blck_y+46, box_x+10, box_y,3, WHITE); 
  tft.setCursor(lft_blck_x+79, lft_blck_y+51);
  tft.print(flightData.bnoGyro[1]);

  tft.fillRoundRect(box_x+11, lft_blck_y+65, box_x+10, box_y,3, WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+70);
  tft.print(flightData.bnoGyro[2]);

 //tft.fillRoundRect(box_x+11, lft_blck_y+91, box_x+10, box_y,3, WHITE); 
 //tft.setCursor(lft_blck_x+79, lft_blck_y+94);
 if(pyro_stat_arr[6]==1)MACH_LOCK_COLOR=RED;
 tft.fillCircle(lft_blck_x+85, lft_blck_y+97, 5, MACH_LOCK_COLOR);

tft.fillRoundRect(rght_blck_x+80, rght_blck_y+25, box_x+6, box_y,3, WHITE); 
tft.setCursor(rght_blck_x+86, rght_blck_y+29);
tft.print(flightData.bnoMag[0]);

tft.fillRoundRect(rght_blck_x+80, rght_blck_y+49, box_x+6, box_y,3, WHITE);
tft.setCursor(rght_blck_x+86, rght_blck_y+53);
tft.print(flightData.bnoMag[1]);

tft.fillRoundRect(rght_blck_x+80, rght_blck_y+67, box_x+6, box_y,3, WHITE);
tft.setCursor(rght_blck_x+86, rght_blck_y+71);
tft.print(flightData.bnoMag[2]);

tft.fillRoundRect(rght_blck_x+76, rght_blck_y+91, box_x+10, box_y,3, WHITE);
tft.setCursor(rght_blck_x+86, rght_blck_y+96);
tft.print(flightData.bnoRaw[0]);

tft.fillRoundRect(box_x+11, lft_blck_y+114, box_x+10, box_y,3, WHITE);
tft.setCursor(rght_blck_x+82, rght_blck_y+118);
tft.print(flightData.bnoRaw[1]);

tft.fillRoundRect(box_x+11, lft_blck_y+134, box_x+10, box_y,3, WHITE);  
tft.setCursor(rght_blck_x+82, rght_blck_y+138);
tft.print(flightData.bnoRaw[2]);


  }

  
 
     /*  while(SerialGPS.available() > 0){ // check for gps data
       if(gps.encode(SerialGPS.read())){ // encode gps data
       if(counterr > 2) {
        /*Serial.print("SATS: ");
        Serial.println(gps.satellites.value());
        Serial.print("LAT: ");
        Serial.println(gps.location.lat(), 6);
        Serial.print("LONG: ");
        Serial.println(gps.location.lng(), 6);
        Serial.print("ALT: ");
        Serial.println(gps.altitude.meters()); 
        
        Serial.print("Date: ");
        Serial.print(gps.date.day()); Serial.print("/");
        Serial.print(gps.date.month()); Serial.print("/");
        Serial.println(gps.date.year());
    
        Serial.print("Hour: ");
        Serial.print(gps.time.hour()); Serial.print(":");
        Serial.print(gps.time.minute()); Serial.print(":");
        Serial.println(gps.time.second());
               Serial.println("---------------------------");*/
     /*   counterr = 0;
       }
       else counterr++;
       }
       }*/

if(gps.satellites.value()<4)time_gpsfix=millis();
if(gps.satellites.value()>=4 && gps_cnt==1){time_gpsfix=millis()-time_gpsfix; gps_cnt=0;}

if(alt_calc==0){
        grnd_alt=bmp.readAltitude(1030.0);
        alt_calc=1;
}
        
       if((screen_id==5 && k==0) ){ 
     compass.read();
     float heading = compass.heading();
        
      tft.fillRoundRect(lft_blck_x+134, lft_blck_y+6, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk
     
     //tft.setFreeFont(FF33);
     tft.setTextColor(  BLACK,WHITE);
     tft.setFreeFont(FM9);
     tft.setCursor(lft_blck_x+140, lft_blck_y+19);
     tft.print(gps.location.lat(), 6);
     tft.fillRoundRect(lft_blck_x+134, lft_blck_y+27, box_x+117, box_y+3,3, WHITE);
     tft.setCursor(lft_blck_x+140, lft_blck_y+40);
     tft.print(gps.location.lng(), 6);
     tft.setTextColor( BLACK,WHITE);
     tft.setCursor(lft_blck_x+140, lft_blck_y+60);
     tft.fillRoundRect(lft_blck_x+134, lft_blck_y+48, box_x+117, box_y+3,3, WHITE);
     tft.print(gps.satellites.value());

     tft.fillRoundRect(lft_blck_x+134, lft_blck_y+94, box_x+117, box_y+3,3, WHITE);
     tft.setCursor(lft_blck_x+140, lft_blck_y+106);
     
     tft.print(bmp.readAltitude(1030.0)-grnd_alt);
  
     tft.fillRoundRect(lft_blck_x+134, lft_blck_y+114, box_x+117, box_y+3,3, WHITE);
     tft.setCursor(lft_blck_x+140, lft_blck_y+127);
     tft.print(heading,1);
   

      }
      
 

 
  /*  Serial.print(" ekr=");
    Serial.println(screen_id);
    Serial.print(" k=");
    Serial.print(k);
    Serial.print(" ");
    Serial.print(" i=");
    Serial.print(i);
    Serial.print(" frq kts");
    Serial.print(frq_kts);*/






if(!(i==2 || i==3 || i==4 || i==5 || i==7 || i==8 || i==9 || i==10 || i==13)){
    tft.setFreeFont(FF33);
    if (off_btn.justPressed()) {
        off_btn.drawButton(true);     
        screen_id++;
        k=1;
    }
     if (on_btn.justPressed()) {
        on_btn.drawButton(true);
           
         screen_id--;
         k=1; 
         }

        if (off_btn.justReleased())
            off_btn.drawButton();

        if (on_btn.justReleased())
            on_btn.drawButton();
}


if(i==5){ 
                 tft.setFreeFont(FF33);
                 if (plus_btn.justReleased()) plus_btn.drawButton();
                 if (minus_btn.justReleased()) minus_btn.drawButton();  

                 if (plus_btn.justPressed() ){
                     plus_btn.drawButton(true);
                     dbm = dbm+1;
                     if(dbm >= 22)dbm=22;
                     tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                     tft.setTextColor( BLACK,WHITE);
                     tft.setCursor(122, 106);
                     tft.print(dbm);
                     tft.print(" dbm");

                 }        
                    

               if  (minus_btn.justPressed()){
                    minus_btn.drawButton(true);
                    dbm = dbm-1;
                    if(dbm <= 0)dbm=0;
                    tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                    tft.setTextColor( BLACK,WHITE);
                    tft.setCursor(122, 106);
                    tft.print(dbm);
                    tft.print(" dbm");
                } 

                 if (on_btn.justPressed()) {
                     on_btn.drawButton(true); 
                     frq_config_page();
                     
                    } 
                 }

    if(i==4){

                 tft.setFreeFont(FF33);
                 if (plus_btn.justReleased()) plus_btn.drawButton();
                 if (minus_btn.justReleased()) minus_btn.drawButton();  
     
                 if(frq_ktss==1) point_one_btn.drawButton(true);
                     else point_one_btn.drawButton();
                 if(frq_ktss==2) point_t_btn.drawButton(true);
                     else point_t_btn.drawButton();
                 if(frq_ktss==3) point_five_btn.drawButton(true);
                     else  point_five_btn.drawButton();
                 if(frq_ktss==4) one_thousand_btn.drawButton(true);
                     else one_thousand_btn.drawButton();

                 if (point_one_btn.justPressed() ) {
                     point_one_btn.drawButton(true);     
                     frq_kts=0.10;
                     frq_ktss=1;
                    }    
                 if (point_t_btn.justPressed() ) {
                     point_t_btn.drawButton(true);      
                     frq_kts=0.20;
                     frq_ktss=2;
                    } 
                 if (point_five_btn.justPressed() ) {
                     point_five_btn.drawButton(true);      
                     frq_kts=0.50;
                     frq_ktss=3;
                    } 
                 if (one_thousand_btn.justPressed() ) {
                     one_thousand_btn.drawButton(true);      
                     frq_kts=1;
                     frq_ktss=4;
                    }                                  

                 if (plus_btn.justPressed() ){
                     plus_btn.drawButton(true);
                     frq = frq+frq_kts;
                     tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                     tft.setTextColor( BLACK,WHITE);
                     tft.setCursor(118, 106);
                     tft.print(frq);
                     tft.print(" MHz");

                 }

               if  (minus_btn.justPressed()){
                    minus_btn.drawButton(true);
                    frq = frq-frq_kts;
                    if(frq <= 860.00)frq=860.00;
                    tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                    tft.setTextColor( BLACK,WHITE);
                    tft.setCursor(118, 106);
                    tft.print(frq);
                    tft.print(" MHz");
                } 
           

                 if (on_btn.justPressed()) {
                     on_btn.drawButton(true);     
                     frq_config_page();
                    
                    }  
                 }

     if(i==3){
                 tft.setFreeFont(FF33);
                 if (plus_btn.justReleased()) plus_btn.drawButton();
                 if (minus_btn.justReleased()) minus_btn.drawButton();  

                 if(kts==1) one_btn.drawButton(true);
                     else one_btn.drawButton();
                 if(kts==10) ten_btn.drawButton(true);
                     else ten_btn.drawButton();
                 if(kts==100) hundred_btn.drawButton(true);
                     else  hundred_btn.drawButton();
                 if(kts==500) fivehundred_btn.drawButton(true);
                     else fivehundred_btn.drawButton();



                 if (one_btn.justPressed() ) {
                     one_btn.drawButton(true);     
                     kts=1;
                    }    
                 if (ten_btn.justPressed() ) {
                     ten_btn.drawButton(true);      
                     kts=10;
                    } 
                 if (hundred_btn.justPressed() ) {
                     hundred_btn.drawButton(true);      
                     kts=100;
                    } 
                 if (fivehundred_btn.justPressed() ) {
                     fivehundred_btn.drawButton(true);      
                     kts=500;
                    }                                  
 
                 if (plus_btn.justPressed() ){
                     plus_btn.drawButton(true);
                     main = main+kts;
                     tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                     tft.setTextColor( BLACK,WHITE);
                     tft.setCursor(122, 106);
                     tft.print(main);
                     tft.print(" ");
                     tft.print(unit[d]);

                 }

               if (minus_btn.justPressed()){
                    minus_btn.drawButton(true);
                    main = main-kts;
                    if(main <= 0)main=0;
                    tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                    tft.setTextColor( BLACK,WHITE);
                    tft.setCursor(122, 106);
                    tft.print(main);
                    tft.print(" ");
                    tft.print(unit[d]);
                    
                } 
                    if (on_btn.justPressed()){
                     on_btn.drawButton(true);    
                     prsht_config_page();
                    }

              }

/*if(i==2){
                      tft.setFreeFont(FF33);
                      if (plus_btn.justReleased()) plus_btn.drawButton();
                      if (minus_btn.justReleased()) minus_btn.drawButton();  

                      if(kts==1) one_btn.drawButton(true);
                        else one_btn.drawButton();
                     if(kts==10) ten_btn.drawButton(true);
                        else ten_btn.drawButton();
                     if(kts==100) hundred_btn.drawButton(true);
                        else  hundred_btn.drawButton();
                     if(kts==500) fivehundred_btn.drawButton(true);
                        else fivehundred_btn.drawButton();



                          if (one_btn.justPressed() ) {
                          one_btn.drawButton(true);     
                          kts=1;
                        }    
                      if (ten_btn.justPressed() ) {
                          ten_btn.drawButton(true);      
                          kts=10;
                        } 
                          if (hundred_btn.justPressed() ) {
                          hundred_btn.drawButton(true);      
                          kts=100;
                        } 
                      if (fivehundred_btn.justPressed() ) {
                          fivehundred_btn.drawButton(true);      
                          kts=500;
                        }                                  

                      if (plus_btn.justPressed() ){
                      plus_btn.drawButton(true);
                      apogee = apogee+kts;
                      tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                      tft.setTextColor( BLACK,WHITE);
                      tft.setCursor(122, 106);
                      tft.print(apogee);
                      tft.print(" ");
                      tft.print(unit[d]);

                    }

                      if (minus_btn.justPressed()){
                      minus_btn.drawButton(true);
                      apogee = apogee-kts;
                      if(apogee <= 0)apogee=0;
                      tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
                      tft.setTextColor( BLACK,WHITE);
                      tft.setCursor(122, 106);
                      tft.print(apogee);
                      tft.print(" ");
                      tft.print(unit[d]);
  
                   }
                           if (on_btn.justPressed()){
                           on_btn.drawButton(true);  
                           prsht_config_page();
                        }
            }

  */

    if(i==1){


                tft.setFreeFont(FF33);
             if (prsht_btn.justPressed()){
                 prsht_btn.drawButton(true);
                 delay(250);
                 prsht_config_page();
                  
               }
               
               if (frq_btn.justPressed()){
                  frq_btn.drawButton(true);
                  delay(250);
                  frq_config_page();
               }

     }

    if(i==8){

              config_page=1;
              tft.setFreeFont(FF33);
              if((t_x > 120 && t_x < 215) && (t_y > 87 && t_y < 112 ) ){

                   tft.setTextColor( BLACK,a);
                   tft.fillRoundRect(120, 87, 95, 25,3, DARKGREY);
                   tft.setCursor(140, 104);
                   tft.print("ACTIVE");
                   delay(250);
                   tft.fillRoundRect(120, 87, 95, 25,3, WHITE);
                   tft.setCursor(140, 104);
                   tft.print("ACTIVE");
              }



       if(compare==5){
               
               if(snd_btn.justReleased()) snd_btn.drawButton();
               if(snd_btn.justPressed()){
                  snd_btn.drawButton(true);
                  tft.fillRoundRect(70, 50, 190, 130,9, YELLOW);
                  tft.setTextColor( RED,BLACK);
                  tft.setCursor(115, 80);
                  tft.print("SENDING");
                  stt=3;
                  i=7;
                  time_com=0;
                  prsht_snd_err=1;
                }
          }

         
               if(rqst_btn.justReleased()) rqst_btn.drawButton();
               if(rqst_btn.justPressed()){
                  rqst_btn.drawButton(true);
                  tft.fillRoundRect(70, 50, 190, 130,9, YELLOW);
                  tft.setTextColor( RED,BLACK);
                  tft.setCursor(115, 80);
                  tft.print("WAITING");
                  stt=4;
                  time_com=0;
                  i=7;
                  check_data=0;
                  prsht_rqst_err=1;
               }



                 if(erase_btn.justReleased()) erase_btn.drawButton();
                   if(erase_btn.justPressed()){
                    erase_btn.drawButton(true);
                    tft.setTextColor( WHITE,BLACK);
                    tft.setCursor(235, 115);
                    tft.print("ERASED");
                }

            
           /*   if((t_x > 120 && t_x < 215) && (t_y > 23 && t_y < 48 ) )
             { 
                 i=2;
                 tft.fillRoundRect(120, 23, 95, 25,3, DARKGREY);
                 a= DARKGREY;
                 
                 tft.setTextColor( BLACK,a);
                 tft.setCursor(125, 40);
                 tft.print(apogee);
                 tft.print(" ");
                 tft.print(unit[d]);
                 delay(250);
                 prsht_config(apogee);

              }*/
           if((t_x > 120 && t_x < 215) && (t_y > 55 && t_y < 80 ) )
             { 
                 i=3;
                 tft.fillRoundRect(120, 55, 95, 25,3, DARKGREY);
                 a= DARKGREY;
                 tft.setTextColor( BLACK,a);
                 tft.setCursor(125, 72);
                 tft.print(main);
                 tft.print(" ");
                 tft.print(unit[d]);
                 delay(250);
                 prsht_config(main);

              }

                     if (on_btn.justPressed()) {
                     on_btn.drawButton(true);      
                     rocketconfig_page();
                    } 

     }


     
     if(i==9){

       config_page=0;

tft.setFreeFont(FF33);
        if((t_x > 20 && t_x < 160) && (t_y > 150 && t_y < 185)){
         
  
  tft.fillRoundRect(20, 150, 140, 25,3, WHITE);
  tft.setCursor(27, 167);
  tft.print("FRQ CHNGD");
  selffrq_set=1;
          }
        
       if(compare==5){
              
               if(snd_btn.justReleased()) snd_btn.drawButton();
               if(snd_btn.justPressed()){
                  snd_btn.drawButton(true);
                  tft.fillRoundRect(70, 50, 190, 130,9, YELLOW);
                  tft.setTextColor( RED,BLACK);
                  tft.setCursor(115, 80);
                  tft.print("SENDING");
                  stt=2;
                  i=7;
                  time_com=0;
                  snd_err=1;
                }
             }

         
               if(rqst_btn.justReleased()) rqst_btn.drawButton();
               if(rqst_btn.justPressed()){
                  rqst_btn.drawButton(true);
                  tft.fillRoundRect(70, 50, 190, 130,9, YELLOW);
                  tft.setTextColor( RED,BLACK);
                  tft.setCursor(115, 80);
                  tft.print("WAITING");
                  stt=1;
                  time_com=0;
                  i=7;
                  check_data=0;
                  rqst_err=1;
               }



                 if(erase_btn.justReleased()) erase_btn.drawButton();
                   if(erase_btn.justPressed()){
                    erase_btn.drawButton(true);
                    tft.setTextColor( WHITE,BLACK);
                    tft.setCursor(235, 115);
                    tft.print("ERASED");
                }


              if((t_x > 120 && t_x < 215) && (t_y > 23 && t_y < 48 ) )
             { 
                 i=4;
                 tft.fillRoundRect(120, 23, 95, 25,3, DARKGREY);
                 a= DARKGREY;
                 tft.setTextColor( BLACK,a);
                 tft.setCursor(125, 40);
                 tft.print(frq);
                 tft.print(" MHz");
                 delay(250);
                 frqconfig_page(frq);

              }
              
           if((t_x > 120 && t_x < 215) && (t_y > 55 && t_y < 80 ) )
             { 
                 i=5;
                 tft.fillRoundRect(120, 55, 95, 25,3, DARKGREY);
                 a= DARKGREY;
                 tft.setTextColor( BLACK,a);
                 tft.setCursor(125, 72);
                 tft.print(dbm);
                 tft.print(" dbm");
                 delay(250);
                dbmconfig_page(dbm);

             }

                    if (on_btn.justPressed()){
                     on_btn.drawButton(true);     
                     rocketconfig_page();
                    }
                


     }



if(i==7){
       

                  tft.setFreeFont(FF33);
                  
                  if(cnt==5 && kk==0 && v==2){
                    
                  tft.fillRoundRect(70, 50, 190, 130,9, YELLOW);
                  tft.setTextColor( RED,WHITE);
                  tft.setCursor(85, 80);
                  tft.print("COMMUNICATION");
                  tft.setCursor(125, 105);
                  tft.print("FAILED");
                  try_btn.initButton(&tft,160, 145, 60, 25,  BLACK, CYAN, BLACK, "OK",1);
                  try_btn.drawButton(false);
                  v=0;
                  }

                  if(try_btn.justPressed() && cnt==5 ){
                  try_btn.drawButton(true);
                  if(config_page==1)prsht_config_page();
                  else frq_config_page();
                  cnt=0;
                  rqst_err=0;
                  snd_err=0;
                  prsht_rqst_err=0;
                  prsht_snd_err=0;
                  }
    
                  if(compare==0 && v==4){
                    
                  tft.fillRoundRect(70, 50, 190, 130,9, YELLOW);
                  tft.setTextColor( BLACK,WHITE);
                  tft.setCursor(100, 80);
                  tft.print("DATA DID NOT");
                  tft.setCursor(125, 105);
                  tft.print("MATCHED");
                  try_com=1;                    // tekrar veri istemek için değişken
                  v=0;
                  compare=5;
                  }

                  if(try_com==1){
  
                  try_btn.initButton(&tft,160, 145, 60, 25,  BLACK, CYAN, BLACK, "OK",1);
                  try_btn.drawButton(false);
                  if(try_btn.justPressed()){
                  try_btn.drawButton(true);
                  if(config_page==1)prsht_config_page();
                  else frq_config_page();
                  cnt=0;
                  rqst_err=0;
                  snd_err=0;
                  prsht_rqst_err=0;
                  prsht_snd_err=0;
                  try_com=0;
                  
                  }
                  
                    
                    
                    }
                    if((compare==1  && last_st==1)){        
                      
                  tft.fillRoundRect(70, 50, 190, 130,9, YELLOW);
                  tft.setTextColor( BLACK,WHITE);
                  tft.setCursor(90, 80);
                  tft.print("NEW FREQUENCY");
                  ok_btn.initButton(&tft,160, 145, 60, 25,  BLACK, CYAN, BLACK, "OK",1);
                  ok_btn.drawButton(false);
                  try_com=0;                    // tekrar veri istemek için değişken
                  v=1;                          // buranın sadece ekrana 1 defa basması için olan değişken
                  rqst_err=0;
                  snd_err=0;
                  prsht_rqst_err=0;
                  prsht_snd_err=0;
                  compare=5;
                  last_st=0;
                      
                      }
                  
                   if((compare==1 && v==0 && last_st==0 ) || (compare==4 && v==0)){
                    
                  tft.fillRoundRect(70, 50, 190, 130,9, YELLOW);
                  tft.setTextColor( BLACK,WHITE);
                  tft.setCursor(115, 80);
                  tft.print("SUCCESS");
                  ok_btn.initButton(&tft,160, 145, 60, 25,  BLACK, CYAN, BLACK, "OK",1);
                  ok_btn.drawButton(false);
                  try_com=0;                    // tekrar veri istemek için değişken
                  v=1;                          // buranın sadece ekrana 1 defa basması için olan değişken
                  rqst_err=0;
                  snd_err=0;
                  prsht_rqst_err=0;
                  prsht_snd_err=0;
                  compare=5;
                  }
                  
                         
  
               
               //if(ok_btn.justReleased()) ok_btn.drawButton();
               if(ok_btn.justPressed() && v==1 ){
                  ok_btn.drawButton(true);
                  tft.fillRoundRect(70, 50, 190, 130,9, GREEN);
                  tft.setTextColor( BLACK,WHITE);
                  tft.setCursor(115, 80);
                  tft.print("UPGRATED");
                  carrierFreq = frq;
                  outputPower = dbm;
                  compare=5;
                  color_press_snd=GREEN;
                  v=0;
                  delay(1000);
                  if(config_page==1)prsht_config_page();
                  else frq_config_page();

               }
       
       }

       if(i==10){

  compass.read();
  
  running_min.x = min(running_min.x, compass.m.x);
  running_min.y = min(running_min.y, compass.m.y);
  running_min.z = min(running_min.z, compass.m.z);

  running_max.x = max(running_max.x, compass.m.x);
  running_max.y = max(running_max.y, compass.m.y);
  running_max.z = max(running_max.z, compass.m.z);


 // min values
tft.fillRoundRect(20, 50, 90, 22,7, WHITE); 
tft.fillRoundRect(120, 50, 90, 22,7, WHITE); 
tft.fillRoundRect(220, 50, 90, 22,7, WHITE); 

//max vaules
tft.fillRoundRect(20, 90, 90, 22,7, WHITE); 
tft.fillRoundRect(120, 90, 90, 22,7, WHITE); 
tft.fillRoundRect(220, 90, 90, 22,7, WHITE);

         tft.setTextColor(  BLACK,WHITE);
         tft.setFreeFont(FF33);
         tft.setCursor(35, 65);
         tft.print(running_min.x);
         tft.setCursor(135, 65);
         tft.print(running_min.y);
         tft.setCursor(235, 65);
         tft.print(running_min.z);

         tft.setCursor(35, 105);
         tft.print(running_max.x);
         tft.setCursor(135, 105);
         tft.print(running_max.y);
         tft.setCursor(235, 105);
         tft.print(running_max.z);

         if(ok_btn.justPressed()){
            ok_btn.drawButton(true);
         compass.init();
         compass.enableDefault();
         compass.m_min = (LSM303::vector<int16_t>){ running_min.x,   running_min.y,  running_min.z};
         compass.m_max = (LSM303::vector<int16_t>){ running_max.x,   running_max.y,   running_max.z};
         compmin_x= running_min.x;
         compmin_y= running_min.y;
         compmin_z= running_min.z;
         compmax_x= running_max.x;
         compmax_y= running_max.y;
         compmax_z= running_max.z;

         
         EEPROM.writeInt(11,compmin_x);
         EEPROM.writeInt(16,compmin_y);
         EEPROM.writeInt(21,compmin_z);
         EEPROM.writeInt(26,compmax_x);
         EEPROM.writeInt(31,compmax_y);
         EEPROM.writeInt(36,compmax_z);
         EEPROM.commit();
         tft.setTextColor(WHITE,BLACK);
         tft.setCursor(112, 130);
         tft.print("CALİBRATED");
          }
          if(ok_btn.justReleased()) ok_btn.drawButton();
        
         if (on_btn.justPressed()){
             on_btn.drawButton(true);     
             myconfig_page();
            // k=1;
                    }
        }


        if(i==11){
          
         
         if (main_btn.justPressed()){
             main_btn.drawButton(true);
             screen_id=1;
             myconfig_page();   
         }
         if(main_btn.justReleased()) main_btn.drawButton();

         if (cnf_btn.justPressed()){
             cnf_btn.drawButton(true);
             screen_id=2;
             rocketconfig_page();   
         }
         if(cnf_btn.justReleased()) cnf_btn.drawButton();

         if (roc_one_btn.justPressed()){
             roc_one_btn.drawButton(true);
             screen_id=3;
             rocketpage1();   
         }
         if(roc_one_btn.justReleased()) roc_one_btn.drawButton();

         if (roc_two_btn.justPressed()){
             roc_two_btn.drawButton(true);
             screen_id=4;
             rocketpage2();   
         }
         if(roc_two_btn.justReleased())roc_two_btn.drawButton();

         if (gs_btn.justPressed()){
             gs_btn.drawButton(true);
             screen_id=5;
             grndstn();   
         }
         if(gs_btn.justReleased()) gs_btn.drawButton();

         if (flight_sttcs_btn.justPressed()){
             flight_sttcs_btn.drawButton(true);
             screen_id=6;
             flightsttcs_page();   
         }
         if(flight_sttcs_btn.justReleased()) flight_sttcs_btn.drawButton();

         if (finder_btn.justPressed()){
             finder_btn.drawButton(true);
             screen_id=9;
              tft.fillScreen(BLACK);
             rocket_finder();   
         }
         if(finder_btn.justReleased()) finder_btn.drawButton();

         
          }

       if(i==13){
        
        if (plus_btn.justPressed()){
            plus_btn.drawButton(true);
            utc_value+=60;
               
               }

         if (minus_btn.justPressed()){
             minus_btn.drawButton(true);
             utc_value-=60;
               
               }
         if(utc_value>=720)utc_value=720;
         if(utc_value<=-660)utc_value=-660;
               
         tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
         tft.setFreeFont(FF33);
         tft.setTextSize(1);
         tft.setTextColor( BLACK,WHITE);
         tft.setCursor(122, 106);
         tft.print("UTC ");
         UTC=utc_value/60;
         tft.print(UTC);
         EEPROM.writeInt(38,utc_value);
         EEPROM.commit();
         CEST.offset=utc_value;
         CET.offset=utc_value;
         CE.setRules(CEST, CET);
       
   
        
        
        if(plus_btn.justReleased()) plus_btn.drawButton();
        if(minus_btn.justReleased()) minus_btn.drawButton();
        
        if(on_btn.justPressed()){
           on_btn.drawButton(true);
           myconfig_page();
          }
        
        }
      

         if(screen_id==1 && i==6){
         tft.setFreeFont(FF33);
            if(imprl_btn.justReleased()) imprl_btn.drawButton();
            if(mtrc_btn.justReleased()) mtrc_btn.drawButton();

            
            if(lsm_btn.justReleased()) lsm_btn.drawButton();

             if (lsm_btn.justPressed()){
                 lsm_btn.drawButton(true);
                 lsm_config_page();
                
               }
            

             if (imprl_btn.justPressed() ){
                imprl_btn.drawButton(true);
                 u=1;
                 d=6;
                 v1=7;
                 v2=8;
                 t=9;
               }
              if (mtrc_btn.justPressed() ){
                  mtrc_btn.drawButton(true);

                u=0;
                d=2;
                v1=3;
                v2=4;
                t=5;
                }

              if (utc_btn.justPressed()){
                  utc_btn.drawButton(true);
                  utc_setpage();
                
               }
        
        if(utc_btn.justReleased()) utc_btn.drawButton();
          
                tft.setFreeFont();
                tft.setCursor(126, 1);
                tft.setTextColor(BLACK,LIGHTGREY);
                tft.print(unit[u]);


            }

            

            

            if(screen_id==6){

tft.fillRoundRect(box_x+11, lft_blck_y+4, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


tft.fillRoundRect(box_x+11, lft_blck_y+25, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


tft.fillRoundRect(box_x+11, lft_blck_y+50, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


tft.fillRoundRect(box_x+11, lft_blck_y+69, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


tft.fillRoundRect(box_x+11, lft_blck_y+88, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


tft.fillRoundRect(box_x+11, lft_blck_y+137, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


tft.fillRoundRect(box_x+11, lft_blck_y+156, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);



  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+8);
  tft.print(FLIGHT_TIME);

  tft.setCursor(lft_blck_x+82, lft_blck_y+29); // max altitude
  tft.print(flightData.MAX_ALT);
  tft.print(unit[d]);

  tft.setCursor(lft_blck_x+82, lft_blck_y+54); // max speed
  tft.print(flightData.MAX_SPEED,3);
  tft.print(unit[v2]);

  tft.setCursor(lft_blck_x+82, lft_blck_y+73);
  tft.print(flightData.APOGEE_FALL_SPEED);
  tft.print(" m/s");
  
  tft.setCursor(lft_blck_x+82, lft_blck_y+92);
  tft.print(flightData.MAIN_FALL_SPEED);
  tft.print(" m/s");
  //tft.print(unit[v1]);  

  tft.setCursor(lft_blck_x+82, lft_blck_y+141);
  tft.print(crc_err);

  tft.setCursor(lft_blck_x+14, lft_blck_y+160);
  tft.print(""); 
  
  tft.setCursor(lft_blck_x+82, lft_blck_y+160);
  tft.print("");   
//****************************************************************************************


tft.fillRoundRect(rght_blck_x+80, rght_blck_y+5, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+80, rght_blck_y+24, box_x+6, box_y,3, WHITE);          //ikinci kutucuk


tft.fillRoundRect(rght_blck_x+93, rght_blck_y+49, box_x-7, box_y,3, WHITE);          //ikinci kutucuk


tft.fillRoundRect(rght_blck_x+80, rght_blck_y+67, box_x+6, box_y,3, WHITE);          //ikinci kutucuk


//tft.fillRoundRect(rght_blck_x+80, rght_blck_y+92, box_x+6, box_y,3, WHITE);          //ikinci kutucuk


tft.fillRoundRect(rght_blck_x+76, rght_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+76, rght_blck_y+136, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


tft.fillRoundRect(rght_blck_x+76, rght_blck_y+155, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

  tft.setCursor(rght_blck_x+85, rght_blck_y+9);
  tft.print(flightData.APOGEE_TIME/1000);

  tft.setCursor(rght_blck_x+85, rght_blck_y+28);
  tft.print(flightData.DESCENT_TIME/1000);

  tft.setCursor(rght_blck_x+97, rght_blck_y+54);
  tft.print(flightData.ENGINEBURN_TIME);

  tft.setCursor(rght_blck_x+85, rght_blck_y+72);
  tft.print(flightData.MAX_SIV);


  tft.setCursor(rght_blck_x+100, rght_blck_y+122);
  tft.print(flightData.MAX_ACCEL[0]);  

  tft.setCursor(rght_blck_x+100, rght_blck_y+140);
  tft.print(flightData.MAX_ACCEL[1]);   

  tft.setCursor(rght_blck_x+100, rght_blck_y+159);
  tft.print(flightData.MAX_ACCEL[2]);             
              
              }
              
    if(screen_id==0 && k==1)menupage();
    if(screen_id==1 && k==1)myconfig_page();
    if(screen_id==2 && k==1)rocketconfig_page();
    if(screen_id==3 && k==1)rocketpage1();
    if(screen_id==4 && k==1)rocketpage2();
    if(screen_id==5 && k==1)grndstn();
    if(screen_id==6 && k==1)flightsttcs_page();
    if(screen_id==7 && k==1)baboon();
    if(screen_id==8 && k==1)mainpage();
    if(screen_id==9 && k==1){ tft.fillScreen(BLACK); rocket_finder();}
    if(screen_id==9)rocket_finder();
    
    if(screen_id==-1)screen_id=9;
    if(screen_id==10)screen_id=0;

   
    
}



void utc_setpage(){
k=0;
i=13;

  tft.fillScreen(BLACK);
  
  tft.fillRoundRect(75, 211, 155, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(100, 227);
  tft.print("UTC PAGE");

  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));


  
tft.setFreeFont(FF33);

  tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(122, 106);
  tft.print("UTC ");
  UTC=utc_value/60;
  tft.print(UTC);
  

plus_btn.initButton(&tft,  70, 100, 60, 25, WHITE, GREEN, BLACK, "+", 1);
minus_btn.initButton(&tft,  250, 100, 60, 25, WHITE, BLUE, BLACK, "-", 1);
plus_btn.drawButton(false); 
minus_btn.drawButton(false); 

on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 
 
}


double ReadVoltage(byte pin){
  double reading = analogRead(pin); // Reference voltage is 3v3 so maximum reading is 3v3 = 4095 in range 0 to 4095
  if(reading < 1 || reading > 4095) return 0;
  // return -0.000000000009824 * pow(reading,3) + 0.000000016557283 * pow(reading,2) + 0.000854596860691 * reading + 0.065440348345433;
  return -0.000000000000016 * pow(reading,4) + 0.000000000118171 * pow(reading,3)- 0.000000301211691 * pow(reading,2)+ 0.001109019271794 * reading + 0.034143524634089;
}





void rocket_finder(){
  i=12;
  k=0;
  tft.setFreeFont(FF33);
   // Create the dial Sprite and dial (this temporarily hijacks the use of the needle Sprite)
  createDialScale(0, 360, 30);   // create scale (start angle, end angle, increment angle)
  //drawEmptyDial("Label", 12345);    // draw the centre of dial in the Sprite

  //dial.pushSprite(110, 0);          // push a copy of the dial to the screen so we can see it

  // Create the needle Sprite
  createNeedle();                // draw the needle graphic
 // needle.pushSprite(95, 7);    // push a copy of the needle to the screen so we can see it
   compass.read();

//40.910602285109974, 29.218639767724778
dist_calculation();


flon1 = radians(flon1);  //also must be done in radians

x2lon = radians(x2lon);  //radians duh.

headingg = atan2(sin(x2lon-flon1)*cos(x2lat),cos(flat1)*sin(x2lat)-sin(flat1)*cos(x2lat)*cos(x2lon-flon1)),2*3.1415926535;
headingg = headingg*180/3.1415926535;  // convert from radians to degrees

int head =headingg; //make it a integer now

if(head<0){

  headingg+=360;   //if the heading is negative then add 360 to make it positive

}




  //int heading = compass.heading();
 /* for(int i=0;i<9;i++)
  {
    pusula+=compass.heading();
    
    }*/
   // pusula= pusula/10;
    pusula=compass.heading();
    pusula=simpleKalmanFilter.updateEstimate(pusula);
    int p= headingg-pusula;
    p+=90;
    
  // Push the needle sprite to the dial Sprite at different angles and then push the dial to the screen
  // Use angle increments in range 1 to 6 for smoother or faster movement
  //  for (int16_t angle = -180; angle <= 180; angle += 3) {
  
    plotDial(100, 0, p, "ROCKET",  pusula);

  tft.fillRoundRect(5, 100, 95, 30,3, TFT_WHITE);
  tft.setTextSize(1);
  tft.setTextColor( TFT_BLACK,TFT_WHITE);
  tft.setCursor(7, 118);
  tft.print(dist_calc);
  tft.print(" M");


  tft.fillRoundRect(5, 40, 135, 30,3, TFT_WHITE);
  tft.setTextSize(1);
  tft.setTextColor( TFT_BLACK,TFT_WHITE);
  tft.setCursor(7, 58);
   tft.print(" ALT.= ");
  tft.print(bmp.readAltitude(1030.0)-grnd_alt);
  tft.print(" M");

    //delay(50);
   // yield(); // Avoid a watchdog time-out
  //}
 //Serial.println(heading);
 // delay(100);
  //delay(1000);  // Pause

  // Update the dial Sprite with decreasing angle and plot to screen at 0,0, no delay
 /* for (int16_t angle = 180; angle >= -180; angle -= 6) {
    plotDial(100, 0, angle, "COMPASS", angle + 180);
    yield(); // Avoid a watchdog time-out
  }*/

  // Now show plotting of the rotated needle direct to the TFT
 // tft.setPivot(45, 150); // Set the TFT pivot point that the needle will rotate around

  // The needle graphic has a black border so if the angle increment is small
  // (6 degrees or less in this case) it wipes the last needle position when
  // it is rotated and hence it clears the swept area to black
 /* for (int16_t angle = 0; angle <= 360; angle += 5)
  {
    needle.pushRotated(angle); // Plot direct to TFT at specifed angle
    yield();                   // Avoid a watchdog time-out
  }*/
tft.setFreeFont(FF33);
on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 

off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
off_btn.drawButton(false); 
  
  }

  void dist_calculation(){
    
float gps_lat=EEPROM.readFloat(42);
float gps_long=EEPROM.readFloat(46);

// 40.968784, 29.165933
// 40.0812907116133, 29.519972928962456

flat1= 2;//gps.location.lat();     // flat1 = our current latitude. flat is from the gps data. 
flon1= 29.217879;//gps.location.lng(); // flon1 = our current longitude. flon is from the fps data.
dist_calc=0;
dist_calc2=0;
diflat=0;
diflon=0;
x2lat= 40.910901;//double(gps_lat)/1000000;   //enter a latitude point here   this is going to be your waypoint
x2lon= 29.217798;//double(gps_long)/1000000;
//---------------------------------- distance formula below. Calculates distance from current location to waypoint
diflat=radians(x2lat-flat1);  //notice it must be done in radians
flat1=radians(flat1);    //convert current latitude to radians
x2lat=radians(x2lat);  //convert waypoint latitude to radians
diflon=radians((x2lon)-(flon1));   //subtract and convert longitudes to radians
dist_calc = (sin(diflat/2.0)*sin(diflat/2.0));
dist_calc2= cos(flat1);
dist_calc2*=cos(x2lat);
dist_calc2*=sin(diflon/2.0);                                       
dist_calc2*=sin(diflon/2.0);
dist_calc +=dist_calc2;
dist_calc=(2*atan2(sqrt(dist_calc),sqrt(1.0-dist_calc)));
dist_calc*=6371000.0; //Converting to meters
    
    
    }

void menupage(){
k=0;
i=11;

  tft.fillScreen(BLACK);
  
  tft.fillRoundRect(75, 211, 155, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("MENU PAGE");

  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));
  
tft.setFreeFont(FF33);
main_btn.initButton(&tft,75, 40, 60, 25,  WHITE, CYAN, BLACK, "MN",1);
main_btn.drawButton(false);

cnf_btn.initButton(&tft,155, 40, 60, 25,  WHITE, CYAN, BLACK, "CNF",1);
cnf_btn.drawButton(false);

roc_one_btn.initButton(&tft,235, 40, 60, 25,  WHITE, CYAN, BLACK, "R1",1);
roc_one_btn.drawButton(false);

roc_two_btn.initButton(&tft,75, 100, 60, 25,  WHITE, CYAN, BLACK, "R2",1);
roc_two_btn.drawButton(false);

gs_btn.initButton(&tft,155, 100, 60, 25,  WHITE, CYAN, BLACK, "GS",1);
gs_btn.drawButton(false);

flight_sttcs_btn.initButton(&tft,235, 100, 60, 25,  WHITE, CYAN, BLACK, "FS",1);
flight_sttcs_btn.drawButton(false);

finder_btn.initButton(&tft,155, 160, 60, 25,  WHITE, CYAN, BLACK, "FIN",1);
finder_btn.drawButton(false);


on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 

off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
off_btn.drawButton(false); 
}

void lsm_config_page(){
k=0;
i=10;

  compass.init();
  compass.enableDefault();
  
  tft.fillScreen(BLACK);
  
  tft.fillRoundRect(75, 211, 225, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(81, 227);
  tft.print("LSM303 CALİBRATE PAGE");

   tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));
  
tft.setFreeFont(FF33);
// min values
tft.fillRoundRect(20, 50, 90, 22,7, WHITE); 
tft.fillRoundRect(120, 50, 90, 22,7, WHITE); 
tft.fillRoundRect(220, 50, 90, 22,7, WHITE); 

//max vaules
tft.fillRoundRect(20, 90, 90, 22,7, WHITE); 
tft.fillRoundRect(120, 90, 90, 22,7, WHITE); 
tft.fillRoundRect(220, 90, 90, 22,7, WHITE); 


ok_btn.initButton(&tft,160, 170, 60, 25,  BLACK, CYAN, BLACK, "OK",1);
ok_btn.drawButton(false);

on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 

}

void flightsttcs_page()
{
k=0;
i=6;
  tft.fillScreen(BLACK);
  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));
  
  tft.fillRoundRect(75, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(95, 227);
  tft.print("FLIGT STATICS");
  
 tft.fillRoundRect(lft_blck_x, lft_blck_y, 156, 176,3, CYAN);               //sol blok


tft.fillRoundRect(lft_blck_x+2, lft_blck_y+2, 152, 19,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+4, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+4, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+25, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+25, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+46, 152, 62,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+50, box_x+4, box_y*3+8,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+50, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
//tft.fillRoundRect(lft_blck_x+5, lft_blck_y+69, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+69, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+89, 152, 19,2, BLACK);           //5. satır kutu altlığı
//tft.fillRoundRect(lft_blck_x+5, lft_blck_y+88, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+88, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+114, 152, 60,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+118, box_x*2+16, box_y,3, WHITE);      //ilk kutucuk
//tft.fillRoundRect(box_x+11, lft_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+137, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+137, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+155, 152, 19,2, BLACK);           //8. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+156, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+156, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+8);
  tft.print("FLIGHT TIME");

  /*tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+8);
  tft.print("3:25");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+29);
  tft.print("MAX ALT.");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+29);
  tft.print("3500");
   tft.print(" ");
  tft.print(unit[d]);*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+54);
  tft.print(rckt_v2);
   tft.print(" ");
  tft.print(unit[v2]);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);

  tft.setCursor(lft_blck_x+22, lft_blck_y+55);
  tft.print("MAX");
  tft.setCursor(lft_blck_x+11, lft_blck_y+68);
  tft.print("VELOCITY");
  tft.setCursor(lft_blck_x+8, lft_blck_y+81);
  tft.print("AP.FALL SPD");
  tft.setCursor(lft_blck_x+8, lft_blck_y+94);
  tft.print("MN.FALL SPD");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+73);
  tft.print("1.51 mach");*/

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+92);
  tft.print("VELOCITY");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+92);
  tft.print(rckt_v1);
   tft.print(" ");
  tft.print(unit[v1]);  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+24, lft_blck_y+122);
  tft.print("LoRa STATUS"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+141);
  tft.print("CRC ERROR");
  
 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+141);
  tft.print(crc_err); */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+160);
  tft.print(""); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+160);
  tft.print("");   
//****************************************************************************************

 tft.fillRoundRect(rght_blck_x, rght_blck_y, 156, 176,3, CYAN);               //sağ blok


tft.fillRoundRect(rght_blck_x+2, rght_blck_y+2, 152, 40,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+5, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+5, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+24, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+24, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+45, 152, 41,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+49, box_x+20, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+93, rght_blck_y+49, box_x-7, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+67, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+67, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+92, 152, 19,2, BLACK);           //5. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+94, box_x+81, box_y,3, WHITE);      //ilk kutucuk
//tft.fillRoundRect(rght_blck_x+80, rght_blck_y+92, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+114, 152, 60,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+118, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+136, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+136, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+155, 152, 19,2, BLACK);           //8. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+155, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+155, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+9);
  tft.print("APOGEE TIME");
 /* tft.setCursor(rght_blck_x+85, rght_blck_y+9);
  tft.print("50 sn");*/

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+83, rght_blck_y+8);
  tft.print("SLEEPY MODE");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+28);
  tft.print("DESCENT TIME");
  /*tft.setCursor(rght_blck_x+85, rght_blck_y+28);
  tft.print("2:45");*/

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+29);
  tft.print("11.1 V");*/
 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7,rght_blck_y+53);
  tft.print("ENGINE BURN T.");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+97, rght_blck_y+54);
  tft.print("5 sec.");*/


  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+71);
  tft.print("MAX SAT.");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+85, rght_blck_y+72);
  tft.print("0.01 sec.");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+30, rght_blck_y+98);
  tft.print("MAX ACCELERATION");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+96);
  tft.print("11.1 V");  */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+122);
  tft.print("ACCEL-X");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+122);
  tft.print("-200 g");  
*/
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+140);
  tft.print("ACCEL-Y");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+140);
  tft.print("400 g");  */ 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+159);
  tft.print("ACCEL-Z");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+159);
  tft.print("20 g");  */ 


     tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);

  }

void prsht_config(int apg){


  tft.fillScreen(BLACK);
   tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));

  tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(122, 106);
  tft.print(apg);
  tft.print(" ");
  tft.print(unit[d]);

tft.setFreeFont(FF33);
one_btn.initButton(&tft,  45, 55, 40, 25, WHITE, RED, BLACK, "1", 1);  
ten_btn.initButton(&tft,  125, 55, 40, 25, WHITE, RED, BLACK, "10", 1);
hundred_btn.initButton(&tft,  190, 55, 40, 25, WHITE, RED, BLACK, "100", 1);
fivehundred_btn.initButton(&tft,  270, 55, 40, 25, WHITE, RED, BLACK, "500", 1);
one_btn.drawButton(false); 
ten_btn.drawButton(false); 
hundred_btn.drawButton(false); 
fivehundred_btn.drawButton(false); 

plus_btn.initButton(&tft,  70, 100, 60, 25, WHITE, GREEN, BLACK, "+", 1);
minus_btn.initButton(&tft,  250, 100, 60, 25, WHITE, BLUE, BLACK, "-", 1);
plus_btn.drawButton(false); 
minus_btn.drawButton(false); 



on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 

}

/*void main_config(){

i=3;
tft.fillScreen(BLACK);

tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(122, 106);
  tft.print(main);
  tft.print(" ");
  tft.print(unit[d]);

one_btn.initButton(&tft,  45, 55, 40, 25, WHITE, RED, BLACK, "1", 1);  
ten_btn.initButton(&tft,  125, 55, 40, 25, WHITE, RED, BLACK, "10", 1);
hundred_btn.initButton(&tft,  190, 55, 40, 25, WHITE, RED, BLACK, "100", 1);
fivehundred_btn.initButton(&tft,  270, 55, 40, 25, WHITE, RED, BLACK, "500", 1);
one_btn.drawButton(false); 
ten_btn.drawButton(false); 
hundred_btn.drawButton(false); 
fivehundred_btn.drawButton(false); 

plus_btn.initButton(&tft,  70, 100, 60, 25, WHITE, GREEN, BLACK, "+", 1);
minus_btn.initButton(&tft,  250, 100, 60, 25, WHITE, BLUE, BLACK, "-", 1);
plus_btn.drawButton(false); 
minus_btn.drawButton(false); 



on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 

}*/
void dbmconfig_page(int dbm){
  
  
  tft.fillScreen(BLACK);
   tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));

  tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(118, 106);
  tft.print(dbm);
  tft.print(" dbm");



plus_btn.initButton(&tft,  70, 100, 60, 25, WHITE, GREEN, BLACK, "+", 1);
minus_btn.initButton(&tft,  250, 100, 60, 25, WHITE, BLUE, BLACK, "-", 1);
plus_btn.drawButton(false); 
minus_btn.drawButton(false);




on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 
  
  
  }


void frqconfig_page(float frq){
  
  
  tft.fillScreen(BLACK);
   tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));
  
  tft.fillRoundRect(110, 89, 100, 25,3, WHITE);
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(115, 106);
  tft.print(frq);
  tft.print(" MHz");


point_one_btn.initButton(&tft,  45, 55, 40, 25, WHITE, RED, BLACK, "0.1", 1);  
point_t_btn.initButton(&tft,  125, 55, 40, 25, WHITE, RED, BLACK, "0.2", 1);
point_five_btn.initButton(&tft,  190, 55, 40, 25, WHITE, RED, BLACK, "0.5", 1);
one_thousand_btn.initButton(&tft,  270, 55, 40, 25, WHITE, RED, BLACK, "1", 1);
point_one_btn.drawButton(false); 
point_t_btn.drawButton(false); 
point_five_btn.drawButton(false); 
one_thousand_btn.drawButton(false);
 

plus_btn.initButton(&tft,  70, 100, 60, 25, WHITE, GREEN, BLACK, "+", 1);
minus_btn.initButton(&tft,  250, 100, 60, 25, WHITE, BLUE, BLACK, "-", 1);
plus_btn.drawButton(false); 
minus_btn.drawButton(false); 

on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
on_btn.drawButton(false); 
  
  
  }

void myconfig_page(){
  k=0;
  i=6;
  tft.fillScreen(BLACK);
   tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));

  tft.fillRoundRect(78, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("MY CONFIG PAGE");
  
     tft.setFreeFont(FF33);
     tft.fillRoundRect(16, 23, 290, 25,3, CYAN);  // MEASURİNG UNIT paraşüt kutusu



      tft.fillRoundRect(28, 97, 125, 25,3, CYAN);  // priave key kutusu

     tft.fillRoundRect(174, 97, 115, 25,3, WHITE);  //priave key değer kutucuğu

 
  tft.setCursor(29, 114);
  tft.print("PRIVATE KEY");

  tft.setTextColor( BLACK,a);
  tft.setCursor(193, 114);
  tft.print("978258");
 
     imprl_btn.initButton(&tft,  90, 70, 130, 25, WHITE, GREEN, BLACK, "IMPERIAL", 1);
     mtrc_btn.initButton(&tft,  230, 70, 130, 25, WHITE, GREEN, BLACK, "MERTIC", 1);
     lsm_btn.initButton(&tft,  90, 150, 130, 25, WHITE, ORANGE, BLACK, "LSM303", 1);
     utc_btn.initButton(&tft,  230, 150, 130, 25, WHITE, ORANGE, BLACK, "UTC", 1);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  
     imprl_btn.drawButton(false);  
     mtrc_btn.drawButton(false);
     lsm_btn.drawButton(false);
     utc_btn.drawButton(false);


  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,CYAN);
  tft.setCursor(75, 40);
  tft.print("MEASURMENT UNIT");

  
  }


 void frq_config_page(){
    k=0;
    i=9;
  tft.fillScreen(BLACK);
  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));

  tft.fillRoundRect(75, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("FREQ. CONFIG");
  
  tft.fillRoundRect(20, 150, 140, 25,3, WHITE);
  tft.setCursor(27, 167);
  tft.print("SET FRQ");
  
     snd_btn.initButton(&tft,  268, 190, 100, 25, WHITE, color_press_snd, BLACK, "SEND", 1);
     erase_btn.initButton(&tft,  268, 60, 100, 75, WHITE, RED, BLACK, "ERASE", 1);
     rqst_btn.initButton(&tft,  268, 160, 100, 25, WHITE, color_press_rqst, BLACK, "RQST", 1);
     
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     on_btn.drawButton(false);
       
     snd_btn.drawButton(false);
     erase_btn.drawButton(false);
     rqst_btn.drawButton(false);

     tft.fillRoundRect(0, 23, 115, 25,3, CYAN);  // drag paraşüt kutusu
     tft.fillRoundRect(0, 55, 115, 25,3, CYAN);  // main parşüt kutusu
    

     tft.fillRoundRect(120, 23, 95, 25,3, WHITE);  // drag paraşüt değer kutucuğu
     tft.fillRoundRect(120, 55, 95, 25,3, WHITE);  // main paraşüt değer kutucuğu
    


  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,CYAN);
  tft.setCursor(1, 40);
  tft.print("FREQUENCY");

  tft.setCursor(10, 72);
  tft.print("DBM");


  tft.setTextColor( BLACK,a);
  tft.setCursor(125, 40);
  tft.print(frq);
  tft.print(" MHz ");


  tft.setTextColor( BLACK,b);
  tft.setCursor(125, 72);
  tft.print(dbm);
  tft.print(" dbm");


  }

  void prsht_config_page(){
    k=0;
    i=8;
  tft.fillScreen(BLACK);
  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));

  tft.fillRoundRect(75, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("PRSHT CONFIG");
  
     tft.setFreeFont(FF33);
     snd_btn.initButton(&tft,  268, 190, 100, 25, WHITE, color_press_snd, BLACK, "SEND", 1);
     erase_btn.initButton(&tft,  268, 60, 100, 75, WHITE, RED, BLACK, "ERASE", 1);
     rqst_btn.initButton(&tft,  268, 160, 100, 25, WHITE, color_press_rqst, BLACK, "RQST", 1);
     
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     on_btn.drawButton(false);  
     snd_btn.drawButton(false);
     erase_btn.drawButton(false);
     rqst_btn.drawButton(false);

    // tft.fillRoundRect(0, 23, 115, 25,3, CYAN);  // drag paraşüt kutusu
     tft.fillRoundRect(0, 55, 115, 25,3, CYAN);  // main parşüt kutusu
     tft.fillRoundRect(0, 87, 115, 25,3, CYAN);  // roket durum kutusu
     

    // tft.fillRoundRect(120, 23, 95, 25,3, WHITE);  // drag paraşüt değer kutucuğu
     tft.fillRoundRect(120, 55, 95, 25,3, WHITE);  // main paraşüt değer kutucuğu
     tft.fillRoundRect(120, 87, 95, 25,3, WHITE);  //roket durum değer kutucuğu
    

  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,CYAN);
 /* tft.setCursor(1, 40);
  tft.print("DRAG PRSHT");*/

  tft.setCursor(1, 72);
  tft.print("MAIN PRSHT");

  tft.setCursor(25, 104);
  tft.print("MODE");

  tft.setTextColor( BLACK,a);
  tft.setCursor(128, 104);
  //tft.print("IN SLEEP");


 /** tft.setTextColor( BLACK,a);
  tft.setCursor(125, 40);
  tft.print(apogee);
  tft.print(" ");
  tft.print(unit[d]);*/

  tft.setTextColor( BLACK,b);
  tft.setCursor(125, 72);
  tft.print(main);
  tft.print(" ");
  tft.print(unit[d]);

  }


void rocketconfig_page(){
    k=0;
    i=1;
  tft.fillScreen(BLACK);
  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));

  tft.fillRoundRect(75, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("ROCKET CONFIG");
  
     tft.setFreeFont(FF33);


     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false); 
     prsht_btn.initButton(&tft,  80, 105, 120, 160, WHITE, CYAN, BLACK, "PRSHT", 1);
     frq_btn.initButton(&tft, 240, 105, 120, 160, WHITE, RED, BLACK, "FREQ.", 1);
     
     prsht_btn.drawButton(false);
     frq_btn.drawButton(false);


  }


void baboon(){
  
  k=0;
  i=6;
   
  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));
  
   TJpgDec.drawJpg(0,0, dog, sizeof(dog));
   tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  

  }

void mainpage()
{   
   k=0;
    i=6;
  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));
    
     tft.fillScreen(BLACK);
      TJpgDec.drawJpg(0, 0, uzay, sizeof(uzay));
     tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<", 1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);  
     
  }

void grndstn()
{
  k=0; 
  i=6;
  tft.fillScreen(BLACK);
  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));
  
  tft.fillRoundRect(lft_blck_x, lft_blck_y, 320, 159,3, CYAN);
 
 tft.fillRoundRect(lft_blck_x+2, lft_blck_y+3, 316, 66,2, BLACK);           //1. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+6, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+6, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+27, box_x+60, box_y+3,3, WHITE);      //2. SATIR ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+27, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+48, box_x+60, box_y+3,3, WHITE);      //3. SATIR ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+48, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk 


 tft.fillRoundRect(lft_blck_x+2, lft_blck_y+72,316, 84,2, BLACK);           //4. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+74, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+74, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

 //tft.fillRoundRect(lft_blck_x+2, lft_blck_y+68, 196, 62,2, BLACK);           //5. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+94, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+94, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

  //tft.fillRoundRect(lft_blck_x+2, lft_blck_y+68, 196, 62,2, BLACK);           //6. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+114, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+114, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

  //tft.fillRoundRect(lft_blck_x+2, lft_blck_y+68, 196, 62,2, BLACK);           //7. satır kutu altlığı
 tft.fillRoundRect(lft_blck_x+4, lft_blck_y+134, box_x+60, box_y+3,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(lft_blck_x+134, lft_blck_y+134, box_x+117, box_y+3,3, WHITE);          //ikinci kutucuk

 /*tft.fillRoundRect(rght_blck_x+4, rght_blck_y+47, 142, 79,2, BLACK);           //2. satır kutu altlığı
 tft.fillRoundRect(rght_blck_x+7, rght_blck_y+50, box_x-20, box_y*2+9,3, WHITE);      //ilk kutucuk
 tft.fillRoundRect(rght_blck_x+58, rght_blck_y+50, box_x+20, box_y*2+9,3, WHITE);          //ikinci kutucuk*/

  tft.fillRoundRect(75, 211, 165, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(80, 227);
  tft.print("GROUND STATION");

  
  tft.setFreeFont(FM9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+6, lft_blck_y+19);
  tft.print("GPS LAT.");

 
 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+19);
  tft.print("-179.123456");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+6, lft_blck_y+40);
  tft.print("GPS LONG.");


  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+40);
  tft.print("59.123456");*/


 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+6, lft_blck_y+60);
  tft.print("GPS SAT");


 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+60);
  tft.print(gps.satellites.value());*/

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+87);
  tft.print("TEMPERATURE");

  tft.setFreeFont(FM9);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+87);
  tft.print(my_temp);
  tft.print(" ");
  tft.print(unit[t]);*/


  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+106);
  tft.print("ALTITUDE");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+106);
  tft.print(bmp.readAltitude(1019.0));
  tft.print(" ");
  tft.print(unit[d]);*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+127);
  tft.print("COMPASS");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+147);
  tft.print("TEMPRATURE:"); 

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+140, lft_blck_y+147);
   tft.print(rckt_dist);
  tft.print(unit[d]);   */

     tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);
  
  }


  void rocketpage2()
{

   k=0;
   i=6;

  tft.fillScreen(BLACK);
  
   tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));
  
  tft.fillRoundRect(80, 211, 150, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("ROCKET PAGE 2");
  
  tft.fillRoundRect(lft_blck_x, lft_blck_y, 156, 154,3, CYAN);               //sol blok

  tft.fillRoundRect(lft_blck_x+2, lft_blck_y+2, 152, 19,2, BLACK);           //1. satır kutu altlığı
  tft.fillRoundRect(lft_blck_x+5, lft_blck_y+4, box_x*2+16, box_y,3, WHITE);      //ilk kutucuk
//tft.fillRoundRect(box_x+11, lft_blck_y+4, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+23, 152, 63,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+27, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+27, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+45, 152, 63,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+46, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+46, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+65, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+65, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+89, 152, 19,2, BLACK);           //5. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+91, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+91, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+111, 152, 41,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+114, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+114, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+134, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+134, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+57, lft_blck_y+8);
  tft.print("BNO080");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+31);
  tft.print("GYRO-X");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
 /* tft.setCursor(lft_blck_x+79, lft_blck_y+31);
  tft.print("-00.00 rad/s");*/
 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+51);
  tft.print("GYRO-Y");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
 /* tft.setCursor(lft_blck_x+79, lft_blck_y+51);
  tft.print("-34.56 rad/s");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+70);
  tft.print("GYRO-Z");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  /* tft.setCursor(lft_blck_x+79, lft_blck_y+70);
  tft.print("-25.17 rad/s");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+94);
  tft.print("MACH LOCK");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
 /* tft.setCursor(lft_blck_x+79, lft_blck_y+94);
  tft.print("-8.0 g");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+118);
  tft.print("ACCEL-Y"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+118);
  tft.print("5.2 g");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+138);
  tft.print("ACCEL-Z");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+79, lft_blck_y+138);
  tft.print("0.0 g"); */
//****************************************************************************************

 tft.fillRoundRect(rght_blck_x, rght_blck_y, 156, 154,3, CYAN);               //sağ blok


tft.fillRoundRect(rght_blck_x+2, rght_blck_y+2, 152, 19,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+4, box_x*2+16, box_y,3, WHITE);     //ilk kutucuk
//tft.fillRoundRect(rght_blck_x+80, rght_blck_y+4, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+25, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+25, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+45, 152, 41,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+49, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+49, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+67, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+67, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+89, 152, 19,2, BLACK);           //5. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+91, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+91, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+111, 152, 41,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+114, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+114, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+134, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+134, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+57, rght_blck_y+8);
  tft.print("BNO080");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+29);
  tft.print("MAGNO-X");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+29);
  tft.print("53 uT");*/
 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11,rght_blck_y+53);
  tft.print("MAGNO-Y");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+54);
  tft.print("930 mph");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+71);
  tft.print("MAGNO-Z");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+71);
  //tft.print("MAGNO-Z");*/

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+73);
  tft.print("0.44 mach");*/


  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+96);
  tft.print("YAW");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+96);
  tft.print("-180 dgree"); */ 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+118);
  tft.print("PITCH"); 

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+118);
  tft.print("90 dgree"); */ 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+11, rght_blck_y+138);
  tft.print("ROLL");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+138);
  tft.print("90 dgree");   */
  
  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+14, rght_blck_y+138);
  tft.print("GPS Lng");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+138);
  tft.print("59.560833"); */

     tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);
  
  }

  
void rocketpage1()
{
k=0;
i=6;

  tft.fillScreen(BLACK);
  tft.fillRect(0, 0, 320, 22, LIGHTGREY);  
  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor(BLACK,LIGHTGREY);
  double battery= ReadVoltage(35)*6.6;
  tft.setCursor(10, 1);
  tft.print(battery);
  tft.print("   ");
  tft.print(flightData.batteryy);
  tft.setCursor(150, 1);

  tft.print(hour(local));
  tft.print(":");
  tft.print(minute(local));
  
  tft.setCursor(90, 1);
  tft.print("(unit=");
  tft.print(unit[u]);
  tft.print(")");
  tft.setCursor(200, 1);
  tft.print("SD=%");
  tft.print(sd_cap);
  tft.setCursor(260, 1);
  tft.print(day(local)); 
  tft.print("/");
  tft.print(month(local));
  tft.print("/");
  tft.print(year(local));
  
  tft.fillRoundRect(80, 211, 150, 22,7, WHITE);               //başlık
  tft.setFreeFont(FF33);
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(85, 227);
  tft.print("ROCKET PAGE 1");
  
 tft.fillRoundRect(lft_blck_x, lft_blck_y, 156, 176,3, CYAN);               //sol blok


tft.fillRoundRect(lft_blck_x+2, lft_blck_y+2, 152, 19,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+4, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+4, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+25, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+25, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+46, 152, 62,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+50, box_x+4, box_y*3+8,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+50, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
//tft.fillRoundRect(lft_blck_x+5, lft_blck_y+69, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+69, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+89, 152, 19,2, BLACK);           //5. satır kutu altlığı
//tft.fillRoundRect(lft_blck_x+5, lft_blck_y+88, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+88, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(lft_blck_x+2, lft_blck_y+114, 152, 60,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+118, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+137, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+11, lft_blck_y+137, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(lft_blck_x+2, lft_blck_y+155, 152, 19,2, BLACK);           //8. satır kutu altlığı
tft.fillRoundRect(lft_blck_x+5, lft_blck_y+156, box_x+19, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(box_x+26, lft_blck_y+156, box_x-5, box_y,3, WHITE);          //ikinci kutucuk

  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+7, lft_blck_y+8);
  tft.print("GPS TIME");

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+29);
  tft.print("ALTITUDE");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+29);
  tft.print(rckt_alt);
   tft.print(" ");
  tft.print(unit[d]);*/
 
 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+54);
  tft.print("VELOCITY");*/

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+54);
  tft.print(rckt_v2);
   tft.print(" ");
  tft.print(unit[v2]);*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+55);
  tft.print("-VELOCITY");
  tft.setCursor(lft_blck_x+12, lft_blck_y+73);
  tft.print("-GPS VEL.");
  tft.setCursor(lft_blck_x+12, lft_blck_y+91);
  tft.print("-GPS FIX.");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+73);
  tft.print("0.44 mach");*/

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+92);
  tft.print("VELOCITY");*/

/*  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+82, lft_blck_y+92);
  tft.print(rckt_v1);
   tft.print(" ");
  tft.print(unit[v1]);  */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+122);
  tft.print("GPS Lat"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
 // tft.setCursor(lft_blck_x+82, lft_blck_y+122);
 //tft.print("-179.123456");  

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+141);
  tft.print("GPS Lng");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  //tft.setCursor(lft_blck_x+82, lft_blck_y+141);
 //tft.print("59.560833"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+14, lft_blck_y+160);
  tft.print("GPS SATALITE"); 

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(lft_blck_x+94, lft_blck_y+160);
  tft.print("8 SAT.");   
//****************************************************************************************

 tft.fillRoundRect(rght_blck_x, rght_blck_y, 156, 176,3, CYAN);               //sağ blok


tft.fillRoundRect(rght_blck_x+2, rght_blck_y+2, 152, 40,2, BLACK);           //1. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+5, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+5, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+23, 152, 19,2, BLACK);           //2. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+24, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+24, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+45, 152, 41,2, BLACK);           //3. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+49, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+49, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+67, 152, 19,2, BLACK);           //4. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+67, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+67, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+90, 152, 19,2, BLACK);           //5. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+92, box_x+8, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+80, rght_blck_y+92, box_x+6, box_y,3, WHITE);          //ikinci kutucuk

tft.fillRoundRect(rght_blck_x+2, rght_blck_y+114, 152, 60,2, BLACK);           //6. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+118, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+118, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+133, 152, 19,2, BLACK);           //7. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+136, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+136, box_x+10, box_y,3, WHITE);          //ikinci kutucuk

//tft.fillRoundRect(rght_blck_x+2, rght_blck_y+155, 152, 19,2, BLACK);           //8. satır kutu altlığı
tft.fillRoundRect(rght_blck_x+5, rght_blck_y+155, box_x+4, box_y,3, WHITE);      //ilk kutucuk
tft.fillRoundRect(rght_blck_x+76, rght_blck_y+155, box_x+10, box_y,3, WHITE);          //ikinci kutucuk


  tft.setFreeFont();
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+9);
  tft.print("400 G-X");

  //tft.fillCircle(rght_blck_x+93, rght_blck_y+12, 5, GREEN);
  //tft.fillCircle(rght_blck_x+113, rght_blck_y+12,5, RED);

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+83, rght_blck_y+8);
  tft.print("SLEEPY MODE");*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+28);
  tft.print("400 G-Y");

  //tft.fillCircle(rght_blck_x+93, rght_blck_y+31, 5, GREEN);
  //tft.fillCircle(rght_blck_x+113, rght_blck_y+31,5, RED);

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+86, rght_blck_y+29);
  tft.print("11.1 V");*/
 
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7,rght_blck_y+53);
  tft.print("400 G-Z");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+54);
  tft.print("930 mph");*/
  //tft.fillCircle(rght_blck_x+93, rght_blck_y+56, 5, GREEN);
  //tft.fillCircle(rght_blck_x+113, rght_blck_y+56,5, RED);

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+7, rght_blck_y+71);
  tft.print("PYRO 1 and 2");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+73);
  tft.print("0.44 mach");*/

  /* tft.fillCircle(rght_blck_x+93, rght_blck_y+74, 5, GREEN);
   tft.fillCircle(rght_blck_x+113, rght_blck_y+74, 5, RED);*/

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+13, rght_blck_y+96);
  tft.print("DISTANCE");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+96);
  tft.print("11.1 V");  */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+122);
  tft.print("ACCEL-X");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+122);
  tft.print("-200 g");  */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+140);
  tft.print("ACCEL-Y");

  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+140);
  tft.print("400 g");   */

  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+17, rght_blck_y+159);
  tft.print("ACCEL-Z");

 /* tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+100, rght_blck_y+159);
  tft.print("20 g");   */
  /*tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+14, rght_blck_y+138);
  tft.print("GPS Lng");
  
  tft.setTextSize(1);
  tft.setTextColor( BLACK,WHITE);
  tft.setCursor(rght_blck_x+82, rght_blck_y+138);
  tft.print("59.560833"); */

     tft.setFreeFont(FF33);
     on_btn.initButton(&tft,  35, 223, 60, 25, WHITE, ORANGE, BLACK, "<",1);
     off_btn.initButton(&tft, 285, 223, 60, 25, WHITE, ORANGE, BLACK, ">", 1);
     off_btn.drawButton(false);
     on_btn.drawButton(false);

  }

void showTime(uint32_t msTime) {
  //tft.setCursor(0, 0);
  //tft.setTextFont(1);
  //tft.setTextSize(2);
  //tft.setTextColor(TFT_WHITE, TFT_BLACK);
  //tft.print(F(" JPEG drawn in "));
  //tft.print(msTime);
  //tft.println(F(" ms "));
  Serial.print(F(" JPEG drawn in "));
  Serial.print(msTime);
  Serial.println(F(" ms "));
}

bool tft_output(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t* bitmap)
{
   // Stop further decoding as image is running off bottom of screen
  if ( y >= tft.height() ) return 0;

  // This function will clip the image block rendering automatically at the TFT boundaries
  tft.pushImage(x, y, w, h, bitmap);

  // This might work instead if you adapt the sketch to use the Adafruit_GFX library
  // tft.drawRGBBitmap(x, y, bitmap, w, h);

  // Return 1 to decode next block
  return 1;
}



void touch_calibrate()
{
  uint16_t calData[5];
  uint8_t calDataOK = 0;

  // check file system exists
  if (!LITTLEFS.begin()) {
    Serial.println("Formating file system");
    LITTLEFS.format();
    LITTLEFS.begin();
  }

  // check if calibration file exists and size is correct
  if (LITTLEFS.exists(CALIBRATION_FILE)) {
    if (REPEAT_CAL)
    {
      // Delete if we want to re-calibrate
      LITTLEFS.remove(CALIBRATION_FILE);
    }
    else
    {
      File f = LITTLEFS.open(CALIBRATION_FILE, "r");
      if (f) {
        if (f.readBytes((char *)calData, 14) == 14)
          calDataOK = 1;
        f.close();
      }
    }
  }

  if (calDataOK && !REPEAT_CAL) {
    // calibration data valid
    tft.setTouch(calData);
  } else {
    // data not valid so recalibrate
    tft.fillScreen(TFT_BLACK);
    tft.setCursor(20, 0);
    tft.setTextFont(2);
    tft.setTextSize(1);
    tft.setTextColor(TFT_WHITE, TFT_BLACK);

    tft.println("Touch corners as indicated");

    tft.setTextFont(1);
    tft.println();

    if (REPEAT_CAL) {
      tft.setTextColor(TFT_RED, TFT_BLACK);
      tft.println("Set REPEAT_CAL to false to stop this running again!");
    }

    tft.calibrateTouch(calData, TFT_MAGENTA, TFT_BLACK, 15);

    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.println("Calibration complete!");

    // store data
    File f = LITTLEFS.open(CALIBRATION_FILE, "w");
    if (f) {
      f.write((const unsigned char *)calData, 14);
      f.close();
    }
  }
}

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if(!root){
    Serial.println("Failed to open directory");
    return;
  }
  if(!root.isDirectory()){
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while(file){
    if(file.isDirectory()){
      Serial.print("  DIR : ");
      Serial.println(file.name());
      if(levels){
        listDir(fs, file.name(), levels -1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.println(file.size());
    }
    file = root.openNextFile();
  }
}

void createDir(fs::FS &fs, const char * path){
  Serial.printf("Creating Dir: %s\n", path);
  if(fs.mkdir(path)){
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

void removeDir(fs::FS &fs, const char * path){
  Serial.printf("Removing Dir: %s\n", path);
  if(fs.rmdir(path)){
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\n", path);

  File file = fs.open(path);
  if(!file){
    Serial.println("Failed to open file for reading");
    return;
  }

  Serial.print("Read from file: ");
  while(file.available()){
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if(!file){
    Serial.println("Failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

void appendFile(fs::FS &fs, const char * path, String message){
 // Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if(!file){
    Serial.println("Failed to open file for appending");
    return;
  }
  if(file.print(message)){
      Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}

void renameFile(fs::FS &fs, const char * path1, const char * path2){
  Serial.printf("Renaming file %s to %s\n", path1, path2);
  if (fs.rename(path1, path2)) {
    Serial.println("File renamed");
  } else {
    Serial.println("Rename failed");
  }
}

void deleteFile(fs::FS &fs, const char * path){
  Serial.printf("Deleting file: %s\n", path);
  if(fs.remove(path)){
    Serial.println("File deleted");
  } else {
    Serial.println("Delete failed");
  }
}


void createDialScale(int16_t start_angle, int16_t end_angle, int16_t increment)
{
  // Create the dial Sprite
  dial.setColorDepth(8);       // Size is odd (i.e. 91) so there is a centre pixel at 45,45
  dial.createSprite(320, 240);   // 8bpp requires 91 * 91 = 8281 bytes
  dial.setPivot(120, 120);       // set pivot in middle of dial Sprite

  // Draw dial outline
  dial.fillSprite(TFT_TRANSPARENT);           // Fill with transparent colour
  dial.fillCircle(120, 120, 95, TFT_RED);  // Draw dial outer

  // Hijack the use of the needle Sprite since that has not been used yet!
  needle.createSprite(4, 4);     // 3 pixels wide, 3 high
  needle.fillSprite(TFT_WHITE);  // Fill with white
  needle.setPivot(1, 95);        //  Set pivot point x to the Sprite centre and y to marker radius

  for (int16_t angle = start_angle; angle <= end_angle; angle += increment) {
    needle.pushRotated(&dial, angle); // Sprite is used to make scale markers
    yield(); // Avoid a watchdog time-out
  }

  needle.deleteSprite(); // Delete the hijacked Sprite
}


// =======================================================================================
// Add the empty dial face with label and value
// =======================================================================================

void drawEmptyDial(String label, int16_t val)
{
  // Draw black face
  dial.fillCircle(120, 120, 91, TFT_BLACK);
  dial.drawPixel(120, 120, TFT_WHITE);        // For demo only, mark pivot point with a while pixel

  dial.setTextDatum(TC_DATUM);              // Draw dial text
  dial.drawString(label, 120, 75, 2);
  dial.drawNumber(val, 120, 170, 2);
}

// =======================================================================================
// Update the dial and plot to screen with needle at defined angle
// =======================================================================================

void plotDial(int16_t x, int16_t y, int16_t angle, String label, uint16_t val)
{
  // Draw the blank dial in the Sprite, add label and number
  drawEmptyDial(label, val);

  // Push a rotated needle Sprite to the dial Sprite, with black as transparent colour
  needle.pushRotated(&dial, angle, TFT_BLACK); // dial is the destination Sprite

  // Push the resultant dial Sprite to the screen, with transparent colour
  dial.pushSprite(x, y, TFT_TRANSPARENT);
}

// =======================================================================================
// Create the needle Sprite and the image of the needle
// =======================================================================================

void createNeedle(void)
{
  needle.setColorDepth(8);
  needle.createSprite(11, 90); // create the needle Sprite 11 pixels wide by 49 high

  needle.fillSprite(TFT_BLACK);          // Fill with black

  // Define needle pivot point
  uint16_t piv_x = needle.width() / 2;   // x pivot of Sprite (middle)
  uint16_t piv_y = needle.height() - 10; // y pivot of Sprite (10 pixels from bottom)
  needle.setPivot(piv_x, piv_y);         // Set pivot point in this Sprite

  // Draw the red needle with a yellow tip
  // Keep needle tip 1 pixel inside dial circle to avoid leaving stray pixels
  needle.fillRect(piv_x - 1, 2, 3, piv_y +10, TFT_RED);
  needle.fillTriangle(5, 0, 0, 10,10,10, TFT_YELLOW);
 // needle.drawString(N, 120, 75, 2);
  // Draw needle centre boss
  needle.fillCircle(piv_x, piv_y, 5, TFT_MAROON);
  needle.drawPixel( piv_x, piv_y, TFT_WHITE);     // Mark needle pivot point with a white pixel
}
