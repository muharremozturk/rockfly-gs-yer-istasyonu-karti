/*
 * Copyright 2022 NXP
 * SPDX-License-Identifier: MIT
 */

#ifndef GUİ_GUİDER_H
#define GUİ_GUİDER_H
#ifdef __cplusplus
extern "C" {
#endif

#include <lvgl.h>
#include "guider_fonts.h"

typedef struct
{
	lv_obj_t *m1m;
	lv_obj_t *m1m_tileview_1;
	lv_obj_t *m1m_tileview_1_tileview;
	lv_obj_t *m1m_btn_1;
	lv_obj_t *m1m_btn_1_label;
	lv_obj_t *m1m_label_1;
}lv_ui;

void setup_ui(lv_ui *ui);
extern lv_ui guider_ui;
void setup_scr_m1m(lv_ui *ui);

#ifdef __cplusplus
}
#endif
#endif
