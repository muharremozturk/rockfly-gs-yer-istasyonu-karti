#include"CC1200.h"        // TI CC1200 RF Radio

#define MODE // Define this for TX, otherwise code is RX

// Node Addresses
#ifdef MODE
#define THIS_NODE 0x01
#define TARG_NODE 0x02
#else
#define THIS_NODE 0x02
#define TARG_NODE 0x01
#endif

// CC1200 Radio Interrupt Pin
#define RadioTXRXpin  27  // CC1200 Packet Semaphore 

// BUFFER
#define FIFO_SIZE   100    // TX and RX Buffer Size
byte readBytes; // Number of bytes read from RX FIFO
byte rxBuffer[FIFO_SIZE]; // Fill with RX FIFO
byte writeBytes; // Number of bytes written to TX FIFO
byte txBuffer[FIFO_SIZE]; // Fill with TX FIFO

#define idxLength   0   // Length index
#define idxTargetNode 1   // TargetNode index
#define idxFrameCount 3   // FrameCount index
#define TIMEOUT         1000   // TIMEOUT

union DoubleBytes
{
  double DoubleVal;
  byte AsBytes[sizeof(double)]; // 4 Bytes
} DoubleBytes;


// GLOBAL VARIABLES
byte counter = 0x00;
volatile bool packetSemaphore; // RX/TX success flag. Volatile prevents undesired optimizations by the compiler

// Set Packet Semaphore 
void setSemaphore() {
  packetSemaphore = true;
}

// Clear Packet Semaphore 
void clearSemaphore() {
  packetSemaphore = false;
}



// Try transmitting outgoing, if any, within the TOUT duration.
bool TryTransmit(unsigned long qTimeout) {
  bool transmitStatus = false;
  byte marcstate = 0x00;
  Serial.println("Wait Transmission...");

  do
  {
    marcstate = cc1200.GetStat(StatType::MARC_STATE, 0x1F); // MARC_STATE[4:0]
    if (marcstate == MARC_STATE_IDLE)
    {
      cc1200.FlushTxFifo(); delay(1); // Flush ony in ERR or IDLE
      cc1200.WriteTxFifo(txBuffer, txBuffer[idxLength]); delay(1);
      cc1200.Transmit(); delay(3);
      Serial.println("\tTX");
    }
    else if (marcstate == MARC_STATE_TX_FIFO_ERR)
    {
      cc1200.FlushTxFifo(); delay(1);
      Serial.println("\tTXFIFO Flushed!");
    }
    else if (marcstate == MARC_STATE_RX_FIFO_ERR)
    {
      cc1200.FlushRxFifo(); delay(1);
      Serial.println("\tRXFIFO Flushed!");
    }
    else if (marcstate >= MARC_STATE_XOFF && marcstate <= MARC_STATE_ENDCAL)
    {
      Serial.print("\tSettling: "); Serial.println(marcstate);
    }

    // Timeout Implementation
    if (millis() >= qTimeout) //isTimeOut(qTimeout)
    {
      Serial.println("\tTOUT!");
      cc1200.Idle();
      break;
    }

    delay(10);
  } while (!packetSemaphore);

  if (packetSemaphore)
  {
    cc1200.Idle(); delay(1); // Flush only in IDLE or FIFOERR
    cc1200.FlushTxFifo();
    transmitStatus = true;
    clearSemaphore();
  }

  return transmitStatus;
}
struct Data{
byte byteArray[100];
int counter=0;
};
  Data data;

void setup(){
    // SERIAL
  Serial.begin(115200);

  Serial.println("\n>>Start Setup Chain");
   uint8_t MISO_PIN = 12;
  uint8_t MOSI_PIN = 13;
  uint8_t SCK_PIN = 14;  
  uint8_t SS_PIN = 15;
  // CC1200 RADIO
  cc1200.Init( SS_PIN,  MOSI_PIN,  MISO_PIN, SCK_PIN, PIN_UNUSED); // SS, MOSI, MISO, SCK, RadioResetpin
  cc1200.Configure(rxSniffSettings, rxSniffSettLen); // 2sec internal delay
  cc1200.SetAddress(THIS_NODE); delay(10);
  cc1200.Strobe(CC120X_SCAL); delay(1000);
  byte readNode = cc1200.GetAddress(false);
  Serial.print("\tNode: "); Serial.println(readNode);
  if (THIS_NODE == readNode)
  {
        Serial.print("\tNode "); Serial.print(THIS_NODE); Serial.println(" Ok!");
  }
    else{
        Serial.print("\tERROR \n");
        while (true);
    }
  cc1200.FlushRxFifo(); delay(100);
  cc1200.FlushTxFifo(); delay(100);
  Serial.println("\tRadio Config");

  // RADIO INTERRUPT
  packetSemaphore = false;
  pinMode(RadioTXRXpin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(RadioTXRXpin), setSemaphore, FALLING); // LOW, CHANGE, RISING, FALLING
  Serial.println("\tPacket Interrupt Config");

    Serial.flush();

    writeBytes = FIFO_SIZE; // idxFrameCount+1
    txBuffer[idxLength] = writeBytes;
    txBuffer[idxTargetNode] = TARG_NODE;
    txBuffer[idxFrameCount] = counter;
    floatAddByte(0.49,&data.counter,data.byteArray);
    floatAddByte(-0.49,&data.counter,data.byteArray);
    floatAddByte(1.22,&data.counter,data.byteArray);

    floatAddByte(36.52,&data.counter,data.byteArray);
    floatAddByte(996.33,&data.counter,data.byteArray);

    floatAddByte(-0.46,&data.counter,data.byteArray);
    floatAddByte(-0.14,&data.counter,data.byteArray);
    floatAddByte(-9.66,&data.counter,data.byteArray);
    floatAddByte(-0.00,&data.counter,data.byteArray);
    floatAddByte(0.00,&data.counter,data.byteArray);
    floatAddByte(0.00,&data.counter,data.byteArray);
    floatAddByte(-25.69,&data.counter,data.byteArray);
    floatAddByte(-35.19,&data.counter,data.byteArray);
    floatAddByte(12.44,&data.counter,data.byteArray);
    floatAddByte(-179.00,&data.counter,data.byteArray);
    floatAddByte(2.93,&data.counter,data.byteArray);
    floatAddByte(-33.18,&data.counter,data.byteArray);

    longAddByte(-118334965,&data.counter,data.byteArray);
    longAddByte(95731051,&data.counter,data.byteArray);
    longAddByte(892087,&data.counter,data.byteArray);
    byteAddByte(0,&data.counter,data.byteArray);
    longAddByte(2021824,&data.counter,data.byteArray);
    longAddByte(14564,&data.counter,data.byteArray);

}

void loop(){

DoubleBytes.DoubleVal = 88.0 + counter;
// txBuffer[4] =DoubleBytes.AsBytes[0];
// txBuffer[5] =DoubleBytes.AsBytes[1];
// txBuffer[6] =DoubleBytes.AsBytes[2];
// txBuffer[7] =DoubleBytes.AsBytes[3];
for (int i = 0; i < FIFO_SIZE-4; i++)
{
    txBuffer[4+i] =data.byteArray[i];
}

txBuffer[idxFrameCount] = counter;
long start = 0;
long end =0;
start=micros();
if (TryTransmit(TIMEOUT + millis()))
{
  end=micros();
  Serial.println(end-start);
    counter++;
    Serial.print(F("TX: "));
    for (int i = 0; i < writeBytes; i++)
    {
        Serial.printf( "%02x",txBuffer[i]); Serial.print(F(", "));
    }
    Serial.println(F(""));
}
}

void floatAddByte(float data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void intAddByte(int data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}
