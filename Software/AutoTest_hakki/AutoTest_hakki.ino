#include <Wire.h>
#include <Adafruit_BMP085.h>
#include <LSM303.h>//pololu
#include "SparkFun_Ublox_Arduino_Library.h" //https://github.com/sparkfun/SparkFun_Ublox_Arduino_Library

#define GpsPwr  12
#define BattPin A5
#define BattMin 4.7f
#define BattMax 4.9f
#define TempMin 27.0f
#define TempMax 35.0f
#define PressMin  88800
#define PressMax  95500
#define gpsTime 2020
#define gpsLat 410171491
#define gpsLong 289755673
#define gpsAlt 0

Adafruit_BMP085 bmp;
LSM303 compass;
SFE_UBLOX_GPS myGPS;

void setup() {
  SerialUSB.begin(115200);
  while(!SerialUSB);
  SerialUSB.println("SerialUSB    :[OK]");
  SerialUSB.flush(); 
  SerialUSB.print("GPS POWER OFF   :");
  pinMode(GpsPwr, OUTPUT);    
  digitalWrite(GpsPwr, HIGH);
  SerialUSB.println("[OK]");
  SerialUSB.flush();
  fullTest();
}

void loop() {
  // put your main code here, to run repeatedly:

}

float readBatt() {

  float R1 = 560000.0; // 560K
  float R2 = 100000.0; // 100K
  float value = 0.0f;
  do {    
    value =analogRead(BattPin);
    value +=analogRead(BattPin);
    value +=analogRead(BattPin);
    value = value / 3.0f;
    value = (value * 3.32) / 1024.0f;
    value = value / (R2/(R1+R2));
  } while (value > 10.0);
  return value ;

}

void fullTest(){
//--------- Analog ---------------------------------  
  SerialUSB.print("AnalogRead   :");
  SerialUSB.flush();  
  float batV=readBatt();
  SerialUSB.println("[OK]");
  SerialUSB.print("Batarya Voltaji  (");
  SerialUSB.print(batV);
  SerialUSB.print(" V):");
  if(batV<BattMin or BattMax>4.9f){
    SerialUSB.println("Batarya Voltaji Uygun Aralikta Degil!!!");
    SerialUSB.flush();
    while(1);
  }else SerialUSB.println("[OK]");

//--------- Wire ----------------------------------------  
  SerialUSB.print("Wire Start   :");
  SerialUSB.flush();
  Wire.begin();
  SerialUSB.println("[OK]");
  SerialUSB.flush();
  byte error, address;
  int nDevices=0;
  byte bmpS=0; byte lsmA=0; byte lsmM=0; byte ubloxG=0;
 for(address = 1; address < 127; address++ ) 
  {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error == 0)
    {
      if(address==119){//0x77
        SerialUSB.println("BMP180 0x77    :[OK]");
        SerialUSB.flush();
        bmpS=1;
      }else if(address==25){//0x19
        SerialUSB.println("LSM303DLHC 0x19 (Acceerometer)  :[OK]");
        SerialUSB.flush();
        lsmA=1;
      }else if(address==30){//0x1E
        SerialUSB.println("LSM303DLHC 0x1E (Magnetometer) :[OK]");
        SerialUSB.flush();
        lsmM=1;
      }else if(address==66){//0x42
        SerialUSB.println("UBLOX MAX-M8Q 0x42  :[OK]");
        SerialUSB.println("UBLOX Power Mosfet Hatasi. Bu Asamada Ublox a Erisilmemeli!!");
        SerialUSB.flush();
        while(1);
      }else{
        SerialUSB.print("Bilinmeyen I2C aygit bulundu. Address 0x");
        if (address<16) 
        SerialUSB.print("0");
        SerialUSB.print(address,HEX);
        SerialUSB.println("  !!!");
        SerialUSB.flush();
      }
      nDevices++;
    }
    else if (error==4) 
    {
      SerialUSB.print("Bilinmeyen Hata. Address 0x");
      if (address<16) 
        SerialUSB.print("0");
      SerialUSB.println(address,HEX);
    }    
  }
  if (nDevices==3 && bmpS==1 && lsmA==1 && lsmM==1){
    SerialUSB.println("Bulunan adres 3 :[OK]");
  }else{
    SerialUSB.print("Bulunan adres :");
    SerialUSB.println(nDevices);
    if(bmpS==0) SerialUSB.println("BMP180 0x77    :[ERROR!]");
    if(lsmA==0) SerialUSB.println("LSM303DLHC 0x19 (Acceerometer)  :[ERROR!]");
    if(lsmM==0) SerialUSB.println("LSM303DLHC 0x1E (Magnetometer) :[ERROR!]");
    SerialUSB.println("I2C aygitlarini ve PULLUP direnclerini kontrol edin!!!");
    SerialUSB.flush();
    while(1);
  }

//-----------------GPS Power ON----------------------  
  SerialUSB.print("GPS POWER ON   :");   
  digitalWrite(GpsPwr, LOW);
  SerialUSB.println("[OK]"); 
  delay(100);
  error=0; nDevices=0;
  bmpS=0; lsmA=0; lsmM=0; ubloxG=0;
   for(address = 1; address < 127; address++ ) 
  {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error == 0)
    {
      if(address==119){//0x77
        SerialUSB.println("BMP180 0x77  :[OK]");
        SerialUSB.flush();
        bmpS=1;
      }else if(address==25){//0x19
        SerialUSB.println("LSM303DLHC 0x19 (Acceerometer)  :[OK]");
        SerialUSB.flush();
        lsmA=1;
      }else if(address==30){//0x1E
        SerialUSB.println("LSM303DLHC 0x1E (Magnetometer) :[OK]");
        SerialUSB.flush();
        lsmM=1;
      }else if(address==66){//0x42
        SerialUSB.println("UBLOX MAX-M8Q 0x42  :[OK]");
        SerialUSB.flush();
        ubloxG=1;
      }else{
        SerialUSB.print("Bilinmeyen I2C aygit bulundu. Address 0x");
        if (address<16) 
        SerialUSB.print("0");
        SerialUSB.print(address,HEX);
        SerialUSB.println("  !!!");
        SerialUSB.flush();
      }
      nDevices++;
    }
    else if (error==4) 
    {
      SerialUSB.print("Bilinmeyen Hata. Address 0x");
      if (address<16) 
        SerialUSB.print("0");
      SerialUSB.println(address,HEX);
    }    
  }
  if (nDevices==4 && bmpS==1 && lsmA==1 && lsmM==1 && ubloxG==1){
    SerialUSB.println("Bulunan adres 4 :[OK]");
  }else{
    if(bmpS==0) SerialUSB.println("BMP180 0x77    :[ERROR!]");
    if(lsmA==0) SerialUSB.println("LSM303DLHC 0x19 (Acceerometer)  :[ERROR!]");
    if(lsmM==0) SerialUSB.println("LSM303DLHC 0x1E (Magnetometer) :[ERROR!]");
    if(ubloxG==0) SerialUSB.println("UBLOX MAX-M8Q 0x42  :[ERROR!]");
    SerialUSB.println("I2C aygitlarini ve PULLUP direnclerini kontrol edin!!!");
    SerialUSB.flush();
    while(1);
  }
  SerialUSB.print("GPS POWER OFF   :");   
  digitalWrite(GpsPwr, HIGH);
  SerialUSB.println("[OK]");
//-----------BMP180-------------------------------------------------  
SerialUSB.print("BMP180 Start :");
SerialUSB.flush();
if (!bmp.begin()) {
  SerialUSB.println("Hata!!! BMP180 Baslatilamadi");
  SerialUSB.flush();
  while (1) {}
  }
SerialUSB.println("[OK]");

float tmpC=bmp.readTemperature();
SerialUSB.print("Temperature = ");
SerialUSB.print(tmpC);
SerialUSB.print(" *C :");
if(tmpC<TempMin or tmpC>TempMax){
  SerialUSB.println("Sicaklik Uygun Aralikta Degil!!!");
  SerialUSB.flush();
  while(1);
}else SerialUSB.println("[OK]");

uint32_t tmpP=bmp.readPressure();
SerialUSB.print("Pressure = ");
SerialUSB.print(tmpP);
SerialUSB.print(" Pa :");
if(tmpP<PressMin or tmpP>PressMax){
  SerialUSB.println("Basinc Uygun Aralikta Degil!!!");
  SerialUSB.flush();
  while(1);
}else SerialUSB.println("[OK]"); 
//-------- LSM303 -------------------
SerialUSB.print("LSM303 Start :");
  compass.init();
  compass.enableDefault();
  SerialUSB.println("[OK]");

  compass.m_min = (LSM303::vector<int16_t>){-32767, -32767, -32767};
  compass.m_max = (LSM303::vector<int16_t>){+32767, +32767, +32767};
  SerialUSB.print("LSM303 Read  :");
  SerialUSB.flush();
  compass.read();
  SerialUSB.println("[OK]"); 
  SerialUSB.print("Anteni Güneye Cevirin  :");
  while (compass.heading()>20){
    compass.read();  
  }
  SerialUSB.println("[OK]"); 
//-------- GPS TEST -----------------------------------
  SerialUSB.print("GPS POWER ON   :");   
  digitalWrite(GpsPwr, LOW);
  SerialUSB.println("[OK]");
  SerialUSB.print("Set Wire Speed 400Khz :"); 
  SerialUSB.flush();
  Wire.setClock(400000);
  SerialUSB.println("[OK]");
  SerialUSB.print("GPS Wire Begin :");
  SerialUSB.flush();
  delay(100);
if (myGPS.begin() == false) //Connect to the Ublox module using Wire port
  {
    SerialUSB.println(F("GPS I2C Baglantisi Kurulamadı!!!"));
    while (1);
  }
  SerialUSB.println(F("[OK]"));
  SerialUSB.print(F("GPS Config :"));
  // do not overload the buffer system from the GPS, disable UART output
  myGPS.setUART1Output(0); //Disable the UART1 port output 
  myGPS.setUART2Output(0); //Disable Set the UART2 port output
  myGPS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)
  myGPS.saveConfiguration(); //Save the current settings to flash and BBR
  SerialUSB.println("[OK]");
  SerialUSB.print(F("GPS Saati Bekleniyor :"));
  while(1)
  {
    SerialUSB.print(F("."));
    if (myGPS.getPVT())
    {
      if (myGPS.getYear()>gpsTime)
      {
        SerialUSB.println(F("[OK]"));
        break;
      }
    }
  }
  SerialUSB.print(F("GPS Konumu Bekleniyor :"));
  while(1)
  {
    SerialUSB.print(F("."));
    if (myGPS.getPVT())
    {
      if (myGPS.getLatitude()>gpsLat-10000 &&  myGPS.getLatitude()<gpsLat+10000 && 
      myGPS.getLongitude()>gpsLong-10000 && myGPS.getLongitude()<gpsLong+10000 )
      {
        SerialUSB.println(F("[OK]"));
        break;
      }
    }
  }
  
digitalWrite(GpsPwr, HIGH);

SerialUSB.println(F("  _______        _      ____  _  __"));
SerialUSB.println(F(" |__   __|      | |    / __ \\| |/ /"));
SerialUSB.println(F("    | | ___  ___| |_  | |  | | ' / "));
SerialUSB.println(F("    | |/ _ \\/ __| __| | |  | |  <  "));
SerialUSB.println(F("    | |  __/\\__ \\ |_  | |__| | . \\ "));
SerialUSB.println(F("    |_|\\___||___/\\__|  \\____/|_|\\_\\"));
 /*                                  
 */                                  
  
//--------SON-------------------


}
